---
author: Mireille Coilhac
title: Ressources
---

## Lien vers le site de première

[NSI 1ère](https://mcoilhac.forge.apps.education.fr/site-nsi/){ .md-button target="_blank" rel="noopener" }


## Entraînement à l'épreuve pratique : 

[CodEx le codage par les exercices](https://codex.forge.apps.education.fr/){ .md-button target="_blank" rel="noopener" }




## Lien pour réviser les bases de données et SQL

[SQL par Nicolas Revéret](https://nreveret.forge.apps.education.fr/exercices_bdd/){ .md-button target="_blank" rel="noopener" }

Pour la page suivante, il n'est pas nécessaire de s'inscrire :  
[Colibri](https://colibri.unistra.fr/fr/course/list/notions-de-base-en-sql){ .md-button target="_blank" rel="noopener" }


## lien pour approfondir les tris : utile pour préparer l'écrit du bac

Suivre le lien suivant.

Faire tout à partir de ce niveau du chapitre. En cas de besoin, faire les paragraphes précédants.

[Tris - Nicolas Revéret](https://nreveret.forge.apps.education.fr/tris/02_complexite/01_recherches/){ .md-button target="_blank" rel="noopener" }

## Lien pour approfondir la récursivité

[Récursivité - Franck Chambon](https://pratique.forge.apps.education.fr/recursivite/){ .md-button target="_blank" rel="noopener" }


## Lien vers le site d'entraînement à l'épreuve écrite : 

[BAC écrit annales corrigées](https://pratique.forge.apps.education.fr/ecrit/){ .md-button target="_blank" rel="noopener" }

## Lien vers les annales du site de Fabrice Nativel : 

[BAC écrit  F. Nativel](https://fabricenativel.github.io/index_annales/){ .md-button target="_blank" rel="noopener" }


## Lien vers des ressources Interstices

[interstices.info](https://interstices.info/?s=&fwp_type=animation){ .md-button target="_blank" rel="noopener" }


## Lien vers le site de sujets du bac : ToutMonExam

[ToutMonExam](https://toutmonexam.fr/){ .md-button target="_blank" rel="noopener" }


## Lien pour le postbac : Parcoursup, orientation et études

* Dans Parcoursup, depuis cette année 2025, on peut voir la probabilité d’être admis suivant le couple de spécialité choisi et sa moyenne générale. Il faut choisir une formation, puis sélectionner l’onglet « Visualiser les chiffres d’accès à la formation » : [Parcoursup](https://dossier.parcoursup.fr/Candidat/carte){ .md-button target="_blank" rel="noopener" }

* Dans Parcoursup, pour chaque formation désirée, bien lire ce qui se trouve dans l'onglet : "Comprendre les critères d'analyse des candidatures"

* Quelques chiffres : [Site suptracker](https://beta.suptracker.org/?tab=page_accueil&type_bac=tous){ .md-button target="_blank" rel="noopener" }

* MOOCs pour réviser les maths ou la physique en vue des études supérieures : [MOOC de révisions et mises à niveau](https://www.fun-mooc.fr/fr/cours/?collections=2803&limit=21&offset=0){ .md-button target="_blank" rel="noopener" }

## Liens sur des sujets divers

[Pi](https://fchambon.forge.aeif.fr/piday/){ .md-button target="_blank" rel="noopener" }

### Les nombres réels, des suites, etc.

 Vous devez travailler sur [Basthon](https://notebook.basthon.fr/){ .md-button target="_blank" rel="noopener" }

🌐 TD `les_nombres_et_python.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/les_nombres_et_python.ipynb)

### Des maths

[Maths cnrs](https://images.math.cnrs.fr/){ .md-button target="_blank" rel="noopener" }

Et particulièrement : regarder la rubrique Dossiers


[Mysteres de la suite de Fibonacci](https://images.math.cnrs.fr/Mysteres-arithmetiques-de-la-suite-de-Fibonacci?lang=es&id_forum=11953){ .md-button target="_blank" rel="noopener" }

[Vidéos Maths cnrs](https://video.math.cnrs.fr/category/selection-de-videos/){ .md-button target="_blank" rel="noopener" }

[Nombre d'or](https://images.math.cnrs.fr/Le-Nombre-d-or.html?lang=es&id_forum=6702){ .md-button target="_blank" rel="noopener" }

## Python

[Python par W3 schools](https://www.w3schools.com/python/default.asp){ .md-button target="_blank" rel="noopener" }

## Après le bac

[Diaporama présentant la spécialité NSI et les poursuites d'études](a_telecharger/Spe_NSI_pour_term_2025.pdf){ .md-button target="_blank" rel="noopener" }

[Parcours de formation au campus Saint-Aspais](a_telecharger/PARCOURS%20FORMATIONS_Campus_St_Aspais.pdf){ .md-button target="_blank" rel="noopener" }

[Chaine youtube du commandement de la cyberdéfense](https://www.youtube.com/@Comcyber){ .md-button target="_blank" rel="noopener" }


