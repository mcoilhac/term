# Tests
assert taille(((None, 6, None), 15, None)) == 2
assert taille(None) == 0

# Autres tests
assert taille((((None, 3, None), 8, (None, 5, None)), 19, ((None, 7, None), 11, (None, 4, None)))) == 7
