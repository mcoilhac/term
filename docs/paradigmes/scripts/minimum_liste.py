def minimum(liste):
    if ...:  # dernier élément de la liste
        return ...
    else:
        return min(..., minimum(...))


# Tests

assert minimum(cons(4, cons(13, cons(1, cons(5, creer_liste()))))) == 1
