def contient(liste, v):
    if est_vide(liste):
        return False
    elif tete(liste) == v:
        return True
    else:
        return contient(queue(liste), v)
        