def somme_fonctions(f, g, x):
    return f(x) + g(x)

def f1(x):
    return x**2

def f2(x):
    return 2*x

print(somme_fonctions(f1, f2, 5))
