def est_pair(n):
  return n % 2 == 0

nombres = (1, 2, 3, 4, 5)
resultat = filter(est_pair, nombres)

print(resultat)  # Un objet itérable
print(type(resultat))
print(list(resultat))
