# Tests
assert appartient(None, 3) is False
assert appartient(((((None, 3, None), 8, (None, 5, None)), 19, ((None, 7, None), 11, (None, 4, None)))), 2) is False
assert appartient(((((None, 3, None), 8, (None, 5, None)), 19, ((None, 7, None), 11, (None, 4, None)))), 3) is True

# Autres tests
assert appartient(((((None, 3, None), 8, (None, 5, None)), 19, ((None, 7, None), 11, (None, 4, None)))), 12) is False
assert appartient(((((None, 3, None), 8, (None, 5, None)), 19, ((None, 7, None), 11, (None, 4, None)))), 4) is True
