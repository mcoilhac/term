def taille(arbre):
    if est_vide(arbre):
        return 0
    else :
        return 1 + taille(gauche(arbre)) + taille(droite(arbre))
