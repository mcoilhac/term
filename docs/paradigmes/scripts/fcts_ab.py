def est_vide(arbre):
    ...

def gauche(arbre):
    ...

def droite(arbre):
    ...

def racine(arbre):
    ...

# Tests
assert est_vide(None) is True
assert est_vide(((None, 6, None), 15, None)) is False
assert gauche(((None, 6, None), 15, None)) == (None, 6, None)
assert droite(((None, 6, None), 15, None)) is None
assert droite((((None, 3, None), 8, (None, 5, None)), 19, ((None, 7, None), 11, (None, 4, None)))) == ((None, 7, None), 11, (None, 4, None))
assert racine((((None, 6, None), 15, None))) == 15

