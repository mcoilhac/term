def minimum(liste):
    if est_vide(queue(liste)): # dernier élément de la liste
        return tete(liste)
    else:
        return min(tete(liste), minimum(queue(liste)))
