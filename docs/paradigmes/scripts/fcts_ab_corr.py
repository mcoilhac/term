def est_vide(arbre):
    return arbre is None

def gauche(arbre):
    return arbre[0]

def droite(arbre):
    return arbre[2]

def racine(arbre):
    return arbre[1]

