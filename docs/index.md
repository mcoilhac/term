---
author: Mireille Coilhac
title: 🏡 Accueil
---

# Bienvenue sur la page de NSI de terminale du Lycée Saint-Aspais de MELUN

Cours de terminale NSI (classes de Mme Coilhac)

😊 Ce site évoluera pendant l'année ...

!!! info "Exercices Python : Utilisation de l'éditeur sur le site"


    Pour la plupart des exercices il y a un éditeur (IDE) possédant 5 boutons.


    ### Tests publics

    Vous devez compléter le code dans la zone de saisie. Les assertions constituent les **tests publics**. Il reprennent le plus souvent les exemples de l'énoncé.

    Une ligne du type `#!py assert somme(10, 32) == 42` vérifie que la fonction `somme` renvoie bien `#!py 42` lorsqu'on lui propose `#!py 10` et `#!py 32` comme arguments.

    <span class="py_mk_ide" style="display: flex; align-items: center;"><button style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img class="skip_light_box" src="./assets/images/icons8-play-64.png" alt="Exécuter les tests"></button><span> Vous pouvez vérifier que votre fonction passe ces tests publics en cliquant sur le bouton **Exécuter**</span></span>

    ### Tests privés

    <span class="py_mk_ide" style="display: flex; align-items: center;"><button style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img class="skip_light_box" src="./assets/images/icons8-check-64.png" alt="Valider"></button><span> Une fois les tests publics passés, vous pouvez passer les **tests privés** en cliquant sur le bouton **Valider**  ;</span></span>  
    Ceux-ci sont plus nombreux et, comme leur nom l'indique, ne vous sont pas connus. Seul leur résultat vous est indiqué avec, parfois, un commentaire sur la donnée ayant mis en défaut votre code.

    Dans la plupart des exercices, un compteur permet de suivre vos essais. Ce compteur est décrémenté à chaque fois que vous cliquez sur le bouton **Valider**. Lorsque le compteur atteint 0, la solution de l'exercice vous est proposée.
   

    ### Autres boutons

    Il est aussi possible de :
  
    <span class="py_mk_ide" style="display: flex; align-items: center;"><button style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img class="skip_light_box" src="./assets/images/icons8-download-64.png" alt="Télécharger"></button><span> télécharger le contenu de l'éditeur si vous souhaitez le conserver ou travailler en local ;</span></span>

    <span class="py_mk_ide" style="display: flex; align-items: center;"><button style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img class="skip_light_box" src="./assets/images/icons8-upload-64.png" alt="Téléverser"></button><span> téléverser un fichier Python dans l'éditeur afin de rapatrier votre travail local ;</span></span>

    <span class="py_mk_ide" style="display: flex; align-items: center;"><button style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img class="skip_light_box" src="./assets/images/icons8-restart-64.png" alt="Réinitialiser l'IDE"></button><span> recharger l'éditeur dans son état initial ;</span></span>

    <span class="py_mk_ide" style="display: flex; align-items: center;"><button style="margin:0em 0.2em;" onclick="" type="button" class="tooltip"><img class="skip_light_box" src="./assets/images/icons8-save-64.png" alt="Sauvegarder"></button><span> sauvegarder le contenu de l'éditeur dans la mémoire de votre navigateur ;</span></span>


_Dernière mise à jour le 7/03/2025_
