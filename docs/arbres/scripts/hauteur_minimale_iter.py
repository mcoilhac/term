# --- PYODIDE:code --- #

def hauteur_min(nbre_noeuds: int) -> int:
    """
    Cette fonction renvoie la hauteur minimale d'un arbre binaire ayant nbre_noeuds noeuds.
    Convention choisie : la hauteur d'un arbre réduit à sa racine est 1.

    """
    ...


# --- PYODIDE:corr --- #

def hauteur_min(nbre_noeuds: int) -> int:
    """
    Cette fonction renvoie la hauteur minimale d'un arbre binaire ayant nbre_noeuds noeuds.
    Convention choisie : la hauteur d'un arbre réduit à sa racine est 1."""

    cpt = 0
    q = nbre_noeuds
    while q != 0:
        q = q//2
        cpt = cpt + 1
    return cpt


# --- PYODIDE:tests --- #

assert hauteur_min(63) == 6
assert hauteur_min(1) == 1
assert hauteur_min(32) == 6


# --- PYODIDE:secrets --- #

assert hauteur_min(17) == 5
assert hauteur_min(13) == 4