# --- PYODIDE:code --- #

from math import log2
def hauteur_min_log(nbre_noeuds: int) -> int:
    """
    Cette fonction renvoie la hauteur minimale d'un arbre binaire ayant nbre_noeuds noeuds.
    Convention choisie : la hauteur d'un arbre réduit à sa racine est 1.
    """
    ...

# --- PYODIDE:corr --- #

def hauteur_min2(nbre_noeuds: int) -> int:
    """
    Cette fonction renvoie la hauteur minimale d'un arbre binaire ayant nbre_noeuds noeuds.
    Convention choisie : la hauteur d'un arbre réduit à sa racine est 1.
    """

    h_min = int(log2(nbre_noeuds)) + 1
    return h_min


# --- PYODIDE:tests --- #

assert hauteur_min_log(63) == 6
assert hauteur_min_log(1) == 1
assert hauteur_min_log(32) == 6


# --- PYODIDE:secrets --- #

assert hauteur_min_log(17) == 5
assert hauteur_min_log(13) == 4
assert hauteur_min_log(2) == 2
