class Arbre:
    
    def __init__(self, val):
            self.valeur = val
            self.gauche = None
            self.droit = None

    def ajout_gauche(self, val):
            self.gauche = Arbre(val)

    def ajout_droit(self, val):
            self.droit = Arbre(val)
