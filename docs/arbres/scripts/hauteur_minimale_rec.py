# --- PYODIDE:code --- #

def hauteur_min2(nbre_noeuds: int) -> int:
    """
    Cette fonction renvoie la hauteur minimale d'un arbre binaire ayant nbre_noeuds noeuds.
    Convention choisie : la hauteur d'un arbre réduit à sa racine est 1.

    """
    ...


# --- PYODIDE:corr --- #

def hauteur_min2(nbre_noeuds: int) -> int:
    """
    Cette fonction renvoie la hauteur minimale d'un arbre binaire ayant nbre_noeuds noeuds.
    Convention choisie : la hauteur d'un arbre réduit à sa racine est 1.

    """
    if nbre_noeuds == 1:
        return 1
    else:
        return 1 + hauteur_min2(nbre_noeuds // 2)


# --- PYODIDE:tests --- #

assert hauteur_min2(63) == 6
assert hauteur_min2(1) == 1
assert hauteur_min2(32) == 6

# --- PYODIDE:secrets --- #

assert hauteur_min2(17) == 5
assert hauteur_min2(13) == 4