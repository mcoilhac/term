class Arbre:
    def __init__(self, val):
        self.valeur = val
        self.gauche = None
        self.droit = None

    def ajout_gauche(self, val):
        self.gauche = Arbre(val)

    def ajout_droit(self, val):
        self.droit = Arbre(val)
        
# Compléter ci-dessous
abr = Arbre(7)
...

# Test du parcours infixe sur abr
print(parcours_infixe_liste(abr))