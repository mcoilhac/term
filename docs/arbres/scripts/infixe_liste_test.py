# Tests 
arbre3 = Arbre("A")
arbre3.ajout_gauche("B")
arbre3.ajout_droit("C")
arbre3.gauche.ajout_gauche("D")
arbre3.gauche.ajout_droit("E")
arbre3.gauche.gauche.ajout_droit("G")
arbre3.droit.ajout_gauche("F")
assert parcours_infixe_liste(arbre3) == ['D', 'G', 'B', 'E', 'A', 'F', 'C']


# Autres tests
arbre2 = Arbre("A")
arbre2.ajout_gauche("B")
arbre2.ajout_droit("C")
arbre2.gauche.ajout_gauche("D")
arbre2.gauche.ajout_droit("E")
arbre2.gauche.droit.ajout_gauche("H")
arbre2.droit.ajout_droit("F")
assert parcours_infixe_liste(arbre2) == ['D', 'B', 'H', 'E', 'A', 'C', 'F']

