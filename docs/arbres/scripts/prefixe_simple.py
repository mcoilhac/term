# version simple avec print (renvoie None)
def parcours_prefixe(arbre) :
    if arbre is not None :
        ...
        
# création de l'arbre 
arbre1 = Arbre("A")
arbre1.ajout_gauche("B")
...

# parcours
parcours_prefixe(arbre1)

