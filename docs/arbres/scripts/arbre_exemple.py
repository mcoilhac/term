from binarytree import Node

# Créer un arbre
arbre = Node(3)
arbre.left = Node(2)
arbre.left.left = Node(1)
arbre.left.right = Node(4)
arbre.right = Node(6)

print(arbre)
