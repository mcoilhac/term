---
author: Mireille Coilhac
title: Paramètres et attributs
---

## I. méthode `init` n'ayant qu'un seul paramètre : `self`

???+ question "Testons"

    !!! info "Le paramètre `self`"

        La méthode `__init__` ci-dessous n'a qu'**un seul paramètre** qui est `self`.  

        Un objet instance de la classe `Joueur` a **quatre attributs** qui sont `score`, `est_actif`, `nbre_parties_jouees` et `handicap`.  

        Ces attributs sont tous **initialisés** : `score` initialisé à 0, `est_actif` initialisé à `True` etc...

    Exécuter le script ci-dessous :

    {{IDE('scripts/aucun_parametre')}}

    !!! info "Deux instances"

        Nous avons créé deux instances de la classe `Joueur`.  
        On constate qu'à leur création, les attributs des deux objets ont les mêmes valeurs. 

## II. méthode `init` ayant plusieurs paramètres : 

???+ question "Testons"

    Nous voulons rajouter l'attribut `nom`.

    Exécuter le script ci-dessous :

    {{IDE('scripts/aucun_parametre_2')}}

    En procédant ainsi, tous les joueurs sont construits avec le même nom. L'attribut `nom` est **initialisé** à DUPONT.  
    On pourra évidemment par la suite modifier cet attribut.

    Exécuter le script ci-dessous :

    {{IDE('scripts/un_joueur')}}

    Ce n'est pas pratique ! Nous pouvons ajouter un **paramètre** qui va permettre  d’initialiser l’attribut `nom` avec une valeur choisie au moment de la création de l’instance.

???+ question "Testons"

    Nous voulons rajouter l'attribut `nom`.

    Exécuter le script ci-dessous :

    {{IDE('scripts/un_parametre')}}

    On peut maintenant construire des joueurs, directement avec leur nom :

    {{IDE('scripts/un_parametre_2')}}

!!! abstract "Résumé"

    Dans ce dernier exemple, les objets instances de la classe `Joueur` ont **cinq attributs**, le constructeur (la méthode `init`) a **deux paramètres**.

!!! warning "Remarque"

    👉 Très souvent, le nom du paramètre, et le nom de l'attribut auquel il va permettre de transmettre une valeur sont identiques.  

    On voit souvent des classes rédigées comme ci-dessous. Tester.

    {{IDE('scripts/un_parametre_3')}}

???+ question "À vous de jouer 1"

    Compléter la classe `Joueur` pour qu'elle dispose de l'attribut `age`. Le constructeur devra avoir un paramètre permettant de transmettre l'âge du joueur.

    {{IDE('scripts/joueur_age')}}

## III. Paramètre par défaut

!!! example "Exemple sans paramètre par défaut"

    Dans l'exemple précédent, les joueurs sont tous initialisés avec un attribut handicap qui vaut 0. En fait, les joueurs qui sont des "champions" ont un handicap de 50. On pourrait procéder ainsi :

    ```python title=""
    class Joueur_5:
    
        def __init__(self, nom, age):
            self.score = 0
            self.est_actif = True
            self.nbre_parties_jouees = 0
            self.handicap = 0
            self.nom = nom
            self.age = age

    Kevin = Joueur_5("DURAND", 18)
    Champion_1 = Joueur_5("MAITRE", 30)
    Champion_1.handicap = 50
    ```

???+ question "Avec un paramètre par défaut"

    Il y a une autre possibilité, qui est d'utiliser le paramètre par défaut `handicap = 0`.

    A la construction d'un objet, si le paramètre n'est pas précisé, il prendra pour valeur 0. Sinon, il prendra la valeur donnée.

    Tester ci-dessous :

    {{IDE('scripts/handicap')}}

!!! info "Meilleure pratique"

    Dans cet exemple, il vaut mieux ne pas donner de valeur par défaut au paramètre `handicap`

???+ question "À vous de jouer"

    Compléter le script ci-dessous : 

    * Pas de paramètre par défaut
    * `handicap` est un paramètre
    * `Kevin` a pour nom "DURAND", âge 18 ans et ne doit pas avoir de handicap.`Champion_1` a pour nom "MAITRE", âge 30 ans, doit avoir un handicap de 50.

    {{IDE('scripts/handicap_2')}}


!!! danger "Attention"

    ⚠️ Que l'on soit en POO ou pas, les paramètres par défauts doivent toujours se trouver **après** les autres.  
    😢 Ecriture fausse : ~~`def ma_fonction(para_1 = 0, para_2): `~~   
    😊 Ecriture juste :  `def ma_fonction(para_1, para_2 = 0):`


## IV. Ecueils avec les paramètres par défauts

!!! danger "Attention aux paramètres par défaut de type mutable"

    ⚠️⚠️ Il vaut mieux qu'un paramètre par défaut **ne soit pas** de type mutable.
    Pour vous en convaincre, voici un exemple qui montre **ce qu'il ne faut pas faire**.  
    **Le type `list` de Python est mutable**

!!! failure "Echec"

    Dans cet exemple, l'attribut `rencontres` est la liste des adversaires rencontrés par le joueur lors des matchs précédents.

    {{IDE('scripts/mutable')}}

    Quel est le problème ?

    ??? success "Ce qui ne va pas"

        😭 On aurait souhaité que `Jules.rencontres` soit la liste vide ...

    ??? success "Ce qu'il aurait fallu faire"

        Tester ci-dessous

        {{IDE('scripts/rencontres')}}

!!! abstract "Initialiser un paramètre, ou créer un paramètre par défaut ?"

    Nous venons de voir que ce n'est pas équivalent d'initialiser un paramètre, ou de créer un paramètre par défaut.

    Il faut toujours avoir en tête le problème des paramètres de types mutables.


### 💡 Deux exemples sur le caractère mutable des listes en Python

???+ question "🤔 Un premier exemple"

    Tester ci-dessous

    {{IDE('scripts/exemple_append')}}

???+ question "🤔 Un deuxième exemple"

    Tester ci-dessous

    {{IDE('scripts/exemple_rencontres')}}

??? note pliée "En conclusion sur les types mutables"

    😂 **Toujours bien réfléchir lorsqu'on les utilise !**


## V. Utilisation des paramètres par défaut dans le cas des arbres

!!! info "Avec les arbres"

    🌴 Lorsque l'on travaille avec des arbres, il est très courant d'utiliser des paramètres par défaut. C'est un cas où leur utilisation est intéressante. Nous le verrons plus précisément lorsque nous les étudierons.

## VI. QCM


{{ multi_qcm(
    ["on parle d'attribut pour"
,
        ["une méthode", "un objet"],
        [2]
    ],
    ["La méthode `__init__` possède : "
,
        [
            "4 attributs",
            "4 paramètres",
            "6 attributs",
            "6 paramètres",
        ],
        [2],
    ],
    [
"""
```python title=''
ada = Participant('Lovelace', 26)
```

Le nombre d'attributs d'`ada` est
""",
        [
            "2",
            "3",
            "4",
            "1",
            "5",
            "6",
        ],
        [6],
    ],
    ["La méthode `__init__` possède :" 
,
        [
            "4 paramètres dont un par défaut",
            "4 paramètres dont un initialisé",
            "3 paramètres dont un par défaut",
            "3 paramètres dont un initialisé",
            "6 paramètres dont un par défaut",
            "6 paramètres dont un initialisé",
        ],
        [1],
    ],
    ["`score` est :" 
,
        [
            "un attribut par défaut",
            "un attribut initialisé",
            "un paramètre par défaut",
            "un paramètre initialisé",
        ],
        [2],
    ],
    ["`handicap_participant` est :" 
,
        [
            "un attribut par défaut",
            "un attribut initialisé",
            "un paramètre par défaut",
            "un paramètre initialisé",
        ],
        [3],
    ],
    ["`handicap` est :" 
,
        [
            "un attribut",
            "un paramètre par défaut",
        ],
        [1],
    ],

    multi = False,
    qcm_title = "Cocher la bonne réponse",
    shuffle = True,
    description = '
    Ce QCM porte sur cet exemple :

    ```python title=""
    class Participant:
    
        def __init__(self, nom_participant, age_participant, handicap_participant = 0):
            self.score = 0
            self.est_actif = True
            self.nbre_parties_jouees = 0
            self.handicap = nbre_handicap
            self.nom = nom_participant
            self.age = age_participant
    ```
    '
) }}
