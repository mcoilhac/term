---
author: Mireille Coilhac
title: Exercices de POO - série 3
---

!!! info "Exercices types BAC"

    Les exercices qui suivent peuvent servir à l'entraînement de l'épreuve pratique du bac

   

## Exercice 1 `programmeur.saisit(code)`

???+ question "Syntaxe de la POO"  

    [Syntaxe de la POO](https://codex.forge.apps.education.fr/exercices/syntaxe_poo/){ .md-button target="_blank" rel="noopener" }

Cet exercice a été réalisé par Nicolas Revéret et Romain Janvier.


## Exercice 2

???+ question "Chien en POO"

    [Chien POO](https://codex.forge.apps.education.fr/exercices/poo_chien/){ .md-button target="_blank" rel="noopener" }
    

## Exercice 3
    

???+ question "Train en POO"

    [Train POO](https://codex.forge.apps.education.fr/exercices/poo_train/){ .md-button target="_blank" rel="noopener" }

## Exercice 4

???+ question "Courses de rentrée"

    [Courses de rentrée](https://codex.forge.apps.education.fr/en_travaux/course_rentree/){ .md-button target="_blank" rel="noopener" }
