class Joueur_8:
    
    def __init__(self, nom, age, rencontres = []):  # !!! A ne pas faire 
        self.score = 0
        self.est_actif = True
        self.nbre_parties_jouees = 0
        self.handicap = 0
        self.nom = nom
        self.age = age
        self.rencontres = rencontres
        
    def ajoute_joueur_rencontre(self, personne):
        self.rencontres.append(personne)

Kevin = Joueur_8("DURAND", 18)
print("Rencontres de Kevin : ", Kevin.rencontres)
Kevin.ajoute_joueur_rencontre("MARTIN")
print("Rencontres de Kevin : ", Kevin.rencontres)
Jules = Joueur_8("DUPOND", 18)  # A priori l'attribut rencontre  pour Jules est la liste vide
print("Rencontres de Jules : ", Jules.rencontres)  # Observez le problème