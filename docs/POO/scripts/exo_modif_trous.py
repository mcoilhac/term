# --------- PYODIDE:env --------- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        self.prenom = prenom
        self.nom = nom
        self.classe = classe
        self.moyennes = {}


    def modifie_moyenne(self, matiere, moyenne):
        pass




# --------- PYODIDE:code --------- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        self.prenom = prenom
        self.nom = nom
        self.classe = classe
        self.moyennes = {}

    
    def ...(..., ..., ...):
        ...[...] = ...

        


# --------- PYODIDE:corr --------- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        self.prenom = prenom
        self.nom = nom
        self.classe = classe
        self.moyennes = {}

    def modifie_moyenne(self, matiere, moyenne):
        self.moyennes[matiere] = moyenne


# --------- PYODIDE:tests --------- #

carl = Eleve("Carl Friedrich", "Gauss", "Te3")
carl.modifie_moyenne("arithmétique", 20)
carl.modifie_moyenne("chimie", 12)
assert carl.moyennes == {"arithmétique": 20, "chimie": 12}
carl.modifie_moyenne("chimie", 13)
assert carl.moyennes == {"arithmétique": 20, "chimie": 13}

# --------- PYODIDE:secrets --------- #
albert = Eleve("Albert", "Einstein", "Te2")
albert.modifie_moyenne("physique", 20)
albert.modifie_moyenne("maths", 20)
albert.modifie_moyenne("allemand", 17)
assert albert.moyennes == {
    "physique": 20,
    "maths": 20,
    "allemand": 17,
}, "Erreur lors de l'ajout de moyennes"
albert.modifie_moyenne("allemand", 20)
assert albert.moyennes == {
    "physique": 20,
    "maths": 20,
    "allemand": 20,
}, "Erreur lors de l'ajout de moyennes"


