# --- PYODIDE:code --- #

liste_1 = [1, 2, 3]
liste_2 = liste_1
print("liste_1 = ", liste_1)
print("liste_2 = ", liste_2, "cela semble logique")
liste_1.append(4)
print("liste_1 = ", liste_1, "c'est logique")
print("liste_2 = ", liste_2, "😭 liste_2 a aussi été modifiée !!!")

