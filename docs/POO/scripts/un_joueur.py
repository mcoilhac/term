# --- PYODIDE:env --- #

class Joueur_2:
    
    def __init__(self):
        self.score = 0
        self.est_actif = True
        self.nbre_parties_jouees = 0
        self.handicap = 0
        self.nom = "DUPONT"

# --- PYODIDE:code --- #

joueur_1 = Joueur_2()
print(joueur_1.nom)
joueur_1.nom = "Martin"
print(joueur_1.nom) 