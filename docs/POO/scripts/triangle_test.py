# Tests
assert TriangleRect(3, 4).hypotenuse == 5
assert TriangleRect(5, 12).hypotenuse == 13
assert TriangleRect(8, 15).hypotenuse == 17
