Notons, qu'en Python, une fonction sans `#!py return` renvoie par défaut `#!py None`.

Ainsi le code précédent pourrait être écrit sans le `#!py else` qui serait géré implicitement :

```python title=""
def moyenne_de(self, matiere):
    if matiere in self.moyennes:
        return self.moyennes[matiere]
``` 


