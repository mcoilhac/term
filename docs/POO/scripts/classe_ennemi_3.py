# --- PYODIDE:code --- #

class Ennemi_3 :
    def __init__(self, p, r) :
        self.posX = 0
        self.posY = 0
        self.pv = p
        self.rapidite = r
        
    def recoitDegats(self, deg) :
        self.pv = self.pv - deg

# exemple d'usage :
mechant = Ennemi_3(400, 10)

mechant.recoitDegats(25)
print("mechant a encore : ", mechant.pv , "points de vies")