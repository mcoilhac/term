# --------- PYODIDE:env --------- #
class Eleve:
    pass


# --------- PYODIDE:code --------- #
class Eleve: 
    ...


# --------- PYODIDE:corr --------- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        self.prenom = prenom
        self.nom = nom
        self.classe = classe
        self.moyennes = {}


# --------- PYODIDE:tests --------- #
albert = Eleve("Albert", "Einstein", "Te2")
assert albert.prenom == "Albert"
assert albert.nom == "Einstein"
assert albert.classe == "Te2"
assert albert.moyennes == {}

# --------- PYODIDE:secrets --------- #
gustave = Eleve("Gustave", "Flaubert", "Te1")
assert gustave.prenom == "Gustave", "Erreur sur l'attribut prenom"
assert gustave.nom == "Flaubert", "Erreur sur l'attribut nom"
assert gustave.classe == "Te1", "Erreur sur l'attribut classe"
assert gustave.moyennes == {}, "Erreur sur l'attribut moyennes"


