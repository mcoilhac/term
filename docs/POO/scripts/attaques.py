from random import randint

class Personnage_9:
    def __init__(self, nbre_vies, age):
        self.vie = nbre_vies
        self.age = age

    def donne_etat(self):
        return self.vie

    def perd_vies(self, vies_perdues):
        perdues = randint(1, vies_perdues)
        self.vie = self.vie - perdues
        if self.vie < 0:
            self.vie = 0

    def boire_potion(self):
        self.vie = self.vie + 1

gollum = Personnage_9(100, 127)
bilbo = Personnage_9(100, 127)
...

