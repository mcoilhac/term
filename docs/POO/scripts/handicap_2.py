# --- PYODIDE:code --- #

class Joueur_7:
    
    def __init__(...):
        ...

Kevin = ...
Champion_1 = ...

# --- PYODIDE:corr --- #

class Joueur_7:
    
    def __init__(self, nom, age, handicap):
        self.score = 0
        self.est_actif = True
        self.nbre_parties_jouees = 0
        self.handicap = handicap
        self.nom = nom
        self.age = age

Kevin = Joueur_7("DURAND", 18, 0)
Champion_1 = Joueur_7("MAITRE", 30, 50)


# --- PYODIDE:tests --- #

assert Kevin.handicap == 0
assert Champion_1.handicap == 50


# --- PYODIDE:secrets --- #

joueur_2 = Joueur_7("MARCHAND", 22, 80)
assert joueur_2.handicap == 80
