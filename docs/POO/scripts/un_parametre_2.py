# --- PYODIDE:env --- #

class Joueur_3:
    
    def __init__(self, nom_joueur):
        self.score = 0
        self.est_actif = True
        self.nbre_parties_jouees = 0
        self.handicap = 0
        self.nom = nom_joueur

# --- PYODIDE:code --- #

joueur_1 = Joueur_3("LAPORTE")
print(joueur_1.nom)