# --- PYODIDE:code --- #

class Joueur_9:
    
    def __init__(self, nom, age):  
        self.score = 0
        self.est_actif = True
        self.nbre_parties_jouees = 0
        self.handicap = 0
        self.nom = nom
        self.age = age
        self.rencontres = []
        
    def ajoute_joueur_rencontre(self, personne):
        self.rencontres.append(personne)

Kevin = Joueur_9("DURAND", 18)
print("Rencontres de Kevin : ", Kevin.rencontres)
Kevin.ajoute_joueur_rencontre("MARTIN")
print("Rencontres de Kevin : ", Kevin.rencontres)
Jules = Joueur_9("DUPOND", 18)  # A priori l'attribut rencontre  pour Jules est la liste vide
print("Rencontres de Jules : ", Jules.rencontres)  # On voit que c'est bien le cas !