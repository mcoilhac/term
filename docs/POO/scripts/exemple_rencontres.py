class Joueur_10:  
    def __init__(self, nom, age, rencontres):
        self.score = 0
        self.est_actif = True
        self.nbre_parties_jouees = 0
        self.handicap = 0
        self.nom = nom
        self.age = age
        self.rencontres = rencontres
        
    def ajoute_joueur_rencontre(self, personne):
        self.rencontres.append(personne)

mes_rencontres = ["PROBLEME", "MUTABLE"]
Kevin = Joueur_10("DURAND", 18, mes_rencontres)
Jules = Joueur_10("DUPOND", 18, mes_rencontres)

# A priori on n'ajoute pas "MARTIN" à la liste de rencontres de Jules
Kevin.ajoute_joueur_rencontre("MARTIN")  
print("Rencontres de Kevin : ", Kevin.rencontres)
print("Rencontres de Jules : ", Jules.rencontres)  # 😭 Hélas "MARTIN" a été ajouté