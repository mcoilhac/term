# --- PYODIDE:code --- #

class Joueur_5:
    
    def __init__(..., ..., ...):
        ...


# --- PYODIDE:corr --- #

class Joueur_5:
    
    def __init__(self, nom, age):
        self.score = 0
        self.est_actif = True
        self.nbre_parties_jouees = 0
        self.handicap = 0
        self.nom = nom
        self.age = age


# --- PYODIDE:tests --- #

joueur_1 = Joueur_5("BEAUGRAND", 27)
assert joueur_1.nom == "BEAUGRAND"
assert joueur_1.age == 27


# --- PYODIDE:secrets --- #

joueur_2 = Joueur_5("MARCHAND", 22)
assert joueur_2.nom == "MARCHAND"
assert joueur_2.age == 22