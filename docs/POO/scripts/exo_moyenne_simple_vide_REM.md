Comme indiqué dans l'exercice précédent, le :  

``` python title=""
else: 
    return None
```
peut être omis.

Ainsi le code précédent est équivalent à :

```python title=""
def moyenne_simple(self):
    if len(self.moyennes) > 0:
        total = 0
        for moy in self.moyennes.values():
            total += moy
        return total / len(self.moyennes)
``` 


