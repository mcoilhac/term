# --------- PYODIDE:env --------- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        self.prenom = prenom
        self.nom = nom
        self.classe = classe
        self.moyennes = {}
        

    def modifie_moyenne(self, matiere, moyenne):
        self.moyennes[matiere] = moyenne


    def moyenne_de(self, matiere):
        if matiere in self.moyennes:
            return self.moyennes[matiere]
        else:
            return None

    def moyenne_simple(self):
        pass
            
               
# --------- PYODIDE:code --------- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        self.prenom = prenom
        self.nom = nom
        self.classe = classe
        self.moyennes = {}
        

    def modifie_moyenne(self, matiere, moyenne):
        self.moyennes[matiere] = moyenne


    def moyenne_de(self, matiere):
        if matiere in self.moyennes:
            return self.moyennes[matiere]
        else:
            return None

    def ... :
        if ... > 0:
            total = ...
            for moy in self.moyennes....():
                total += ...
            return ... / ...
        else:
            return ...



# --------- PYODIDE:corr --------- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        self.prenom = prenom
        self.nom = nom
        self.classe = classe
        self.moyennes = {}

    def modifie_moyenne(self, matiere, moyenne):
        self.moyennes[matiere] = moyenne

    def moyenne_de(self, matiere):
        if matiere in self.moyennes:
            return self.moyennes[matiere]

    def moyenne_simple(self):
        if len(self.moyennes) > 0:
            total = 0
            for moy in self.moyennes.values():
                total += moy
            return total / len(self.moyennes)
        else:
            return None


# --------- PYODIDE:tests --------- #
jane = Eleve("Jane", "Goodall", "Te3")
jane.modifie_moyenne("éthologie", 20)
jane.modifie_moyenne("théorie des groupes", 1)
assert jane.moyenne_simple() == 10.5

# --------- PYODIDE:secrets --------- #
albert = Eleve("Albert", "Einstein", "Te2")
albert.modifie_moyenne("physique", 20)
albert.modifie_moyenne("maths", 20)
albert.modifie_moyenne("allemand", 17)
assert albert.moyenne_simple() == 19.0, "Erreur lors du calcul d'une moyenne simple"
evariste = Eleve("Evariste", "Galois", "Te3")
assert evariste.moyenne_simple() is None, "Erreur lors du calcul d'une moyenne simple"
