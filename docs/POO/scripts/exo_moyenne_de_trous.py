# --------- PYODIDE:env --------- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        self.prenom = prenom
        self.nom = nom
        self.classe = classe
        self.moyennes = {}
        

    def modifie_moyenne(self, matiere, moyenne):
        self.moyennes[matiere] = moyenne

    def moyenne_de(self, matiere):
        pass


# --------- PYODIDE:code --------- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        self.prenom = prenom
        self.nom = nom
        self.classe = classe
        self.moyennes = {}
        
    def modifie_moyenne(self, matiere, moyenne):
        self.moyennes[matiere] = moyenne

    def ...(..., ...):
        if ... in ...:
            ...
        else:
            return ...


# --------- PYODIDE:corr --------- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        self.prenom = prenom
        self.nom = nom
        self.classe = classe
        self.moyennes = {}

    def modifie_moyenne(self, matiere, moyenne):
        self.moyennes[matiere] = moyenne

    def moyenne_de(self, matiere):
        if matiere in self.moyennes:
            return self.moyennes[matiere]
        else:
            return None


# --------- PYODIDE:tests --------- #
donald = Eleve("Donald", "Knuth", "Te7")
donald.modifie_moyenne("informatique", 20)
donald.modifie_moyenne("musique", 13)
assert donald.moyenne_de("informatique") == 20
assert donald.moyenne_de("musique") == 13
assert donald.moyenne_de("lancer de javelot") is None

# --------- PYODIDE:secrets --------- #
albert = Eleve("Albert", "Einstein", "Te2")
albert.modifie_moyenne("physique", 20)
assert (
    albert.moyenne_de("physique") == 20
), "Erreur lors de la récupération d'une moyenne"
assert (
    albert.moyenne_de("japonais") is None
), "Erreur lors de la récupération d'une moyenne"


