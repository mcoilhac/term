Comme indiqué dans l'exercice précédent, le :  

``` python title=""
else: 
    return None
```
peut être omis.

Ainsi le code précédent est équivalent à :

```python title=""
def moyenne_ponderee(self, coeffs):
    if len(self.moyennes) > 0:
        total = 0
        total_coeffs = 0
        for matiere in self.moyennes:
            total += self.moyennes[matiere] * coeffs[matiere]
            total_coeffs += coeffs[matiere]
        return total / total_coeffs
``` 