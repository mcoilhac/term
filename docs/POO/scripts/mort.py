from random import randint

class Personnage_8:
    def __init__(self, nbre_vies, age):
        self.vie = nbre_vies
        self.age = age

    def donne_etat(self):
        return self.vie

    def perd_vies(self, vies_perdues):
        perdues = randint(1, vies_perdues)
        ...

    def boire_potion(self):
        self.vie = self.vie + 1

monstre = Personnage_8(100, 130)
...





