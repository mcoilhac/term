# --------- PYODIDE:env --------- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        self.prenom = prenom
        self.nom = nom
        self.classe = classe
        self.moyennes = {}
        

    def modifie_moyenne(self, matiere, moyenne):
        self.moyennes[matiere] = moyenne

    def moyenne_de(self, matiere):
        if matiere in self.moyennes:
            return self.moyennes[matiere]
        else:
            return None   

    def moyenne_simple(self):
        if len(self.moyennes) > 0:
            total = 0
            for moy in self.moyennes.values():
                total += moy
            return total / len(self.moyennes)
        else:
            return None

    def moyenne_ponderee(self, coeffs):
        pass
        
# --------- PYODIDE:code --------- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        self.prenom = prenom
        self.nom = nom
        self.classe = classe
        self.moyennes = {}
        

    def modifie_moyenne(self, matiere, moyenne):
        self.moyennes[matiere] = moyenne

    def moyenne_de(self, matiere):
        if matiere in self.moyennes:
            return self.moyennes[matiere]
        else:
            return None   

    def moyenne_simple(self):
        if len(self.moyennes) > 0:
            total = 0
            for moy in self.moyennes.values():
                total += moy
            return total / len(self.moyennes)
        else:
            return None

    def ... :
        if ...:
            total = ...
            total_coeffs = ...
            for matiere in ...:
                total += ...
                total_coeffs += ...
            return ... / ...
        else:
            return ...

            


# --------- PYODIDE:corr --------- #
class Eleve:
    def __init__(self, prenom, nom, classe):
        self.prenom = prenom
        self.nom = nom
        self.classe = classe
        self.moyennes = {}

    def modifie_moyenne(self, matiere, moyenne):
        self.moyennes[matiere] = moyenne

    def moyenne_de(self, matiere):
        if matiere in self.moyennes:
            return self.moyennes[matiere]

    def moyenne_simple(self):
        if len(self.moyennes) > 0:
            total = 0
            for moy in self.moyennes.values():
                total += moy
            return total / len(self.moyennes)

    def moyenne_ponderee(self, coeffs):
        if len(self.moyennes) > 0:
            total = 0
            total_coeffs = 0
            for matiere in self.moyennes:
                total += self.moyennes[matiere] * coeffs[matiere]
                total_coeffs += coeffs[matiere]
            return total / total_coeffs
        else:
            return None


# --------- PYODIDE:tests --------- #
margaret = Eleve("Margaret", "Hamilton", "Te5")
margaret.modifie_moyenne("études spatiales", 20)
margaret.modifie_moyenne("maths", 14)
coeffs = {"études spatiales": 1, "maths": 0.5}
assert margaret.moyenne_ponderee(coeffs) == 18.0

# --------- PYODIDE:secrets --------- #
albert = Eleve("Albert", "Einstein", "Te2")
albert.modifie_moyenne("physique", 20)
albert.modifie_moyenne("maths", 20)
albert.modifie_moyenne("allemand", 17)
coeffs = {"physique": 1, "maths": 1, "allemand": 0.5}
assert albert.moyenne_ponderee(coeffs) == 19.4
