# --- PYODIDE:code --- #

class Ennemi_5 :
    def __init__(self, p, r) :
        self.posX = 0
        self.posY = 0
        self.pv = p
        self.rapidite = r
        
    def recoitDegats(self, deg) :
        self.pv = self.pv - deg
        
    def seDeplace(self, nouveauX, nouveauY) :
        self.posX = nouveauX
        self.posY = nouveauY
        
    def affiche(self) :
        print(' Ennemi [ posX =', self.posX, ' posY = ', self.posY, ' pv =', self.pv, ' rapidite =', self.rapidite, '] ')
        
    def estVivant(self) :
        return self.pv > 0

grosMechant = Ennemi_5(100, 2)
  
