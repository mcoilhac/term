# --- PYODIDE:env --- #

class Ennemi_6 :
    def __init__(self, p, r) :
        self.posX = 0
        self.posY = 0
        self.pv = p
        self.rapidite = r
        
    def recoitDegats(self, deg) :
        self.pv = self.pv - deg
        
    def seDeplace(self, nouveauX, nouveauY) :
        self.posX = nouveauX
        self.posY = nouveauY
        
#   def affiche(self) :
#       print(' Ennemi [ posX =', self.posX, ' posY = ', self.posY, ' pv =', self.pv, ' rapidite =', self.rapidite, '] ')
        
    def estVivant(self) :
        return self.pv > 0
    
    def __str__(self): 
        return 'posX = ' + str( self.posX) +' posY = ' + str( self.posY) +' pv = ' + \
    str( self.pv) + ' rapidite = ' + str( self.rapidite)

# --- PYODIDE:code --- #

x = Ennemi_6(10, 20)
y = Ennemi_6(10, 20)
z = y

print(x)
print(y)
print(z)
