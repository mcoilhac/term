On notera que pour nommer les méthodes, l'usage est d'utiliser des verbes conjugués à l'impératif : `guy.donne(truc)` et non `guy.donner(truc)`.
