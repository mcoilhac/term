# --- PYODIDE:code --- #

class Joueur_6:
    
    def __init__(self, nom, age, handicap = 0):
        self.score = 0
        self.est_actif = True
        self.nbre_parties_jouees = 0
        self.handicap = handicap
        self.nom = nom
        self.age = age

Kevin = Joueur_6("DURAND", 18)  # On utilise le paramètre par défaut
Champion_1 = Joueur_6("MAITRE", 30, 50)  # On n'utilise pas le paramètre par défaut
print(Kevin.handicap)
print(Champion_1.handicap)