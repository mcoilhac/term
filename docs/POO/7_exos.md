---
author: Nicolas revéret et Mireille Coilhac
title: Exercices de POO - série 4
tags:
    - POO
---


## Exercice 1 : Élève en POO



On souhaite dans cet exercice créer une classe `Eleve` ayant quatre attributs :

* un prénom `prenom` de type `#!py str` ;

* un nom `nom` de type `#!py str` ;

* une classe `classe` de type `#!py str` ;

* des moyennes `moyennes` de type `#!py dict`. Ce dictionnaire associe à des intitulés de matières (`#!py str`), les moyennes correspondantes (au format `#!py int` ou `#!py float`).

Cet exercice est en plusieurs parties et demande de compléter la classe `Eleve` en ajoutant différentes méthodes. 
Il est progressif, il ne faut pas passer à la question suivante sans avoir terminé celle en cours.



??? question "Constructeur"

    Lors de la création d'un objet de type `Eleve`, on fournit les valeurs des attributs `prenom`, `nom` et `classe` (**dans cet ordre**).
    
    L'attribut `moyennes` est initialement vide.
    
    Compléter le constructeur de la classe `Eleve`.
    
    ??? example "Exemple"

        ```pycon title=""
        >>> albert = Eleve("Albert", "Einstein", "Te2")
        >>> albert.prenom
        'Albert'
        >>> albert.nom
        'Einstein'
        >>> albert.classe
        'Te2'
        >>> albert.moyennes
        {}
        ```
        
    === "Version vide"
        {{ IDE('scripts/exo_constructeur_vide')}}
    === "Version à compléter"
        {{ IDE('scripts/exo_constructeur_trous')}}

??? question "Méthode `modifie_moyenne`"

    La méthode `modifie_moyenne` prend deux paramètres, un intitulé de matière (`#!py str`) et une moyenne (au format `#!py int` ou `#!py float`) et ajoute ce couple `(clé: valeur)` à l'attribut `moyennes` d'un objet `Eleve`.

    Écrire la méthode `modifie_moyenne`.
    
    ??? example "Exemple"

        ```pycon title=""
        >>> carl = Eleve("Carl Friedrich", "Gauss", "Te3")
        >>> carl.modifie_moyenne("arithmétique", 20)
        >>> carl.modifie_moyenne("chimie", 12)
        >>> carl.moyennes
        {'arithmétique': 20, 'chimie': 12}
        >>> carl.modifie_moyenne("chimie", 13)
        >>> carl.moyennes
        {'arithmétique': 20, 'chimie': 13}
        ```
    
    === "Version vide"
        {{ IDE('scripts/exo_modif_vide')}}
    === "Version à compléter"
        {{ IDE('scripts/exo_modif_trous')}}

??? question "Méthode `moyenne_de`"

    La méthode `moyenne_de` prend en unique paramètre un intitulé de matière (`#!py str`) et renvoie la moyenne de cet élève dans cette matière.
    
    Si l'élève ne possède pas de moyenne dans cette matière, la fonction renverra `#!py None`.
    
    Écrire la méthode `moyenne_de`.
    
    ??? example "Exemple"

        ```pycon title=""
        >>> donald = Eleve("Donald", "Knuth", "Te7")
        >>> donald.modifie_moyenne("informatique", 20)
        >>> donald.modifie_moyenne("musique", 13)
        >>> donald.moyenne_de("informatique")
        20
        >>> donald.moyenne_de("musique")
        13
        >>> donald.moyenne_de("lancer de javelot")
        >>>
        ```
    
    === "Version vide"
        {{ IDE('scripts/exo_moyenne_de_vide')}}
    === "Version à compléter"
        {{ IDE('scripts/exo_moyenne_de_trous')}}

??? question "Méthode `moyenne_simple`"

    La méthode `moyenne_simple` calcule et renvoie la moyenne générale de l'élève. Celle-ci se calcule en effectuant la moyenne des moyennes.
    
    Si l'élève n'a aucune moyenne, la fonction renverra `#!py None`.
    
    Écrire la méthode `moyenne_simple`.
    
    ??? example "Exemple"

        ```pycon title=""
        >>> jane = Eleve("Jane", "Goodall", "Te3")
        >>> jane.modifie_moyenne("éthologie", 20)
        >>> jane.modifie_moyenne("théorie des groupes", 1)
        >>> jane.moyenne_simple()
        10.5
        ```
        
    === "Version vide"
        {{ IDE('scripts/exo_moyenne_simple_vide')}}
    === "Version à compléter"
        {{ IDE('scripts/exo_moyenne_simple_trous')}}
    
??? question "Méthode `moyenne_ponderee`"

    La méthode `moyenne_ponderee` prend comme unique paramètre un dictionnaire `coeffs` associant des intitulés de matières (`#!py str`) à des coefficients (au format `#!py int` ou `#!py float`).
    
    Cette fonction calcule la moyenne pondérée de l'élève en appliquant les coefficients fournis en paramètre.
    
    Si l'élève n'a aucune moyenne, la fonction renverra `#!py None`.
    
    On garantit que dictionnaire `#!py coeffs` contient toutes les clés correspondant aux matières du dictionnaire `#!py moyennes`.
    
    Écrire la méthode `moyenne_ponderee`.
    
    ??? example "Exemple"

        ```pycon title=""
        >>> margaret = Eleve("Margaret", "Hamilton", "Te5")
        >>> margaret.modifie_moyenne("études spatiales", 20)
        >>> margaret.modifie_moyenne("maths", 14)
        >>> coeffs = {"études spatiales": 1, "maths": 0.5}
        >>> margaret.moyenne_ponderee(coeffs)
        18.0
        ```
        
    === "Version vide"
        {{ IDE('scripts/exo_moyenne_ponderee_vide')}}
    === "Version à compléter"
        {{ IDE('scripts/exo_moyenne_ponderee_trous')}}
    

    
_D'après un exercice de Nicolas Revéret_