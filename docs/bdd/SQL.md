---
author: Mireille Coilhac
title: SGBD et SQL
---

## I. Système de gestion de base de données

Pour manipuler les données présentes dans une base de données (écrire, lire ou encore modifier), il est nécessaire d'utiliser un type de logiciel appelé "**s**ystème de **g**estion de **b**ase de **d**onnées" très souvent abrégé en SGBD.
Il existe une multitude de SGBD : gratuits/payants, libres/propriétaires...

### Les fonctions du SGBD

Les SGBD permettent de grandement simplifier la gestion des bases de données :

1. **Lecture, écriture, modification des informations** : les SGBD permettent de gérer les contenus dans une base de données

2. **Autorisations d'accès** : Il est en effet souvent nécessaire de contrôler les accès par exemple en permettant à l'utilisateur A de lire et d'écrire dans la base de données alors que l'utilisateur B aura uniquement la possibilité de lire les informations contenues dans cette même base de données.

3. **Sécuriser les bases de données** : les fichiers des bases de données sont stockés sur des disques durs, et peuvent subir des pannes. Il est souvent nécessaire que l'accès aux informations contenues dans une base de données soit maintenu, même en cas de panne matérielle. Les bases de données sont donc dupliquées sur plusieurs ordinateurs afin qu'en cas de panne d'un ordinateur A, un ordinateur B contenant une copie de la base de données présente dans A, puisse prendre le relais. Tout cela est très complexe à gérer, en effet toute modification de la base de données présente sur l'ordinateur A doit entrainer la même modification de la base de données présente sur l'ordinateur B. Cette synchronisation entre A et B doit se faire le plus rapidement possible, il est fondamental d'avoir des copies parfaitement identiques en permanence. C'est aussi les SGBD qui assurent la maintenance des différentes copies de la base de données.

4. **Accès concurrent** : plusieurs personnes peuvent avoir besoin d'accéder aux informations contenues dans une base de données en même temps. Cela peut parfois poser problème, notamment si les 2 personnes désirent modifier la même donnée au même moment (on parle d'accès concurrent). Ces problèmes d'accès concurrent sont aussi gérés par les SGBD. 

??? note pliée "Utilisation des SGBD"

    L'utilisation des SGBD explique en partie la supériorité de l'utilisation des BDD sur des solutions plus simples, mais aussi beaucoup plus limitées, comme les fichiers au format CSV.

## II. Le langage SQL

[Vidéo : cours SQL sur Lumni](https://www.lumni.fr/video/interrogation-d-une-base-de-donnees-relationnelle#containerType=serie&containerSlug=la-maison-lumni-lycee){ .md-button target="_blank" rel="noopener" }

## III. Premiers pas en SQL

!!! info "SQL"

    Nous avons vu en cours ce qu'est le modèle relationnel et ce qu'est le schéma relationnel.

    Nous allons maintenant aborder la pratique : créer, gérer, consulter une base de données.

    Pour cela nous utiliserons le langage le plus utilisé : Structured Query Langage, plus communément nommé **SQL**.

    Dans ce tout premier TP nous allons créer une base de données ne contenant qu'une seule table.

### 1. création et gestion d'une table

!!! example "La table : eleves"

    Attributs : 

    * id (un identifiant unique servant de clef primaire)
    * nom
    * prénom
    * date_naiss
    * classe
    * nb_heures (nombre d'heures dans son EDT)

!!! info "Syntaxe pour créer une table"

    ```sql title=""
    CREATE TABLE (
        attribut type ,
        attribut type ,
        attribut type
      );
    ```

    On peut parfois préceder de :
    <pre>
    DROP TABLE IF EXISTS table ;
    </pre>
    ceci afin de supprimer la table si elle existe déjà. 

🖐 **attention** ceci est à manier avec précaution, vous allez détruire une table existante ! Mais c'est pratique dans nos TP : quand vous aurez fait une erreur, vous devrez souvent reconstruire la table, et cette ligne DROP permettra de le faire facilement. Vous pourrez aussi utiliser le menu Cellule -> exécuter tout pour refaire tout le TP jusqu'au point ou vous en étiez.

On peut ajouter des **contraintes**, par exemple, au moins l'un des attributs sera clef primaire.

✏️ La contrainte NOT NULL est utilisée pour indiquer que cet attribut doit avoir une valeur non nulle (champ obligatoire).

✏️ Le type, pour nous, sera : INTEGER, TEXT, REAL ou DATE. Mais il en existe d'autres.

✏️ pour les dates, on précise le format (voir ci-dessous).

✏️Des virgules séparent les attributs, le dernier n'est pas ponctué par la virgule.

Les schéma de notre BDD sera limité à une seule table :

<b><FONT courier new>eleves (<u>id</u> , nom , prenom, date_naiss, classe, nb_heures)</font></b>


Allez, on se lance :

{!{ sqlide titre="Création de la table eleves" sql="bdd/sql/creer_table_eleves.sql" espace="eleves"}!}

Nous avons créé une table, **ajoutons des éléments.**

!!! info "Insérer des éléments"

    ✏️ Pour cela on utilise 
    <pre>
    INSERT INTO table
    (liste des attributs)
    VALUES
    (... , ... , ...) ,
    (... , ... , ...) ,
    (... , ... , ...) ;
    </pre>

La liste des attributs n'est pas obligatoire mais recommandée.

**Ajoutons 2 élèves :**

{!{ sqlide titre="Ajoutons deux éleves" sql="bdd/sql/ajout_2_eleves.sql" espace="eleves"}!}

### Les contraintes d'intégrité :

Le SGBD vérifie toujours que tout est correct, et refuse des insertions qui briseraient l'intégrité de la base.

Essayons d'ajouter un 3ème élève avec un **id** déjà utilisé, rompant la contrainte d'unicité de la clef primaire :

{!{ sqlide titre="Insertion" sql="bdd/sql/insert_faux_1.sql" espace="eleves"}!}

Le SGBD a fait son travail et nous a empêché de mettre la table en erreur.

Bon, ajoutons quand même Alfred mais essayons d'omettre sa date de naissance, qu'on va mettre à NULL :

{!{ sqlide titre="Insertion" sql="bdd/sql/insert_faux_2.sql" espace="eleves"}!}

Rien ne lui échappe !!!!

Bon cette fois, on le remet correctement :

{!{ sqlide titre="Insertion" sql="bdd/sql/insert_3.sql" espace="eleves"}!}

???+ question "À vous de jouer 1"

    Ajoutez les élèves suivants :

    ![élèves à ajouter](images/ajout_eleves.png){ width=50% }

    {!{ sqlide titre="Insertion" sql="bdd/sql/a_vous_1.sql" espace="eleves"}!}

    ??? success "Solution"

        ```sql
        INSERT INTO eleves
        (id, nom, prenom, date_naiss, classe, nb_heures)
        VALUES
        (4, 'Torvalds', 'Linus', '28.12.1969', 'TG1', 22),
        (5, 'Babbage', 'Charles', '26.12.1791', 'TG1', 39),
        (6, 'Turing', 'Alan', '23.06.1912', 'TG1', 29); 
        ```

**Vérifions notre table :**   

Nous reviendrons plus tard sur l'instruction `SELECT`. Pour l'instant, il est pratique de connaître la syntaxe suivante, qui permet d'afficher tout le contenu de la table `eleves`.

{!{ sqlide titre="Sélection" sql="bdd/sql/select_tout.sql" espace="eleves"}!}

!!! example "Modifier un enregistrement"

    Il y a une erreur dans la table : Georges Clooney est bien né le 6 mai, mais en 1961, pas en 1960.

!!! info "Modifier un enregistrement"

    ✏️ Rectifions. Pour cela on utilise :

    <pre>UPDATE table
    SET attribut = nouvelle_valeur
    WHERE condition;
    </pre>

    Il peut y avoir différentes conditions. Nous allons voir deux exemples différents

!!! example "Exemple 1"

    {!{ sqlide titre="Modification" sql="bdd/sql/modif_1.sql" espace="eleves"}!}

!!! example "Exemple 2"

    Mais une nouvelle règle s'applique : tous les élèves ayant moins de 30h de cours 
    se voient imposer 2h supplémentaires d'accompagnement :

    {!{ sqlide titre="Modification" sql="bdd/sql/modif_2.sql" espace="eleves"}!}

???+ question "À vous de jouer 2"

    Il y a une erreur : tous les élèves de TG1 sont en réalité dans la classe InfoMaster

    Faire la modification (vous devez modifier tout en une seule fois)

    {!{ sqlide titre="modification" sql="bdd/sql/a_vous_2.sql" espace="eleves"}!}

    ??? success "Solution"

        ```sql title=""
        UPDATE eleves
        SET classe =  'infoMaster'
        WHERE classe = 'TG1';
        ```

    Comment vérifier ?

    {!{ sqlide titre="Vérifier" sql="bdd/sql/verif.sql" espace="eleves"}!}

    ??? success "Solution"

        ```sql title=""
        SELECT * 
        FROM eleves;
        ```

!!! info "Supprimer un (ou plusieurs) enregistrement"

    ✏️ La syntaxe est :  

    ```sql title=""
    DELETE 
    FROM table
    WHERE condition;
    ```

!!! example "Supprimer Belmondo"

    {!{ sqlide titre="Suppression" sql="bdd/sql/supprimer_bebel.sql" espace="eleves"}!}

    Bon, allez, on remet Bebel !

    {!{ sqlide titre="Ajout" sql="bdd/sql/remettre_bebel.sql" espace="eleves"}!}

???+ question "À vous de jouer 3"

    **1.** Supprimer l'élève Turing, puis vérifier la base de données,

    {!{ sqlide titre="" sql="bdd/sql/a_vous_3_1.sql" espace="eleves"}!}

    ??? success "Solution"

        ```sql title=""
        DELETE
        FROM eleves
        WHERE nom = 'Turing';

        SELECT * 
        FROM eleves;
        ```

    **2.** Réinscrire Turing et vérifier à nouveau la base de données.

    {!{ sqlide titre="" sql="bdd/sql/a_vous_3_2.sql" espace="eleves"}!}

    ??? success "Solution"

        ```sql title=""
        INSERT INTO eleves
        (id, nom, prenom, date_naiss, classe, nb_heures)
        VALUES
        (6, 'Turing', 'Alan', '23.06.1912', 'infoMaster', 31);

        SELECT * 
        FROM eleves;
        ```

### 2. Consultation d'une table

!!! info "Sélection"

    ```sql title=""
    SELECT * FROM <nom_table>
    ```
    
    Cette requête va nous donner **toutes les lignes et toutes les colonnes** de la table.

!!! info "Projection" 

    ```sql title=""
    SELECT <nom_attribut>, <nom_attribut> FROM <nom_table>
    ```

    sélectionne une  ou plusieurs colonnes : 

    <table>
        <tr>
            <td style="border:1px solid"></td>
            <td style="border:1px solid"></td>
            <td style="border:1px solid; background-color:orange"></td>
            <td style="border:1px solid"></td>
            <td style="border:1px solid; background-color:orange"></td>
        </tr>
        <tr>
            <td style="border:1px solid"></td>
            <td style="border:1px solid"></td>
            <td style="border:1px solid; background-color:orange"></td>
            <td style="border:1px solid"></td>
            <td style="border:1px solid; background-color:orange"></td>
        </tr>
        <tr>
            <td style="border:1px solid"></td>
            <td style="border:1px solid"></td>
            <td style="border:1px solid; background-color:orange"></td>
            <td style="border:1px solid"></td>
            <td style="border:1px solid; background-color:orange"></td>
        </tr>
        <tr>
            <td style="border:1px solid"></td>
            <td style="border:1px solid"></td>
            <td style="border:1px solid; background-color:orange"></td>
            <td style="border:1px solid"></td>
            <td style="border:1px solid; background-color:orange"></td>
        </tr>
    </table>

!!! example "Les élèves"

    {!{ sqlide titre="" sql="bdd/sql/select_colonnes.sql" espace="eleves"}!}

!!! info "les conditions courantes"

   
    <nom_attribut> [=, >, <, <=, >=] <valeur>
    <nom_attribut> LIKE 
    <chaîne de caractères avec des caractères génériques comme % ou _>

    Le caractère `%` représente n'importe quelle chaîne de caractères. On peut l'insérer n'importe où dans la chaîne de caractères de comparaison. 

    Par exemple : 
        
    ```sql title=""
    SELECT *
    FROM eleve
    WHERE nom LIKE '%a%'
    ```
        
    sélectionne toutes les lignes où la valeur de l'attribut *nom* est une chaîne de caractères qui contient un *a*. 

!!! info "Restriction : extraire une ou des lignes"

    Exemple : quels sont les élèves de la classe TG8 ?

    ```sql title=""
    SELECT *
    FROM eleves
    WHERE classe = 'TG8';
    ```

!!! info "Restriction avec projection"

    On va combiner les deux pour obtenir seulement les cases rouges : 

    <table>
        <tr>
            <td style="border:1px solid"></td>
            <td style="border:1px solid"></td>
            <td style="border:1px solid; background:orange"></td>
            <td style="border:1px solid"></td>
        </tr>
        <tr>
            <td style="border:1px solid"></td>
            <td style="border:1px solid"></td>
            <td style="border:1px solid; background:orange"></td>
            <td style="border:1px solid"></td>
        </tr>
        <tr>
            <td style="border:1px solid; background:orange"></td>
            <td style="border:1px solid; background:orange"></td>
            <td style="border:1px solid; background:red"></td>
            <td style="border:1px solid; background:orange"></td>
        </tr>
        <tr>
            <td style="border:1px solid"></td>
            <td style="border:1px solid"></td>
            <td style="border:1px solid; background:orange"></td>
            <td style="border:1px solid"></td>
        </tr>
        <tr>
            <td style="border:1px solid; background:orange"></td>
            <td style="border:1px solid; background:orange"></td>
            <td style="border:1px solid; background:red"></td>
            <td style="border:1px solid; background:orange"></td>
        </tr>
        <tr>
            <td style="border:1px solid"></td>
            <td style="border:1px solid"></td>
            <td style="border:1px solid; background:orange"></td>
            <td style="border:1px solid"></td>
        </tr>
        
    </table>

!!! example "Exemple"

    Quels sont les prénoms des élèves qui ne sont pas en TG8 ?

    {!{ sqlide titre="" sql="bdd/sql/pas_TG8.sql" espace="eleves"}!}

    ??? note "Autre syntaxe"

        Autre syntaxe équivalente : 

        ```sql title=""
        SELECT nom, prenom
        FROM eleves
        WHERE classe != 'TG8';
        ```

!!! info "Les conditions courantes"

    * = ou != (noté aussi <>)
    * < &nbsp;&nbsp; >  &nbsp;&nbsp; <= &nbsp;&nbsp;  >= 
    * LIKE
    * is (nous verrons plus tard le **is**)

!!! exemple "Exemples"

    {!{ sqlide titre="" sql="bdd/sql/prenom_b.sql" espace="eleves"}!}

    {!{ sqlide titre="" sql="bdd/sql/nom_o.sql" espace="eleves"}!}

    {!{ sqlide titre="" sql="bdd/sql/heures.sql" espace="eleves"}!}

    {!{ sqlide titre="" sql="bdd/sql/heures_sup.sql" espace="eleves"}!}


???+ question "À vous de jouer 4"

    Écrire la requête qui permet d'afficher le nom, la classe et le nombre d'heures des élèves qui ont moins de 25 heures dans leur emploi du temps.

    {!{ sqlide titre="" sql="bdd/sql/a_vous_4.sql" espace="eleves"}!}

    ??? success "Solution"

        ```sql title=""
        SELECT nom, classe, nb_heures
        FROM eleves
        WHERE nb_heures < 25;
        ``` 

### 3. Faire des calculs, afficher de nouvelles colonnes

???+ question "Tester"

    **1.** Tester

    {!{ sqlide titre="" sql="bdd/sql/count.sql" espace="eleves"}!}

    ??? success "Solution"

        Cela nous donne le nombre d'élèves de la table `eleves`

    **2.** Tester

    {!{ sqlide titre="" sql="bdd/sql/min.sql" espace="eleves"}!}

    **3.** Tester

    {!{ sqlide titre="" sql="bdd/sql/min_max_moy.sql" espace="eleves"}!}

    ??? success "Solution"

        `AVG` donne la moyenne.

### 4. Renommer des colonnes

!!! example "AS"

    {!{ sqlide titre="" sql="bdd/sql/renommer.sql" espace="eleves"}!}

    {!{ sqlide titre="" sql="bdd/sql/renommer_2.sql" espace="eleves"}!}

???+ question "À vous de jouer 5"

    Ecrire la requête qui affiche le nombre moyen d'heures des élèves en infoMaster

    {!{ sqlide titre="" sql="bdd/sql/a_vous_5.sql" espace="eleves"}!}

    ??? success "Solution"

        ```sql title=""
        SELECT AVG(nb_heures)
        FROM eleves
        WHERE classe = 'infoMaster';
        ```

### 5. Trier

???+ question "Tester"

    **1.** Tester

    {!{ sqlide titre="" sql="bdd/sql/tri_croissant.sql" espace="eleves"}!}

    **2.** Tester

    {!{ sqlide titre="" sql="bdd/sql/tri_decroissant.sql" espace="eleves"}!}

!!! info "Le tri de chaînes de caractères"

    Le tri de chaînes de caractères se fait par ordre alphabétique.


???+ question "À vous de jouer 6"

    Ecrire la requête qui permet d'afficher les noms, et dates de naissances, par ordre alphabétique, des élèves nés au 20ème siècle.

    {!{ sqlide titre="" sql="bdd/sql/a_vous_6.sql" espace="eleves"}!}

    ??? tip "Aide 1"

        utiliser la date de naissance et `LIKE`

    ??? tip "Aide 2"

        On peut utiliser plusieurs fois `%`

    ??? success "Solution"

        ```sql title=""
        SELECT nom, date_naiss
        FROM eleves
        WHERE date_naiss LIKE '%.%.19%'
        ORDER BY nom ; 
        ```
    
## IV. Un peu plus loin avec une seule table

!!! info "En bref"

    😊 Nous avons eu l'occasion d'étudier la structure d'une base de données relationnelle, nous allons maintenant apprendre à utilise le langage SQL (Structured Query Language) pour :

    * créer une base des données
    * créer des attributs,
    * ajouter de données
    * modifier des données
    * interroger une base de données afin d'extraire des informations.

    SQL est propre aux bases de données **relationnelles** (les autres types de bases de données utilisent d'autres langages).

??? note "Différents langages"

    👉  SQLite est un système de gestion de base de données relationnelles très répandu.
    Noter qu'il existe d'autres systèmes de gestion de base de données relationnelle comme MySQL ou PostgreSQL.  
    Dans tous les cas, le langage de requête utilisé est le SQL (même si parfois on peut noter quelques petites différences). Ce qui sera vu ici avec SQLite pourra, à quelques petites modifications près, être utilisé avec, par exemple, MySQL.

### 1. Création d'une base de données

Exécuter :

{!{ sqlide titre="" sql="bdd/sql/creer_livre.sql" espace="livre"}!}

!!! info "Clé primaire"

    😊 Vous venez de créer votre première table.


    👉 Revenons sur cette première requête :
    CREATE TABLE : permet de créer la table nommée "livre".

    (id INT PRIMARY KEY, titre TEXT, auteur TEXT, ann_publi INT, note INT)  
    👉 Cette ligne permet de créer les attributs et leur domaine :
    id : entier
    titre : texte
    auteur : texte
    ann_pulbi : entier
    note : entier

    👉 Le terme de **PRIMARY KEY** associé à id permet de définir la clé primaire et le SGBD nous avertira si l'on tente d'attribuer 2 fois la même valeur à l'attribut.

### 2. Ajout de données

!!! info "INSERT INTO"

    ```sql title=""
    INSERT INTO
    ```

Exécuter :

{!{ sqlide titre="" sql="bdd/sql/ajouts_livre.sql" espace="livre"}!}

???+ question "À vous de jouer 7"

    Écrivez et testez une requête permettant d'ajouter ces deux livres :

    * Le grand secret, de René Barjavel, paru en 1973, note 7
    * Voyage au centre de la Terre, et celui de Jules Verne, paru en 1864, note 8.

    {!{ sqlide titre="" sql="bdd/sql/a_vous_7.sql" espace="livre"}!}

    ??? success "Solution"

        ```sql title=""
        INSERT INTO livre 
        (id, titre, auteur, ann_publi, note)
        VALUES
        (17, 'Le grand secret', 'Barjavel', 1973, 7 ),
        (18, 'Voyage au centre de la Terre', 'Verne', 1864, 8);
        ```

### 3. Suppression des données

!!! info "DELETE FROM"

    ```sql title=""
    DELETE FROM
    ```

    👉 DELETE FROM est utilisée pour supprimer les enregistrements existants dans une table.

Exécuter :

{!{ sqlide titre="" sql="bdd/sql/supprimer_livre.sql" espace="livre"}!}


???+ question "À vous de jouer 8"

    Ecrire la requête qui permet de visualiser les données et vérifier que les suppressions ont bien été effectuées.

    {!{ sqlide titre="" sql="bdd/sql/a_vous_8.sql" espace="livre"}!}

    ??? success "Solution"

        ```sql title=""
        SELECT * 
        FROM livre;
        ```

???+ question "À vous de jouer 9"

    Nous allons remettre ces données dans la table. A vous de le faire !

    {!{ sqlide titre="" sql="bdd/sql/a_vous_9.sql" espace="livre"}!}

    ??? success "Solution"

        ```sql title=""
        INSERT INTO livre
        (id, titre, auteur, ann_publi, note)
        VALUES
        (3, 'Fondation', 'Asimov', 1951, 9),
        (10, 'Les Robots', 'Asimov', 1950, 9),
        (15, 'La Fin de l’éternité', 'Asimov', 1955, 8);
        ```

### 4. Mise à jour des données

!!! info "UPDATE"

    ```sql title=""
    UPDATE
    ```

    👉 Vous pouvez utiliser la requête `UPDATE` avec une clause `WHERE` pour mettre à jour la ligne sélectionnée, sinon, toutes les lignes seront mises à jour.

???+ question "Tester"

    {!{ sqlide titre="" sql="bdd/sql/barjavel.sql" espace="livre"}!}

    ??? success "Solution"

        👉 Permet de remplacer Barjavel par R. Barjavel dans tous les enregistrements.

### 5. Rechercher des données

!!! info "SELECT"

    ```sql title=""
    SELECT
    ```

Exécuter :

{!{ sqlide titre="" sql="bdd/sql/select_livre.sql" espace="livre"}!}

???+ question "À vous de jouer 10"

    Écrivez et testez une requête permettant d'obtenir uniquement les titres des livres.

    {!{ sqlide titre="" sql="bdd/sql/a_vous_10.sql" espace="livre"}!}

    ??? success "Solution"

        ```sql title=""
        SELECT titre
        FROM livre;
        ```

### 6. Rechercher avec un critère de sélection

!!! info "WHERE"

    Pour imposer une (ou des) condition(s) permettant de sélectionner uniquement certains attributs ou valeurs
    👉 utiliser la clause `WHERE`


!!! example "Exemple 1"

    Pour rechercher tous les livres d'Isaac Asimov :

    {!{ sqlide titre="" sql="bdd/sql/asimov_1.sql" espace="livre"}!}

!!! example "Exemple 2"

    Cette commande sélectionne tous les t-uplet ou auteur se termine par Asimov (par exemple Isaac Asimov ou I. Asimov).

    {!{ sqlide titre="" sql="bdd/sql/asimov_2.sql" espace="livre"}!}


???+ question "À vous de jouer 11"

    Écrivez et testez une requête permettant d'obtenir uniquement les titres des livres de K.Dick.

    {!{ sqlide titre="" sql="bdd/sql/a_vous_11.sql" espace="livre"}!}

    ??? success "Solution"

        ```sql title=""
        SELECT titre
        FROM livre
        WHERE auteur = 'K.Dick'
        ```

!!! info "AND, OR"

    👉 En compléments de `WHERE` il est possible de préciser les conditions avec `OR` ou `AND`.

!!! example "Tester"

    Exemple : Pour rechercher tous les livres d'Isaac Asimov écrit avant 1954

    {!{ sqlide titre="" sql="bdd/sql/asimov_3.sql" espace="livre"}!}


???+ question "À vous de jouer 12"

    D'après vous (sans taper la requête !) que donne la requête suivante :

    ```sql title=""
    SELECT titre, ann_publi
    FROM livre
    WHERE auteur = 'Asimov' AND note >= 9;
    ```

    ??? success "Solution"

        Cette requête affiche le titre et l'année de publication de tous les livres de Asimov ayant une note supérieure ou égale à 9.


???+ question "À vous de jouer 13"

    Écrivez et testez une requête permettant d'obtenir uniquement les livres de K.Dick. et d'Asimov publié après 1950 et dont la note est supérieure ou égale à 8.

    Votre requête doit renvoyer l'auteur, le titre du livre et l'année de publication.

    {!{ sqlide titre="" sql="bdd/sql/a_vous_13.sql" espace="livre"}!}

    ??? success "Solution"

        ```sql title=""
        SELECT auteur, titre, ann_publi
        FROM livre
        WHERE (auteur = 'Asimov' OR auteur = 'K.Dick') AND (ann_publi > 1950 AND note >= 8);
        ```
    
    ??? danger "Attention"

        Attention, il y a des régles de priorités pour les connecteurs logiques (OR, AND etc). Un connecteur logique doit se trouver entre deux expressions booléennes. Dans le cas d'enchaînements de plus de deux connecteurs, des règles de priorités interviennent. Il est extrêmement recommandé d'utiliser des parenthèses, pour clairement voir la structure.

        Dans l'exemple précédant, une absence de parenthèses conduit à l'affichage du livre "Les Robots" (si vous l'avez bien remis dans la table). Or l'année de publication de ce roman est égale à 1950. Il ne doit pas apparaître dans une requète où l'on cherche ann_publi > 1950

### 7. Recherche avec un résultat classé selon un ordre précis

!!! info  "ORDER BY"

    Il est aussi possible de rajouter la clause `ORDER BY` après la condition `WHERE` afin d'obtenir les résultats classés selon un ordre précis.
    La clause `ORDER BY attributs` peut être suivie des clause `ASC` ou `DESC`  pour un classement selon l'ordre croissant ou décroissant.

    Par défaut, sans précision, le classement est par ordre croissant.

???+ question "À vous de jouer 14"

    Écrivez et testez une requête permettant d'obtenir tous les livres écrits depuis 1960 classés par date de publication décroissante.

    Votre requête doit renvoyer l'auteur, le titre du livre et l'année de publication.

    {!{ sqlide titre="" sql="bdd/sql/a_vous_14.sql" espace="livre"}!}

    ??? success "Solution"

        ```sql title=""
        SELECT auteur,titre,ann_publi
        FROM livre
        WHERE  ann_publi >= 1960 
        ORDER BY ann_publi DESC;
        ```

!!! example "Avec du texte"

    👉 Si la clause ORDER BY porte sur un attribut de type TEXT, Le classement est fait ordre alphabétique ou alphabétique inverse.

    Exécuter :

    {!{ sqlide titre="" sql="bdd/sql/order_alpha.sql" espace="livre"}!}

### 8. Recherche en évitant des doublons

!!! info "DISTINCT"

    On peut éviter des doublons grâce à la clause `DISTINCT`

Exécuter :

{!{ sqlide titre="" sql="bdd/sql/distinct.sql" espace="livre"}!}

### 9. Valeurs SQL `NULL`

!!! info "`NULL`"

    Un champ (un attribut) avec une valeur `NULL` est un champ sans valeur.

    Si un champ d'une table est facultatif, il est possible d'insérer un nouvel enregistrement ou de mettre à jour un enregistrement sans ajouter de valeur à ce champ. Ensuite, le champ sera enregistré avec une valeur `NULL`.

??? warning "Remarque"

    👉 Remarque : Une valeur NULL est différente d'une valeur zéro ou d'un champ contenant des espaces.
    
    🌵 Un champ avec une valeur NULL est un champ qui a été laissé vide lors de la création de l'enregistrement.

!!! info "Comment tester les valeurs NULL ?"

    Nous devrons utiliser les opérateurs `IS NULL `et `IS NOT NULL`.

    ```sql
    SELECT column_names
    FROM table_name
    WHERE column_name IS NULL;
    ```

    ou : 

    ```sql
    SELECT column_names
    FROM table_name
    WHERE column_name IS NOT NULL;
    ```

!!! example "Exemple"

    Ajoutons un titre dont l'auteur, l'année et la note nous sont inconnus :

    {!{ sqlide titre="" sql="bdd/sql/mystere.sql" espace="livre"}!}


???+ question "À vous de jouer 15"

    Extraire les livres où l'auteur est non renseigné.

    Votre requête doit renvoyer l'auteur, le titre du livre et l'année de publication.

    {!{ sqlide titre="" sql="bdd/sql/a_vous_15.sql" espace="livre"}!}

    ??? success "Solution"

        ```sql title=""
        SELECT auteur, titre, ann_publi
        FROM livre
        WHERE auteur IS NULL;
        ```


<!--- 

???+ question "TD premiers pas"

    Après avoir téléchargé le fichier, vous pourrez le lire à partir de [Basthon en SQL](https://notebook.basthon.fr/?kernel=sql){ .md-button target="_blank" rel="noopener" }

    🌐 TD à télécharger : Fichier `premiers_pas_sql_2023_sujet.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/premiers_pas_sql_2023_sujet.ipynb)
 
 
    😀 La correction est arrivée ...
    
    🌐 Fichier `premiers_pas_sql_2023_corr.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/premiers_pas_sql_2023_corr.ipynb)

 
 ⏳ La correction viendra bientôt ... 
😀 La correction est arrivée ...
    
🌐 Fichier `premiers_pas_sql_2023_corr.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/premiers_pas_sql_2023_corr.ipynb)
 

### SQL : allons un peu plus loin


???+ question "TD/Cours : une seule table"

    Après avoir téléchargé le fichier, vous pourrez le lire à partir de [Basthon en SQL](https://notebook.basthon.fr/?kernel=sql){ .md-button target="_blank" rel="noopener" }

    🌐 TD à télécharger : Fichier `une_table_sujet_2023.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/une_table_sujet_2023.ipynb)

    😀 La correction est arrivée ...

    🌐 Fichier `une_table_corr_2023_v2.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/une_table_corr_2023_v2.ipynb)

    



⏳ La correction viendra bientôt ... 

😀 La correction est arrivée ...

🌐 Fichier `une_table_corr_2023_v2.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/une_table_corr_2023_v2.ipynb)
-->

<!--- 
???+ question "TD/Cours : plusieurs tables - jointures"

    Après avoir téléchargé le fichier, vous pourrez le lire à partir de [Basthon en SQL](https://notebook.basthon.fr/?kernel=sql){ .md-button target="_blank" rel="noopener" }

    🌐 TD à télécharger : Fichier `jointures_sujet_2023.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/jointures_sujet_2023.ipynb)

    😀 La correction est arrivée ...

    🌐 Fichier `jointures_corr_2023.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/jointures_corr_2023.ipynb)
    

    


⏳ La correction viendra bientôt ... 
😀 La correction est arrivée ...
🌐 Fichier `jointures_corr_2023.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/jointures_corr_2023.ipynb)
-->

### Bilan

!!! abstract "CREATE TABLE"

    ```sql
    CREATE TABLE LIVRES
    (id INT PRIMARY KEY, titre TEXT, auteur TEXT, ann_publi INT, note INT);
    ```

!!! abstract "INSERT INTO"

    ```sql
    INSERT INTO LIVRES
    (id, titre, auteur, ann_publi, note)
    VALUES
    (1, '1984', 'Orwell', 1949, 10);
    ```

!!! abstract "UPDATE... SET... WHERE..."

    ```sql
    UPDATE LIVRES
    SET auteur = 'R. Barjavel'
    WHERE auteur = 'Barjavel';
    ```

!!! abstract "DELETE FROM ... WHERE..."

    ```sql
    DELETE FROM LIVRES 
    WHERE auteur = 'Asimov';
    ```

!!! abstract "SELECT ... FROM ... WHERE..."

    ```sql
    SELECT titre, ann_publi
    FROM LIVRES
    WHERE auteur = 'Asimov';
    ```
    Autres comparaisons :

    ```sql
    SELECT titre, ann_publi
    FROM LIVRES
    WHERE 'Asimov' and ann_publi < 1954 
    WHERE auteur LIKE '%Asimov';
    ```

!!! abstract "ORDER BY"

    ```sql
    SELECT auteur, titre, ann_publi
    FROM LIVRES
    WHERE  ann_publi >= 1060
    ORDER BY ann_publi DESC;
    ```

!!! abstract "DISTINCT"

    ```sql
    SELECT DISTINCT auteur
    FROM LIVRES;
    ```

!!! abstract "SELECT ... FROM ... JOIN ON ..."

    ```sql
    SELECT *
    FROM LIVRES
    JOIN AUTEURS ON LIVRES.id_auteur = AUTEURS.id;
    ```

!!! abstract "SELECT ... FROM ... AS ... JOIN ON... AS ..."

    ```sql
    SELECT *
    FROM LIVRES AS l
    JOIN AUTEURS AS a ON l.id_auteur = a.id ;
    ```

#### Les agrégats

!!! abstract "COUNT(...)"

    ```sql
    SELECT COUNT(*)
    FROM LIVRES
    WHERE auteur LIKE "%Asimov";
    ```

!!! abstract "SUM(...)"

    ```sql
    SELECT SUM(note)
    FROM LIVRES
    WHERE auteur LIKE "%Asimov";
    ```

!!! abstract "AVG(...)"

    ```sql
    SELECT AVG(note)
    FROM LIVRES
    WHERE auteur LIKE "%Asimov";
    ```

!!! abstract "MIN(...) et MAX(...)"

    Les syntaxes pour MIN(...) et MAX(...) sont analogues à celles pour SUM et AVG



## Crédits

Jean-Louis Thirot et Mireille Coilhac    