---
author: Mireille Coilhac
title: Les jointures
---

## I. Les jointures

Nous allons créer une nouvelle base de données ayant deux tables :

???+ question "À vous de jouer 1"

    **1.** Créez une table `auteur` comportant les attributs suivants

    * id : entier - clé primaire
    * nom : texte
    * prenom : texte
    * ann_naissance : entier
    * langue_ecriture : texte

    Créez une table `livre` comportant les attributs suivants

    * id : entier - clé primaire
    * titre : texte
    * id_auteur : entier
    * ann_publi : entier
    * note : entier

    {!{ sqlide titre="" sql="bdd/sql/a_vous_join_1.sql" espace="livre_auteur"}!}

    ??? success "Solution"

        ```sql title=""
        CREATE TABLE auteur
        (id INT PRIMARY KEY, nom TEXT, prenom TEXT, ann_naissance INT, langue_ecriture TEXT);
        
        CREATE TABLE livre
        (id INT PRIMARY KEY, titre TEXT, id_auteur INT, ann_publi INT, note INT);
        ```

    **2.** Nous allons ajouter les données de la table `auteur` et de la table `livre`

    Exécuter :

    {!{ sqlide titre="" sql="bdd/sql/insertions_livre_auteur.sql" espace="livre_auteur"}!}

😊 Nous avons maintenant 2 tables que nous allons pouvoir associer grâce aux jointures. 

!!! abstract "Résumé"

    Il existe différents types de jointures en SQL. Cette année nous n’étudierons que : `JOIN … ON`  

    🌵 Remarque :  vous trouverez parfois écrit `INNER JOIN … ON`. C'est la même chose.

!!! info "`JOIN ... ON`"

    `JOIN ... ON`


!!! Example "Exemple"

    Tester :

    {!{ sqlide titre="" sql="bdd/sql/jointure_livre_auteur.sql" espace="livre_auteur"}!}

    😊 Il s’agit de la jointure la plus simple. C’est une jointure dite interne. Ce type de jointure va renvoyer les entrées des deux tables si jamais une correspondance a été trouvée entre les deux tables.

    Ici l’identifiant `id` de la table `auteur` correspond à l’identifiant : `id` stocké dans la table `livre`.

    `FROM livre JOIN auteur` permet de créer une jointure entre les tables `livre` et `auteur` c’est-à-dire réunir les tables `livre` et `auteur` en une seule table.

    `ON livre.id_auteur = auteur.id` signifie qu'une ligne de la table `livre` devra être fusionnée avec la ligne de la table `auteur` à condition que l'attribut `id_auteur` de la ligne de la table `livre` soit égal à l'attribut `id` de la ligne de la table `auteur`.

!!! info "Noms d'attributs identiques"

    🌵 Si un même nom d'attribut est présent dans les 2 tables (par exemple ici l'attribut `id`), il est nécessaire d'ajouter le nom de la table devant afin de pouvoir les distinguer (`auteur.id` et `livre.id`)

!!! Example "`AS`"

    L’utilisation des « alias », mot clé AS, permet d’obtenir des écritures plus compactes. C’est très souvent utilisé. 

    Reprenons l’exemple précédant mais en écrivant les lignes suivantes :   
    La table `livre` est désignée par la lettre `l` et la table `auteur` par la lettre `a`.

    Exécuter :

    {!{ sqlide titre="" sql="bdd/sql/as.sql" espace="livre_auteur"}!}

???+ question "À vous de jouer 2"

    Exécutez

    {!{ sqlide titre="" sql="bdd/sql/a_vous_join_2.sql" espace="livre_auteur"}!}

    Quelle différence constatez-vous avec la requête précédente ?

    ??? success "Solution"

        On a d'abord les colonnes de la table `auteur`, puis ensuite celles de la table `livre`.
  
???+ question "À vous de jouer 3"

    Écrire la jointure entre la table `livre` et la table `auteur` en ne sélectionnant que les attributs titre de la tables `livre` et `nom` et `prenom` de la table `auteur`.

    {!{ sqlide titre="" sql="bdd/sql/a_vous_join_3.sql" espace="livre_auteur"}!}

    ??? success "Solution"

        ```sql title=""
        SELECT  titre, nom , prenom
        FROM livre
        JOIN auteur ON livre.id_auteur = auteur.id;
        ```
!!! info "Syntaxes"

    👉 Dans le cas d'une jointure, Il est possible d'utiliser la clause `WHERE` et `ORDER BY` après le `JOIN ... ON ...`

???+ question "À vous de jouer 4"

    Écrire la jointure entre la table `livre` et la table `auteur` en ne sélectionnant que les attributs `titre`, `nom`, `prenom` `ann_publi` où seules les années de publication supérieures à 1950 sont considérées.

    Le résultat sera donné par ordre alphabétique des nom d'auteur.

    {!{ sqlide titre="" sql="bdd/sql/a_vous_join_4.sql" espace="livre_auteur"}!}

    ??? success "Solution"

        ```sql title=""
        SELECT  titre, nom , prenom , ann_publi
        FROM livre
        JOIN auteur ON livre.id_auteur = auteur.id
        WHERE ann_publi >= 1950
        ORDER BY nom ASC;
        ```

## II. Les clés étrangères 

!!! info "Clé étrangère"

    Les jointures permettent d’associer plusieurs tables dans une même requête. Pour cela nous avons besoin comme nous l'avons vu dans le schéma relationnel d'une **clé primaire** et d'une **clé étrangère**.

    Dans notre exemple l'attribut `id_auteur` de la tables `livre` est bien une clé étrangère puisque cet attribut correspond à l'attribut `id` de la table `auteur`.

    Il est possible lors de la création d'une table de définir une clé étrangère.

???+ question "À vous de jouer 5"

    **1.** Ajoutez à la table `livre` un enregistrement pour lequel `id_auteur` n’est pas possible compte-tenu de la table existante (par exemple 12). 

    {!{ sqlide titre="" sql="bdd/sql/a_vous_join_5.sql" espace="livre_auteur"}!}

    ??? success "Solution"

        ```sql title=""
        INSERT INTO livre
        (id, titre, id_auteur, ann_publi, note)
        VALUES
        (17, "L'Odyssée", 12, 1571, 9);
        ```

    **2.** Que se passe-t-il ?

    ??? success "Solution"

        😢 Il ne se passe rien ... L'anomalie n'a pas été détectée.

!!! example "Exemple"

    👉 Nous allons créer une nouvelle table livre_2, de la façon suivante :

    {!{ sqlide titre="" sql="bdd/sql/fk.sql" espace="livre_auteur"}!}

    Recommencer :  essayez d’ajouter à la table livre_2 un enregistrement pour lequel id_auteur n’est pas possible compte-tenu de la table existante (par exemple 12)

    {!{ sqlide titre="" init="bdd/sql/option.sql" sql="bdd/sql/ajout_avec_fk.sql" espace="livre_auteur"}!}

    !!! info "Clé étrangère"

        Grâce à cette clé étrangère, SQLite sera capable de détecter les anomalies au niveau de la clé étrangère.

## Crédits

Jean-Louis THIROT, Valérie MOUSSEAUX et Mireille COILHAC 
    
D'après David Roche, Pixees et Stéphan Van Zuijlen Lycée Jean Moulin 

