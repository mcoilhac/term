-- les tirets permettent d'insérer des commentaires
DROP TABLE IF EXISTS eleves; -- notez bien ce ; en fin de chaque bloc d'instructions
CREATE TABLE eleves (
    id INTEGER PRIMARY KEY ,  
    nom TEXT NOT NULL ,
    prenom TEXT NOT NULL ,
    date_naiss DATE FORMAT 'dd.mm.yyyy' NOT NULL , 
    classe TEXT NOT NULL ,
    nb_heures INTEGER

); -- notez bien de nouveau ce ;
