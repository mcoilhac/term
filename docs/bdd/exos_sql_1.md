---
author: Mireille Coilhac
title: Exercices de SQL - 1
---

## TP Nos héros de bandes dessinées

???+ question "Nos héros de bandes dessinées"

    Après avoir téléchargé le fichier, vous pourrez le lire à partir de [Basthon en SQL](https://notebook.basthon.fr/?kernel=sql){ .md-button target="_blank" rel="noopener" }

    🌐 TD à télécharger : Fichier `heros_bd_sujet.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/heros_bd_sujet.ipynb)

	😀 La correction est arrivée ...

	Fichier `heros_bd_corr.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/heros_bd_corr.ipynb)

<!--- 
⏳ La correction viendra bientôt ... 
😀 La correction est arrivée ...
🌐 Fichier `heros_bd_corr.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/heros_bd_corr.ipynb)
-->


