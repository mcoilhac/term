---
author: Mireille Coilhac
title: Les bases de données - généralités
---

## I. Présentation des bases de données

En première nous avons eu l'occasion de travailler sur des données structurées en les stockant dans des fichiers au format CSV. Même si cette méthode de stockage de l'information peut s'avérer pratique dans certains cas précis, il est souvent souhaitable d'utiliser une base de données pour stocker des données.

👉 Une petite video pour commencer sur l'histoire des bases de données :

<div style="position:relative;padding-bottom:56.25%;height:0;overflow:hidden;"> <iframe style="width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden" frameborder="0" type="text/html" src="https://www.dailymotion.com/embed/video/x71hy5c?autoplay=1" width="100%" height="100%" allowfullscreen title="Dailymotion Video Player" allow="autoplay"> </iframe> </div>

 <!-- lien privé 
<iframe width="560" height="315" src="https://www.youtube.com/embed/iu8z5QtDQhY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
-->

👉 Une autre vidéo importante pour introduire la notion de base de données :

<iframe width="560" height="315" src="https://www.youtube.com/embed/pqoIBiM2AvE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

👉 La vidéo de Lumni :

[BDD sur Lumni](https://www.lumni.fr/video/qu-est-ce-qu-une-base-de-donnees-relationnelle){ .md-button target="_blank" rel="noopener" }

![](images/chrono_bdd.jpg){ width=40%; align=right }

Le terme base de données est apparu au début des années 60. L'apparition des disques durs à la fin des années 50 a permis d'utiliser de stocker et manipuler des données. Avec l'apparition du Web, la quantité de données à stocker a explosé. Aujourd'hui, la plupart des sites internet (du petit site personnel au grand site d'e-commerce) utilisent au moins une base de données.

Les bases de données jouent un rôle fondamental dans notre monde devenu numérique où il est extrêmement facile de dupliquer l'information. Voilà pourquoi nous allons cette année les étudier. Il existe différents types de bases de données. Les bases de données relationnelles sont le plus utilisées au monde, c'est ce type de base de données que nous allons étudier.


!!! info "Bases de données relationnelles"

    Les bases de données relationnelles ont été mises au point en 1970 par Edgar Franck Codd, informaticien britannique (1923-2003). Ces bases de données sont basées sur la théorie mathématique des ensembles.

!!! info "Définition"

	Une base de données est un ensemble structuré d'informations.

	* Dans le langage courant, elle peut désigner n'importe quelle source importante d'informations (dictionnaires, encyclopédies, etc.)
	* En informatique, il s'agit d'informations stockées sous forme de fichiers et organisées de façon à être facilement manipulées.

## II. Vocabulaire

La notion de relation est au cœur des bases de données relationnelles.

!!! info "Un peu de vocabulaire"

	Une **relation**, aussi appelée **table**, peut être vue comme un tableau à 2 dimensions, composé :

	*	d'une **en-tête** (intitulés des attributs)
	* d'un corps qui est composé :

		* d'**enregistrement** ou **p-uplets** (lignes)
		* d' **attributs**(colonnes)

!!! info "En pratique"

	On parlera indifféremment de table ou relation, ainsi que d'enregistrement ou p-uplet


![](images/panneau_leger.png){ width=40%; align=right }
**L'enregistrement (p-uplet) encadré en rose** sur le schéma ci-contre contient les éléments suivant :  
⇢ 10, Les Robots, Azimov, 1950 et 10.

**L'attribut "titre" en bleu** est composé des éléments suivants :  
⇢1984, Dune, Fondation, Le meilleur des mondes, Fahrenheit 451, Ubik, Chroniques martiennes, La nuit des temps, Blade Runner, Les Robots, La Planète des singes, Ravage, Le Maître du Haut Château, Le monde des Ā, La Fin de l’éternité et De la Terre à la Lune.
	
!!! info "Le domaine"

	Pour chaque attribut d'une relation, il faut définir un domaine

	Le domaine d'un attribut est le type des valeurs possibles (entiers, flottants, chaînes de caractères, dates...).

	* INTEGER ou INT, INT permettent de coder des entiers sur 4 octets (-2.147.483.648 à 2.147.483.647) ou 2 octets (-32.768 à 32.767).
	* NUMERIC(X) désigne un entier de X chiffres au maximum.
	* DECIMAL(X,Y) ou NUMERIC(X,Y) où X et Y sont optionnels et désignent respectivement le nombre de chiffres maximum pouvant composer le nombre, et le nombre de chiffres après la virgule.
	* FLOAT(X), REAL avec X définissant la précision (nombre de bits de codage de la mantisse)..
	* TEXT, CHAR(X)
	* DATE (AAAA-MM-JJ)
	* DATETIME (AAAA-MM-JJ HH:MM:SS)

👉 Voici un exemple de table : La relation "livre"

![](images/table_1_blanc.png){  width=50% }

Ici : 

* le domaine de l'attribut "id" correspond à l'ensemble des entiers (noté INT) : la colonne "id" devra obligatoirement contenir des entiers.
* le domaine de l'attribut "titre" correspond à l'ensemble des chaînes de caractères (noté TEXT).
* le domaine de l'attribut "note" correspond à l'ensemble des entiers positifs.

???+ question "Exercice"

    **1.**Faire la liste des éléments appartenant à l'attribut "auteur". 

    ??? success "Solution"

        Orwell, Asimov, Herbert, Huxley, Bredbury, K.Dick, Barjavel, Boulle, Van Vogt, Verne.

    **2.** Quel est, selon vous, le domaine de l'attribut "auteur" ?

    ??? success "Solution"

        TEXT

!!! info "Renseigner le domaine"

	Au moment de la création d'une relation, il est nécessaire de renseigner le domaine de chaque attribut.  
	Le Systeme de Gestion de la Base de Données s'assure qu'un élément ajouté à une relation respecte bien le domaine de l'attribut correspondant : si par exemple vous essayez d'ajouter une note non entière (par exemple 8.5), le SGBD signalera cette erreur et n'autorisera pas l'écriture de cette nouvelle donnée.

## III. Unicité d'un enregistrement & clé primaire - primary key

Autre contrainte très importante dans les bases de données relationnelles, une relation ne peut pas contenir 2 p-uplets identiques. Il faut donc pouvoir **identifier de façon unique un p-uplet**.  
Par exemple, la situation ci-dessous n'est pas autorisée (ici aussi le SGBD veillera au grain) :

![nom image](images/bdd_faux_blanc.png){ width=30% }

Afin d'être sûr de respecter cette contrainte des p-uplets identiques, on définit la notion de clé primaire de la relation.

!!! info "Clé primaire"

	Une clef primaire est un attribut ou un ensemble d'attributs (couple, triplet, ...) dont la valeur permet d'identifier de manière unique un p-uplet de la relation.

!!! info "Unicité"

	Autrement dit, si un attribut est considéré comme clé primaire, on ne doit pas trouver dans toute la relation 2 fois la même valeur pour cet attribut.

!!! warning "Attention"

    Le critère d'unicité n'est pas suffisant pour définir une clef primaire. Il faut en réalité respecter 3 critères :

    * Unicité : 2 p-uplets ne peuvent pas avoir la même valeur de cet attribut
	* Existence : Cet attribut est obligatoire, il a une valeur non nulle pour tous les p-uplets.
	* Stabilité : La valeur de cet attribut n'est jamais modifiée. Ce critère peut sembler moins évident mais nous verrons plus loin que la clef primaire peut être utilisée comme clef étrangère dans une autre table. Dès lors, une modification de sa valeur obligerait à maintenir toutes les relations utilisant cette clef, ce qui poserait de graves problèmes.

???+ question "Exercice : clé primaire"

	![](images/panneau_leger_blanc.png){ width=60% }

    **1.**L'attribut "note" peut-il jouer le rôle de clef primaire ? 

    ??? success "Solution"

        Non, car il est possible de trouver 2 fois la même note

    **2.** L'attribut "ann_publi" peut-il jouer le rôle de clef primaire ?

    ??? success "Solution"

        Non, car il est possible de trouver 2 fois la même année.

    **3.** L'attribut "auteur" peut-il jouer le rôle de clé primaire ?

    ??? success "Solution"

        Non, car il est possible de trouver 2 fois le même auteur.

    **4.**  L'attribut "titre" peut-il jouer le rôle de clé primaire ?

    ??? success "Solution"

        A priori oui, car l'attribut "titre" ne comporte pas 2 fois le même titre de roman. Mais, ce n'est pas forcément une bonne idée, car il est tout à fait possible d'avoir un même titre pour 2 romans différents. Par exemple, en 2013, l’Américaine Jill McCorkle et l’Anglaise Kate Atkison publiaient avec seulement six jours d’écart un livre intitulé "Life After Life" !

    **2.** L'attribut "id" peut-il jouer le rôle de clef primaire ?

    ??? success "Solution"

        Il nous reste donc l'attribut "id". En fait, l'attribut "id" ("id" comme "identifiant") a été placé là pour jouer le rôle de clé primaire. A chaque fois qu'un roman est ajouté à la relation, son "id" correspond à l'incrémentation de l'id (id du nouveau=id de l'ancien+1) du roman précédemment ajouté.

        👉 Il est donc impossible d'avoir deux romans avec le même id.

!!! info "Un attribut "id""

	Ajouter un attribut "id" afin qu'il puisse jouer le rôle de clé primaire est une pratique courante (mais non obligatoire) dans les bases de données relationnelles.

Dans le cas précis qui nous intéresse, il aurait été possible de ne pas utiliser d'attribut "id", car chaque livre édité possède un numéro qui lui est propre : l'ISBN, cet ISBN aurait donc pu jouer le rôle de clef primaire.

!!! warning "Un couple comme id"

    À noter qu'en toute rigueur, une clé primaire peut être constituée de plusieurs attributs, par exemple le couple ("auteur","titre") pourrait jouer le rôle de clé primaire (à moins qu'un auteur écrive 2 romans différents, mais portant tous les deux le même titre).


???+ question "Exercice films"

	Voici un extrait d'une relation référençant des films :

	![](images/exo_pk_blanc.png){ width=50% }

    * Listez les différents attributs de cette relation.
	* Donnez le domaine de chaque attribut.
	* Pour chaque attribut dire si cet attribut peut jouer le rôle de clef primaire

    ??? success "Solution"

    	* id : INT - Clef primaire : oui
		* titre : TEXT - Clef primaire : non (unicité)
		* réalisateur : TEXT - Clef primaire : non (unicité)
		* ann_sortie : DATE - Clef primaire : non (unicité)
		* note : INT - Clef primaire : non (unicité)

		Tous les attributs sont stables et existant (sauf la note qui pourrait ne pas exister). Seule l'id respecte le critère d'unicité dans tous les p-uplet.

???+ question "Exercice réseau social"

	Voici le formulaire d'inscription à un nouveau réseau social, destiné aux enseignants :

	![](images/prof_blanc.png){ width=40% }

	Les données sont enregistrées dans une base de données.

	Pour chaque attribut, précisez s'il satisfait les 3 critères :

	* Unicité
	* Existence
	* Stabilité

    ??? success "Solution"

		|Attribut|Critères|
		|:---|:---|
		|Nom |Non unique (homonymes), stable et existant (obligatoires)|
		|Prénom |Non unique (homonymes), stable et existant (obligatoires)|
		|email|Unique et existant, non stable|
		|date de naissance|Non unique, stable et existant|
		|Numen|Unique, stable mais non existant (non obligatoire)|

		Aucun attribut ne peut être clef primaire.

		Solutions :

		* Rendre le numen obligatoire à l'inscription
		* Ajouter un attibut id à cette table.

## IV. Clé étrangère - foreign key (fk)

Revenons à notre relation "livre". Nous désirons maintenant un peu enrichir cette relation en ajoutant des informations supplémentaires sur les auteur, nous obtenons alors :

![](images/livres_ajouts_blanc_def.png){ width=60% }

Nous avons ajouté 3 attributs ("prenom_auteur", "date_nai_auteur" et "langue_ecriture_auteur"). Nous avons aussi renommé l'attribut "auteur" en "nom_auteur".

😢 Comme vous l'avez peut-être remarqué, il y a pas mal d'informations dupliquées.  
Par exemple, on retrouve 3 fois "K.Dick Philip 1928 anglais", même chose pour "Asimov Isaac 1920 anglais"...Cette duplication est-elle indispensable ? Non ! Est-elle souhaitable ? Non plus ! En effet, dans une base de données, on évite autant que possible de dupliquer l'information (sauf à des fins de sauvegarde, mais ici c'est toute autre chose). Si nous dupliquons autant de données inutilement c'est que notre structure ne doit pas être la bonne !
Dans une base de donnée, on évite autant que possible la **redondance d'informations**.

🤔 Mais alors, comment faire pour avoir aussi des informations sur les auteur des livre ?  


💡 La solution est relativement simple : travailler avec 2 relations (2 tables) au lieu d'une seule et créer un "lien" entre ces 2 relations.

<!--- OLD
![](images/deux_tables.png){ width=80% }
-->

Relation auteur

|id|nom|prenom|ann_naissance|langue_ecriture|
|:---|:---|:---|:---|:---|
|1 |Orwell|George | 1903|anglais|
|2|Herbert|Franck|1920 |anglais|
|3|Asimov|Isaac |1920 |anglais|
|4|Huxley|Aldous |1894 |anglais|
|5|Bradbury |Ray |1920 |anglais|
|6|K.Dick |Philip |1928 |anglais|
|7|Barjavel |René |1911 |français|
|8|Boulle |Pierre |1912 |français|
|9|Van Gogt|Alfred Elton |1912 |anglais|
|10|Vernes |Jules |1828 |français|

Relation livre

|id|titre|id_auteur|ann_publi|note|
|:---|:---|:---|:---|:---|
|1|1984|1|1949|10|
|2|Dune|2|1965|8|
|3|Fondation|3|1951|9|
|4|Le meilleur des mondes|4|1931|7|
|5|Fahrenheit 451|5|1953|7|
|6|ubik|6|1969|9|
|7|Chroniques martiennes|5|1950|8|
|8|La nuit des temps|7|1968|7|
|9|Blade Runner|6|1968|8|
|10|Les Robots|3|1950|9|
|11|La planète des singes|8|1963|8|
|12|Ravage|7|1943|8|
|13|Le Maître du Haut Château|6|1962|8|
|14|Le monde des A|9|1945|7|
|15|La fin de l'éternité|3|1955|8|
|16|De la terre à la Lune|10|1865|10|




Nous avons créé une relation auteur et nous avons modifié la relation livre :

* Dans la relation auteur, chaque auteur est identifié par l'attribut "id"(clé primaire de la relation)
* Dans la relation livre, on a rajouté un attribut "id_auteur" qui est la clé primaire de la relation auteur.
* L'attribut "id_auteur" est ce que l'on nomme une clé étrangère de la relation livre, elle permet de faire le lien entre les deux relations.  
👉 C'est une clé étrangère qui fait référence à l'attribut "id" (clé primaire) de la relation auteur.  

L'introduction d'une relation auteur et la mise en place de liens entre cette relation et la relation livre permettent d'éviter la redondance d'informations.
Remarque : il peut y avoir plusieurs clés étrangères dans une relation.

!!! info "Définition : clé étrangère"

	Pour établir un lien entre 2 relations $R_A$ et $R_B$, on ajoute à $R_A$ un attribut x qui prendra les valeurs de la clé primaire de $R_B$.

	Cet attribut x est appelé **clé étrangère** (l'attribut correspond à la clé primaire d'une autre table, d'où le nom).

Dans l'exemple ci-dessus, l'attribut "id_auteur" de la relation livre permet bien d'établir un lien entre la relation livre et la relation auteur, "id_auteur" correspond bien à la clef primaire "id" de la relation auteur, conclusion : "id_auteur" est une clé étrangère.

!!! info "Définition : Intégrité"

	Pour préserver l'intégrité d'une base de données, il est important de bien vérifier que toutes les valeurs de la clé étrangère correspondent bien à des valeurs présentes dans la clef primaire

Ici, nous aurions un problème d' intégrité de la base de données si une valeur de l'attribut "id_auteur" de la relation livre ne correspondait à aucune valeur de la clef primaire de la relation auteur.
Certains SGBD ne vérifient pas cette contrainte (ne renvoie aucune erreur en cas de problème), ce qui peut provoquer des comportements erratiques.

???+ question "Exercice films"

	<table width="95%">
	<tr><td colspan="5" style="text-align:center;">film</td></tr>
	<tr style="background-color:#aaaaaa;text-align:center;">
	<td width="5">id</td><td width="240">titre</td><td width="10">ann_sortie</td><td width="5">note_sur_10</td>
	</tr>
	<tr><td>1</td><td style="text-align:center;">Alien, le huitième passager</td></td><td style="text-align:center;">1979</td><td style="text-align:center;">10</td></tr>
	<tr><td>2</td><td style="text-align:center;">Dune</td></td><td style="text-align:center;">1986</td ><td style="text-align:center;">5</td></tr>
	<tr><td>3</td><td style="text-align:center;">2001 : Odyssée de l'espace</td><td style="text-align:center;">1968</td><td style="text-align:center;">9</td></tr>
	<tr><td>4</td><td style="text-align:center;">Blade Runner</td><td style="text-align:center;">1982</td><td style="text-align:center;">10</td></tr>
	</table>


	<ul>
	 <li>En partant de la relation film ci-dessus, créez une relation réalisateur.
	<br>Vous prendrez comme attributs de la relation réalisateur : id, nom, prenom et ann_naissance.
	<br> &#128522; Vous chercherez toutes les informations nécessaires sur le Web.
	 </li>
	<br>
	<li>Modifiez ensuite la relation film afin d'établir un lien entre les relations film et
	réalisateur. Vous préciserez l'attribut qui jouera le rôle de clef étrangère.</li>
	</ul>


	??? success "Solution"

		<table width="45%">
		<tr><td colspan="4" style="text-align:center;">réalisateur</td></tr>
		<tr style="background-color:#aaaaaa;text-align:center;">
		<td width="5">id</td><td width="40">nom</td><td width="40">prénom</td><td width="10">ann_naissance</td>
		</tr>
		<tr><td>1</td><td style="text-align:center;">Scott</td><td style="text-align:center;">Ridley</td><td style="text-align:center;">1937</td></tr>
		<tr><td>2</td><td style="text-align:center;">Lynch</td><td style="text-align:center;">David</td><td style="text-align:center;">1946</td></tr>
		<tr><td>3</td><td style="text-align:center;">Kubrick</td><td style="text-align:center;">Stanley</td><td style="text-align:center;">1928</td></tr>

		</table>

		<table width="95%">
		<tr><td colspan="5" style="text-align:center;">film</td></tr>
		<tr style="background-color:#aaaaaa;text-align:center;">
		<td width="5">id</td><td width="240">titre</td><td width="40">id_realisateur</td><td width="10">ann_sortie</td><td width="5">note_sur_10</td>
		</tr>
		<tr><td>1</td><td style="text-align:center;">Alien, le huitième passager</td><td style="text-align:center;">1</td><td style="text-align:center;">1979</td><td style="text-align:center;">10</td></tr>
		<tr><td>2</td><td style="text-align:center;">Dune</td><td style="text-align:center;">2</td><td style="text-align:center;">1986</td ><td style="text-align:center;">5</td></tr>
		<tr><td>3</td><td style="text-align:center;">2001 : Odyssée de l'espace</td><td style="text-align:center;">3</td><td style="text-align:center;">1968</td><td style="text-align:center;">9</td></tr>
		<tr><td>4</td><td style="text-align:center;">Blade Runner</td><td style="text-align:center;">1</td><td style="text-align:center;">1982</td><td style="text-align:center;">10</td></tr>
		</table>
		<br><br>
		L'attribut id_realisateur de la table <b>film</b> est une clé étrangère. Elle fait référence à la clé primaire id de la table <b>réalisateur</b>


## V. Schéma relationnel

Lorsqu’une base de données comporte plusieurs tables, l’ensemble des schémas de ces relations s’appelle le **schéma relationnel** de la base de données.

!!! info "Définition : Schéma relationnel"

	On appelle schéma relationnel l'ensemble des relations présentes dans une base de données.  
	Quand on vous demande le schéma relationnel d'une base de données, il est nécessaire de fournir les informations suivantes :

	* Les noms des différentes relations
	* pour chaque relation, la liste des attributs et de leurs domaines
	* pour chaque relation, la clef primaire (on la souligne)
	* les clés étrangères (précédées d'un #)

Pour notre exemple et les relations livre et auteur :

👉 Schéma relationnel :  
auteur(<u>id</u> : INT, nom : TEXT, prenom : TEXT, ann_naissance : INT, langue_ecriture : TEXT)  
livre(<u>id</u> : INT, titre : TEXT, #id_auteur : INT, ann_publi : INT, note : INT)

Ce schéma relationnel peut aussi être représenté de la façon suivante :

![livre auteur](images/jointure_livre_auteur.jpg){ width=40% }

👉 La flèche part toujours de la clé étrangère pour aller vers la clé primaire à laquelle elle fait référence.

👉 La clé étrangère est précédée de #.

👉 La clé primaire est soulignée.

???+ question "Exercice films - suite"

	Donnez le schéma relationnel de la base de données que vous avez définie dans l'exercice films

	??? success "Solution"

		realisateur(<u>id</u> : INT , nom : TEXT, prénom : TEXT, ann_naissance : INT)

		film(<u>id</u> : INT , titre : TEXT , # id_realisateur : INT , ann_sortie : INT ; note_sur_10 : INT)


!!! info "Les contraintes d'intégrité"

	* **contrainte de domaine** : type de données (entier, texte, flottant, ...)
	* **contrainte de relation** : chaque enregistrement d'une relation doit pouvoir être identifié par une clé primaire
	* **Lorsque les relations sont liées** , les 3 règles suivantes doivent être respectées :

		* Une clé étrangère doit être une valeur qui est la clé primaire de la table à laquelle elle se réfère.
		* Un enregistrement de la table primaire ne peut être effacé s’il possède des enregistrements liés.
	    * La clé primaire peut être changée dans la table primaire uniquement si un enregistrement considéré ne possède pas d'enregistrements liés.

## VI. Exercices

## Exercice 1
*(d'après Prépabac NSI, Terminale, G.CONNAN, V.PETROV, G.ROZSAVOLGYI, L.SIGNAC, éditions HATIER.)*

Deux relations modélisent la flotte de voitures d'un réseau de location de voitures.

**Agences**

| id_agence | ville     | département |
|-----------|-----------|-------------|
| 1         | Paris     | 75          |
| 2         | Lyon      | 69          |
| 3         | Marseille | 13          |
| 4         | Aubagne   | 13          |


**Voitures**

| id_voiture | marque  | modèle | kilométrage | couleur | id_agence |
|------------|---------|--------|-------------|---------|-----------|
| 1          | Renault | Clio   | 12000       | Rouge   | 2         |
| 2          | Peugeot | 205    | 22000       | Noir    | 3         |
| 3          | Toyota  | Yaris  | 33000       | Noir    | 3         |


???+ question "Questions"
    === "Énoncé"
        1. Combien la relation ```Voitures``` comporte-t-elle d'attributs ?
        2. Quel est le domaine de l'attribut ```id_agence```  dans la relation ```Voitures``` ?
        3. Quel est le schéma relationnel de la relation ```Agences ``` ?
        4. Quelle est la clé primaire de la relation ```Agences ``` ?
        5. Quelle est la clé primaire de la relation ```Voitures ``` ?
        6. Quelle est la clé étrangère de la relation ```Voitures ``` ?

    === "Correction"
        1. 6
        2. Entier (```Int``` )
        3. (id_agence : Int, ville : String, département :Int)
        4. ```id_agence``` 
        5. ```id_voiture``` 
        6. ```id_agence``` 




## Exercice 2

Prenons la base Tour de France 2020 :


**Relation Équipes**

| codeEquipe | nomEquipe                      |
|------|-----------------------------|
| ALM  |  AG2R La Mondiale           |
| AST  |  Astana Pro Team            |
| TBM  |  Bahrain - McLaren          |
| BOH  |  BORA - hansgrohe           |
| CCC  |  CCC Team                   |
| COF  |  Cofidis, Solutions Crédits |
| DQT  |  Deceuninck - Quick Step    |
| EF1  |  EF Pro Cycling             |
| GFC  |  Groupama - FDJ             |
| LTS  |  Lotto Soudal               |
| ...  | ...                         |




**Relation Coureurs**

| dossard | nomCoureur  | prénomCoureur | codeEquipe |
|---------------|-------------|---------------|------------|
| 141           | LÓPEZ       | Miguel Ángel  | AST        |
| 142           | FRAILE      | Omar          | AST        |
| 143           | HOULE       | Hugo          | AST        |
| 11            | ROGLIČ      | Primož        | TJV        |
| 12            | BENNETT     | George        | TJV        |
| 41            | ALAPHILIPPE | Julian        | DQT        |
| 44            | CAVAGNA     | Rémi          | DQT        |
| 45            | DECLERCQ    | Tim           | DQT        |
| 121           | MARTIN      | Guillaume     | COF        |
| 122           | CONSONNI    | Simone        | COF        |
| 123           | EDET        | Nicolas       | COF        |
| …             | …           | …             | …          |





**Relation Étapes**

| numéroEtape | villeDépart | villeArrivée      | km  |
|-------------|-------------|-------------------|-----|
| 1           | Nice        | Nice              | 156 |
| 2           | Nice        | Nice              | 185 |
| 3           | Nice        | Sisteron          | 198 |
| 4           | Sisteron    | Orcières-Merlette | 160 |
| 5           | Gap         | Privas            | 198 |
| ...         | ...         | ...               | ... |






**Relation Temps**

| dossard | numéroEtape | tempsRéalisé |
|:-------------:|:-----------:|:------------:|
| 41            | 2           | 04:55:27     |
| 121           | 4           | 04:07:47     |
| 11            | 5           | 04:21:22     |
| 122           | 5           | 04:21:22     |
| ...           | ...         | ...          |



![](images/schema_tdf.png){ width=60% }

???+ question "Questions"
    === "Énoncé"
        1. Quel temps a réalisé Guillaume MARTIN sur l'étape Sisteron / Orcières-Merlette ?
        2. À l'arrivée à Privas, qui est arrivé en premier entre Primož ROGLIČ et Simone CONSONNI ?

    === "Correction"
        1. Temps de Guillaume Martin (dossard 121): 04:07:47
        2. Aucun des deux, ils sont arrivés dans le même temps (04:21:22)




### TD1

<div class="centre">
<iframe 
src="../a_telecharger/BDD_TD1_modele_rel.pdf"
width="1000" height="1000" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>

<!--- 
🌐 TD à télécharger : Fichier `BDD_TD1_modele_rel.pdf` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/BDD_TD1_modele_rel.pdf)

⏳ La correction viendra bientôt ... 
-->

😀 La correction est arrivée ...

<div class="centre">
<iframe 
src="../a_telecharger/BDD_TD1_mod_rel_cor.pdf"
width="1000" height="1000" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>


<!--- 
😀 La correction est arrivée ...

<div class="centre">
<iframe 
src="../a_telecharger/BDD_TD1_mod_rel_corr.pdf"
width="1000" height="1000" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>
-->



### TD2

<div class="centre">
<iframe 
src="../a_telecharger/BDD_TD2_mod_rel_sujet.pdf"
width="1000" height="1000" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>

<!--- 
⏳ La correction viendra bientôt ... 

<div class="centre">
<iframe 
src="../a_telecharger/cor_BDD_TD2_2023.pdf"
width="1000" height="1000" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>
-->

😀 La correction est arrivée ...

<div class="centre">
<iframe 
src="../a_telecharger/cor_BDD_TD2_2023_v2.pdf"
width="1000" height="1000" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>




## Crédits

D'après David Roche, Pixees et Stéphan Van Zuijlen Lycée Jean Moulin adapté par François Halle, Valérie Mousseaux, Mireille COILHAC et Jean-Louis Thirot publié sous licence CC BY SA
Exercices réalisés par Gilles Lassus.