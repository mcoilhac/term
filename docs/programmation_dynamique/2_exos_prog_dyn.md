---
author: Mireille Coilhac
title: Programmation dynamique - Exercices
---


[Suite de Fibonacci - 3](https://codex.forge.apps.education.fr/exercices/fib_3/){ .md-button target="_blank" rel="noopener" }

[Énumérations des permutations](https://codex.forge.apps.education.fr/exercices/nb_factoriel/){ .md-button target="_blank" rel="noopener" }

[Le meilleur filon](https://codex.forge.apps.education.fr/en_travaux/mineur/){ .md-button target="_blank" rel="noopener" }
