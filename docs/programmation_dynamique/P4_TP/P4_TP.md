---
author: Frédéric Junier
title:  Travaux pratiques
---


[TP du sac à dos](https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/TP_sac_dos.ipynb){ .md-button target="_blank" rel="noopener" }

[TP sous tableau de somme maximale](https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/TP_sous_tableau_somme_maxi.ipynb){ .md-button target="_blank" rel="noopener" }

