# --- PYODIDE:code --- #

def somme_max_iter_solution(t):
    """Renvoie un couple constitué de :
    - la somme maximale dans un triangle de nombres t qui est une liste de listes
    - une liste de nombres réalisant cette somme maximale"""    
    n = len(t)
    s = [[0 for _ in range(len(t[i]))] for i in range(n)]
    # initialisation de la dernière ligne
    for j in range(n):
        s[n - 1][j] = t[n - 1][j]
    # on remonte de la dernière ligne jusqu'au sommet du triangle
    for i in range(n - 2, -1, -1):
        for j in range(len(s[i])):
            s[i][j] = t[i][j] + max(s[i + 1][j], s[i + 1][j + 1])
    # ici s contient les solutions de tous les sous-problèmes
    # on stocke chaque terme d'une somme maximale dans la liste sol
    # on initialise sol avec le sommet du triangle
    sol = [t[0][0]]
    # on redescend du sommet du triangle jusqu'à la dernière ligne
    j = 0
    # à compléter
    ...
    return (s[0][0], sol)

# --- PYODIDE:corr --- #

def somme_max_iter_solution(t):
    """Renvoie un couple constitué de :
    - la somme maximale dans un triangle de nombres t qui est une liste de listes
    - une liste de nombres réalisant cette somme maximale"""    
    n = len(t)
    s = [[0 for _ in range(len(t[i]))] for i in range(n)]
    # initialisation de la dernière ligne
    for j in range(n):
        s[n - 1][j] = t[n - 1][j]
    # on remonte de la dernière ligne jusqu'au sommet du triangle
    for i in range(n - 2, -1, -1):
        for j in range(len(s[i])):
            s[i][j] = t[i][j] + max(s[i + 1][j], s[i + 1][j + 1])
    # ici s contient les solutions de tous les sous-problèmes
    # on stocke chaque terme d'une somme maximale dans la liste sol
    # on initialise sol avec le sommet du triangle
    sol = [t[0][0]]
    # on redescend du sommet du triangle jusqu'à la dernière ligne
    j = 0
    for i in range(n - 1):
        if s[i + 1][j + 1] > s[i + 1][j]:            
            sol.append(t[i + 1][j + 1])
            j = j + 1
        else:
            sol.append(t[i + 1][j])
    return (s[0][0], sol)


# --- PYODIDE:tests --- #

t = [[3], [40, 38], [34, 31, 33], [3, 4, 22, 25], [42, 24, 41, 38, 5]]
rep1 = (137, [3, 40, 31, 22, 41])
rep2 = (137, [3, 38, 33, 22, 41])
rep3 = (137, [3, 38, 33, 25, 38])
assert somme_max_iter_solution(t) in [rep1, rep2, rep3]


# --- PYODIDE:secrets --- #

t2 = [[1], [2, 3], [4, 5, 6]]
assert somme_max_iter_solution(t2) == (10, [1, 3, 6])