def somme_max_iter2(t):
    """Renvoie la somme maximale dans un triangle de nombres
    t qui est une liste de listes"""    
    n = len(t)
    s = [t[n - 1][j] for j in range(n)]
    # à compléter
    ...
    return s[0]

def test_somme_max_iter2():
    t = [[3], [40, 38], [34, 31, 33], [3, 4, 22, 25], [42, 24, 41, 38, 5]]
    assert somme_max_iter2(t) == 137
    print("tests réussis")
    
test_somme_max_iter2()