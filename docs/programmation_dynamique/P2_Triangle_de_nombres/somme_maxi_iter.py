def somme_max_iter(t):
    """Renvoie la somme maximale dans un triangle de nombres
    t qui est une liste de listes"""    
    n = len(t)
    s = [[0 for _ in range(len(t[i]))] for i in range(n)]
    # initialisation de la dernière ligne
    for j in range(n):
        s[n - 1][j] = ...
    # on remonte de la dernière ligne jusqu'au sommet du triangle
    ...
    # on renvoie la somme maximale du triangle complet s(0,0)
    return s[0][0]

def test_somme_max_iter():
    t = [[3], [40, 38], [34, 31, 33], [3, 4, 22, 25], [42, 24, 41, 38, 5]]
    assert somme_max_iter(t) == 137
    print("tests réussis")

test_somme_max_iter()