import time
import random

def generer_triangle2(n):
    return  [[random.randint(0, 50) for _ in range(m + 1)] for m in range(n)]

def somme_max_memo(t):
    """Renvoie la somme maximale dans un triangle de nombres
    t qui est une liste de listes"""
    
    def s(i, j):
        """Fonction auxiliaire récursive"""
        # si memo[i][j] == None alors s(i, j) n'a pas été déjà calculée
        if memo[i][j] == None:            
            if i == len(t) - 1:
                memo[i][j] =  ...
            else:
                memo[i][j] = ...
        return ...
    
    # tableau memo pour mémoiser de mêmes dimensions que t
    # initialisé avec des None

    memo = [[None for j in range(i + 1)] for i in range(len(t))]
    return s(0, 0)

def test_somme_max_memo():
    t = [[3], [40, 38], [34, 31, 33], [3, 4, 22, 25], [42, 24, 41, 38, 5]]
    assert somme_max_memo(t) == 137
    print("tests réussis")
    
def test_doubling_ratio():
    """Test de performance :
    On observe l'évolution du coefficient multiplicateur pour
    le temps d'exécution lorsque le nombre de lignes double    
    """
    n = 2 ** 5
    temps_preced = None
    for _ in range(5):
        t = generer_triangle2(n)
        debut = time.perf_counter()
        res = somme_max_memo(t)
        temps = time.perf_counter() - debut
        if temps_preced is not None:
            print(f"Nombre de lignes : {n}, Ratio temps / temps_preced : {temps /  temps_preced}")
        temps_preced = temps
        n = n * 2
    
test_somme_max_memo()
test_doubling_ratio()
