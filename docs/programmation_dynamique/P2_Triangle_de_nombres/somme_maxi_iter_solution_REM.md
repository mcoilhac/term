Nous avons vu qu'il y avait plusieurs listes possibles donnant le même résultat.

Ligne 21, nous aurions pu écrire par exemple : `if s[i + 1][j + 1] >= s[i + 1][j]: `.

Nous aurions ainsi trouvé une autre des trois solutions possibles.