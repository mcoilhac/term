# --- PYODIDE:code --- #

def rendu_monnaie_dyna_solution(montant, pieces):
    """Renvoie un couple avec :
    - le nombre minimal de pièces pour rendre la monnaie
    sur montant avec le système monétaire pieces qui contient une pièce de 1
    - une liste de pièces réalisant cet optimum
    """
    memo = [0 for _ in range(montant + 1)]
    choix = [0 for _ in range(montant + 1)]
    for m in range(1, montant + 1):
        memo[m] = montant # m pièces de 1
        for p in pieces:
            if p <= m:
                if memo[m - p] + 1 ...:
                    memo[m] = ...
                    choix[m] = ...
    solution = [choix[montant]]
    m = montant - choix[montant]
    while ...
        ...
        ...
    return (memo[montant], solution)


# --- PYODIDE:corr --- #

def rendu_monnaie_dyna_solution(montant, pieces):
    """Renvoie un couple avec :
    - le nombre minimal de pièces pour rendre la monnaie
    sur montant avec le système monétaire pieces qui contient une pièce de 1
    - une liste de pièces réalisant cet optimum
    """
    memo = [0 for _ in range(montant + 1)]
    choix = [0 for _ in range(montant + 1)]
    for m in range(1, montant + 1):
        memo[m] = montant # m pièces de 1
        for p in pieces:
            if p <= m:
                if memo[m - p] + 1 <= memo[m]:
                    memo[m] = 1 +  memo[m - p]
                    choix[m] = p
    solution = [choix[montant]]
    m = montant - choix[montant]
    while m != 0:
        solution.append(choix[m])
        m = m - choix[m]
    return (memo[montant], solution)


# --- PYODIDE:tests --- #

assert rendu_monnaie_dyna_solution(8, [1, 4, 6]) == (2, [4, 4])
systeme_euro = [1, 2, 5, 10, 20, 50, 100, 200, 500]
assert rendu_monnaie_dyna_solution(49, systeme_euro) == (5, [20, 20, 5, 2, 2])

# --- PYODIDE:secrets --- #

systeme_euro = [1, 2, 5, 10, 20, 50, 100, 200, 500]
assert rendu_monnaie_dyna_solution(76, systeme_euro) == (4, [50, 20, 5, 1])
