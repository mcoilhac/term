def rendu_monnaie1(montant, pieces):
    """Renvoie le nombre minimal de pièces pour rendre la monnaie
    sur montant avec le système monétaire pieces qui contient une pièce de 1"""
    if montant == 0:
        return 0
    rmin = montant # montant pièces de 1, pire des cas
    for p in pieces:
        if p <= montant:
            if ...:
                rmin = ...
    return rmin

def test_rendu_monnaie1():
    assert rendu_monnaie1(10, [1, 2]) == 5
    assert rendu_monnaie1(8, [1, 4, 6]) == 2
    print("tests réussis")
    
test_rendu_monnaie1()