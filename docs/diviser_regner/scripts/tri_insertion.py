# --- PYODIDE:code --- #

def tri_insertion(tab: list) -> list:
    """
    tab est de type liste
    La fonction renvoie la liste triée par ordre croissant.
    Elle utilise l'algorithme de tri par insertion. C'est un tri en place.
    """
    ...

# --- PYODIDE:corr --- #

def tri_insertion(tab: list) -> list:
    """
    tab est de type liste
    La fonction renvoie la liste triée par ordre croissant.
    Elle utilise l'algorithme de tri par insertion. C'est un tri en place.
    """
    for indice in range(1, len(tab)):
        element = tab[indice]
        i = indice
        while i > 0 and tab[i - 1] > element:
            tab[i] = tab[i - 1]
            i = i - 1
        tab[i] = element


# --- PYODIDE:tests --- #

tab_1 = [4, 3, 8, 2, 7, 1, 5]
tri_insertion(tab_1)
assert tab_1 == [1, 2, 3, 4, 5, 7, 8]

# --- PYODIDE:secrets --- #

tab_2 = [1, 2, 3, 4]
tri_insertion(tab_2) 
assert tab_2 == [1, 2, 3, 4]

tab_3 = [4, 2, 3, 1]
tri_insertion(tab_3) 
assert tab_3 == [1, 2, 3, 4]