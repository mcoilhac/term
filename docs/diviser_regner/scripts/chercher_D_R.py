# --- PYODIDE:code --- #

def recherche(lst, nombre, d, f):
    """
    Précondition : lst est une liste à priori non triée, nombre est un élément cherché dans la liste 
    d est le rang du début de la recherche, et f le rang de la fin de la recherche dans lst
    postcondition : la fonction renvoie du booléen : 
    True si nombre est dans la liste, et False sinon.
    """
    ...


# --- PYODIDE:corr --- #

def recherche(lst, nombre, d, f):
    """
    Précondition : lst est une liste à priori non triée, nombre est un élément cherché dans la liste 
    d est le rang du début de la recherche, et f le rang de la fin de la recherche dans lst
    postcondition : la fonction renvoie du booléen : 
    True si nombre est dans la liste, et False sinon.
    """
    if d == f:
        return lst[d] == nombre
    m = (d + f) // 2
    return recherche(lst, nombre, d, m) or recherche(lst, nombre, m + 1, f)


# --- PYODIDE:tests --- #

ma_liste = [95, 28, 36, 52, 85, 56, 34, 59, 17, 26, 
16, 25, 69, 98, 4, 85, 81, 48, 11, 57]
assert recherche(ma_liste , 28, 0, len(ma_liste) - 1) is True
assert recherche(ma_liste , 59, 0, len(ma_liste) - 1) is True
assert recherche(ma_liste , 57, 0, len(ma_liste) - 1) is True
assert recherche(ma_liste , 95, 0, len(ma_liste) - 1) is True
assert recherche(ma_liste , 11, 0, len(ma_liste) - 1) is True
assert recherche(ma_liste , 9, 0, len(ma_liste) - 1) is False



# --- PYODIDE:secrets --- #

assert recherche(ma_liste , 95, 1, len(ma_liste) - 1) is False
assert recherche(ma_liste , 57, 0, len(ma_liste) - 2) is False
assert recherche(ma_liste , 36, 2, len(ma_liste) - 3) is True