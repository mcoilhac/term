# --- PYODIDE:code --- #

def maximum_recursif_v2(tab, debut = None, fin = None):
    """
    Préconditions : tab est une liste, debut est l'indice du début de la recherche, 
    et fin l'indice de la fin de la recherche
    Postcondition : la fonction renvoie le maximum de la liste tab
    """
    if debut == None:
        debut = ...
    if fin == None:
        fin = ...
    if debut == fin :
        ...
    else:
        ...

# --- PYODIDE:corr --- #

def maximum_recursif_v2(tab, debut = None, fin = None):
    """
    Préconditions : tab est une liste, debut est l'indice du début de la recherche, 
    et fin l'indice de la fin de la recherche
    Postcondition : la fonction renvoie le maximum de la liste tab
    """
    if debut == None:
        debut = 0
    if fin == None:
        fin = len(tab)-1
    if debut == fin :
        return tab[debut]
    else:
        milieu = (debut + fin) // 2
        x = maximum_recursif_v2(tab, debut, milieu)
        y = maximum_recursif_v2(tab, milieu + 1, fin)
        if x <= y:
            return y
        else:
            return x


# --- PYODIDE:tests --- #

assert maximum_recursif_v2([95, 28, 36, 52, 85, 56, 34, 59, 17, 
26, 16, 25, 69, 98, 4, 85, 81, 48, 11, 57]) == 98


# --- PYODIDE:secrets --- #

assert maximum_recursif_v2([950, 28, 36, 52, 85, 56, 34, 59, 17, 
26, 16, 25, 69, 98, 4, 85, 81, 48, 11, 57]) == 950

assert maximum_recursif_v2([95, 28, 36, 52, 85, 56, 34, 59, 17, 
26, 16, 25, 69, 98, 4, 85, 81, 48, 11, 570]) == 570