# --- PYODIDE:env --- #

import matplotlib.pyplot as plt
fig2 = PyodidePlot('cible_2')  
fig2.target()


# --- PYODIDE:code --- #
# L'import suivant a été fait dans du code caché : 
# import matplotlib.pyplot as plt

import timeit
from random import shuffle, randint

def max_iter(tab):
    max = tab[0]
    for elt in tab:
        if elt > max :
            max = elt
    return max

def maximum_recursif_v2(tab, debut = None, fin = None):
    """
    Préconditions : tab est une liste, debut est l'indice du début de la recherche, 
    et fin l'indice de la fin de la recherche
    Postcondition : la fonction renvoie le maximum de la liste tab
    """
    if debut == None:
        debut = 0
    if fin == None:
        fin = len(tab)-1
    if debut == fin :
        return tab[debut]
    else:
        milieu = (debut + fin) // 2
        x = maximum_recursif_v2(tab, debut, milieu)
        y = maximum_recursif_v2(tab, milieu + 1, fin)
        if x <= y:
            return y
        else:
            return x

# différentes tailles de listes
abscisse = [100, 200, 300, 400, 500, 600, 800]

ordonnee_dr = [] # liste des temps par diviser pour régner
ordonnee_seq = [] # liste des temps par parcours séquentiel

# Création des listes non triées de tailles n, et de l'élément x à chercher
for n in abscisse:
    L = [randint(0, n) for i in range(n)] # liste de n entiers aléatoires compris entre 0 et n

    # Création des listes d'ordonnées correspondantes pour chaque graphique
    # Pour diviser pour régner :
    temps=timeit.timeit("maximum_recursif_v2(L, 0, None)", number=100, globals=globals())
    ordonnee_dr.append(temps)
    # Par parcours séquentiel
    temps=timeit.timeit("max_iter(L)", number=100, globals=globals())
    ordonnee_seq.append(temps)

# Pour tracer la fonction ci-dessous
# Graphique pour des recherches par diviser pour régner en rouge
# Graphique pour des recherches par parcours séquentiel en bleu

plt.plot(abscisse, ordonnee_dr, "ro-", abscisse, ordonnee_seq, "bo-")
plt.show()
