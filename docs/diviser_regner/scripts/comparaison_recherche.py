# --- PYODIDE:env --- #

import matplotlib.pyplot as plt
fig1 = PyodidePlot('cible_1')  
fig1.target()


# --- PYODIDE:code --- #
# L'import suivant a été fait dans du code caché : 
# import matplotlib.pyplot as plt

import timeit
from random import shuffle

def recherche_seq(element, tab):
    """
    tab est une liste d'éléments de types quelconques
    La fonction renvoie True si element est dans tab, et False sinon
    """
    reponse = False
    for elt in tab: # tab est donc parcouru en entier
        if elt == element:
            reponse = True
    return reponse

def recherche(lst, nombre, d, f):
    """
    Précondition : lst est une liste à priori non triée, nombre est un élément cherché dans la liste 
    d est le rang du début de la recherche, et f le rang de la fin de la recherche dans lst
    postcondition : la fonction renvoie du booléen : 
    True si nombre est dans la liste, et False sinon.
    """
    if d == f:
        return lst[d] == nombre
    m = (d + f) // 2
    return recherche(lst, nombre, d, m) or recherche(lst, nombre, m + 1, f)



# différentes tailles de listes
abscisse = [1000, 50000, 100000, 500000]

ordonnee_dr = [] # liste des temps par diviser pour régner
ordonnee_seq = [] # liste des temps par parcours séquentiel

# Création des listes non triées de tailles n, et de l'élément x à chercher
for n in abscisse:
    l = [i for i in range(n)] # création de la liste de taille n : [0... n-1]
    shuffle(l) # mélange de cette liste
    x = n + 10 # x n'appartient pas à la liste
    d = 0 
    f = n - 1

    # Création des listes d'ordonnées correspondantes pour chaque graphique
    # Pour diviser pour régner :
    temps=timeit.timeit("recherche(l, x, d, f)", number=1, globals=globals())
    ordonnee_dr.append(temps)
    # Pa parcours séquentiel
    temps=timeit.timeit("recherche_seq(x, l)", number=1, globals=globals())
    ordonnee_seq.append(temps)

# Pour tracer la fonction ci-dessous
# Graphique pour des recherches par diviser pour régner en rouge
# Graphique pour des recherches par parcours séquentiel en bleu
plt.plot(abscisse, ordonnee_dr, "ro-", abscisse, ordonnee_seq, "bo-") # en rouge
plt.show()


