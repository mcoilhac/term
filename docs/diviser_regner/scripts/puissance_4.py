# --- PYODIDE:code --- #

def expo_rec(x, n):
    """
    Précondition : x et n sont des entiers
    Postcondition : la fonction renvoie x**n
    """
    ...


# --- PYODIDE:corr --- #

def expo_rec(x, n):
    """
    Précondition : x et n sont des entiers
    Postcondition : la fonction renvoie x**n
    """
    if n == 0:
        return 1
    else :
        return x * expo_rec(x, n - 1)


# --- PYODIDE:tests --- #

assert expo_rec(2, 0) == 1
assert expo_rec(2, 3) == 8


# --- PYODIDE:secrets --- #

assert expo_rec(10, 4) == 10000
assert expo_rec(2, 10) == 1024