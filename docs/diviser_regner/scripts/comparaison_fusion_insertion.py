# --- PYODIDE:env --- #
import matplotlib.pyplot as plt
fig5 = PyodidePlot('cible_5')  
fig5.target()

def tri_insertion(tab:list)->list:
    """
    tab est de type liste
    La fonction renvoie la liste triée par ordre croissant.
    Elle utilise l'algorithme de tri par insertion. C'est un tri en place.
    """
    for indice in range(1, len(tab)):
        element = tab[indice]
        i = indice
        while i > 0 and tab[i - 1] > element:
            tab[i] = tab[i - 1]
            i = i - 1
        tab[i] = element

def fusion(l1, l2):
    """
    Précondition : l1 et l2 sont deux listes triées
    Postcondition : la fonction renvoie une liste triée constituée de la fusion 
    de l1 et l2
    Exemple :
    fusion([2, 3, 5, 8],[1, 4]) renvoie [1, 2, 3, 5, 8]
    """
    n1 = len(l1)
    n2 = len(l2)
    lst = [] # initialisation de la fusion de l1 et l2 
    i1 = 0 # indice qui sert à parcourir l1
    i2 = 0 # indice qui sert à parcourir l2
    while i1 < n1 and i2 < n2 :
        if l1[i1] < l2[i2]:
            lst.append(l1[i1])
            i1 = i1 + 1
        else :
            lst.append(l2[i2])
            i2 = i2 + 1
    if i1 == n1:
        return lst + l2[i2:]
    if i2 == n2:
        return lst + l1[i1:]

def tri_fusion(lst):
    """
    Précondition : lst est une liste
    Postcondition : la fonction renvoie une liste qui est la liste triée

    """
    n = len(lst)
    if n <= 1:
        return lst
    else :
        m = n // 2
        return fusion(tri_fusion(lst[:m]), tri_fusion(lst[m:])) 


# --- PYODIDE:code --- #
# L'import suivant a été fait dans du code caché : 
# import matplotlib.pyplot as plt

import timeit
from random import shuffle

# différentes tailles de listes
abscisse =  [10, 100, 400, 600]

ordonnee_insertion = [] # liste des temps par tri insertion
ordonnee_fusion = [] # liste des temps par tri fusion

# Création des listes non triées de tailles n
for n in abscisse:
    l = [i for i in range(n)] # création de la liste de taille n : [0... n-1]
    shuffle(l) # mélange de cette liste

    # Création des listes d'ordonnées correspondantes pour chaque graphique
    # Pour  le tri par insertion :
    # Attention le tri par insertion est un tri "en place" qui trie la liste l. 
    # Il faut donc pour chaque mesure prendre une copie de la liste l initiale, 
    # avec l[:]
    temps=timeit.timeit("tri_insertion(l[:])", number=100, globals=globals())
    ordonnee_insertion.append(temps)

    # Par tri fusion avec slices
    temps=timeit.timeit("tri_fusion(l)", number=100, globals=globals())
    ordonnee_fusion.append(temps)

# Graphique pour le tri insertion en rouge
# Graphique pour le tri fusion avec slices en bleu


plt.plot(abscisse, ordonnee_insertion, "ro-", abscisse, ordonnee_fusion, "bo-")
plt.show()
