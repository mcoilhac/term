# --- PYODIDE:code --- #

def fusion(tab1, tab2):
    print("on fusionne", tab1, "et", tab2)
    n1 = len(tab1)
    n2 = len(tab2)
    tab = [0] * (n1+n2)
    i1 = 0
    i2 = 0
    # On continue jusqu'à avoir fini un des tableaux
    while i1 < n1 and i2 < n2:
        if tab1[i1] < tab2[i2]:
            tab[i1+i2] = ...
            i1 += 1
        else:
            tab[i1+i2] = ...
            i2 += 1
    # On copie le reste de tab1 si nécessaire
    while i1 < n1:
        tab[...] = tab1[i1]
        i1 += 1
    # On copie le reste de tab2 si nécessaire
    while i2 < n2:
        tab[...] = tab2[i2]
        i2 += 1
    return tab

def tri_fusion(tab):
    print("on trie", tab)
    n = len(tab)
    if n > 1:
        # On copie les tableaux
        tab1 = [tab[i] for i in range(0, n//2)]
        tab2 = [tab[i] for i in range(n//2, n)]
        # On fusionne les deux tableaux triés
        return ...
    else:
        return ...


# --- PYODIDE:corr --- #

def fusion(tab1, tab2):
    print("on fusionne", tab1, "et", tab2)
    n1 = len(tab1)
    n2 = len(tab2)
    tab = [0] * (n1+n2)
    i1 = 0
    i2 = 0
    # On continue jusqu'à avoir fini un des tableaux
    while i1 < n1 and i2 < n2:
        if tab1[i1] < tab2[i2]:
            tab[i1+i2] = tab1[i1]
            i1 += 1
        else:
            tab[i1+i2] = tab2[i2]
            i2 += 1
    # On copie le reste de tab1 si nécessaire
    while i1 < n1:
        tab[i1+i2] = tab1[i1]
        i1 += 1
    # On copie le reste de tab2 si nécessaire
    while i2 < n2:
        tab[i1+i2] = tab2[i2]
        i2 += 1
    return tab

def tri_fusion(tab):
    print("on trie", tab)
    n = len(tab)
    if n > 1:
        # On copie les tableaux
        tab1 = [tab[i] for i in range(0, n//2)]
        tab2 = [tab[i] for i in range(n//2, n)]
        # On fusionne les deux tableaux triés
        return fusion(tri_fusion(tab1), tri_fusion(tab2))
    else:
        return tab


# --- PYODIDE:tests --- #

assert tri_fusion([9, 8, 7, 6, 5, 4, 3, 2, 1, 0]) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
assert tri_fusion([9, 5, 8, 2]) == [2, 5, 8, 9]
assert tri_fusion([1, 5, 3, 4, 2]) == [1, 2, 3, 4, 5]


# --- PYODIDE:secrets --- #

assert tri_fusion([2, 7, 3, 1, 5, 4, 8, 6]) == [1, 2, 3, 4, 5, 6, 7, 8]