# --- PYODIDE:env --- #
import matplotlib.pyplot as plt
fig3 = PyodidePlot('cible_3')  
fig3.target()


# --- PYODIDE:code --- #
# L'import suivant a été fait dans du code caché : 
# import matplotlib.pyplot as plt

import timeit

def expo_rec(x, n):
    """
    Précondition : x et n sont des entiers
    Postcondition : la fonction renvoie x**n
    """
    if n == 0:
        return 1
    else :
        return x * expo_rec(x, n - 1)
        
abscisse = [100, 200, 300, 400, 500, 600]
x = 2
ordonnee = []
for n in abscisse:
    temps=timeit.timeit("expo_rec(x, n)", number=100, globals=globals())
    ordonnee.append(temps)

plt.plot(abscisse, ordonnee, "ro-")
plt.show()