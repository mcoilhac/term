# --- PYODIDE:code --- #

def tri_selection(liste):
    """
    liste est de type liste
    La fonction renvoie la liste triee par ordre croissant.
    Elle utilise l'algorithme de tri par selection. C'est un tri "en place"
    """
    ...

# --- PYODIDE:corr --- #

def tri_selection(liste):
    """
    liste est de type liste
    La fonction renvoie la liste triee par ordre croissant.
    Elle utilise l'algorithme de tri par selection. C'est un tri "en place"
    """
    for i in range(len(liste) - 1):
        indice_min = i
        for j in range(i + 1, len(liste)): # recherche de l'indice du minimum
                                        # dans le reste de la liste(à droite)
            if liste[j] < liste[indice_min]:
                indice_min = j
        if indice_min != i: # echange s'il y a une plus petite valeur dans
                          # la partie droite de la liste
            liste[i], liste[indice_min] = liste[indice_min], liste[i]


# --- PYODIDE:tests --- #

tab_1 = [4, 3, 8, 2, 7, 1, 5]
tri_selection(tab_1)
assert tab_1 == [1, 2, 3, 4, 5, 7, 8]

# --- PYODIDE:secrets --- #

tab_2 = [1, 2, 3, 4]
tri_selection(tab_2) 
assert tab_2 == [1, 2, 3, 4]

tab_3 = [4, 2, 3, 1]
tri_selection(tab_3) 
assert tab_3 == [1, 2, 3, 4]


