# --- PYODIDE:code --- #

def maximum(tab):
    return maximum_recursif(tab, 0, len(tab) - 1) 

def maximum_recursif(tab, debut, fin):
    """
    Préconditions : tab est une liste, debut est l'indice du début de la recherche, 
    et fin l'indice de la fin de la recherche
    Postcondition : la fonction renvoie le maximum de la liste tab
    """
    if debut == fin : # Cas de base un seul élément
        ...
    else:
        milieu = (debut + fin) // 2
        x = maximum_recursif(tab, debut, milieu)
        y = ...
        if x <= y:
            return ...
        else:
            return ...


# --- PYODIDE:corr --- #

def maximum_recursif(tab, debut, fin):
    """
    Préconditions : tab est une liste, debut est l'indice du début de la recherche, 
    et fin l'indice de la fin de la recherche
    Postcondition : la fonction renvoie le maximum de la liste tab
    """
    if debut == fin : # Cas de base un seul élément
        return tab[debut]
    else:
        milieu = (debut + fin) // 2
        x = maximum_recursif(tab, debut, milieu)
        y = maximum_recursif(tab, milieu + 1, fin)
        if x <= y:
            return y
        else:
            return x


# --- PYODIDE:tests --- #

assert maximum([95, 28, 36, 52, 85, 56, 34, 59, 17, 
26, 16, 25, 69, 98, 4, 85, 81, 48, 11, 57]) == 98


# --- PYODIDE:secrets --- #

assert maximum([950, 28, 36, 52, 85, 56, 34, 59, 17, 
26, 16, 25, 69, 98, 4, 85, 81, 48, 11, 57]) == 950

assert maximum([95, 28, 36, 52, 85, 56, 34, 59, 17, 
26, 16, 25, 69, 98, 4, 85, 81, 48, 11, 570]) == 570