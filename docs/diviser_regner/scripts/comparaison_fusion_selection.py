# --- PYODIDE:env --- #
import matplotlib.pyplot as plt
fig4 = PyodidePlot('cible_4')  
fig4.target()

def tri_selection(liste):
    """
    liste est de type liste
    La fonction renvoie la liste triee par ordre croissant.
    Elle utilise l'algorithme de tri par selection. C'est un tri "en place"
    """
    for i in range(len(liste) - 1):
        indice_min = i
        for j in range(i + 1, len(liste)): # recherche de l'indice du minimum
                                        # dans le reste de la liste(à droite)
            if liste[j] < liste[indice_min]:
                indice_min = j
        if indice_min != i: # echange s'il y a une plus petite valeur dans
                          # la partie droite de la liste
            liste[i], liste[indice_min] = liste[indice_min], liste[i]

def fusion(l1, l2):
    """
    Précondition : l1 et l2 sont deux listes triées
    Postcondition : la fonction renvoie une liste triée constituée de la fusion 
    de l1 et l2
    Exemple :
    fusion([2, 3, 5, 8],[1, 4]) renvoie [1, 2, 3, 5, 8]
    """
    n1 = len(l1)
    n2 = len(l2)
    lst = [] # initialisation de la fusion de l1 et l2 
    i1 = 0 # indice qui sert à parcourir l1
    i2 = 0 # indice qui sert à parcourir l2
    while i1 < n1 and i2 < n2 :
        if l1[i1] < l2[i2]:
            lst.append(l1[i1])
            i1 = i1 + 1
        else :
            lst.append(l2[i2])
            i2 = i2 + 1
    if i1 == n1:
        return lst + l2[i2:]
    if i2 == n2:
        return lst + l1[i1:]

def tri_fusion(lst):
    """
    Précondition : lst est une liste
    Postcondition : la fonction renvoie une liste qui est la liste triée

    """
    n = len(lst)
    if n <= 1:
        return lst
    else :
        m = n // 2
        return fusion(tri_fusion(lst[:m]), tri_fusion(lst[m:])) 


# --- PYODIDE:code --- #
# L'import suivant a été fait dans du code caché : 
# import matplotlib.pyplot as plt

import timeit
from random import shuffle

# différentes tailles de listes
abscisse =  [10, 100, 400, 600]

ordonnee_selection = [] # liste des temps par tri sélection
ordonnee_fusion = [] # liste des temps par tri fusion avec slices

# Création des listes non triées de tailles n
for n in abscisse:
    l = [i for i in range(n)] # création de la liste de taille n : [0... n-1]
    shuffle(l) # mélange de cette liste

    # Création des listes d'ordonnées correspondantes pour chaque graphique
    # Pour  le tri par sélection :
    # Attention le tri sélection est un tri "en place" qui trie la liste l. 
    # Il faut donc pour chaque mesure prendre une copie de la liste l initiale, avec l[:]
    temps=timeit.timeit("tri_selection(l[:])", number=100, globals=globals())
    ordonnee_selection.append(temps)

    # Par tri fusion avec slices
    temps=timeit.timeit("tri_fusion(l)", number=100, globals=globals())
    ordonnee_fusion.append(temps)


# Graphique pour le tri sélection en rouge
# Graphique pour le tri fusion avec slices en bleu


plt.plot(abscisse, ordonnee_selection, "ro-", abscisse, ordonnee_fusion, "bo-")
plt.show()
