---
author: Mireille Coilhac
title: Exercices Diviser pour Régner
---

## Le tri fusion : autre version

???+ question "Comprendre les appels du tris fusion"

    **1.** Compléter le script ci-dessous :

    {{IDE('scripts/tri_fusion_v2', MAX_SIZE=45)}}

    **2.** Bien comprendre les affichages en console.

## Labyrinthe en POO

???+ question "D'après bac 2022, Métropole, J2, Ex. 5"

    Un labyrinthe est composé de cellules possédant chacune quatre murs (voir ci-dessous). La cellule en haut à gauche du labyrinthe est de coordonnées (0, 0).

    ![Le labyrinthe](./images/grille_murs.png){width=55%}

    On définit la classe `Cellule` ci-dessous. Le constructeur possède un attribut `murs` de type `#!py dict` dont les clés sont `'N'`, `'E'`, `'S'` et `'O'` et dont les valeurs sont des booléens (`#!py True` si le mur est présent et `#!py False` sinon).

    ```python title=""
    class Cellule:
        def __init__(self, murNord, murEst, murSud, murOuest):
            self.murs = {
                "N": murNord,
                "E": murEst,
                "S": murSud,
                "O": murOuest
                }
    ```

    **1.** Recopier et compléter sur la copie l'instruction Python suivante permettant de créer une instance `cellule` de la classe `Cellule` possédant tous ses murs sauf le mur Est.

    ```python title=""
    cellule = Cellule(...)
    ```

    ??? success "Réponse"

        ```python title=""
        cellule = Cellule(True, False, True, True)
        ```

    **2.** Le constructeur de la classe `Labyrinthe` ci-dessous possède un seul attribut `grille`.

    Cette grille est un tableau à deux dimensions `hauteur` et `largeur` contenant des cellules possédant chacune ses quatre murs.

    Recopier et compléter sur la copie les lignes 4 à 8 de la classe `Labyrinthe`.

    ```python linenums="1" title=""
    class Labyrinthe:
        def __init__(self, hauteur, longueur):
            self.grille=self.construire_grille(hauteur, longueur)

        def construire_grille(self, hauteur, longueur):
            grille = []
            for i in range(...):
                ligne = []
                for j in range(...):
                    cellule = ...
                    ligne.append(...)
                grille.append(ligne)
            return grille
    ```

    ??? success "Réponse"

        ```python linenums="1" title=""
        class Labyrinthe:
            def __init__(self, hauteur, longueur):
                self.grille=self.construire_grille(hauteur, longueur)

            def construire_grille(self, hauteur, longueur):
                grille = []
                for i in range(hauteur):
                    ligne = []
                    for j in range(longueur):
                        cellule = Cellule(True, True, True, True)
                        ligne.append(cellule)
                    grille.append(ligne)
                return grille
        ```

    Pour générer un labyrinthe, on munit la classe `Labyrinthe` d'une méthode `creer_passage` permettant de supprimer des murs entre deux cellules ayant un côté commun afin de créer un passage.

    Cette méthode prend en paramètres les coordonnées `(c1_lig, c1_col)` d’une cellule notée `cellule1` et les coordonnées `(c2_lig, c2_col)` d’une cellule notée `cellule2` et crée un passage entre `cellule1` et `cellule2`.

    ```python linenums="15" title=""
    def creer_passage(self, c1_lig, c1_col, c2_lig, c2_col):
        cellule1 = self.grille[c1_lig][c1_col]
        cellule2 = self.grille[c2_lig][c2_col]
        # cellule2 au Nord de cellule1
        if c1_lig - c2_lig == 1 and c1_col == c2_col:
            cellule1.murs['N'] = False
            ...
        # cellule2 à l'Ouest de cellule1
        elif ...
            ...
            ...
    ```

    **3.** La ligne 20 permet de supprimer le mur Nord de `cellule1`. Un mur de `cellule2` doit aussi être supprimé pour libérer un passage entre `cellule1` et `cellule2`.

    Écrire l'instruction Python que l'on doit ajouter à la ligne 21.

    ??? success "Réponse"

        ```python linenums="21" title=""
        cellule2.murs['S'] = False
        ```

    **4.** Recopier et compléter sur la copie le code Python des lignes 23 à 25 qui permettent le traitement du cas où `cellule2` est à l'Ouest de `cellule1`.

    ![Deux cellules](./images/voisins.png){ .autolight width=30%}

    ??? success "Réponse"
        
        ```python linenums="15" title=""
        def creer_passage(self, c1_lig, c1_col, c2_lig, c2_col):
            cellule1 = self.grille[c1_lig][c1_col]
            cellule2 = self.grille[c2_lig][c2_col]
            # cellule2 au Nord de cellule1
            if c1_lig - c2_lig == 1 and c1_col == c2_col:
                cellule1.murs['N'] = False
                cellule2.murs['S'] = False
            # cellule2 à l'Ouest de cellule1
            elif c1_lig == c2_lig and c1_col - c2_col == 1:
                cellule1.murs['O'] = False
                cellule2.murs['E'] = False
        ```

    Pour créer un labyrinthe, on utilise la méthode diviser pour régner en appliquant récursivement l'algorithme `creer_labyrinthe` sur des sous-grilles obtenues en coupant la grille en deux puis en reliant les deux sous-labyrinthes en créant un passage entre eux.

    ![Création récursive](./images/diviser.png){width=90%}


    La méthode creer_labyrinthe permet, à partir d’une grille, de créer un labyrinthe de
    hauteur `haut` et de longueur `long` dont la cellule en haut à gauche est de coordonnées `(ligne, colonne)`.
    Le cas de base correspond à la situation où la grille est de hauteur 1 ou de largeur 1. Il suffit alors de supprimer tous les murs intérieurs de la grille.

    ![Cas de base](./images/base.png){width=40%}

    **5.** Recopier et compléter sur la copie les lignes 28 à 33 de la méthode `creer_labyrinthe` traitant le cas de base.

    ```python linenums="21" title=""
    def creer_labyrinthe(self, ligne, colonne, haut, long):
            if haut == 1 : # Cas de base
                for k in range(...):
                    self.creer_passage(ligne, colonne + k, ligne, colonne + k + 1)
            elif long == 1: # Cas de base
                for k in range(...):
                    self.creer_passage(..., ..., ..., ...)
            else: # Appels récursifs
                # Code non étudié (Ne pas compléter)
    ```

    ??? success "Réponse"

        ```python linenums="21" title = ""
        def creer_labyrinthe(self, ligne, colonne, haut, long):
                if haut == 1 : # Cas de base
                    for k in range(long - 1):
                        self.creer_passage(ligne, colonne + k, ligne, colonne + k + 1)
                elif long == 1: # Cas de base
                    for k in range(haut - 1):
                        self.creer_passage(haut + k, colonne, haut + k + 1, colonne)
                else: # Appels récursifs
                    # Code non étudié (Ne pas compléter)
        ```

    **6.** Dans cette question, on considère une grille de hauteur `haut = 4` et de longueur `long = 8` dont chaque cellule possède tous ses murs.

    On fixe les deux contraintes supplémentaires suivantes sur la méthode `creer_labyrinthe` :

    * Si `haut` est supérieure ou égale à `long`, on coupe horizontalement la grille en deux sous-labyrinthes de même dimension ;
    * Si `haut` est strictement inférieur à `long`, on coupe verticalement la grille en deux sous-labyrinthes
    de même dimension.

    L'ouverture du passage entre les deux sous-labyrinthes se fait le plus au Nord pour une coupe verticale et le plus à l'Ouest pour une coupe horizontale.

    Dessiner le labyrinthe obtenu suite à l'exécution complète de l'algorithme `creer_labyrinthe` sur cette grille.

    ??? success "Réponse"

        Avez-vous réellement dessiné votre réponse ? Sinon, commencer par le faire avant de cliquer sur la solution ... 😉

        ??? success "Réponse"

            ![gif du labyrinthe](images/labyrinthe_bac_projet_boucle.gif){width=30%}

        

_Crédits : Romain Janvier, Mireille Coilhac_
