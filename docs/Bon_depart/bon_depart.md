---
author: Mireille Coilhac
title: Python - Bon départ
---

## I. Parcours de listes et de chaînes de caractères

Ce paragraphe a principalement été rédigé par Romain Janvier

??? abstract "Rappels sur les parcours de listes et de textes"
    En Python, il y a principalement 2 méthodes pour parcourir les listes Python (et donc les tableaux) et les textes.

    === "Par valeur"
        ```python
        def parcours_par_valeur(donnees):
            for val in donnees:
                print(val)  # Quelque chose avec val
        ```

        ```pycon
        >>> parcours_par_valeur([3, -2, 4])
        3
        -2
        4
        >>> parcours_par_valeur("AVION")
        A
        V
        I
        O
        N
        ```

    === "Par indices"
        ```python
        def parcours_par_indice(donnees):
            for i in range(len(donnees)):
                print(i, donnees[i])  # Quelque chose avec donnees[i] et/ou i
        ```

        ```pycon
        >>> parcours_par_indice([3, -2, 4])
        0 3
        1 -2
        2 4
        >>> parcours_par_indice("AVION")
        0 A
        1 V
        2 I
        3 O
        4 N
        ```

    ??? note "Vous pouvez tester les deux fonctions ici"
        {{ IDE('scripts/exemples_parcours') }}


    Lors d'un parcours par valeur, on n'a pas accès à l'indice mais uniquement à la valeur. Pour choisir entre les deux, il faut donc se poser la question suivante : **Est-ce que j'ai besoin de connaître l'indice de la valeur que je regarde ?**

    * Si la réponse est **oui**, il faut faire un parcours par indice.
    * Si la réponse est **non**, vous pouvez utiliser un parcours par valeur. Mais un parcours par indice fonctionnerait aussi.
    

??? question "Exercice 1 : `#!python compter(symbole, texte)`"
    Compléter le code de la fonction `#!python compter` qui prend en paramètre un texte d'un seul caractère `#!python symbole` ainsi qu'un texte `#!python texte` et qui renvoie le nombre de fois où `#!python symbole` apparaît dans `#!python texte`.

    ```pycon
    >>> compter('b', 'bulle')
    1
    >>> compter('l', 'bulle')
    2
    >>> compter('v', 'bulle')
    0
    ```

    === "Version sans code à trous"
        {{ IDE('scripts/exo_1a') }}
    === "Version avec code à trous"
        {{ IDE('scripts/exo_1b') }}



??? question "Exercice 2 : `#!python position(val, nombres)`"
    Compléter le code de la fonction `#!python position` qui prend en paramètre un entier `#!python val` ainsi qu'une liste d'entiers `#!python nombres` et renvoie l'indice de la première occurrence de `#!python val` dans `#!python nombres` s'il y en a une et `#!python None` sinon.

    ```pycon
    >>> position(7, [5, -1, 7, 4, 6, 4, 2])
    2
    >>> position(4, [5, -1, 7, 4, 6, 4, 2])
    3
    >>> position(0, [5, -1, 7, 4, 6, 4, 2])
    None
    ```

    === "Version sans code à trous"
        {{ IDE('scripts/exo_2a') }}
    === "Version avec code à trous"
        {{ IDE('scripts/exo_2b') }}


??? note "Sortie anticipée"

    Dans cet exercice nous avons un `return` dans la boucle. il y a donc sortie anticipée de la boucle s'il n'est pas nécessaire de la terminer.

	Ce procédé est souvent utilisé et permet de gagner en efficacité.


??? abstract "Parcours en sens inverse"
    Il est parfois utile de parcourir une liste Python, ou un texte, en partant du dernier élément et en allant vers le premier. On rappelle que l'indice du dernier élément est `#!python n-1`, où `#!python n` est la longueur de la liste ou du texte.

    === "Avec `#!python range`"
        On peut parcourir les indices dans l'ordre inverse en utilisant les paramètres supplémentaires de `#!python range`. Pour rappel, `#!python range(de, vers, pas)` part de `de` et va de `pas` en `pas` jusqu'à `vers`, en s'arrêtant juste avant de l'atteindre. En mettant `-1` pour le `pas`, on parcourt les nombres dans l'ordre inverse.

        ```python
        def parcours_inverse1(donnees):
            for i in range(len(donnees)-1, -1, -1): # on part du dernier indice 
                print(i, donnees[i])
        ```

        ```pycon
        >>> parcours_inverse1([3, -2, 4])
        2 4
        1 -2
        0 3
        >>> parcours_inverse1("AVION")
        4 N
        3 O
        2 I
        1 V
        0 A
        ```
    === "Avec une formule"
        Si on ne veut pas utiliser les paramètres supplémentaires de `#!python range`, il faut calculer l'indice de l'élément regardé à chaque tour de boucle. Si on note `#!python i` la variable de boucle, `#!python n` la longueur de la liste ou du texte, alors à chaque tour de boucle, il faut regarder l'élément d'indice `#!python n-1-i`.

        ```python
        def parcours_inverse2(donnees):
            n = len(donnees)
            for i in range(n):
                indice = n - 1 - i
                print(indice, donnees[indice])
        ```

        ```pycon
        >>> parcours_inverse2([3, -2, 4])
        2 4
        1 -2
        0 3
        >>> parcours_inverse2("AVION")
        4 N
        3 O
        2 I
        1 V
        0 A
        ```

    ??? note "Vous pouvez tester les deux fonctions ici"
        {{ IDE('scripts/exemples_parcours_inverses') }}

    Vous pouvez utiliser ces parcours, ou pas, pour l'exercice suivant.

??? question "Exercice 3 : `#!python derniere_position(val, nombres)`"
    Compléter le code de la fonction `#!python derniere_position` qui prend en paramètre un entier `#!python val` ainsi qu'une liste d'entiers `#!python nombres` et renvoie l'indice de la dernière occurrence de `#!python val` dans `#!python nombres` s'il y en a une et `#!python None` sinon.

    ```pycon
    >>> derniere_position(7, [5, -1, 7, 4, 6, 4, 2])
    2
    >>> derniere_position(4, [5, -1, 7, 4, 6, 4, 2])
    5
    >>> derniere_position(0, [5, -1, 7, 4, 6, 4, 2])
    None
    ```

    === "Version vide"
        {{ IDE('scripts/exo_3a') }}
    === "Code à trous et parcours inverse"
        {{ IDE('scripts/exo_3b') }}
    === "Code à trous et parcours normal"
        {{ IDE('scripts/exo_3c') }}


??? question "Exercice 4 : `#!python indice_egal_valeurs(nombres)`"
    Compléter le code de la fonction `#!python indice_egal_valeurs` qui prend en paramètre une liste d'entiers `#!python nombres` et renvoie `#!python True` s'il existe un indice `i` tel que `#!python nombres[i]==i` et `#!python False` sinon.

    ```pycon
    >>> indice_egal_valeur([7, 1, 8])  # nombres[1] = 1
    True
    >>> indice_egal_valeur([9, -7, 2, 9, 6])  # nombres[2] = 2
    True
    >>> indice_egal_valeur([1, 2, 3, 4])
    False
    ```

    === "Version sans code à trous"
        {{ IDE('scripts/exo_4a') }}
    === "Version avec code à trous"
        {{ IDE('scripts/exo_4b') }}

??? question "Exercice 5 : `#!python moyenne(valeurs)`"
    Compléter le code de la fonction `#!python moyenne` qui prend en paramètre une liste non vide de nombres `#!python valeurs` et renvoie la moyenne de ces nombres. On n'utilisera pas la fonction `#!python len`.

    ??? warning "Comparaison de nombres réels"
        On rappelle que la représentation des nombres réels en Python est basée sur le principe de la virgule flottante et qu'à cause de cela, deux expressions mathématiquement égales ne donnent pas forcément le même résultat en Python. 

        Pour vérifier le résultat de la fonction `#!python moyenne` avec le résultat attendu, nous utiiserons la fonction `#!python indiscernables` qui prend deux nombres et renvoie un booléen indiquant si la distance entre ces deux nombres (mathématiquement la valeur absolue de leur différence) est inférieure à $10^{-15}$.

        ```pycon
        >>> 0.1 * 3 == 0.3  # Erreur de virgule flottante classique
        False
        >>> indiscernables(0.1*3, 0.3)
        True
        ```

    ```pycon
    >>> moyenne([5])
    5.0
    >>> moyenne([5, 15, 8])
    9.333333333333334
    >>> moyenne([5, 15, 10])
    10.0
    ```

    === "Version sans code à trous"
        {{ IDE('scripts/exo_5a') }}
    === "Version avec code à trous"
        {{ IDE('scripts/exo_5b') }}

??? question "Exercice 6 : `#!python moyenne_ponderee(valeurs)`"
    Compléter le code de la fonction `#!python moyenne_ponderee` qui prend en paramètre une liste non vide `#!python valeurs` de couples de nombres `#!python (note, coeff)` et renvoie la moyenne pondérée correspondant à ces notes. On suppose que tous les coefficients sont positifs et qu'il y a au moins un coefficient non nul.

    ??? note "Calcul d'une moyenne pondérée"
        Pour calculer une moyenne pondérée, il faut aditionner le produit de chacune des notes avec le coefficient correspondant et diviser par la somme des coefficients.

        Par exemple, avec 5 notes $n_1$, $n_2$, ..., $n_5$ et les coefficients correspondants $c_1$, $c_2$, ..., $c_5$, la moyenne pondérée est :

        <p style="text-align: center;">\(\dfrac{n_1\times c_1 + n_2\times c_2 + n_3\times c_3 + n_4\times c_4 + n_5\times c_5}{c_1+c_2+c_3+c_4+c_5}\)</p>

    ??? note "Parcours d'une liste de couples"
        On rappelle que lors d'un parcours par valeur d'une liste composée de couples, on peut décomposer chacun des couples de la manière suivante :

        ```python
        def parcours_liste_couples(couples):
            for a, b in couples: # (1)
                print(a, b)
        ```

		1. On peut aussi écrire `#!py for (a, b) in couples` . Les parenthèses sont facultatives pour les tuples.

        ```pycon
        >>> parcours_liste_couples([("voiture", 3), ("vélo", 5), ("moto", 1)])
        voiture 3
        vélo 5
        moto 1
        ```

    ```pycon
    >>> moyenne_ponderee([(5, 1), (15, 1)])
    10.0
    >>> moyenne_ponderee([(5, 1), (15, 2)]) # 5*1 + 15*2 = 35 -> 35/3
    11.666666666666666
    >>> moyenne_ponderee([(5, 1), (15, 3)]) # 5*1 + 15*3 = 50 -> 50/4
    12.5
    >>> moyenne_ponderee([(5, 1), (15, 3), (20, 0)]) # le coeff 0 ne compte pas
    12.5
    ```

    === "Version sans code à trous"
        {{ IDE('scripts/exo_6a') }}
    === "Version avec code à trous"
        {{ IDE('scripts/exo_6b') }}

??? question "Exercice 7 : `#!python effectifs(donnees)`"
    Compléter le code de la fonction `#!python effectifs` qui prend en paramètre une liste `#!python donnees` contenant des entiers ou des textes et qui renvoie un dictionnaire qui associe à chaque valeur apparaissant dans `#!python donnees` le nombre de fois où elle y apparaît (c'est à dire le nombre d'occurences de `valeur` dans `donnees`). 

    Par exemple dans la liste `#!python [4, 1, 2, 4, 2, 2, 6]` il y a une fois la valeur 1, trois fois la valeur 2, deux fois la valeur 4 et une fois la valeur 6. Le résultat attendu pour cette liste sera donc le dictionnaire `#!python {1: 1, 2: 3, 4: 2, 6: 1}`.

    On rappelle aussi que dans un dictionnaire, l'ordre dans lequel on donne les valeurs n'a pas d'importance. Ainsi, les dictionnaires `#!python {1: 1, 2: 3, 4: 2, 6: 1}` et 
    `#!python {4: 2, 1: 1, 6: 1, 2: 3}` sont égaux.

    ```pycon
    >>> effectifs([4, 1, 2, 4, 2, 2, 6])
    {4: 2, 1: 1, 2: 3, 6: 1}
    >>> assert effectifs(["chien", "chat", "chien", "chien", "poisson", "chat"])
    {'chien': 3, 'chat': 2, 'poisson': 1}
    ```

    === "Version sans code à trous"
        {{ IDE('scripts/exo_7a', MAX_SIZE=20) }}
    === "Version avec un peu d'aide"
        {{ IDE('scripts/exo_7b', MAX_SIZE=20) }}
    === "Version avec beaucoup d'aide"
        {{ IDE('scripts/exo_7c', MAX_SIZE=20) }}

## II. Les listes

### Exercice 1

Vous devez générer une liste contenant les entiers de 1 à 49 dans l'ordre croissant. Ensuite, votre code doit tirer, sans remise, 6 numéros qu'on stocke dans une liste, puis, toujours sans remise, un dernier numéro (le numéro complémentaire).
 
!!! example "Exemple"

    Votre code affichera la liste des 6 numéros puis le complémentaire comme ceci :

	```pycon
	[31, 3, 12, 45, 13, 27]
	18
	```

!!! danger "Attention"

    Vous pourrez importer le module `random`.
    Vous ne devez pas utiliser la fonction `choice` du module random.  

???+ question "A vous de jouer"

	{{ IDE() }}
    
??? tip "Astuce"
    
    🌵 Envisagez différentes façons de coder ce problème, utilisant `pop`, `del` ou `remove` ...
 
??? success "Solution avec la méthode `pop`"

    C'est la solution préférable.

    ```python
    from random import randint
    liste = [i for i in range(1, 50)]
    reponse = []
    for i in range(6):
        numero = liste.pop(randint(0, len(liste) - 1))
        reponse.append(numero)
    print(reponse)
    complementaire = liste.pop(randint(0, len(liste) - 1))
    print(complementaire)
    ```
??? success "Solution avec `del`"

    ```python
    from random import randint
    liste = [i for i in range(1, 50)]
    reponse = []
    for i in range(6):
        i = randint(0, len(liste) - 1)
        reponse.append(liste[i])
        del liste[i]
    print(reponse)
    i = randint(0, len(liste) - 1)
    print(liste[i])
    ```

??? success "Solution avec la méthode `remove`"

    ```python
    from random import randint
    liste = [i for i in range(1,50)]
    reponse = []
    for i in range(6):
        i = randint(0, len(liste) - 1)
        reponse.append(liste[i])
        liste.remove(liste[i])
    print(reponse)
    i = randint(0, len(liste) - 1)

    print(liste[i])
    ```
### Exercice 2 

**1.** On considère le programme suivant :

```python
liste1 = [0]*100
liste2 = [0 for k in range(100)]
liste3 = []
for k in range(100):
	liste3.append(0)
```

Quel est le contenu de chacune des listes ?

??? success "Solution"

    😀 Utiliser l'éditeur Python pour vérifier les réponses ...

	{{ IDE() }}



**2.** Ecrire un programme python permettant de créer les listes suivantes :

a. Une liste contenant 12 fois le chiffre 7.  
b. La liste des nombres entiers de 1 à 100.  
c. Une liste contenant 50 nombres tirés au sort entre 1 et 6.  

???+ question "A vous de jouer"

	{{ IDE() }}


??? success "Solution"

    ```python
    from random import randint
    liste_a = [7 for _ in range(12)]
    liste_b = [i for i in range(1, 101)]
    liste_c = [randint(1, 6) for _ in range(50)]
    print(liste_a)
    print(liste_b)
    print(liste_c)
    ```


### Exercice 3

Écrire une fonction `sans_doublon` qui prend en paramètre une liste de nombres pouvant contenir des nombres répétés plusieurs fois et renvoie la liste ne contenant qu'une fois chaque nombre.

!!! example "Exemple"

    ```python
    >>> sans_doublon([1, 2, 4, 6, 6])
    [1, 2, 4, 6]
    >>> sans_doublon([2, 5, 7, 7, 7, 9])
    [2, 5, 7, 9]
    >>> sans_doublon([5, 1, 1, 2, 5, 6, 3, 4, 4, 4, 2])
    [5, 1, 2, 6, 3, 4]
    ```

!!! danger "Attention"

    Il est **interdit** d'utiliser `count`
    

???+ question "Compléter le code ci-dessous"

    {{ IDE('scripts/sans_doublon', SANS='count') }}

### Exercice 4 : Savoir compter

Écrire une fonction `compter_triples` qui prend en paramètre une liste `entiers` de nombres entiers et renvoie le nombre de multiples de 3 de cette liste

!!! danger "Attention"

    Il est **interdit** d'utiliser `count`

???+ question "Compléter le code ci-dessous"

    {{ IDE('scripts/compter_triple', SANS='count') }}


### Exercice 5 :

[Indice du minimum](https://codex.forge.apps.education.fr/exercices/ind_min/){ .md-button target="_blank" rel="noopener" }

### Exercice 6 :

[Maximum](https://codex.forge.apps.education.fr/exercices/maximum_nombres/){ .md-button target="_blank" rel="noopener" }


### Exercice 7 :

[Distribution de costumes](https://codex.forge.apps.education.fr/exercices/distribution_costume/){ .md-button target="_blank" rel="noopener" }

### Exercice 8 :

[Aplatir un tableau](https://codex.forge.apps.education.fr/exercices/aplatir/){ .md-button target="_blank" rel="noopener" }


### Exercice 9 : Le nombre mystère

Trouvez le nombre mystère qui répond aux conditions suivantes :

* Il est composé de 3 chiffres.
* Il est strictement inférieur à 300.
* Il est pair.
* Deux de ses chiffres sont identiques.
* La somme de ses chiffres est égale à 7.

On vous propose d'employer une méthode dite de « force brute », c'est-à-dire de tester tous les nombres possibles répondants aux trois premières conditions.

???+ question

    Compléter le script suivant 

    * `possibles` est la liste des nombres répondant aux trois premiers critères.
    * `liste` est la liste des nombres répondant au problème.

    {{ IDE('scripts/nombre_mystere') }}


## III. Les chaines de caractères

[Renverser une chaîne](https://codex.forge.apps.education.fr/exercices/renverse_chaine/){ .md-button target="_blank" rel="noopener" }


## IV. Les dictionnaires

???+ question

    Compléter le script suivant pour qu'il affiche la liste `pokemons_rapides` des noms des pokémons dont la vitesse est supérieure à 60.  
	On doit obtenir l'affichage : `['Salameche', 'Reptencil']` ou `['Reptencil', 'Salameche']`


    {{ IDE('scripts/pok_rapides') }}


??? success "Solution 1"

    Correction possible utilisant la méthode items:

    ```python
    pokemon1 = {'Nom' : 'Carapuce', 'HP' : 44, 'Attaque' : 48, 'Defense' : 65, 'Vitesse' : 43, 'Type' : ['Eau']}
	pokemon2 = {'Nom' : 'Evoli', 'HP' : 55, 'Attaque' : 55, 'Defense' : 50, 'Vitesse' : 55, 'Type' : ['Normal']}
	pokemon3 = {'Nom' : 'Salameche', 'HP' : 39, 'Attaque' : 52, 'Defense' : 43, 'Vitesse' : 65, 'Type' : ['Feu']}
	pokemon4 = {'Nom' : 'Reptencil', 'HP' : 58, 'Attaque' : 64, 'Defense' : 58, 'Vitesse' : 80, 'Type' : ['Eau']}
	liste_pokemon = [pokemon1, pokemon2, pokemon3, pokemon4]
	pokemons_rapides = []
	for pokemon in liste_pokemon:
    	for cle, valeur in pokemon.items():
        	if cle == 'Vitesse':
            	if valeur > 60 :
                	pokemons_rapides.append(pokemon['Nom'])
	print(pokemons_rapides)
    ```       

??? success "Solution 2"

	Correction possible sans utiliser la méthode items:

	```python
	pokemon1 = {'Nom' : 'Carapuce', 'HP' : 44, 'Attaque' : 48, 'Defense' : 65, 'Vitesse' : 43, 'Type' : ['Eau']}
	pokemon2 = {'Nom' : 'Evoli', 'HP' : 55, 'Attaque' : 55, 'Defense' : 50, 'Vitesse' : 55, 'Type' : ['Normal']}
	pokemon3 = {'Nom' : 'Salameche', 'HP' : 39, 'Attaque' : 52, 'Defense' : 43, 'Vitesse' : 65, 'Type' : ['Feu']}
	pokemon4 = {'Nom' : 'Reptencil', 'HP' : 58, 'Attaque' : 64, 'Defense' : 58, 'Vitesse' : 80, 'Type' : ['Eau']}
	liste_pokemon = [pokemon1, pokemon2, pokemon3, pokemon4]
	pokemons_rapides = []
	for pokemon in liste_pokemon:
    	if pokemon['Vitesse'] > 60:
        	pokemons_rapides.append(pokemon['Nom'])
	print(pokemons_rapides)
	```

??? success "Solution 3"

	😀 : Solution beaucoup plus élégante avec une **liste en compréhension** : 

	```python
	pokemon1 = {'Nom' : 'Carapuce', 'HP' : 44, 'Attaque' : 48, 'Defense' : 65, 'Vitesse' : 43, 'Type' : ['Eau']}
	pokemon2 = {'Nom' : 'Evoli', 'HP' : 55, 'Attaque' : 55, 'Defense' : 50, 'Vitesse' : 55, 'Type' : ['Normal']}
	pokemon3 = {'Nom' : 'Salameche', 'HP' : 39, 'Attaque' : 52, 'Defense' : 43, 'Vitesse' : 65, 'Type' : ['Feu']}
	pokemon4  ={'Nom' : 'Reptencil', 'HP' : 58, 'Attaque' : 64, 'Defense' : 58, 'Vitesse' : 80, 'Type' : ['Eau']}
	liste_pokemon = [pokemon1, pokemon2, pokemon3, pokemon4]
	pokemons_rapides = [pokemon["Nom"] for pokemon in liste_pokemon if pokemon["Vitesse"] > 60]
	print(pokemons_rapides)
	```

