# Tests
assert sans_doublon([1, 2, 4, 6, 6]) == [1, 2, 4, 6]
assert sans_doublon([2, 5, 7, 7, 7, 9]) == [2, 5, 7, 9]
assert sans_doublon([5, 1, 1, 2, 5, 6, 3, 4, 4, 4, 2]) == [5, 1, 2, 6, 3, 4]
assert sans_doublon([]) == []
assert sans_doublon([1, 2, 3, 4]) == [1, 2, 3, 4]

# Autres tests
assert sans_doublon(10*[0] + 11*[1] + 12*[2]) == [0, 1, 2]