def indiscernables(nombre1, nombre2):
    return abs(nombre1 - nombre2) < 1e-15

# tests
assert indiscernables(moyenne([5]), 5.0)
assert indiscernables(moyenne([5, 15, 8]), 9.333333333333334)
assert indiscernables(moyenne([5, 15, 10]), 10.0)
# tests secrets
# On vérifie quand même que indiscernables n'est pas trop permissive
assert not indiscernables(moyenne([5]), 5.1)
# Ni trop stricte
assert indiscernables(0.3, 0.1*3)

assert indiscernables(moyenne([0]*100), 0)
assert indiscernables(moyenne([1]*100), 1)
assert indiscernables(moyenne([2]*100), 2)
assert indiscernables(moyenne([20]*1000), 20)
assert indiscernables(moyenne([1, 7, 2, 9, 5, 14, 19]), (1+7+2+9+5+14+19)/7)
assert indiscernables(moyenne([1, 2, 3, 4, 5, 6]), (1+2+3+4+5+6)/6)
