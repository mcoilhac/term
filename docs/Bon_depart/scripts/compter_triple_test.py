# Tests
assert compter_triples([1, 3, 18, 5]) == 2
assert compter_triples([15, 3, 18]) == 3
assert compter_triples([5, 13, 11, 2]) == 0
assert compter_triples([]) == 0
assert compter_triples([5, 13,0, 2]) == 1

# Autres tests
assert compter_triples([k*3 for k in range(50)]) == 50
assert compter_triples([k*3 + 1 for k in range(50)]) == 0