def derniere_position(val, nombres):
    for i in range(len(nombres)-1, -1, -1):
        if nombres[i] == val:
            return i
    return None
