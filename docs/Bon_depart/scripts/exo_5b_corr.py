def indiscernables(nombre1, nombre2):
    return abs(nombre1 - nombre2) < 1e-15

def moyenne(valeurs):
    somme = 0
    nb_notes = 0
    for note in valeurs:
        somme = somme + note
        nb_notes = nb_notes + 1
    return somme / nb_notes
