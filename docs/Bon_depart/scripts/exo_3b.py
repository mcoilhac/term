def derniere_position(val, nombres):
    for i in range(...): # On peut mettre plusieurs paramètres
        if ...:
            ...
    return ...

# tests
assert derniere_position(7, [5, -1, 7, 4, 6, 4, 2]) == 2
assert derniere_position(4, [5, -1, 7, 4, 6, 4, 2]) == 5
assert derniere_position(0, [5, -1, 7, 4, 6, 4, 2]) == None
