def deux_chiffes_ident(nbre: int) -> bool:
    """
    renvoie True si nbre a deux chiffres identiques, False sinon
    Exemples :
    >>> deux_chiffes_ident(121)
    True
    >>> deux_chiffes_ident(123)
    False

    """
    mot = str(nbre)
    ...

def somme_chiffres(nbre: int) -> int:
    """
    renvoie la somme des chiffes de nbre
    Exemple :
    >>> somme_chiffres(123)
    6

    """
    mot = str(nbre)
    ...



possibles = [i for i in range(...) if ...]
liste = ...
print(liste[0])

# Tests
assert deux_chiffes_ident(121) == True
assert deux_chiffes_ident(123) == False
assert somme_chiffres(123) == 6




