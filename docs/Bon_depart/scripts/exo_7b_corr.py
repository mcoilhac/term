def effectifs(donnees):
    dico_effect = dict()
    for v in donnees:
        if v in dico_effect:
            dico_effect[v] = dico_effect[v] + 1
        else:
            dico_effect[v] = 1
    return dico_effect
