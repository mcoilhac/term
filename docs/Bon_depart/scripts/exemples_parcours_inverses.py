def parcours_inverse1(donnees):
    for i in range(len(donnees)-1, -1, -1): # on part du dernier indice
        print(i, donnees[i])

def parcours_inverse2(donnees):
    n = len(donnees)
    for i in range(n):
        indice = n - 1 - i
        print(indice, donnees[indice])
