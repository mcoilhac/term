# --- exo, 1 --- #
def compter(symbole, texte):
    ...
    for s in texte:
        if ...:
            ...
    return ...

# --- vide, 1 --- #
def compter(symbole, texte):
    ...

# --- corr, 1 --- #
def compter(symbole, texte):
    c = 0
    for s in texte:
        if s == symbole:
            c = c + 1
    return c
# --- tests, 1 --- #
assert compter('b', 'bulle') == 1
assert compter('l', 'bulle') == 2
assert compter('v', 'bulle') == 0
# --- secrets, 1 --- #
assert compter('b', '') == 0
assert compter('B', 'bulle') == 0
assert compter('e', 'bulle') == 1
assert compter('a', 'maman') == 2
assert compter('x', 'x'*1000) == 1000
# --- exo, 2 --- #
def position(val, nombres):
    for i in ...:
        if ...:
            return ...
    return ...

# --- vide, 2 --- #
def position(val, nombres):
    ...

# --- corr, 2 --- #
def position(val, nombres):
    for i in range(len(nombres)):
        if nombres[i] == val:
            return i
    return -1
# --- tests, 2 --- #
assert position(7, [5, -1, 7, 4, 6, 4, 2]) == 2
assert position(4, [5, -1, 7, 4, 6, 4, 2]) == 3
assert position(0, [5, -1, 7, 4, 6, 4, 2]) == -1
# --- secrets, 2 --- #
assert position(0, []) == -1
assert position(0, [0]) == 0
assert position(0, [0, 0]) == 0
assert position(0, [1, 0, 2]) == 1
assert position(0, [1, 2, 0]) == 2
# --- exo, 3a --- #
def derniere_position(val, nombres):
    ...

# --- exo, 3b --- #
def derniere_position(val, nombres):
    for i in range(...): # On peut mettre plusieurs paramètres
        if ...:
            ...
    return ...

# --- exo, 3c --- #
def derniere_position(val, nombres):
    ...
    for i in range(len(nombres)):
        if nombres[i] == val:
            ...
    return ...

# --- corr, 3a, 3b --- #
def derniere_position(val, nombres):
    for i in range(len(nombres)-1, -1, -1):
        if nombres[i] == val:
            return i
    return -1
# --- corr, 3c --- #
def derniere_position(val, nombres):
    reponse = -1
    for i in range(len(nombres)):
        if nombres[i] == val:
            reponse = i
    return reponse
# --- tests, 3a, 3b, 3c --- #
assert derniere_position(7, [5, -1, 7, 4, 6, 4, 2]) == 2
assert derniere_position(4, [5, -1, 7, 4, 6, 4, 2]) == 5
assert derniere_position(0, [5, -1, 7, 4, 6, 4, 2]) == -1
# --- secrets, 3a, 3b, 3c --- #
assert derniere_position(0, []) == -1
assert derniere_position(0, [0]) == 0
assert derniere_position(0, [0, 0]) == 1
assert derniere_position(0, [1, 0, 2]) == 1
assert derniere_position(0, [0, 2, 1]) == 0
# --- exo, 4 --- #
def indice_egal_valeur(nombres):
    for i in ...:
        if ...:
            return ...
    return ...

# --- vide, 4 --- #
def indice_egal_valeur(nombres):
    ...

# --- corr, 4 --- #
def indice_egal_valeur(nombres):
    for i in range(len(nombres)):
        if nombres[i] == i:
            return True
    return False
# --- tests, 4 --- #
assert indice_egal_valeur([7, 1, 8])
assert indice_egal_valeur([9, -7, 2, 9, 6])
assert not indice_egal_valeur([1, 2, 3, 4])
# --- secrets, 4 --- #
assert indice_egal_valeur([7, 1, 8]) is True
assert indice_egal_valeur([9, -7, 2, 9, 6]) is True
assert indice_egal_valeur([1, 2, 3, 4]) is False

assert indice_egal_valeur([]) is False
assert indice_egal_valeur([0]) is True
assert indice_egal_valeur([1]) is False

assert indice_egal_valeur([1,0]) is False
assert indice_egal_valeur([20, 20, 2, 20, 20]) is True
assert indice_egal_valeur([9, 8, 7, 6, 5, 4, 3, 2, 1, 0]) is False
assert indice_egal_valeur([9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 10]) is True
assert indice_egal_valeur([10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]) is True
# --- entete, 5 --- #
def indiscernables(nombre1, nombre2):
    return abs(nombre1 - nombre2) < 1e-15

# --- exo, 5 --- #
def moyenne(valeurs):
    somme = ...
    nb_notes = ...
    for note in valeurs:
        somme = ...
        nb_notes = ...
    return ...

# --- vide, 5 --- #
def moyenne(valeurs):
    ...

# --- corr, 5 --- #
def moyenne(valeurs):
    somme = 0
    nb_notes = 0
    for note in valeurs:
        somme = somme + note
        nb_notes = nb_notes + 1
    return somme / nb_notes
# --- tests, 5 --- #
assert indiscernables(moyenne([5]), 5.0)
assert indiscernables(moyenne([5, 15, 8]), 9.333333333333334)
assert indiscernables(moyenne([5, 15, 10]), 10.0)
# --- secrets, 5 --- #
# On vérifie quand même que indiscernables n'est pas trop permissive
assert not indiscernables(moyenne([5]), 5.1)
# Ni trop stricte
assert indiscernables(0.3, 0.1*3)

assert indiscernables(moyenne([0]*100), 0)
assert indiscernables(moyenne([1]*100), 1)
assert indiscernables(moyenne([2]*100), 2)
assert indiscernables(moyenne([20]*1000), 20)
assert indiscernables(moyenne([1, 7, 2, 9, 5, 14, 19]), (1+7+2+9+5+14+19)/7)
assert indiscernables(moyenne([1, 2, 3, 4, 5, 6]), (1+2+3+4+5+6)/6)
# --- entete, 6 --- #
def indiscernables(nombre1, nombre2):
    return abs(nombre1 - nombre2) < 1e-15

# --- exo, 6 --- #
def moyenne_ponderee(resultats):
    somme_notes = ...
    somme_coeffs = ...
    for note, coeff in resultats:
        ...
        ...
    return ...

# --- vide, 6 --- #
def moyenne_ponderee(valeurs):
    ...

# --- corr, 6 --- #
def moyenne_ponderee(resultats):
    somme_notes = 0
    somme_coeffs = 0
    for note, coeff in resultats:
        somme_notes = somme_notes + note*coeff
        somme_coeffs = somme_coeffs + coeff
    return somme_notes / somme_coeffs
# --- tests, 6 --- #
assert indiscernables(moyenne_ponderee([(5, 1), (15, 1)]), 10.0)
assert indiscernables(moyenne_ponderee([(5, 1), (15, 2)]), 11.666666666666666)
assert indiscernables(moyenne_ponderee([(5, 1), (15, 3)]), 12.5)
assert indiscernables(moyenne_ponderee([(5, 1), (15, 3), (20, 0)]), 12.5)
# --- secrets, 6 --- #
# On vérifie quand même que indiscernables n'est pas trop permissive
assert not indiscernables(moyenne_ponderee([(5, 1), (15, 2)]), 11.66666666666667)
# Ni trop stricte
assert indiscernables(0.3, 0.1*3)

assert indiscernables(moyenne_ponderee([(11, 1), (19, 3), (6, 2)]), 13.333333333333334)
assert indiscernables(moyenne_ponderee([(7, 79)]), 7)
assert indiscernables(moyenne_ponderee([(0, 8)]), 0)
# --- exo, 7a --- #
def effectifs(donnees):
    ...

# --- exo, 7b --- #
def effectifs(donnees):
    dico_effect = dict()
    for v in donnees:
        if ...:
            ...
        else:
            ...
    return ...

# --- exo, 7c --- #
def effectifs(donnees):
    dico_effect = dict()
    for v in donnees:
        if v in dico_effect:
            dico_effect[v] = ...
        else:
            dico_effect[v] = ...
    return dico_effect

# --- corr, 7a, 7b, 7c --- #
def effectifs(donnees):
    dico_effect = dict()
    for v in donnees:
        if v in dico_effect:
            dico_effect[v] = dico_effect[v] + 1
        else:
            dico_effect[v] = 1
    return dico_effect
# --- tests, 7a, 7b, 7c --- #
assert effectifs([4, 1, 2, 4, 2, 2, 6]) == {4: 2, 1: 1, 2: 3, 6: 1}
assert effectifs(["chien", "chat", "chien", "chien", "poisson", "chat"]) == {'chien': 3, 'chat': 2, 'poisson': 1}
# --- secrets, 7a, 7b, 7c --- #
assert effectifs([]) == {}
assert effectifs([0]) == {0: 1}
assert effectifs([0]*1000) == {0: 1000}
assert effectifs([0]*1000 + [1]*100 + [2]*10 + [3]) == {0: 1000, 1: 100, 2: 10, 3: 1}