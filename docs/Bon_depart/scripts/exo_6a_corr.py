def indiscernables(nombre1, nombre2):
    return abs(nombre1 - nombre2) < 1e-15

def moyenne_ponderee(resultats):
    somme_notes = 0
    somme_coeffs = 0
    for note, coeff in resultats:
        somme_notes = somme_notes + note*coeff
        somme_coeffs = somme_coeffs + coeff
    return somme_notes / somme_coeffs
