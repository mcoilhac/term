def position(val, nombres):
    ...

# tests
assert position(7, [5, -1, 7, 4, 6, 4, 2]) == 2
assert position(4, [5, -1, 7, 4, 6, 4, 2]) == 3
assert position(0, [5, -1, 7, 4, 6, 4, 2]) == None
