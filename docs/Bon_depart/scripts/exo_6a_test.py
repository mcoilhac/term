def indiscernables(nombre1, nombre2):
    return abs(nombre1 - nombre2) < 1e-15

# tests
assert indiscernables(moyenne_ponderee([(5, 1), (15, 1)]), 10.0)
assert indiscernables(moyenne_ponderee([(5, 1), (15, 2)]), 11.666666666666666)
assert indiscernables(moyenne_ponderee([(5, 1), (15, 3)]), 12.5)
assert indiscernables(moyenne_ponderee([(5, 1), (15, 3), (20, 0)]), 12.5)
# tests secrets
# On vérifie quand même que indiscernables n'est pas trop permissive
assert not indiscernables(moyenne_ponderee([(5, 1), (15, 2)]), 11.66666666666667)
# Ni trop stricte
assert indiscernables(0.3, 0.1*3)

assert indiscernables(moyenne_ponderee([(11, 1), (19, 3), (6, 2)]), 13.333333333333334)
assert indiscernables(moyenne_ponderee([(7, 79)]), 7)
assert indiscernables(moyenne_ponderee([(0, 8)]), 0)
