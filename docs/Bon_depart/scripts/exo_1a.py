def compter(symbole, texte):
    ...

# tests
assert compter('b', 'bulle') == 1
assert compter('l', 'bulle') == 2
assert compter('v', 'bulle') == 0
