def effectifs(donnees):
    dico_effect = dict()
    for v in donnees:
        if ...:
            ...
        else:
            ...
    return ...

# tests
assert effectifs([4, 1, 2, 4, 2, 2, 6]) == {4: 2, 1: 1, 2: 3, 6: 1}
assert effectifs(["chien", "chat", "chien", "chien", "poisson", "chat"]) == {'chien': 3, 'chat': 2, 'poisson': 1}
