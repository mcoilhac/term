## Remarques

💡 On peut aussi utiliser le fait que le dernier élément d'une liste est d'indice -1, l'avant dernier d'indice -2 etc.

👉 Autre réponse possible : 

```python
def get_diag_2(lst) -> list :
    """
    Entrée : lst : une liste de listes de mêmes longueurs.
    Sortie : diag_2 : une liste dont les éléments sont les éléments de la 2eme diagonale 
    (d'en haut  droite à en bas à gauche)
    >>> m = [ [1, 3, 4],
              [5 ,6 ,8],
              [2, 1, 3] ]
    >>> get_diag_2(m)
    [4, 6, 2]

    """

    return [lst[i][-1-i] for i in range(len(lst))]
```


