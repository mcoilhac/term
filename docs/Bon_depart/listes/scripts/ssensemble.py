# un tableau positifs en compréhension qui contient les nombres réels 
# strictement positifs du tableau nombres
nombres = [1, 0, -2, 9, -5, 4, -7, 5, -8]
positifs = ...


# un tableau voyelle_phrase en compréhension qui ne contient que les voyelles 
# contenues dans la chaine de caractère phrase
phrase = "je ne suis pas sans voix !"
VOYELLES = "aeiouy"
voyelle_phrase = ...
