---
author: Mireille Coilhac
title: Variables locales globales effets de bords
---

## La portée des variables

???+ question "Exemple 1"

    ⌛ Avant d'exécuter le programme ci-dessous, interrogez-vous sur la valeur de la variable `i` à l'issue de l’exécution, puis lancer le code et vérifiez.

    {{ IDE('scripts/exemple_1', ID=1) }}

    ??? success "🌵 Cliquez ici pour comprendre ..."

        Comme vous avez pu le constater, nous avons eu droit à une erreur : `"NameError: name 'i' is not defined"`.

        Pourquoi cette erreur, la variable i est bien définie dans la fonction `fct()` et la fonction `fct()` est bien exécutée, où est donc le problème ?

        En fait, la variable i est une variable dite **locale** : elle a été définie dans une fonction et elle "restera" dans cette fonction.

        Une fois que l'exécution de la fonction sera terminée, la variable i sera "détruite" (supprimée de la mémoire). Elle n'est donc pas accessible depuis "l'extérieur" de la fonction (ce qui explique le message d'erreur que nous obtenons).

        Une variable définie dans une fonction est **locale**.
        Elle ne peut pas être utilisée en dehors de la fonction.

???+ question "Exemple 2"

    ⌛ Avant d'exécuter le programme ci-dessous, interrogez-vous sur ce qui va s'afficher, puis lancez le code et vérifiez.

    {{ IDE('scripts/exemple_2') }}


    ??? success "🌵 Cliquez ici pour comprendre ..."

        Ici, pas de message d'erreur.

        En fait, la variable i est ici une variable dite **globale** : elle a été définie à l'extérieur de le fonction.  
        La fonction peut y accéder.


???+ question "Exemple 3"

    ⌛ Avant d'exécuter le programme ci-dessous, interrogez-vous sur ce qui va s'afficher, puis lancez le code et vérifiez.

    {{ IDE('scripts/exemple_3') }}


    ??? success "🌵 Cliquez ici pour comprendre ..."

        😢 Cela se complique ...  
    
        Pourtant cela ne semble pas très différent de l'exemple précédant.  
        Pourtant il y a une énorme différence : dans la fonction, on a : `i = `  
        Il a donc été créé une variable **locale** `i` qui malheureusement porte le même nom qie la variable **globale** `i`.  
        Dans cette fontion, on ne connait pas cette variable locale, ce qui explique le message d'erreur.

???+ question "Exemple 4"

    ⌛ Avant d'exécuter le programme ci-dessous, interrogez-vous sur ce qui va s'afficher, puis lancez le code et vérifiez.

    {{ IDE('scripts/exemple_4') }}


    ??? success "🌵 Cliquez ici pour comprendre ..."

        Cette fois pas d'erreur, mais à la fin de l'exécution de ce programme, la variable `i` référence la valeur 3.

        En fait, dans cet exemple nous avons 2 variables `i` différentes : la variable i **"globale"** (celle qui a été définie en dehors de toute fonction) et la variable i **"locale"** (celle qui a été définie dans la fonction). _Ces 2 variables portent le même nom (ce n'est pas une bonne idée), mais sont différentes.

        👉 Une variable définie localement dans une fonction peut porter le même nom qu'une variable globale. Dans ce cas, ce sont deux variable distinctes mais de même nom (ce qui n'est pas une bonne pratique).

???+ question "Exemple 5"

    ⌛ Avant d'exécuter le programme ci-dessous, interrogez-vous sur ce qui va s'afficher, puis lancez le code et vérifiez.

    {{ IDE('scripts/exemple_5') }}


    ??? success "🌵 Cliquez ici pour comprendre ..."

        Pour le `print("Dans la fonction i = ", i)` situé dans la fonction le système trouve une variable `i` dans l'espace local de la fonction `"fct"`, et il utilise donc celle ci, même s'il existe une variable globale `i`. 

        Il est important de bien comprendre que lorsque le système trouve une variable i dans l'espace local de la fonction, la "recherche" de la variable i s'arrête là. 

        D'autre part, ici nous avons deux variable `i` : l'une **globale**, définie en dehors de `fct()`, une autre **locale**, définie dans `fct()`. 

        Lorsque la fonction s'arrête, la variable locale est détruite, **la variable globale demeure et n'a pas été modifiée**.

        Visualisons dans Python tutor : 

        <iframe width="800" height="500" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=def%20fct%28%29%3A%0A%20%20%20%20i%20%3D%205%0A%20%20%20%20print%28%22i%20%3D%20%22,%20i%29%0A%20%20%20%20%0Ai%20%3D%202%0Aprint%28%22i%20%3D%20%22,%20i%29%0Afct%28%29%0Aprint%28%22i%20%3D%20%22,%20i%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=311&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>


???+ question "Exemple 6"

    ⌛ Avant d'exécuter le programme ci-dessous, interrogez-vous sur ce qui va s'afficher, puis lancez le code et vérifiez.

    Regardez de nouveau les exemples précédants. N'avons-nous pas déjà rencontré un exemple du même type ?


    {{ IDE('scripts/exemple_6') }}


    ??? success "🌵 Cliquez ici pour comprendre ..."

        😊 Reportez-vous à l'exemple 3 ...

???+ question "Exemple 7"

    ⌛ Avant d'exécuter le programme ci-dessous, interrogez-vous sur ce qui va s'afficher, puis lancez le code et vérifiez.

    Regardez de nouveau les exemples précédants. N'avons-nous pas déjà rencontré un exemple du même type ?


    {{ IDE('scripts/exemple_7', ID=1) }}


    ??? success "🌵 Cliquez ici pour comprendre ..."

        Nous avons juste rajouté la ligne `global i` dans la fonction.  

        Cela change tout car la variable `i ` utilisée dans la fonction, est la variable **globale** `i`.   

        ⚠️ Cette fonction va donc **modifier une variable globale** définie à l'extérieur de l fonction. C'est souvent **très dangereux et déconseillé**.  
        
        C'est parfois utile quand même. Pour pouvoir modifier une variable globale, il faut utiliser dans la fonction l'instruction **`global`**

        🌵 En fait, l'utilisation de "global" est une (très) mauvaise pratique, car cette utilisation entraîne des **"effets de bord"**.

        👓 Observez ce qu'il se passe, et ce qui diffère de l'exemple 5

        <iframe width="800" height="500" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=def%20fct%28%29%3A%0A%20%20%20%20global%20i%0A%20%20%20%20i%20%3D%20i%20%2B%201%0A%0Ai%20%3D%203%0Afct%28%29%0Aprint%28i%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=311&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

## Effets de bords

!!! info "Effet de bord"

    En informatique, une fonction est dite à effet de bord (traduction mot à mot de l'anglais side effect, dont le sens est plus proche d'effet secondaire) si elle modifie un état en dehors de son environnement local (modifie l'état d'une **variable globale**).  
    Source : Wikipedia

???+ question "Exemple 1"

    Reprenons l'exemple précédent : 

    {{ IDE('scripts/exemple_7', ID=2) }}

    Dans notre exemple ci-dessus la fonction `fct` modifie bien la valeur référencée par la variable `i` : 

    * Avant l'exécution de `fct`, la variable `i` référence la valeur `3`, 
    * Après l'exécution de la fonction fct la variable `i` référence la valeur `4`. 
    
    Nous avons donc bien un **effet de bord**.

    🌵 **_Les effets de bord c'est "mal" ! Mais pourquoi est-ce "mal" ?_**

    Les effets de bords provoquent parfois des comportements non désirés par le programmeur (évidemment dans des programmes très complexes, pas dans des cas simplistes comme celui que nous venons de voir). 

    Ils rendent aussi parfois les programmes difficilement lisibles (difficilement compréhensibles). À cause des effets de bord, on risque de se retrouver avec des variables qui référenceront des valeurs qui n'étaient pas prévues par le programmeur.

    On dit aussi qu'à un instant donné, l'état futur des variables est difficilement prévisible à cause des effets de bord.

    👉 **En résumé, on évitera autant que possible l'utilisation du "global".**

    Un paradigme de programmation se propose d'éviter au maximum les effets de bords : la programmation fonctionnelle.  
    Vous étudierez ce paradigme de programmation plus tard.  

    👉 Dans cet exemple, **l'effet de bord était volontaire** et provoqué par l'utilisation de `global`

???+ question "Exemple 2 : comparaison de deux fonctions"

    🐘🐘 Vous pourrez réviser ce qui concerne les copies de listes, que nous avons déjà étudiées.

    * **Une pure fonction sans effet de bord prenant en paramètre un entier ...**   

    (😂 On dit réellement une "pure fonction, ou fonction pure ...)

    {{ IDE('scripts/bord_1') }}

    👉 La variable **globale** `a = 3` **n'a pas été modifiée : il n'y a pas eu d'effet de bord.**

    * **Le même genre de fonction mais cette fois le paramètre est une liste ...**

    {{ IDE('scripts/bord_2') }}


    👉 La variable **globale** `lst = [1, 2, 3]` **a été modifiée sans utilisation de l'instruction `global` : Il y a eu effet de bord.**  
    🌵🌵 Cet effet de bord n'était peut-être pas voulu !

    * Reprenons l'exemple de la fonction pure :

    <iframe width="800" height="500" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=def%20fct_pure%28a%29%20%3A%0A%20%20%20%20a%20%3D%20a%20%2B%201%0A%20%20%20%20print%28%22Dans%20la%20fonction%20a%20%3D%20%22,%20a%29%0A%20%20%20%20%0Aa%20%3D%203%0Aprint%28%22Avant%20appel%20%3A%20a%20%3D%20%22,%20a%29%0Afct_pure%28a%29%0Aprint%28%22Apres%20appel%3A%20a%20%3D%20%22,%20a%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=311&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

    * Reprenons l'exemple prenant une liste comme paramètre

    <iframe width="800" height="500" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=def%20fct%28lst%29%3A%0A%20%20%20%20lst%5B0%5D%20%3D%20lst%5B0%5D%20%2B%201%20%23%20On%20ajoute%201%20au%20premier%20%C3%A9l%C3%A9ment%20de%20lst%0A%20%20%20%20print%28%22Dans%20la%20fonction%20lst%20%3D%20%22,%20lst%29%0A%20%20%20%20%0Alst%20%3D%20%5B1,%202,%203%5D%0Aprint%28%22Avant%20appel%20%3A%20lst%20%3D%20%22,%20lst%29%20%20%0Afct%28lst%29%0Aprint%28%22Apr%C3%A8s%20appel%20%20%3A%20lst%20%3D%20%22,%20lst%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=311&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

    ??? success "pourquoi ?"

        La liste passée en argument est modifiée dans la fonction, mais la valeur dans l'environnement global est aussi modifiée.  

        Contrairement à la variable `a` qui était de type `int`, la variable de type `list` n'est pas dupliquée : c'est la même liste qui est traitée dans la fonction.

## Le tri par sélection

🐘🐘 Vous devez absolument bien connaître ce tri

???+ question "Le tri par sélection classique"

    * Examinons un algorithme de tri par sélection classique :

    {{ IDE('scripts/selection_1') }}

    !!! info "tri en place"

        La liste a été triée **en place**, sans utiliser de `return` : on a utilisé **l'effet de bord**.  
        💡 **Ici, l'effet de bord a été utilisé de façon volontaire**

        Cette fonction ne renvoie rien. (On appelle ces fonctions des **procédure**.)

        En python, il n'y a pas de différence formelles entre fonctions et procédures.

    * 🤔 Imaginons qu'on souhaite ne pas modifier la liste de départ et qu'on essaye simplement d'ajouter un return dans la fonction

    {{ IDE('scripts/selection_2') }}

    !!! info "Tri en place"

        Cela n'a rien changé, **il y a toujours effet de bord** :   la liste initiale est quand même modifiée !

!!! info "À savoir"

    L'algorithme de  tri par sélection à connaître est une procédure qui réalise un **tri en place** par **effet de bord**


## Le tri par insertion

???+ question "Le tri par insertion"

    Exécuter le script suivant avec la **procédure** (pas de `return`) de tri par insertion

    {{ IDE('scripts/insertion') }}

    !!! info "Tri en place"

        Pour le tri par insertion, **il y a aussi effet de bord** :   la liste initiale a été modifiée.

!!! info "À savoir"

    L'algorithme de  tri par insertion à connaître est une procédure qui réalise un **tri en place** par **effet de bord**

## Le tri natif (built in) en Python `sorted`

???+ question "Le tri Python `sorted`"

    Exécuter le script suivant : 

    {{ IDE('scripts/tri_sorted') }}

!!! info "À savoir"

        
    La fonction `sorted` ne réalise **pas un tri en place** et n'a **pas d'effet de bord**






        