def tri_selection(lst):
    for i in range(len(lst)-1):
        imin = i
        for j in range(i + 1, len(lst)):
            if lst[j] < lst[imin]:
                imin = j
        if i!= imin:
            lst[i], lst[imin] = lst[imin], lst[i]
            

ma_liste = [1, 3, 2]
print("Avant appel de la fonction : ma_liste = ", ma_liste)
tri_selection(ma_liste)
print("Après appel de la fonction : ma_liste = ", ma_liste)
