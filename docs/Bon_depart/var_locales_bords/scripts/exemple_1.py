# on définit une fonction très simple :
def fct():
    i = 5
    
# on appelle la fonction
fct()

# que vaut i ?
print(i)
