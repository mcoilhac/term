---
author: Mireille Coilhac, Valérie Mousseaux, Jean-Louis Thirot
title: Recherche textuelle
---

![illus](images/illus.png){ width=30%; : .center }
> Source : Gilles Lassus


## I. La méthode `find` de Python

!!! info "⌛ Avant de commencer"

    Vous devez travailler sur [Basthon](https://notebook.basthon.fr/){ .md-button target="_blank" rel="noopener" }

    Télécharger dans **le même dossier** :  

    * 🌐 Fichier `pg798.txt` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/pg798.txt)

    * 🌐 TD `find_sujet.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/find_sujet.ipynb)

    Dans Basthon ouvrir `find_sujet.ipynb`, puis dans `find_sujet.ipynb` ouvrir `pg798.txt` (cliquer sur OK).

    😀 La correction est arrivée  ... 

    Télécharger **dans le même dossier** :

    * 🌐 Fichier `pg798.txt` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/pg798.txt)

    * 🌐 Correction du TD `find_corr.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/find_corr.ipynb)

    Dans Basthon ouvrir `find_corr.ipynb`, puis dans `find_corr.ipynb` ouvrir `pg798.txt` (cliquer sur OK).



<!--- 

⏳ La correction viendra bientôt ... 
Télécharger dans le même dossier :

* 🌐 Fichier `pg798.txt` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/pg798.txt)

* 🌐 Correction du TD `find_corr.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/find_corr.ipynb)
-->

## II. Recherche textuelle naïve

Regarder les **5 premières minutes** de la vidéo de l'introduction.

<p>L'algorithme naïf et l'algorithme de Horspool en vidéo : <a target="wims_external" href="https://grafikart.fr/tutoriels/boyer-moore-horspool-1197">Recherche textuelle</a>
</p>

???+ tip "Illustration de l'algorithme"

	Auteur : Gilles Lassus

    ![gif naif](images/gif_naive.gif){ width=90% }



!!! info "Animation de Nicolas Revéret"

    [Recherche naïve](https://boyer-moore.codekodo.net/recherche_naive.php){ .md-button target="_blank" rel="noopener" }



???+ question "Algorithme de recherche naïve"

    Compléter ci-dessous :

    {{ IDE('scripts/naif_indice') }}


???+ question "Version booléenne de l'algorithme de recherche naïve"

    Re-écrire l'algorithme précédent en s'arrêtant dès qu'une occurrence de motif est trouvée dans texte.

    La fonction renverra uniquement un booléen.

    {{ IDE('scripts/naif_bool') }}



!!! info "Temps de recherches"

    ⌛ **Avant de commencer**

    Vous devez travailler sur [Basthon](https://notebook.basthon.fr/){ .md-button target="_blank" rel="noopener" }

    Télécharger dans **le même dossier** :  

    * 🌐 Fichier `pg798.txt` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/pg798.txt)

    * 🌐 TD `temps_naif.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/temps_naif.ipynb)

    Dans Basthon ouvrir `temps_naif.ipynb`, puis dans `temps_naif.ipynb` ouvrir `pg798.txt` (cliquer sur OK).


## III Le principe de l'algorithme Boyer Moore Horspool

!!! info "En bio-informatique"

	Les algorithmes de recherche textuelle sont aussi notamment utilisés en bio-informatique. C’est dans ce domaine que l’on va prendre les exemples du TP qui suivra.

	* Comme son nom l'indique, la bio-informatique est issue de la rencontre de l'informatique et de la biologie : la récolte des données en biologie a connu une très forte augmentation ces 30 dernières années. Pour analyser cette grande quantité de données de manière efficace, les scientifiques ont de plus en plus recourt au traitement automatique de l'information, c'est-à-dire à l'informatique.
	* Analyse de l'ADN : Comme vous le savez déjà, l'information génétique présente dans nos cellules est portée par les molécules d'ADN. Les molécules d'ADN sont, entre autres, composées de bases azotées ayant pour noms : Adénine (représenté par un A), Thymine (représenté par un T), Guanine (représenté par un G) et Cytosine (représenté par un C).

	![adn](images/adn.jpg){ width=50%; : .center }

	Auteur du schéma : Victoria Denys/CEA sur https://www.cea.fr/comprendre/Pages/sante-sciences-du-vivant/l-ADN-dechiffrer-pour-mieux-comprendre-le-vivant.aspx?Type=Chapitre&numero=1


???+ question "Algorithme de recherche naïve en partant à l'envers"

	Auteur : Gilles Lassus

    ![gif naif inverse](images/texte_naif_inverse.gif){ width=90% }

    Re-écrire l'algorithme de recherche naïve, mais en démarrant de la fin du motif et non du début. Certes, c'est pour l'instant 
    très artificiel, mais cela va nous aider 😊.

    Compléter ci-dessous :

    {{ IDE('scripts/naif_indice_envers') }}


!!! info "Le principe"

    L'idée est d'améliorer le code précédent (celui où l'on parcourt le motif à l'envers) en sautant directement au prochain endroit 
    potentiellement valide.

    Pour cela on regarde le caractère X du texte sur lequel on s'est arrêté (car X n'était pas égal au caractère de rang équivalent 
    dans le motif):

    Si X n'est pas dans le motif, il est inutile de se déplacer "de 1" : on retomberait tout de suite sur X, c'est du temps perdu. 
    On se décale donc juste assez pour dépasser X, donc de la longueur du motif cherché.
    Si X est dans le motif (sauf à la dernière place du motif!), on va regarder la place de la dernière occurence de X dans le motif. 
    On se décale afin de faire coïncider le X du motif et le X du texte.


!!! info "Visualisation Boyer Moore Horspool par Nicolas Revéret"

    [Boyer Moore Horspool](https://boyer-moore.codekodo.net/recherche_boyer.php){ .md-button target="_blank" rel="noopener" }



## IV. Implémentation de l'algorithme Boyer Moore Horspool

!!! info "utiliser un prétraitement du motif pour déterminer les décalages"

    On va d'abord coder une fonction `dico_lettres` qui renvoie un dictionnaire associant à chaque lettre de `mot` (paramètre d'entrée) 
    son dernier rang dans la variable `mot`. On exclut la dernière lettre, qui poserait un problème lors du décalage (on décalerait de 0...) 

    Dans l'exemple suivant : 

    **Etape 0 :** 

    ![étape 0](images/decalage_maxi.png){ width=50%; : .center }

    Le dictionnaire créé sera donc : `dico = {"s": 0, "t": 1, "r": 2, "i": 3, "n": 4}`

    **Etape 1 :**

    * `i` prend la valeur 5 c'est à dire `len(motif) - 1`
    * `"d"` n'étant pas dans `dico`, on va faire un décalage de 6, c'est à dire de `len(motif)`

    ![étape 1](images/decalage_interm.png){ width=50%; : .center }

    **Etape 2 :**

    * `i` prend la valeur 5 + 6 c'est à dire de `i + decalage`
    * `dico["n"] = 4`, on va faire un décalage de 5 - 4, c'est à dire de `len(motif) - 1 - dico["n"]`

    ![étape 2](images/decalage_fin.png){ width=50%; : .center }  

    **Etape 3 :**

    * `i` prend la valeur 11 + 1 c'est à dire de `i + decalage`
    * On a concordance pour la lettre "g", on vérifie la concordance des lettres en partant vers la gauche,  
     donc pour les indices `i - k`, `k` augmentant de 1 à chaque nouvelle comparaison. On observe qu'il n'y a plus
     concordance lorsque `i - k = 8` .
    * `"p"` n'étant pas dans `dico`, on va faire un décalage de 6, c'est à dire `len(motif)` à partir de l'indice
     où l'on se trouve.   
     `i` prendra la valeur 8 + 6 = 14, c'est à dire que `i` prend la valeur `i - k + len(motif)`

    ![étape 3](images/decalage_der.png){ width=50%; : .center }  

    **Etape 4 :**

    * `i` prend la valeur 14
    * `dico["s"] = 0`, on va faire un décalage de 5 - 0 = 5, c'est à dire `len(motif) - 1 - dico["s"]`

    ![étape 4](images/ultime.png){ width=50%; : .center }


???+ question "A vous de jouer : Implémenter l'algorithme Boyer Moore Horspool"

    Compléter ci-dessous :

    {{ IDE('scripts/bmh', MAX_SIZE=50) }}


??? note "Algorithme de Boyer-Moore-Horspool :heart:"
    
    ```python linenums='1'
    def dico_lettres(mot):
        d = {}
        for i in range(len(mot)-1):
            d[mot[i]] = i
        return d

    def BMH(texte, motif):
        dico = dico_lettres(motif)
        indices = []
        i = 0
        while i <= len(texte) - len(motif):
            k = len(motif)-1
            while k >= 0 and texte[i+k] == motif[k]: #(1)
                k -= 1
            if k == -1: #(2)
                indices.append(i)
                i += 1 #(3)
            else:
                if texte[i+k] in dico: #(4)
                    i += len(motif) - dico[texte[i+k]] - 1 #(5)
                else:
                    i += len(motif) #(6)

        return indices

    ```

    1. On remonte le motif à l'envers, tant qu'il y a correspondance et qu'on n'est pas arrivés au début du motif
    2. Si on est arrivés à la valeur ```k=-1```, c'est qu'on a parcouru tout le mot : on l'a donc trouvé.
    3. On a trouvé le motif, mais attention, il ne faut pas trop se décaler sinon on pourrait rater d'autres occurences du motif (pensez à la recherche du motif «mama» dans le mot «mamamamama»). On se décale donc de 1.
    4. On s'est arrêté avant la fin, sur une lettre présente dans le mot : il va falloir faire un décalage intelligent.
    5. On décale juste de ce qu'il faut pour mettre en correspondance les lettres.
    6. La lettre n'est pas dans le motif : on se positionne juste après elle en se décalant de toute la longueur du motif. 

???+ question "Moins guidé : Implémenter l'algorithme Boyer Moore Horspool"

    Compléter ci-dessous :

    {{ IDE('scripts/bmh_2', MAX_SIZE=50) }}

## V. Crédits

Gilles LASSUS, Jean-Louis THIROT, Marine MERA et Mireille COILHAC  

