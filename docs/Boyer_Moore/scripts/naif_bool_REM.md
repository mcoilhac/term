Autre solution qui utilise une sortie anticipée de la fonction grâce au `return True` à la ligne 12.

```python title="" linenums='1'
def recherche_naive_bool(texte, motif):
    '''
    renvoie un booléen indiquant la présence ou non de
    la chaîne motif dans la chaîne texte.
    '''
    i = 0
    while i <= len(texte) - len(motif):
        k = 0
        while k < len(motif) and texte[i + k] == motif[k]:
            k = k + 1
        if k == len(motif):
            return True
        i = i + 1
    return False
```
