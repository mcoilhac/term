# --- PYODIDE:code --- #

def recherche_naive_bool(texte, motif):
    '''
    renvoie un booléen indiquant la présence ou non de
    la chaîne motif dans la chaîne texte.
    '''
    trouve = False
    i = ...
    while i ... and  ...:
        k = 0
        while k < ... and texte[...] == motif[...]:
            k = k + 1
        if k == ...:
            trouve = ...
        i = ...
    return trouve


# --- PYODIDE:corr --- #

def recherche_naive_bool(texte, motif):
    '''
    renvoie un booléen indiquant la présence ou non de
    la chaîne motif dans la chaîne texte.
    '''
    trouve = False
    i = 0
    while i <= len(texte) - len(motif) and not trouve:
        k = 0
        while k < len(motif) and texte[i + k] == motif[k]:
            k = k + 1
        if k == len(motif):
            trouve = True
        i = i + 1
    return trouve


# --- PYODIDE:tests --- #

assert recherche_naive_bool("une magnifique maison bleue", "maison") is True
assert recherche_naive_bool("une magnifique maison bleue", "nsi") is False
assert recherche_naive_bool("une magnifique maison bleue", "ma") is True


# --- PYODIDE:secrets --- #

assert recherche_naive_bool("une magnifique maison bleue", "za") is False
assert recherche_naive_bool('stupid spring string', 'string') is True
assert recherche_naive_bool('stupid spring string', 'ring') is True