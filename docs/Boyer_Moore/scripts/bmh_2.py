# --- PYODIDE:code --- #

def dico_lettres(mot):
    ...


def BMH(texte, motif):
    dico = ...
    indices = ...
    i = ...
    while i <= ...
        k = ...
        while k ... and ...
            k = ...
        if k == ...: 
            indices.append(i)
            i  = ...
        else:
            if texte[...] ...
                i = ...
            else:
                i = ...
    return indices


# --- PYODIDE:corr --- #

def dico_lettres(mot):
    d = {}
    for i in range(len(mot) - 1):
        d[mot[i]] = i
    return d

def BMH(texte, motif):
    dico = dico_lettres(motif)
    indices = []
    i = 0
    while i <= len(texte) - len(motif):
        k = len(motif) - 1
        while k >= 0 and texte[i + k] == motif[k]: 
            k = k - 1
        if k == -1: 
            indices.append(i)
            i = i + 1 
        else:
            if texte[i + k] in dico: 
                i = i + len(motif) - 1 - dico[texte[i + k]]
            else:
                i = i + len(motif) 

    return indices


# --- PYODIDE:tests --- #

assert dico_lettres("string") == {"s": 0, "t": 1, "r": 2, "i": 3, "n": 4}
assert BMH("stupid spring string", "string") == [14]
assert BMH("stupid spring string", "ing") == [10, 17]
assert BMH("stupid spring string", "ok") == []

# --- PYODIDE:secrets --- #

assert BMH("stupid spring string", "pi") == [3]
assert BMH("stupid spring string", "ri") == [9, 16]