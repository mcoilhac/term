# Tests
assert recherche_naive("une magnifique maison bleue", "maison") == [15]
assert recherche_naive("une magnifique maison bleue", "nsi") == []
assert recherche_naive("une magnifique maison bleue", "ma") == [4, 15]

## Autres tests
assert recherche_naive("une magnifique maison bleue", "za") == []
assert recherche_naive('stupid spring string', 'string') == [14]
assert recherche_naive('stupid spring string', 'ring') == [9, 16]

