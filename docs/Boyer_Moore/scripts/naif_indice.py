def recherche_naive(texte, motif):
    '''
    renvoie la liste des indices (éventuellement vide) des occurrences de
    de la chaîne motif dans la chaîne texte.
    '''
    indices = ...
    i = 0
    while i <= len(texte) - len(motif):
        k = 0
        while k < len(motif) and texte[...] == motif[...]:
            k = ...
        if k == ...:
            indices.append(i)
        i = ...

    return ...

# Tests
assert recherche_naive("une magnifique maison bleue", "maison") == [15]
assert recherche_naive("une magnifique maison bleue", "nsi") == []
assert recherche_naive("une magnifique maison bleue", "ma") == [4, 15]
