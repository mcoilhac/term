# --- PYODIDE:code --- #

def naif_envers(texte, motif):
    indices = []
    i = 0
    while i <= len(texte) - len(motif):
        k = len(motif) - 1   # On part de la fin et on fait diminuer k
        while k >= ... and texte[...] == motif[...]:
            k = ...
        if k == ...:
            indices.append(i)
        i = ...
    return indices


# --- PYODIDE:corr --- #

def naif_envers(texte, motif):
    indices = []
    i = 0
    while i <= len(texte) - len(motif):
        k = len(motif)-1  # On part de la fin et on fait diminuer k
        while k >= 0 and texte[i + k] == motif[k]:
            k = k - 1
        if k == -1:
            indices.append(i)
        i = i + 1
    return indices


# --- PYODIDE:tests --- #

assert naif_envers("une magnifique maison bleue", "maison") == [15]
assert naif_envers("une magnifique maison bleue", "nsi") == []
assert naif_envers("une magnifique maison bleue", "ma") == [4, 15]


# --- PYODIDE:secrets --- #

assert naif_envers("une magnifique maison bleue", "za") == []
assert naif_envers('stupid spring string', 'string') == [14]
assert naif_envers('stupid spring string', 'ring') == [9, 16]