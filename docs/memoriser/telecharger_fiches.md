---
author: Mireille Coilhac
title: Téléchargement des fiches
---


??? question "Les dictionnaires"

    🌐 Fiche à télécharger : Fichier `dicos_trous.pdf` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/dicos_trous.pdf)



??? question "POO"

    🌐 Fiche à télécharger : Fichier `POO_trous.pdf` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/POO_trous.pdf)

