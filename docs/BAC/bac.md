---
author: Gilles Lassus, Mireille Coilhac
title: BAC
---

*Modifications à la rentrée 2023*

## Modalités

L'épreuve se partage en :

- une **Épreuve Écrite** (sur 20 points, à l'écrit, durée 3h30, coefficient 0.75)
- une **Épreuve Pratique** (sur 20 points, sur machine, durée 1h, coefficient 0.25)

Pour résumer, on peut donc dire que la note sur 20 au bacaccalauréat en NSI est constituée de 15 points d'épreuve écrite et de 5 points d'épreuve pratique.

## Épreuve Pratique

!!! info "Dates et sujets session 2025"

    Les épreuves se dérouleront dans chaque établissement, pendant la période de l’épreuve écrite, selon le calendrier fixé par le recteur d’académie ou le vice-recteur d’académie concerné.

   
!!! Abstract "Textes règlementaires"

    [https://www.education.gouv.fr/bo/2025/Hebdo4/MENE2433178N](https://www.education.gouv.fr/bo/2025/Hebdo4/MENE2433178N){ .md-button target="_blank" rel="noopener" }

    
    
- Durée : 1 heure
- L'épreuve pratique donne lieu à une note sur **20 points**, coefficient 0.25.

La partie pratique consiste en la résolution de **deux exercices sur ordinateur**, chacun étant noté sur **10 points**.

Le candidat est évalué sur la base d’un dialogue avec un professeur-examinateur. Un examinateur évalue au maximum quatre élèves. L’examinateur ne peut pas évaluer un élève qu’il a eu en classe durant l’année en cours.
L’évaluation de cette partie se déroule au cours du troisième trimestre pendant la période de l’épreuve écrite de spécialité.

**Premier exercice**

Le premier exercice consiste à programmer un algorithme figurant explicitement au programme, ne présentant pas de difficulté particulière, dont on fournit une spécification. Il s’agit donc de restituer un algorithme rencontré et travaillé à plusieurs reprises en cours de formation. Le sujet peut proposer un jeu de test avec les réponses attendues pour permettre au candidat de vérifier son travail.


**Deuxième exercice**

Pour le second exercice, un programme est fourni au candidat. Cet exercice ne demande pas l’écriture complète d’un programme, mais permet de valider des compétences de programmation suivant des modalités variées : le candidat doit, par exemple, compléter un programme « à trous » afin de répondre à une spécification donnée, ou encore compléter un programme pour le documenter, ou encore compléter un programme en ajoutant des assertions, etc.

### Banque d'exercices

!!! Abstract "Banque Nationale de Sujets"

    - [Sujets visibles au plus tard le 24 mars 2025](https://sujets.examens-concours.gouv.fr/delos/public/bgt/nsi){:target="_blank" }



## Épreuve Écrite
 
À compter de la session 2023, l'épreuve consiste en **trois exercices** qui doivent tous être traités. L'épreuve écrite donne lieu à une note sur **20 points**, coefficient 0.75.

!!! Abstract "Textes règlementaires"

    [Calendrier 2025](https://www.education.gouv.fr/bo/2024/Hebdo27/MENE2403660N){:target="_blank" }


*Les annales des sujets passés (2020, 2021, 2022) comportent tous 5 exercices dont seulement 3 devaient être traités. Ce n'est plus le cas depuis la session 2023 où la totalité du sujet doit être traité.*

### Lien vers le site officiel de toutes les annales

[Annales officielles](https://eduscol.education.fr/3067/annales-des-epreuves-du-baccalaureat-des-voies-generales-et-technologiques){ .md-button target="_blank" rel="noopener" }


### Lien vers le site de sujets du bac : ToutMonExam

[ToutMonExam](https://toutmonexam.fr/){ .md-button target="_blank" rel="noopener" }


## Grand Oral

Quelques liens :   

[Comment se passe le grand oral ?](https://www.education.gouv.fr/reussir-au-lycee/baccalaureat-comment-se-passe-le-grand-oral-100028){ .md-button target="_blank" rel="noopener" }

[Texte officiel](file:///C:/Users/m.coilhac/Downloads/Grand_oral_Igesr_Partie_BGN_NSI_0.pdf){ .md-button target="_blank" rel="noopener" }

[La forme](https://view.genially.com/5ee1da0d0cb7020cf4023911/interactive-content-le-grand-oral-du-baccalaureat){ .md-button target="_blank" rel="noopener" }

[Petits tutos sur le Grand Oral](https://www.youtube.com/playlist?list=PLGo5sm4Twi__D3hg0u6RbbXWLlwf_CW2g){ .md-button target="_blank" rel="noopener" }

[Tutos Lumni du grand oral](https://www.lumni.fr/programme/les-petits-tutos-du-grand-oral){ .md-button target="_blank" rel="noopener" }

[Ma thèse en 180 secondes](https://mt180.fr/){ .md-button target="_blank" rel="noopener" }

[Ressources Le Robert](https://grand-oral.lerobert.com/9782321015383/assets/les-ressources-numeriques-pour-la-nsi-1/preview){ .md-button target="_blank" rel="noopener" }

[Exemples de Thomas Beline](https://kxs.fr/cours/grand-oral/exemples){ .md-button target="_blank" rel="noopener" }

[CultureMath](https://culturemath.ens.fr/thematiques/lycee){ .md-button target="_blank" rel="noopener" }

[Images des mathématiques](https://images.math.cnrs.fr/){ .md-button target="_blank" rel="noopener" }

[Site de Frédéric Mandon](https://www.maths-info-lycee.fr/grantoral.html){ .md-button target="_blank" rel="noopener" }









