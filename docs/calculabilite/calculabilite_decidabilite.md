---
authors: Gilles Lassus et Mireille Coilhac
title: Calculabilité et décidabilité
---

A venir ...


[Calculabilité et décidabilité par Gilles Lassus](https://glassus.github.io/terminale_nsi/T2_Programmation/2.3_Calculabilite_Decidabilite/cours/){ .md-button target="_blank" rel="noopener" }

[Exercice 1 du bac NSI du 12 septembre 2024 rédigé ici par Franck Chambon ](https://pratique.forge.apps.education.fr/ecrit/recursif/24-ME3-J2-ex1/){ .md-button target="_blank" rel="noopener" }


[Vidéo Arte de 11 minutes](https://www.arte.tv/fr/videos/107398-010-A/voyages-au-pays-des-maths/){ .md-button target="_blank" rel="noopener" }


## Crédits 

Gilles Lassus  
Franck Chambon