---
author: Mireille Coilhac, Valérie Mousseaux, Jean-Louis Thirot
title: SoC
---

!!! info "SoC"

    Soc : System on a chip

    Système sur puce

## I. Historique et Présentation

[Lumni : historique](http://www.lumni.fr/video/une-histoire-de-l-architecture-des-ordinateurs){ .md-button target="_blank" rel="noopener" }


Pour regarder la vidéo avec les sous-titres en français :

* Cliquer pour regarder dans YouTube
* Sélectionner les paramètres
* Sélectionner sous-titres
* Sélectionner traduire automatiquement
* Sélectionner français

<iframe width="560" height="315" src="https://www.youtube.com/embed/NKfW8ijmRQ4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

!!! info "Eléments fondamentaux"

    Le fonctionnement d’un outil numérique, que ce soit un ordinateur, une tablette, un téléphone, un assistant GPS ou un appareil photo est basé sur des éléments fondamentaux :

    * le processeur (CPU)
    * la mémoire
    * la carte graphique
    * les interfaces réseaux

Prenons pour commencer un PC : voici son intérieur 

<div class="centrer">
<img src="https://s1.qwant.com/thumbr/0x380/a/2/b997bae5f7826323db05376bce0c7f1ca17c55608fa14e751687cec4604923/take-care-of-your-computer-part-5-how-to-work-on-your-own-pc-15-728.jpg?u=https%3A%2F%2Fimage.slidesharecdn.com%2Ftakecarecomputer-2012-05-24-120605111952-phpapp01%2F95%2Ftake-care-of-your-computer-part-5-how-to-work-on-your-own-pc-15-728.jpg%3Fcb%3D1338895259&q=0&b=1&p=0&a=1" alt="description de l'image"/height=50% width=50%>
</div>

Principalement on remarque la carte mère qui accueille tous les éléments fondamentaux au bon fonctionnement d'un ordinateur : microprocesseur (CPU) (caché sous un système de refroidissement), barrettes de mémoire RAM, carte graphique (qui permet de gérer l'affichage). On trouve aussi sur la carte mère les puces qui gèrent les interfaces réseau (Wifi et Ethernet) et bien d'autres choses...

![tour](images/composantsTour.jpg){ width=70% .center }

Les périphériques tels que cartes audios sont directement insérés dans des emplacements dédiés sur la carte mère, qu'on nomme slots PCI : voir [cette page wikipédia](https://fr.wikipedia.org/wiki/PCI_(informatique)){:target="_blank" }


!!! info "L'architecture de Von-Neumann : figure à connaître par cœur"

    ![Von Neumann](images/Von_Neumann_architecture.svg){ width=40% .center .autolight}

    [Source : Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Von_Neumann_architecture.svg){:target="_blank" }

    !!! info "L'unité centrale"
    
        L’Unité Centrale de Traitement (Central Processing Unit en anglais) ou Processeur est constituée de deux sous-unités :
    
        !!! info "L'unité de contrôle"

            L’Unité de contrôle charge la prochaine instruction dont l’adresse mémoire se trouve dans un registre 
            appelé Compteur de Programme (**PC** en anglais) ou Compteur ordinal, la décode avec le décodeur et 
            commande l’exécution par l’ALU avec le séquenceur. L’instruction en cours d’exécution est chargée dans le Registre d’Instruction (**RI**). 
            L’Unité de contrôle peut aussi effectuer une opération de branchement, un saut dans le programme, en modifiant le Compteur de Programme, 
            qui par défaut est incrémenté de 1 lors de chaque instruction.

        !!! info "L’Unité Arithmétique et Logique"

            L’Unité Arithmétique et Logique (ALU en anglais) qui réalise des opérations arithmétiques (addition, multiplication . . .), 
            logiques (et, ou . . .), de comparaisons ou de déplacement de mémoire (copie de ouvers la mémoire). L’ALU stocke les données dans 
            des mémoires d’accès très rapide appelées registres. Les opérations sont réalisées par des circuits logiques constituant 
            le jeu d’instructions du processeur.

    !!! info "La mémoire"

        La mémoire où sont stockés les données et les programmes.

    !!! info "Les bus"

        Des bus qui sont des fils reliant le CPU et la mémoire et permettant les échanges de données et d’adresses. Les adresses, les données et les commandes circulent par les bus. 
        
        * Le bus d'adresse permet de faire circuler des adresses (par exemple l'adresse d'une donnée à aller chercher en mémoire)
        * Le bus de données permet de faire circuler des données.
        * Le bus de contrôle permet de spécifier le type d'action (exemples : écriture d'une donnée en mémoire, lecture d'une donnée en mémoire).
        
        Un bus ne peut être utilisé que par deux composants (émetteur/récepteur) à la fois !

    !!! info "Les entrées/sorties"
    
        Des dispositifs d’entrées/sorties permettant d’échanger avec l’extérieur (lecture ou écriture de données).


    👉 Dans le modèle de Von Neumann, le processeur exécute une instruction à la fois, de façon séquentielle.


!!! info "Les puces"

    On entend souvent dire que les smartphones sont de véritables ordinateurs, ce qui est vrai. On peut s'interroger sur la taille d'un smartphone par rapport à la taille d'un PC dont la carte mère mesure environ 25 cm sur 30 cm, soit bien plus qu'un smartphone.

    Si les smartphones sont des ordinateurs, on doit obligatoirement trouver à l'intérieur les mêmes composants que dans un PC : CPU, RAM, carte graphique et interfaces réseau (Wifi et Bluetooth dans le cas d'un smartphone) !

    La solution ? Placer tous ces composants dans une puce unique d'une centaine de $\text{mm}^2$ :

    ![puce](images/puce.png){ width=30% .center }

    Ces puces accueillant CPU, RAM, circuit graphique (GPU : équivalent à la carte graphique dans un PC) et circuits radio (Wifi et Bluetooth), sont souvent appelées "système sur puce" , "system on a chip" en anglais dont l'abréviation communes est "SoC".

!!! info "Les composants sur des puces"

    Un système sur puce (System on Chip – SoC), comme le microcontrôleur, rassemble donc sur une seule puce (circuit intégré) différents composants, comme :

    * des microprocesseurs (principal, graphique, …)
    * des mémoires (RAM, flash, …)
    * des périphériques d’interface (Wifi, BlueTooth, …)
    * des capteurs (GPS, …)

!!! info "Les avantages"

    😀 Présents sur les systèmes nomades (smartphone, tablettes, …) et dans les systèmes embarqués (voitures, robots, …), les SoC disposent de nettement plus de puissance de calcul que les microcontrôleurs et sont équipés de périphériques de plus haut niveau.

    Moins puissants que les ordinateurs avec carte mère, ils présentent néanmoins de nombreux avantages :

    * Faible consommation énergétique : composants plus proches, donc moins de câblage
    * Faible coût de production : l’automatisation est favorisée par la compacité du système
    * Plus grande sécurité : la conception globale (matériel et micrologiciel) interdit l’ajout ou l’échange de composants.

!!! info "Les inconvénients"

    En revanche, il devient impossible de remplacer un composant défectueux (mémoire, périphérique, …) , comme on le ferait sur la carte mère.

## II. Fonctionnement du SoC

Les systèmes sur puce ne suivent pas l’architecture de Harvard : les programmes peuvent être chargés dans la même mémoire que les données, respectant ainsi le modèle de **Von Neumann**.


Leurs processeurs sont cadencés à une fréquence élevée (plusieurs gigahertz). Cela a plusieurs conséquences :

* les échanges entre les processeurs et les périphériques , nettement plus lents, imposent l’emploi d’un bus séparé, afin qu’ils ne deviennent pas des goulots d’étranglement en obligeant le CPU d’attendre une réponse. Les SoC sont donc moins adaptés pour des applications en temps réel.
* le dialogue entres les processeurs et la mémoire passe par un autre bus à haute performance
* le dialogue entre les périphériques et la mémoire passent par un dispositif d’accès direct (DMA – Direct Memory Access), qui permet des échanges sans passer par le CPU.

De plus les échanges entre les processeurs et la RAM, plus lente, sont optimisés à l’aide de dispositifs de mémoire cache.

Voici le schéma du circuit d'un SoC :

![schema soc](images/schema_SOC.gif){ width=70%; : .center }

Vous pouvez remarquer que l'on retrouve bien sur ce schéma, un CPU et de la mémoire

!!! info "Performances"

    Les performance des CPU embarquées dans les SoC sont devenus quasiment comparables à celles de processeurs usuels des ordinateurs d'entrée et de moyen de gamme.

???+ question "Vocabulaire"

    **1.** Qu'est-ce que la CPU?  

    ??? success "Solution"

        La CPU est le processeur, l'unité centrale de traitement : «Central Processing Unit» (CPU). C'est le cœur du Soc.

    **2.** Qu'est-ce que la GPU?

    ??? success "Solution"

        La GPU est l'unité graphique de traitement : «Graphics Processing Unit» (GPU).

        C'est est un élément crucial car c’est lui qui est en charge de calculer les images afin de pouvoir les afficher à l’écran.

    **3.** Qu'est-ce que la ROM? 

    ??? success "Solution"

        La ROM (Read Only Memory) est une mémoire "morte" permanente.

        La ROM est une mémoire non volatile qui stocke des instructions pour votre ordinateur de manière permanente.

    **4.** Qu'est-ce que la RAM? 

    ??? success "Solution"

        La RAM (Random Access Memory) est une mémoire "vive" volatile.

        La RAM est une mémoire volatile qui stocke temporairement les fichiers sur lesquels vous travaillez.

    **5.** Qu'est-ce que la mémoire Cache? 

    ??? success "Solution"

        La mémoire cache est une mémoire plus rapide et plus proche du matériel informatique (processeur, disque dur) auquel elle sert des données et des instructions. Son rôle est de stocker les informations les plus fréquemment utilisées par les logiciels et applications lorsqu'ils sont actifs. C'est cet accès direct qui détermine les performances d'un programme car il économise des échanges incessants entre le processeur et la mémoire vive, la RAM (random access memory).


!!! info "Tâches particulières"

    Au sein du CPU, on peut rencontrer des circuits dédiés à des tâches particulières :

    * Calcul en virgule flottante,
    * Calcul matriciel pour algorithmes d’apprentissages,
    * Cryptographie : dédiés au chiffrage des données,
    * Reconnaissance (faciale, digitale, … ),
    * Réalité augmentée,
    * Analyse temps réel d’image pour appareils photo.

!!! info "3 autres unités"

    Outre le CPU lui même, et son cache, 3 unités sont présentes, très proches du CPU, pour effectuer, de façon optimisées, des tâches dédiées:

    * l'unité FPU Floating Point Unit : chargée des calculs numériques
    * l'unité ML Machine Learning : chargée des calculs avec des matrices très utilisée par les applications d'intelligences artificielles.
    * l'unité Crypto : dédiée à la sécurité c'est à dire au chiffrement / déchiffrement.

!!! info "Affichage"  

    Pour toutes les tâches d'affichage, et via le bus haute performance, le CPU fait appel au GPU (Graphical Processing Unit)

!!! info "Les périphériques lents" 

    Les applications utilisent un certain nombre de périphériques qui sont également intégrés sur le SoC:

    * GPS
    * WiFi
    * Capteurs (lumières, image..)
    * Modems (2G/3G/4G)
    * Ethernet
    * Bluetooth
    * Audio
    * HDMI
    * DSP : Digital Signal Processing, éléments dédiés au traitement de signal

    Ces périphériques sont lents, et ils peuvent communiquer via un bus plus lent. Ce bus peut acheminer les données vers le bus haute performance via un pont.

    Pour améliorer encore les performances, les SoC disposent, comme les cartes mères d'ordinateur, d'un système DMA (Direct Memory Access). Les périphériques lents peuvent écrire directement dans cette mémoire DMA sans intervention du CPU. Le DMA se charge ensuite d'avertir le CPU lorsque ces transferts sont réalisés et communique les informations au CPU via le bus haute performance. 

## III. Bilan  

!!! info "Avantages des SoC" 

    Les SoC ont de nombreux avantages par rapport aux systèmes "classiques" (carte mère + CPU + carte graphique...) :

    * la vitesse de traitement et donc l'efficacité est améliorée
    * les SoC consomment beaucoup moins d'énergie qu'un système classique (à puissance de calcul équivalente).
    * Cette consommation réduite permet dans la plupart des cas de s'affranchir de la présence de système de refroidissement actif comme les ventilateurs (voir l'image du PC ci-dessus). Un système équipé de SoC est donc silencieux.
    * Vu les distances réduites entre, par exemple, le CPU et la mémoire, les données circulent beaucoup plus vite, ce qui permet d'améliorer les performances. En effet, dans les systèmes "classiques" les bus sont souvent des "goulots d'étranglement" en termes de performances à cause de la vitesse de circulation des données.
    * Coûts plus importants dans la phase de conception, mais moins importants dans la phase de production : moins de matières premières et fabrication moins onéreuse.
    * Forte miniaturisation (indispensable pour la mobilité - tablettes, smartphones..)

!!! example "Exemple : Rasberry Pi"

    On trouve aussi ce système de SoC sur des nano-ordinateurs comme le Rasberry Pi :

    <div class="centrer">
              <img src="https://s1.qwant.com/thumbr/0x380/b/e/675bb8324c9a405935f6cf41947dc04928eb81ac6b613f377dbac44da87d66/Raspberry-Pi-4-Model-B-Large.jpg?u=https%3A%2F%2Fwww.cnx-software.com%2Fwp-content%2Fuploads%2F2019%2F06%2FRaspberry-Pi-4-Model-B-Large.jpg&q=0&b=1&p=0&a=1" alt="Rasberry Pi version 4"/>
              <figcaption>Raspberry Pi version 4</figcaption>
    </div>

    Dans sa version 4, le Rasberry Pi est équipé du Soc BCM2711 de la société Broadcom

!!! example "Exemple : SoC Exynos 990 qui équipe les Galaxy_S20"

    ![S20](images/SoC_S20.png){ width=40%; : .center }

    On y trouve:

    * **Le processeur** (CPU)
    * **La carte graphique** (GPU)
    * **La puce neuronale ou Neutral Processing Unit** (NPU), est une puce en charge de l’intelligence artificielle de votre smartphone.
    * **Le modem** qui gère non seulement le Wifi, Bluetooth, le NFC ou bien encore les technologies mobiles. C’est-à-dire la 4G, ou plus récemment la 5G mais également de plus vieux réseaux tels que la 3G.
    * **Le processeur de signal numérique ou Digital Signal Processor** (DSP) est en charge de traiter les signaux numériques. Ainsi, il va permettre le filtrage, la compression ou encore l’extraction de différents signaux tels que la musique ou encore une vidéo.
    * **Le processeur d’image ou Image Signal Processor** (ISP) est une puce prenant en charge la création d’images numériques. Dans la réalité et de par leurs tailles minuscules, les capteurs photo de nos smartphones sont mauvais. La qualité qu’il est actuellement possible d’obtenir est intimement liée à cette puce. En effet, c’est grâce à elle que votre smartphone va traiter la prise et la création de votre photo.
    * **Le processeur sécurité ou Secure Processing Unit** (SPU) est le bouclier de votre smartphone. Son alimentation électrique est indépendante afin de ne pas pouvoir être éteint en cas d’attaque sur celui-ci. Le SPU est d’une importance capitale, celui-ci va stocker vos données biométriques, bancaires, votre SIM ou encore vos titres de transport. C’est lui qui contient les clés de chiffrement de vos données.

    👉 N'oublions pas les différentes choses qui ne sont pas représentée sur le schéma, la mémoire, comme La mémoire LPDDR (en anglais : Low Power Double Data Rate, littéralement : « Vitesse de données double à faible consommation »), ou comme l'UFS (pour Universal Flash Storage) qui est un standard de mémoire flash ... 

## IV. Exercices

### 🔎 À faire vous-même 1

Faites des recherches sur le SoC BCM2711, notez les principales caractéristiques de ce SoC.

Voici les noms des SoC utilisés par quelques produits succès :

* Apple iPhone 11 : SoC A13 Bionic
* Samsung Galaxy S11 : SoC Exynos 990
* Nintendo Switch : SoC Nvidia Tegra

Rien qu'à l'évocation des noms cités ci-dessus, vous vous doutez bien que le marché des SoC a aujourd'hui un poids économique très important. On commence même à trouver des ordinateurs portables équipés de SoC à la place des cartes mères "classiques". Les SoC commencent doucement, mais surement à rattraper les systèmes "classiques" en termes de puissance (pour les systèmes "classiques" d'entrées et de moyennes gammes).


### 🔎 À faire vous-même 2

Quel est le nom du SoC de votre smartphone ? Quelles en sont les fonctionnalités ?

### 🔎 À faire vous-même 3

Source : Lycée La Martinière Diderot, Lyon

[Raspberry](http://portail.lyc-la-martiniere-diderot.ac-lyon.fr/srv1/co/Div_Archi_Ordinateur_4.html){ .md-button target="_blank" rel="noopener" }

## V. QCM

{{ multi_qcm(
  ["Dans l’architecture générale de Von Neumann, la partie qui a pour rôle d’effectuer les opérations de base
est", ["l’unité de contrôle", "la mémoire", "l’unité arithmétique et logique", "les dispositifs d’entrée-sortie"], [3]],
  ["Parmi tous les registres internes que possède une architecture mono-processeur, il en existe un appelé compteur ordinal program counter. Quel est le rôle de ce registre ?", ["il contient l’adresse mémoire de la prochaine instruction à exécuter", "il contient le nombre d’instructions contenues dans le programme", "il contient l’adresse mémoire de l’opérande à récupérer", "il contient le nombre d’opérandes utilisés"], [1]],
  [" Dans le modèle d'architecture de Von Neumann, quel est le rôle de l'unité de contrôle ?", ["Gérer le séquençage (l'ordre) des opérations", "Effectuer les opérations", " Lire les données", "Stocker les données"], [1]],  
  ["Dans le modèle d'architecture de Von Neumann, que signifie UAL ?", ["Unité Active de Lecture", "Unité Additive et Logique", "Unité Arithmétique et Logique", "Unité Active et logique"], [3]],
  ["Quel est le rôle de l'unité arithmétique et logique dans un ordinateur ?", ["Afficher les informations", "Définir l'ordre des opérations", "Effectuer les opérations", "Stocker les informations"], [3]],
  ["Quel appareil ne possède généralement pas de SoC ?", ["Une tablette", "Un smartphone", "Un ordinateur", "Une console de jeu"], [3]],
  ["Les données et les programmes sont stockées dans :", ["L’unité logique et arithmétique", "La mémoire", "Les entrées / sorties", "L'unité de contrôle"], [2]],
  ["Les avantages des SoC : ", ["Plus puissants que des ordinateurs", "Il est plus facile de remplacer un composant défectueux ", "Faible coût de production", "Faible consommation énergétique "], [3, 4], {'multi':True}],
multi = False,
qcm_title = "Les SoC",
shuffle = True,

)}}

## VI. Crédits

Auteurs : Mireille Coilhac, Valérie Mousseaux, Jean-Louis Thirot , sur la base du travail de :

* David Roche https://pixees.fr/informatiquelycee/n_site/nsi_term_archi_soc.html
* NSI Terminale, Balabonski et al., - Ellipses
* http://www.monlyceenumerique.fr/nsi_terminale/arse/a1_systeme_%20sur%20_puce.html
* https://nsimeyroneinc.github.io/NSITerm/Archi_Materielle/T3_5_systeme_sur_puce/