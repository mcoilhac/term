---
author: Mireille Coilhac, Gilles Lassus
title: Compléments listes - piles - files
---

## I. Les listes chainées

Lorsque l'implémentation de la liste fait apparaître une chaîne de valeurs, chacune pointant vers la suivante, on dit que la liste est une liste **chaînée**.

![liste chainée](images/liste_chainee.png){ width=60%; : .center }

!!! info "Implémentation choisie "

	- Une liste est caractérisée par un ensemble de cellules.
	- Le lien (on dira souvent le «pointeur») de la variable est un lien vers la première cellule, qui renverra elle-même sur la deuxième, etc.
	- Chaque cellule contient donc une valeur et un lien vers la cellule suivante.
	- Une liste peut être vide (la liste vide est notée ```x``` ou bien ```None``` sur les schémas)

	Une conséquence de cette implémentation sous forme de liste chaînée est la non-constance du temps d'accès à un élément de liste : pour accéder au 3ème élément, il faut obligatoirement passer par les deux précédents.

!!! info "À retenir "

	Dans une liste chaînée, le temps d'accès aux éléments n'est pas constant.


### Exemple d'implémentation minimale d'une liste chaînée

!!! note "Exemple fondateur : implémentation d'une **liste chainée** en POO :heart:"

    ```python linenums='1' title=""
    class Cellule :
        def __init__(self, contenu, suivante):
            self.contenu = contenu
            self.suivante = suivante
    ```

Cette implémentation rudimentaire permet bien la création d'une liste :


```python title=""
>>> lst = Cellule(3, Cellule(5, Cellule(1,None)))
```

La liste créée est donc :  ![3-5-1](images/ex1.png){ width=10% }

Mais plus précisément, on a :

![ex2](images/ex2.png){ width=50%; : .center }


???+ question "Exercice"

    === "Énoncé"
        Retrouvez comment accéder aux éléments 3, 5 et 1.

    === "Correction"

        ```python title=""
        >>> lst.contenu
        3
        >>> lst.suivante.contenu
        5
        >>> lst.suivante.suivante.contenu
        1
        ``` 

On pourra remarquer que l'interface proposée à l'utilisateur n'est pas des plus pratiques...

## II. Implémenter une pile avec une liste chaînée

???+ question "La classe Pile"
	
	Il est impératif de comprendre qu'on peut choisir n'importe quelle implémentation de la classe Pile.
	Il suffit de connaître l'**interface**. 

	Par exemple, on choisit celle avec la liste chaînée.

    Comprendre, puis tester ci-dessous

    {{ IDE('scripts/classe_pile_liste_chainee', MAX_SIZE=55) }}

## Crédits 

Auteur des listes chainées : Gilles Lassus