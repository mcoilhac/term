class File :
    def __init__(self):
        self.contenu = []

    def est_vide_file(self) :
        return self.contenu == []

    def enfiler(self, x):
        self.contenu = [x] + self.contenu

    def defiler(self):
        assert not self.est_vide_file(), "la file est vide"
        return self.contenu.pop()

    def __str__(self) :
        return 'enfilage -> '+str(self.contenu) + ' -> défilage'

