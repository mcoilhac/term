# --- PYODIDE:env --- #

class Pile :
    def __init__(self):
        self.contenu = []

    def est_vide_pile(self) :
        return self.contenu == []

    def empiler(self, x):
        self.contenu.append(x)

    def depiler(self):
        assert not self.est_vide_pile(), "la pile est vide"
        return self.contenu.pop()

    def __str__(self) : # ceci n'est pas une primitive mais utile pour voir la pile
        return str(self.contenu) + " <- sommet"

# --- PYODIDE:code --- #
# À vous de jouer
