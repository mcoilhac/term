# --- PYODIDE:env --- #

def Vide():
    """
    Création d'une liste vide
    Exemple :

    >>> liste = Vide()
    >>> liste
    []

    """
    return []

def Liste(x, liste):
    """
    Cette fonction est un constructeur. Elle ajoute x en tête de liste
    Précondition : x est de n'importe quel type
    Postcondition : la fonction renvoie un type abstrait LISTE

    Exemples :

    >>> liste_1 = Vide()
    >>> liste_2 = Liste(1, liste_1)
    >>> liste_3 = Liste(2, liste_2)
    >>> liste_3
    [2, 1]

    """
    return [x] + liste

def est_vide(liste):
    """
    Cette fonction renvoie True si la liste est vide, et False sinon
    Précondition : liste est de typr abstrait LISTE
    Postcondition : cette fonction renvoie un type booléen.
    Exemples :

    >>> liste_1 = Vide()
    >>> est_vide(liste_1)
    True
    >>> liste_2 = Liste(1, liste_1)
    >>> est_vide(liste_2)
    False

    """
    return liste == []

def tete(liste):

    """
    Cette fonction sélectionne la tête de la liste
    Précondition : liste est du type abstrait LISTE et n'est pas vide.
    Postcondition : Cette fonction renvoie une variable du type de celle qui est
    en tête de liste
    Exemple :

    >>> liste_1 = Vide()
    >>> liste_2 = Liste(1, liste_1)
    >>> liste_3 = Liste(2, liste_2)
    >>> tete(liste_3)
    2

    """
    assert not est_vide(liste), "une liste vide n'a pas de tete"
    return liste[0]

def queue(liste):
    """
    Cette fonction sélectionne la queue de la liste
    Précondition : liste est du type abstrait LISTE et n'est pas vide.
    Postcondition : Cette fonction renvoie un type abstrait liste
    Exemples :

    >>> liste_1 = Vide()
    >>> liste_1 = Liste(1, liste_1)
    >>> liste_1 = Liste(2, liste_1)
    >>> liste_1 = Liste(3, liste_1)
    >>> queue(liste_1)
    [2, 1]
    >>> liste_2 = Vide()
    >>> liste_2 = Liste("a", liste_2)
    >>> queue(liste_2)
    []

    """
    assert not est_vide(liste), "une liste vide n'a pas de queue"
    return liste[1:]

# --- PYODIDE:code --- #

def longueur_rec(liste):
    """
    Cette fonction renvoie la longueur de la liste de type LISTE
    Précondition : liste est du type abstrait liste
    Postcondition : Cette fonction renvoie un entier
    Exemple :
    >>> liste_1 = Vide()
    >>> liste_2 = Liste(1, liste_1)
    >>> liste_2 = Liste(2, liste_2)
    >>> longueur_rec(liste_1)
    0
    >>> longueur_rec(liste_2)
    2

    """
    ...


# Tests
liste_1 = Vide()
liste_2 = Liste(1, liste_1)
liste_2 = Liste(2, liste_2)
assert longueur_rec(liste_1) == 0
assert longueur_rec(liste_2) == 2
