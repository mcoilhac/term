# --- PYODIDE:env --- #

class Pile :
    def __init__(self):
        self.contenu = []

    def est_vide_pile(self) :
        return self.contenu == []

    def empiler(self, x):
        self.contenu.append(x)

    def depiler(self):
        assert not self.est_vide_pile(), "la pile est vide"
        return self.contenu.pop()

    def __str__(self) : # ceci n'est pas une primitive mais utile pour voir la pile
        return str(self.contenu) + " <- sommet"

# --- PYODIDE:code --- #

pile_1 = Pile()
pile_1.empiler("a")
pile_1.empiler("b")
print("Au début", pile_1)
print("Taille de la pile : ", taille_essai(pile_1))
print("A la fin", pile_1)


