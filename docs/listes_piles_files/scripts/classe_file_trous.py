class File_a_trous :
    def __init__(self):
        self.contenu = []

    def est_vide_file(self) :
        ...

    def enfiler(self, x):
        ...

    def defiler(self):
        ...

    def __str__(self) :
        return 'enfilage -> '+str(self.contenu) + ' -> défilage'

# Tests
file_1 = File_a_trous()
file_1.enfiler("a")
print(file_1)
file_1.enfiler("b")
print(file_1)