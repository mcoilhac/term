# --- PYODIDE:env --- #

class Pile :
    def __init__(self):
        self.contenu = []

    def est_vide_pile(self) :
        return self.contenu == []

    def empiler(self, x):
        self.contenu.append(x)

    def depiler(self):
        assert not self.est_vide_pile(), "la pile est vide"
        return self.contenu.pop()

    def __str__(self) : # ceci n'est pas une primitive mais utile pour voir la pile
        return str(self.contenu) + " <- sommet"

# --- PYODIDE:code --- #

def taille(pile):
    ...

pile_1 = Pile()
pile_1.empiler("a")
pile_1.empiler("b")
print("Au début", pile_1)
print("Taille de la pile : ", taille(pile_1))
print("A la fin", pile_1)

# --- PYODIDE:corr --- #
def taille(pile):
    pile_2 = Pile()
    resultat = 0
    while not pile.est_vide_pile():
        pile_2.empiler(pile.depiler())
        resultat = resultat + 1
    while not pile_2.est_vide_pile():
        pile.empiler(pile_2.depiler())
    return resultat

# --- PYODIDE:tests --- #
# Tests
assert taille(pile_1) == 2


# --- PYODIDE:secrets --- #
# Autres tests
pile_1.empiler("c")
assert taille(pile_1) == 3
pile_3 = Pile()
assert taille(pile_3) == 0


