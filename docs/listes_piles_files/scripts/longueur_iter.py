def longueur(liste):
    """
    Cette fonction renvoie la longueur de la liste de type LISTE
    Précondition : liste est du type abstrait liste
    Postcondition : Cette fonction renvoie un entier
    Exemple :
    >>> liste_1 = Vide()
    >>> liste_2 = Liste(1, liste_1)
    >>> liste_2 = Liste(2, liste_2)
    >>> longueur(liste_1)
    0
    >>> longueur(liste_2)
    2

    """
    cpt = 0
    while not est_vide(liste):
        liste = queue(liste)
        cpt = cpt + 1
    return cpt
