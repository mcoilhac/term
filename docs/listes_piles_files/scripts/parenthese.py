# --- PYODIDE:env --- #

class Pile :
    def __init__(self):
        self.contenu = []

    def est_vide_pile(self) :
        return self.contenu == []

    def empiler(self, x):
        self.contenu.append(x)

    def depiler(self):
        assert not self.est_vide_pile(), "la pile est vide"
        return self.contenu.pop()

    def __str__(self) : # ceci n'est pas une primitive mais utile pour voir la pile
        return str(self.contenu) + " <- sommet"

# --- PYODIDE:code --- #

def verification(expr_math):
    """
    Cette fonction renvoie True si une expression mathématique a autant de
    parenthèses ouvrantes que de parenthèses fermantes.
    Précondition : expr_math est de type chaîne de caractères.
    Postcondition : la valeur renvoyée est de type booléen.
    exemples :

    >>> verification("(3*(2-5x)+3)")
    True
    >>> verification("(3*(2-5x)+3")
    False
    >>> verification("")
    True

    """
    ...

# --- PYODIDE:corr --- #

def verification(expr_math):
    """
    Cette fonction renvoie True si une expression mathématique a autant de
    parenthèses ouvrantes que de parenthèses fermantes.
    Précondition : expr_math est de type chaîne de caractères.
    Postcondition : la valeur renvoyée est de type booléen.
    exemples :

    >>> verification("(3*(2-5x)+3)")
    True
    >>> verification("(3*(2-5x)+3")
    False
    >>> verification("")
    True

    """
    p = Pile()
    for car in expr_math:
        if car == '(':
            p.empiler(car)
        if car == ')':
            if p.est_vide_pile():
                return False
            else:
                p.depiler()
    return p.est_vide_pile()

# --- PYODIDE:tests --- #

assert verification("(3)*(2-5x)+3)") is False
assert verification("(3)*(2-5x)+3") is True

# --- PYODIDE:secrets --- #

assert verification("(3)*(2+6x)*(2-5x)+3)") is False
assert verification("(3)*(2-5(2+x)x)+3") is True



