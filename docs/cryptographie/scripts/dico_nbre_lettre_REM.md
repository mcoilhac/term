😊 On peut aussi créer le dictionnaire en compréhension :

```python
def cree_nombre_lettre():
    return {i: chr(97 + i) for i in range(26)}
```
