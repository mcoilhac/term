# --- PYODIDE:code --- #

def cree_nombre_lettre():
    ...

nombre_lettre = cree_nombre_lettre()


# --- PYODIDE:corr --- #

def cree_nombre_lettre():
    dico = {}
    for i in range(26):
        dico[i]= chr(97 + i)
    return dico


# --- PYODIDE:tests --- #

assert nombre_lettre[0] == "a"



# --- PYODIDE:secrets --- #

assert nombre_lettre == {0: 'a', 1: 'b', 2: 'c', 3: 'd', 4: 'e', 5: 'f', 6: 'g', 7: 'h', 8: 'i', 9: 'j', 10: 'k', 11: 'l', 12: 'm', 13:
'n', 14: 'o', 15: 'p', 16: 'q', 17: 'r', 18: 's', 19: 't', 20: 'u', 21: 'v', 22: 'w', 23: 'x', 24: 'y', 25: 'z'}

assert nombre_lettre[1] == "b"