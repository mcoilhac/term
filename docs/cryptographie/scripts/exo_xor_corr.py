masque = "CETTEPHRASEESTVRAIMENTTRESTRESLONGUEMAISCESTFAITEXPRES"

def chiffre(message, masque):
    message_chiffre = ""
    for i in range(len(message)):
        lettre_chiffree = chr(ord(message[i]) ^ ord(masque[i]))
        message_chiffre = message_chiffre + lettre_chiffree
    return message_chiffre
