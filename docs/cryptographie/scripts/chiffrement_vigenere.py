# --- PYODIDE:env --- #

def cree_lettre_nombre() :
    dico = {}
    for i in range(26):
        dico[chr(i + 97)] = i
    return dico

def cree_nombre_lettre():
    dico = {}
    for i in range(26):
        dico[i]= chr(97 + i)
    return dico

# --- PYODIDE:code --- #

def chiffre_vigenere(texte: str, clef: str ) -> str :
    """
    Préconditions :
    - texte est le texte à chiffrer (type str)
    - clef est la clef (type str)
    Postconditions :
    la fonction renvoie le texte chiffré : type str.
    Par exemple chiffre_vigenere('incroyable','nsi') renvoie 'vfkeggnttr'
    """

    ...

nombre_lettre = cree_nombre_lettre() # nombre_lettre = {0: 'a', 1: 'b', 2: 'c',
                                     # 3: 'd', etc...}
lettre_nombre = cree_lettre_nombre() # lettre_nombre = {'a': 0, 'b': 1, 'c': 2,
                                     # 'd': 3, etc...}

# --- PYODIDE:corr --- #

def chiffre_vigenere(texte: str, clef: str ) -> str :
    texte_chiffre = ""
    for i in range(len(texte)) :
        lettre = texte[i]
        indice_clef = i % len(clef)
        lettre_decalage = clef[indice_clef]
        decalage = lettre_nombre[lettre_decalage]
        nombre = (lettre_nombre[lettre] + decalage) % 26
        lettre_chiffree = nombre_lettre[nombre]
        texte_chiffre = texte_chiffre  + lettre_chiffree
    return texte_chiffre

# --- PYODIDE:tests --- #

assert chiffre_vigenere('incroyable','nsi') == 'vfkeggnttr'
assert chiffre_vigenere('incroyable','a') == 'incroyable'

# --- PYODIDE:secrets --- #

assert chiffre_vigenere('hello','nsi') == "uwtyg"