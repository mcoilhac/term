a = 97
n = 110
# On chiffre a
a_chiffre = 97 ^ 110
print("a chiffré avec xor 110 : ", a_chiffre)

# On déchiffre a_chiffre
a_dechiffre = a_chiffre ^ n
print("a_chiffre déchiffré avec xor 110 : ", a_dechiffre)

