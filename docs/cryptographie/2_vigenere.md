---
author: Mireille Coilhac
title: Chiffrement de Vigenère
---

## I. Le principe

<a title="Thomas de Leu
, Public domain, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Vigenere.jpg"><img width="256" alt="Vigenere" src="https://upload.wikimedia.org/wikipedia/commons/1/1a/Vigenere.jpg"></a>


!!! info "Un exemple de chiffrement symétrique"

	Le chiffre de Vigenère a été proposé il y a un peu moins de 500 ans, et utilise la même idée que le chiffrement de César, mais en la complexifiant beaucoup.
	

??? note "Avant de démarrer"

    Vous pouvez commencer par résoudre l'exercice suivant :

    [Le chiffre de César](https://codex.forge.apps.education.fr/exercices/code_cesar/){ .md-button target="_blank" rel="noopener" }


!!! info "Mon info"

	Le chiffre de Vigenère a été proposé il y a un peu moins de 500 ans. C'est un système de chiffrement par substitution qui consiste à coder un texte à l’aide d’une clé donnée cette fois sous forme de texte (plus court généralement) : la première lettre du texte à coder est décalée d'un entier correspondant au premier caractère de la clé et ainsi de suite. Si la clé est plus courte que le texte à coder, elle est répétée..

	La correspondance entre les lettres et les nombres associés est donnée dans le tableau suivant :

	| a | b | c |  d|  e|  f|  g|  h|  i|  j|  k|  l|  m|  n|  o|  p|  q|  r|  s|  t|  u|  v|  w|  x|  y|  z|
	|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|
	|0  | 1 |2  |3  | 4 |5  | 6 | 7 | 8 |  9| 10| 11| 12|13 | 14| 15| 16|17 |18 | 19| 20| 21| 22| 23| 24| 25|


!!! example "Exemple"

    Schématisons cela. Nous allons chiffrer le mot **incroyable** avec la clef **nsi**.  
    On répète la clef pour créer une clef de même longueur que le texte à chiffrer :

    | i | n | c | r |  o| y |  a|  b|  l|  e| 
	|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|
	|n  | s |i  |n  | s |i  |n  | s |i  |n  | 

	En utilisant le tableau précédant, nous obtenons donc :

	|lettre| i | n | c | r |  o| y |  a|  b|  l|  e|  
	|:--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|
	|entier associé|8|13|2|17|14|24|0|1|11|4|
	|décalage à effectuer|13  | 18 |8  | 13 |18  |8  | 13 |18  |8  | 13|
	|entier pour chiffrement|21|31|10|30|32|32|13|19|19|17|

	Nous observons que l'on peut obtenir un nombre supérieur à 25. On trouve alors la lettre correspondante en "bouclant" sur l'alphabet. 26 correspond à la lettre "a", 27 à "b" etc. Pour trouver la lettre associée à un enier `n`, il suffit de regarder la lettre associée à l'entier `n % 26`

	Nous obtenons donc : 

	|lettre| i | n | c | r |  o| y |  a|  b|  l|  e|  
	|:--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|--:|
	|entier associé|8|13|2|17|14|24|0|1|11|4|
	|décalage à effectuer|13  | 18 |8  | 13 |18  |8  | 13 |18  |8  | 13|
	|entier pour chiffrement|21|5|10|4|6|6|13|19|19|17|
	|lettre chiffrée|v|f|k|e|g|g|n|t|r|

	Remarquons que des lettres différentes peuvent être chiffrées par la même, ce qui rend le décryptage difficile.

## II. Exercices

???+ question "correspondance nombre - lettre"

	Compléter le script suivant qui permet de constituer le dictionnaire `nombre_lettre`dont voici un extrait : 

	`nombre_lettre = {0: 'a', 1: 'b', 2: 'c', 3: 'd', etc...}`

	{{IDE('scripts/dico_nbre_lettre')}}



???+ question "correspondance lettre - nombre"

	Compléter le script suivant qui permet de constituer le dictionnaire `lettre_nombre`dont voici un extrait :  

	`lettre_nombre = {'a': 0, 'b': 1, 'c': 2, 'd': 3, etc...}`

	{{IDE('scripts/dico_lettre_nbre')}}



???+ question "Chiffrement de Vigenère"

	Compléter le script suivant :

	{{IDE('scripts/chiffrement_vigenere')}}


