---
author: Franck Chambon, Mireille Coilhac, Valérie Mousseaux, Jean-Louis Thirot
title: Exercices - série 2
---

## Exercice 1 : recherche dichotomique

???+ question "Recherche dichotomique"

    Bob a voulu écrire une version récursive de la recherche par dichotomie.  
	Malheureusement son code ne fonctionne pas. Corrigez-le.

    {{IDE('scripts/dicho_bob')}}

## Exercice 2 : parité

???+ question "Parité"

    Compléter la fonction suivante qui doit être une fonction récursive.

    ⚠️ Il est **interdit** d'utiliser `%`

    {{IDE('scripts/parite')}}

??? tip "Indice pour la parité"

    `n` et `n - 2` ont la même parité (tous les deux pairs, ou tous les deux impairs)
	
## Exercice 3 : longueur d'une liste

???+ question "Longueur de liste"

    Compléter la fonction suivante qui doit être une fonction récursive.

    ⚠️ Il est **interdit** d'utiliser `len`

    {{IDE('scripts/longueur')}}


??? tip "Indice pour la longueur d'une liste"

    Vous pouvez utiliser `.pop()`



## Exercice 4 : bégaiement

???+ question "bégaiement"

    Ecrire une fonction récursive `repeat` qui prendra en argument un entier `n` et une chaine de caractère `txt`, et qui renvoie une chaine de caractères qui contient n fois la chaine de caractères `txt` juxtaposées.

	Exemple: `repeat(5, "bla")` renvoie `blablablablabla`

    {{IDE('scripts/begue')}}


## Exercice 5 : La tête à Toto

???+ question "La tête à Toto"

    Compléter la fonction récursive `toto` telle que :

	* `toto(0)` renvoie `'0'`
	* `toto(1)` renvoie `'(0 + 0)'`
	* `toto(2)` renvoie `'((0 + 0) + (0 + 0))'`
	* etc sur le même principe

	On garantit que `n` est un entier positif ou nul.
    
    Compléter ci-dessous

    {{IDE('scripts/toto')}}


## Exercice 6 : Test de palindrome

!!! info "Palindrome"

	Un mot est un palindrome s'il se lit de la même façon de droite à gauche comme de gauche à droite.

!!! question "Exercice"
    Écrire une fonction récursive `est_palindrome` qui prend en paramètre une chaine de caractères `mot` et renvoie le booléen `True` si `mot` est un palindrome ou `False` sinon.


??? tip "Premiers indices"

    * Le premier caractère de la chaine `mot` est `#!py mot[0]`
    * Le dernier caractère de la chaine `mot` est `#!py mot[-1]`
    * Pour une copie d'une tranche ("slice" en anglais) du second jusqu'à l'avant-dernier caractère d'une chaine `mot`, on peut écrire `#!py mot[1:-1]`

??? tip "Derniers indices"

    * Si la chaine fait zéro ou un caractère, c'est un palindrome.
    * Sinon on peut comparer le premier et le dernier caractère.
    * Enfin, un appel récursif permet de répondre sur le reste de la chaine.

???+ question "Palindrome"

    Compléter ci-dessous

    {{ IDE('scripts/palindrome') }}

## Exercice 7 : triangles

!!! question "A faire"

    Écrire deux fonctions (procédures) récursives `triangle_bas`, puis `triangle_haut` prenant un entier `n` non nul en paramètre et qui **affichent** un triangle. Ces fonctions ne renvoient rien.

    ```pycon
    >>> triangle_bas(4)  # affiche un triangle tête en bas
    ####
    ###
    ##
    #
    ```

    ```pycon
    >>> triangle_haut(4)  # affiche un triangle tête en haut
    #
    ##
    ###
    ####
    ```

???+ question "Triangles"

    Compléter ci-dessous

    {{ IDE('scripts/triangle') }}

    ??? success "Solution"

        !!! info "Attention"

            L'ordre des instructions est bien sûr important !

        ```python
        def triangle_bas(n):
            """Affiche un triangle tête en bas"""
            if n > 0:
                print("#" * n)
                triangle_bas(n - 1)

        def triangle_haut(n):
            """Affiche un triangle tête en haut"""
            if n > 0:
                triangle_haut(n - 1)
                print("#" * n)
        ```

## Exercice 8 : Si vous avez du temps ... problème de la grenouille

???+ question "🐸"

    Une grenouille doit monter un escalier. Quand elle saute pour monter, elle monte de 1 ou 2 marches.  
    Combien de chemins différents existent-ils pour un escalier de n marches ?

    Vous êtes en complète autonomie pour cet exercice facultatif.

    👉 Montrer au professeur votre solution, ou vos tentatives.

    La fonction `nbre_chemins` prend en paramètre un entier `n` que l'on garantit strictement supérieur à zéro.

    Compléter ci-dessous

    {{ IDE('scripts/grenouille', MAX=20) }}







## Crédits

Franck Chambon



