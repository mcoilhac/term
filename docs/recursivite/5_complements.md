---
author: Mireille Coilhac
title: Compléments
---

## I. Un problème célèbre : les tours de Hanoï

<iframe width="560" height="315" src="https://www.youtube.com/embed/rOnRbPKvGQg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## II. Si vous avez suivi la spécialité maths en 1ère

???+ question "Suite définie par récurrence"

	Vous avez étudié les suites définies par récurrence.

	Par exemple :  
	Soit la suite $(u)$ définie par $u_0=5$ et $u_{n+1}=3u_n+2$

	Vous voulez un moyen de déterminer directement $u_{50}$ par exemple, ou d'une manière générale n'importe quel $u_n$, pour un entier $n$ donné.

	Pour cela il est très intuitif d'utiliser une fonction récursive.

	Compléter ci-dessous (être patient, l'exécution des tests prend un peu de temps à la validation)

	{{IDE('scripts/recurrence')}}


III. Approfondissement (au-delà du programme NSI)

Dans le lien proposé ci-dessous  :

* **ne pas lire** dans la rubrique mécanismes (il y des erreurs dans ce paragraphe): ⁉️ Traceback - Récursivité 
* **ne pas lire** dans la rubrique mécanismes : mauvaises pratiques

[Récursivité par Franck Chambon](https://pratique.forge.apps.education.fr/recursivite/){ .md-button target="_blank" rel="noopener" }
