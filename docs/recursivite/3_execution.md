---
author: Mireille Coilhac, Valérie Mousseaux, Jean-Louis Thirot
title: Exécution d'une fonction récursive
---

## Le principe

Il est important de comprendre que chaque appel récursif met « en pause » l'exécution en cours, en attente d'obtenir le résultat qui est déterminé par l'appel suivant. Concrètement :

* Les appels sont tour à tour mis « en pause » jusqu'au dernier appel qui fourni un résultat. On appelle cela le dépliage (ou la descente).

* Ce résultat est ensuite transmis à l'appel précédent qui l'utilise pour calculer son propre résultat et le transmettre à l'appel précédent, et ainsi de suite jusqu'au premier appel qui peut alors calculer le résultat final. On appelle cela l'évaluation (ou la remontée).

!!! example "Exemple"

	Nous allons étudier la fonction récursive (naïve) `deux_puissance` qui prend en paramètre un entier positif `n` et qui renvoie la valeur de $2^n$

	Ecriture de la fonction récursive

	* Le cas de base correspond à n = 0 et dans ce cas $2^n=2^0=1$
	* Sinon, on peut calculer $2^n$ en calculant $2 \times 2^{n-1}$

	👉 on sait donc comment passer du calcul de $2^n$ à celui de $2^{n-1}$ pour notre appel récursif et on connaît le cas de base qui sera notre condition d'arrêt de la récursion

	![2 puissance n](images/2_puiss_n.png){ width=50% }

???+ question "Tester"

    Tester

    {{IDE('scripts/2_puiss_n')}}

## Des représentations

!!! example "Représentation 1"

	![nom image](images/appel_1.png){ width=80% }

!!! example "Représentation 2"

	![nom image](images/rep_2.png){ width=80% }

!!! example "Représentation 3"

	![nom image](images/rep_3.png){ width=80% }

!!! info "Visualisation avec Python tutor"

	<iframe width="800" height="500" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=def%20deux_puissance%28n%29%3A%0A%20%20%20%20if%20n%20%3D%3D%200%3A%0A%20%20%20%20%20%20%20%20return%201%0A%20%20%20%20else%20%3A%0A%20%20%20%20%20%20%20%20return%202*deux_puissance%28n%20-%201%29%0A%0Aprint%28deux_puissance%283%29%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

	

!!! info "Visualisation avec recursionvisualizer"

	[www.recursionvisualizer.com](https://www.recursionvisualizer.com/?function_definition=def%20deux_puissance%28n%29%3A%0A%20%20%20%20if%20n%20%3D%3D%200%3A%0A%20%20%20%20%20%20%20%20return%201%0A%20%20%20%20else%20%3A%0A%20%20%20%20%20%20%20%20return%202*deux_puissance%28n%20-%201%29%0A&function_call=deux_puissance%283%29){ .md-button target="_blank" rel="noopener" }


!!! info "Vos visualisations personnelles avec recursionvisualizer"

	[A vous - recursionvisualizer](https://www.recursionvisualizer.com/){ .md-button target="_blank" rel="noopener" }








