---
author: Franck Chambon, Mireille Coilhac, Valérie Mousseaux, Jean-Louis Thirot
title: Présentation de la récursivité
---

## I. Dans la nature

Remarquons qu'un chou romanesco est constitué d'un ensemble de « florettes » pyramidales disposées en couronnes spiralées. Sa forme géométrique (dite fractale) est très particulière et décorative. 

![Romanesco](images/Romanesco.jpg){ width=20% }

Crédits : [Ivar Leidus - CC BY-SA 4.0 -  via Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Romanesco_broccoli_(Brassica_oleracea).jpg)

## II. Le principe

Bien lire et comprendre la bande dessinée suivante, dessinée par Gilles LASSUS :

![BD Gilles Lassus](images/bd_recurs.png){ width=70% }


## III. La mise en abyme

La mise en abyme est un procédé consistant à représenter une œuvre dans une œuvre similaire, par exemple dans les phénomènes de « film dans un film », ou encore en incrustant dans une image cette image elle-même (en réduction). Ce principe se retrouve dans le phénomène ou le concept d'« autosimilarité », comme dans le principe des figures géométriques fractales ou du principe mathématique de la récursivité.

!!! example inline end "Mise en abyme simple"

    ![Cacao](images/cacao.jpeg){ width=20% }
    > La boite de conserve originale du cacao de la marque Droste, en 1904.

    Crédits : [Domaine public](https://commons.wikimedia.org/wiki/File:Droste_cacao_100gr_blikje,_foto_02.JPG)

!!! danger "Mise en abyme multiple"

    ![vache](images/vache.gif){ width=30% }
    > Création de l'artiste visuel Polonais Feliks Tomasz Konczakowski

## IV. A vous de jouer

Nous désirons créer une fonction qui trace ceci en utisant la bibliothèque `turtle`.  
Il s'agit de triangles de Sierpiński.


![triangles](images/triangles.png){ width=25% }

<!--


<div class="centre">
<iframe 
src=
https://notebook.basthon.fr/?ipynb=eJzFVMtu2zAQ_JUFc5EKpfUjvQjwP-TQnqrAYMlVTIRaEnw0NQx_T5Hv8I91GbuxHCBAmjaJTtwdamYwXHIjFFobRfttIwZMUsskRbvZNvf9ZVp7FK0YZLjR7pZEI6LLQZXelyB3vxxFyATRDN4ipGAkXVv8KLbNCZ1IIceEWrS8wEfkymkcE_fBDZBySMzIvC4k-NBRR70LYMAQBBbBal63HQF_3L-VQVezyaTedyz2qZrOSjlIQ9Y5X9UsgT9R5WQcLZXLlERL2dpGuJx8TiWDq0e-nxHD12SsifsckL2QKgIQdncqh2h-IHjeytFIhQG8zdEgAw9ZxVfJSmMPg0umr5RL2ADVcEjL9ECwWMD0T-MQ4Wm0Y3CccWGrT6FR2KVEGxFa-Avuo89PM3YK5zCtX6Le0dnKaNynUdUAZ8AHoaRaIVgJicPJyJuiR9TVpGZ832Nnnn_sqPDDAniQXmlYHu6MNn2_uwtI6X9Nwvi0p_88-C-Wnr2f9Pz9pC_eVvrpCR6b-vy2pp4ncNWcCNxgIH4xPKpSkRwKk1-nlaM5E2oTvZXr5QG4vAegIJavTJbXx-1iy_7oOz8Vg2Tli2OxHAy5INrZ9jff2FnB
width="900" height="900" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>

-->

???+ question "Traçons un simple triangle." 

    Exécuter le code suivant : 

    {{ IDE('scripts/triangle_1') }}

    {{ figure('cible_1') }}

???+ question "Plusieurs triangle." 

    Utilisons une fonction récursive pour tracer plusieurs triangles.  
    Exécuter le code ci-dessous, et bien observer le résultat.

    🐬 Pour voir la tortue se déplacer plus doucement, exécuter une deuxième fois ce code.

    {{ IDE('scripts/plusieurs_triangles') }}
    
    {{ figure('cible_2') }}

    Testons pour `n = 2`

    {{ IDE('scripts/triangles_2') }}
    
    {{ figure('cible_3') }}

    Testons pour `n = 3`

    {{ IDE('scripts/triangles_3') }}
    
    {{ figure('cible_4') }}

    Testons pour `n = 4`

    {{ IDE('scripts/triangles_4') }}
    
    {{ figure('cible_5') }}

    Testons pour `n = 5`

    {{ IDE('scripts/triangles_5') }}
    
    {{ figure('cible_6') }}


!!! abstract "Définition"

    Une fonction est dite récursive si elle contient un appel à elle-même dans sa propre définition.


???+ question "Question 1"

    Que fait ce script ?

    ```python
    def f_1():
        print("Bonjour")
        f_1()

    f_1()
    ```

    ??? success "Solution"

        Ce script affiche "Bonjour" de nombreuses fois.

        En théorie jusqu'à l'infini, mais Python arrête l'exécution au bout d'un moment et affiche un message d'erreur.
        

???+ question "Question 2"

    Que fait ce script ?


    ```python
    def f_2():
        f_2()
        print("Bonjour")

    f_2()
    ```

    ??? success "Solution"

        Ce script n'affiche jamais "Bonjour", mais...

        Il y a de nombreux appels récursifs, jusqu'au message d'erreur de Python


???+ question "Question 3"

    Que fait ce script ?


    ```python
    def f_3(n):
        print(n)
        if n > 0:
            f_3(n - 1)

    f_3(4)
    ```

    ??? success "Solution"

        Tester :

        {{IDE('scripts/f_3')}}

    
!!! info "Condition d'arrêt"

    Une fonction récursive étant une fonction qui s'appelle elle-même, on peut créer une fonction qui "tourne" théoriquement indéfiniment.  

    Python stoppe de lui même au bout d'un trop grand nombre d'appels de la fonction.



## V. En informatique

En informatique, on trouve :

- des fonctions récursives, l'objet de ce chapitre ;
- des structures de données récursives, comme une pile ou certains arbres, l'objet de chapitres suivants ;
- des fractales ; nous apprendrons à en dessiner certaines.

!!! abstract "le flocon de Koch"

    Le flocon de Koch est l'une des premières courbes fractales à avoir été décrites, bien avant l'invention du terme « fractal(e) » par Benoît Mandelbrot.

    ![Koch](images/Koch_anime.gif){ width=30% }

    Elle a été inventée en 1904 par le mathématicien suédois Helge von Koch.


!!! abstract "La fougère de Barnsley"

    La fougère de Barnsley est une fractale nommée d'après le mathématicien Michael Barnsley qui l'a décrite pour la première fois dans son livre Fractals Everywhere.

    ![Fougère](images/Barnsleys_fern.png){ width=20% }

!!! abstract "L'arbre de Pythagore"

    L'arbre de Pythagore est une fractale plane construite à l'aide de carrés. Elle porte le nom de Pythagore car chaque triplet de carrés en contact enclot un triangle rectangle, une configuration traditionnellement utilisée pour illustrer le théorème de Pythagore. 

    ![Arbre Pythagore](images/Pythagoras_tree_1_1_13_Summer.svg){ width=30% }


## V. Bilan 

!!! info "Définition"

    Une fonction est dite récursive si elle contient un appel à elle-même dans sa propre définition.

!!! info "Comment concevoir une fonction récursive"

    1. Trouver le cas général : trouver l'élément de récursivité qui permet de décomposer le problème en cas plus simple.
    2. Trouver le cas de base : trouver la condition d'arrêt, et la solution du problème dans ce cas particulier.  

        * La condition d'arrêt sera bien atteinte après un nombre **fini** d'appels.  
        * Chaque appel récursif tend à s'approcher du cas de base.

    3. Réunir ces deux cas dans le programme :   

        * Le programme ne contient aucune boucle (`for` ou `while`) dans la partie qui résoud le problème.
        * Le programme contient un général une structure `if` / `else` introduisant le cas de base.

## VI. Crédits

Franck Chambon 