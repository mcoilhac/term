# Tests
assert est_palindrome("") is True
assert est_palindrome("a") is True
assert est_palindrome("mot") is False
assert est_palindrome("tot") is True
assert est_palindrome("elle") is True
assert est_palindrome("toto") is False
assert est_palindrome("canar") is False
assert est_palindrome("tout") is False

# Autres tests
assert est_palindrome("azertyuiopzpoiuytreza") is True
assert est_palindrome("bzertyuiopzpoiuytreza") is False
assert est_palindrome("azertyuiopzpoiuytrezb") is False
assert est_palindrome("azertyuiopzaoiuytreza") is False
assert est_palindrome("azertyuiobzpoiuytreza") is False
assert est_palindrome("azertyuioppoiuytreza") is True
assert est_palindrome("bzertyuioppoiuytreza") is False
assert est_palindrome("azertyuioppoiuytrezb") is False
assert est_palindrome("azertyuiobpoiuytreza") is False

