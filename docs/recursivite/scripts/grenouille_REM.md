!!! danger "Attention"

	Cette solution récursive produit un très grand nombre d'appels qui n'est pas du tout efficace, car des calculs identiques sont réalisés récursivement plusieurs fois. Si vous testez cette solution pour un grand nombre de marches, cela prendra beaucoup de temps, ou fera "planter" Python.

	Nous étudierons plus tard la **programmation dynamique**.