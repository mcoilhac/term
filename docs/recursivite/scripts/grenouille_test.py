# Tests
assert nbre_chemins(1) == 1
assert nbre_chemins(2) == 2
assert nbre_chemins(3) == 3
assert nbre_chemins(4) == 5
assert nbre_chemins(11) == 144

# Autres tests
assert nbre_chemins(10) == 89

