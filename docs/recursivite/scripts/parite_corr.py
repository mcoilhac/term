def est_pair(n: int) -> bool:
    """
    Cette fonction renvoie True si n est pair, False sinon
    Précondition: n est un entier positif ou nul
    Postcondition: la valeur retournée est de type booléen
    Exemple :

    >>> est_pair(4)
    True
    >>> est_pair(5)
    False
    >>> est_pair(0)
    True

    """
    assert n >= 0 ,'n est un entier positif ou nul'
    if n == 0:
        return True
    elif n == 1:
        return False
    else:
        return est_pair(n - 2)

