???+ note dépliée "Autre correction possible"

    ```python
    def est_palindrome(mot):
    "Détermine si mot est un palindrome"

        if len(mot) < 2:
            return True
        else:
            return mot[0] == mot[-1] and est_palindrome(mot[1:-1])
    ```

    Remarquons que la deuxième partie du `and` (`est_palindrome(mot[1:-1])`) ne sera évaluée en Python que si `mot[0] == mot[-1]` est évalué à `True` (évaluation paresseuse).


!!! warning "Remarque"

    ⚠ Cette solution à base de copie de "slices" (tranches en français) n'est pas efficace, nous verrons plus tard une méthode plus efficace avec les indices.
