def est_pair(n: int) -> bool:
    """
    Cette fonction renvoie True si n est pair, False sinon
    Précondition: n est un entier positif ou nul
    Postcondition: la valeur retournée est de type booléen
    Exemple :

    >>> est_pair(4)
    True
    >>> est_pair(5)
    False
    >>> est_pair(0)
    True

    """
    assert n >= 0 ,'n est un entier positif ou nul'
    ...


# tests
assert est_pair(0) is True
assert est_pair(1) is False
assert est_pair(12) is True
assert est_pair(13) is False