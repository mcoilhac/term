def longueur(tableau: list) -> int:
    """
    La fonction renvoie la longueur de tableau
    """
    copie_tableau = list(tableau) # Pour ne pas détruire tableau
    if copie_tableau == []:
        return 0
    else:
        copie_tableau.pop()
        return 1 + longueur(copie_tableau)


