def est_palindrome(mot):
    ...


# Tests
assert est_palindrome("") is True
assert est_palindrome("a") is True
assert est_palindrome("mot") is False
assert est_palindrome("tot") is True
assert est_palindrome("elle") is True
assert est_palindrome("toto") is False
assert est_palindrome("canar") is False
assert est_palindrome("tout") is False