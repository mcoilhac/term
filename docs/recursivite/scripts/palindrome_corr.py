def est_palindrome(mot):
    """Détermine si mot est un palindrome"""

    if len(mot) < 2:
        return True
    else:
        if mot[0] != mot[-1]:
            return False
        else:
            return est_palindrome(mot[1:-1])

