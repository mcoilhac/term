---
authors: Angélique Beucher, Jean-Louis Thirot, Valérie Mousseaux et Mireille Coilhac
title: "Processus"
---
## I. Le système d'exploitation

<iframe width="560" height="315" src="https://www.youtube.com/embed/SpCP2oaCx8A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

!!! info "OS"

    Le système d’exploitation, souvent appelé OS  ( Operating System ) est un ensemble de programmes qui permet d’utiliser les composants physiques d’un ordinateur pour exécuter toutes les applications dont l’utilisateur aura besoin.

    Toute machine est dotée d’un système d’exploitation qui a pour fonction de charger les programmes depuis la mémoire de masse et de lancer leur exécution en leur créant des processus, de gérer l’ensemble des ressources, de traiter les interruptions ainsi que les entrées-sorties et enfin d’assurer la sécurité globale du système.

    *Exemples*

    Windows, Mac OS, Ubuntu Mate  sont des systèmes d’exploitation.

    Attention, Linux n’est pas à  proprement parlé  un OS , c’est en fait le noyau de nombreux OS  que l’on appelle des distributions Linux comme par exemple Gentoo, Debian, Linux Mint, Ubuntu, Fedora, RedHat … 

!!! info "OS en bref"

    Les système d'exploitation font le Lien entre monde applicatif (les applications) et monde matériel (les composants)

??? note "Le monde matériel"

    * la mémoire
    * les processeurs (cerveaux, font des calculs)
    * péripéhriques E/S (imprimantes, écran, clavier, souris, internet...)

??? note "Le monde applicatif"

    * C'est l'ensemble des programmes qui sont en activité à un instant donné.
    * Dans un ordinateur, on a plusieurs applications qui s'exécutent simultanément. chacune exécute une tache.
    * Les applications n'accèdent pas aux éléments de l'ordinateur, elles communiquent seulement avec l'OS.


!!! abstract "Les tâches de l'OS"

    Le système d'exploitation doit allouer à chacun des processus les ressources dont il a besoin en termes de mémoire, entrées-sorties ou temps processeur, et de s'assurer que les processus ne se gênent pas les uns les autres.


!!! example "Gestion de la mémoire"

    La mémoire peut être vue comme un grand tableau disposant de “mots” repérés par des adresses. Si les applications écrivaient directement dans la mémoire, comme elles ne peuvent pas communiquer entre elles, voilà ce qui pourrait arriver :

    Une application écrit toto au mot 1  
    Une seconde application écrit NSI au même endroit

    😢 La seconde application a modifié une donnée dont l'application 1 a besoin....


!!! info "La mémoire virtuelle à la rescousse"

    Au démarrage d'une application, L'OS crée un espace de mémoire virtuelle dédié à cette application.

    L'application écrit dans la mémoire virtuelle. L'OS transfère ensuite dans la vraie mémoire et il mémorise l'information : si l'application demande l'information écrite au mot 1 de sa mémoire virtuelle il sait où trouver l'information dans la vraie mémoire.


!!! info "Ordonanceur"

    Le système d'exploitation va attribuer les ressources, notamment l'accès au CPU. Si une application est bloquée (attente d'une donnée sur disque dur par exemple) l'OS attribue le CPU à une autre application.


!!! info "Accès aux ressources :"

    * Les applications demandent à l'OS lorsqu'elle a besoin d'accéder à une les ressources (écran, CPU, fichier, etc...)

    * L'OS vérifie que l'application a les droits (voir, droits d'accès aux fichiers par exemple) et attribue l'accès ensuite. Cet accès peut être de deux types :
        * Privé : la ressource est attribuée et bloquée pour les autres applications, par exemple c'est le cas d'un accès en écriture dans un fichier.
        * Libre : la ressource est attribuée mais reste disponible pour les autres applications, par exemple c'est le cas d'un accès en lecture dans un fichier.


!!! abstract " A retenir : les trois rôles de l'OS sont :"

    * gérer la mémoire
    * ordonnanceur
    * accès aux ressources

## II. Les processus

<iframe width="560" height="315" src="https://www.youtube.com/embed/bFqud0gcCHM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

!!! info "Qu'est-ce qu'un processus ?"

    Un processus est une instance (tache) de programme en cours d'exécution sur un ordinateur. Il est caractérisé par :

    * un ensemble d'instructions à exécuter - souvent stockées dans un fichier sur lequel on clique pour lancer un programme (par exemple firefox.exe)
    * un espace mémoire dédié à ce processus pour lui permettre de travailler sur des données et des ressources qui lui sont propres : si vous lancez deux instances de firefox, chacune travaillera indépendamment l'une de l'autre.
    * des ressources matérielles : processeur, entrées-sorties (accès à internet en utilisant la connexion Wifi).


!!! note "En résumé"
    Un processus est un "programme" en cours d’exécution.

!!! attention "Attention"
    Il y a une différence entre programme et processus : le même programme peut être lancé plusieurs fois et donner naissances à des processus distincts.

??? note "Analogie avec le domaine de la musique"

    Programme  ~  Partition 

    Processus  ~  Musicien 

    Processeur  ~  Instrument


### Comment voir les processus sur son OS?

Sur chaque OS, il y a des utilitaires en **mode graphique** (GUI) ou en **mode console** ( CLI ) pour observer les processus en cours d’exécution.

#### Exemples d’interfaces graphiques ( GUI = Graphical User Interface )

- Sous Windows 10, il y a le  bien connu  gestionnaire de tâches ( raccourci : `CRTL+MAJ+ECHAP` )

![](images/gui4.png){ width=70% }

- Sous Ubuntu Mate, une distribution Linux,  il y a par exemple **Moniteur Système**. (cf MENU puis Outils Système )

![](images/gui2.png){ width=70% }

- Sous Kubuntu, une autre distribution Linux, il y a par exemple  **KsysGuard**.

![](images/gui10.png){ width=70% }


### Exemples avec la console sous Linux  (CLI= Command Line Interface)

La commande `top` permet d'afficher la liste des processus de manière dynamique ( la liste est rafraîchie toutes les 3 secondes) 

![top](images/console_top.png){ width=70% }


### Vie et mort des processus

!!! example "Différentes situations peuvent générer des processus :"


    * Double cliquer pour lancer un programme
    * Ouvrir un onglet dans un navigateur
    * Exécuter une commande dans une console


Ces processus vont devoir s’exécuter à tours de rôle, pour gérer l’ordre d’exécution le système d’exploitation va attribuer des états aux processus.

!!! abstract "Les états d'un processus"

    Un processus peut donc se trouver dans différents états :

    * **nouveau (initialisation)** : le processus vient d'être créé mais n'existe pas encore qu'à l'état de requête de processus en attendant d'être admis en tant que processus activable
    * **prêt (en attente)**: le processus attend son tour pour prendre la main
    * **élu (en exécution)** : le processus a accès au processeur pour exécuter ses instructions
    * **bloqué (endormi)** : le processus attend qu'un événement se produise (saisie clavier, réception d'une donnée par le réseau ou le disque dur ...)
    * **terminé** : le processus est désormais inactif car il a achevé sa tâche. Il sera détruit prochainement par le système d'exploitation pour libérer de la place en mémoire. Il est parfois conservé pendant un temps à l'état terminé en attendant qu'une entrée/sortie s'achève ou que les données de ce processus soient exploitées par un autre. On parle alors de processus " zombie".


### Ordonnancement des processus par l'OS

!!! info "L'ordonnanceur"

    Dans un système multitâches, plusieurs processus sont actifs simultanément, mais un processeur simple coeur ne peut exécuter qu’une instruction à la fois. Il va donc falloir partager le temps de processeur disponible entre tous les processus : c'est le travail de **l'ordonnanceur (ou scheduler en anglais)**. Ce dernier a pour tâche de sélectionner le processus suivant à exécuter parmi ceux qui sont prêts.

![images](images/Processus_etats.jpg){ width=98% }


!!! abstract "Un processus peut donc se trouver dans différents états :"

    Les chiffres 1, 2, 3 et 4 font référence à la figure ci-dessus.

    1 : L'ordonnanceur choisit ce processus qui passe alors en mode élu ou exécution  
    2 : L'ordonnanceur passe la main à un autre processus. Le processus en cours s'arrête et passe en mode prêt ou en attente  
    3 : Après le 1. (et pas le 2) : le processus en cours attend un événement : saisie clavier, réception d'une donnée par le réseau ou le disque dur, clic de souris...
    Il passe alors en mode bloqué ou endormi  
    4 : L'événement attendu par le processus s'est produit. Le processus repasse en mode prêt ou en attente, en attendant que l'ordonnanceur lui permette de s'exécuter.


??? tip "🌵⚠️🌵"

    Il y a "attendre" ... et ... "attendre"

    * Un processus qui "attend" un événement passe de l'état "élu" à l'état "bloqué".
    * Un processus qui "attend" que l'ordonnanceur lui permette de s'exécuter est dans l'état "prêt" ou "en attente" 

## III. QCM

???+ question "Un processus est :"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] un programme exécutable
        - [ ] un logiciel
        - [ ] une instance d'exécution d'un programme      

    === "Solution"
        
        - :x:
        - :x:
        - :white_check_mark: une instance d'exécution d'un programme

???+ question "Un processus prêt :"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] est exécuté
        - [ ] a été exécuté dans le processeur
        - [ ] attend d'obtenir le processeur    

    === "Solution"
        
        - :x:
        - :x:
        - :white_check_mark: attend d'obtenir le processeur  


???+ question "Un processus en cours d'exécution est dit :"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] élu
        - [ ] bloqué
        - [ ] prêt
        - [ ] actif

    === "Solution"
        
        - :white_check_mark: élu
        - :x:
        - :x:
        - :x:


???+ question "Un processus bloqué :"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] ne peut pas s'exécuter
        - [ ] a été exécuté dans le processeur
        - [ ] attend d'obtenir le processeur    

    === "Solution"
        
        - :white_check_mark:
        - :white_check_mark: a été exécuté dans le processeur avant d'être bloqué
        - :x: attend d'être débloqué par un événement


???+ question "Un programme peut être exécuté :"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] plusieurs fois à la suite par un même processus
        - [ ] plusieurs fois en même temps par un même processus
        - [ ] par plusieurs processus différents  

    === "Solution"
        
        - :x:
        - :x:
        - :white_check_mark: par plusieurs processus différents  

???+ question "A un instant donné un processeur exécute :"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] tous les processus prêts
        - [ ] le processus élu
        - [ ] tous les processus d'un utilisateurs

    === "Solution"
        
        - :x:
        - :white_check_mark: le processus élu
        - :x:

## IV. Exercice

!!! question "Schéma à compléter"

    Compléter le schéma suivant :  

    <iframe id="etats"
        title="Etats d'un processus"
        width="600"
        height="550"
        src="{{ page.canonical_url }}../activites/etat.html">
    </iframe>

_Crédit : Pierre Marquestaut_



