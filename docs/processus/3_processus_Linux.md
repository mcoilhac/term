---
author: Mireille Coilhac
title: Processus et Linux
---

## I. Se lancer dans Debian

??? note "Debian"

    Nous allons utiliser une machine virtuelle avec le système d'exploitation Debian qui est une distribution  de Linux.
    La machine virtuelle a été créée avec Oracle VirtalBox, facilement téléchargeable.
    L'installation de Debian est assez compliquée, elle a été faite au lycée pour vous.

???+ question "Répertoires spéciaux"

    Que représente **`.`** ?

    ??? success

        **`.`** représente le répertoire courant.

    Que représente **`..`** ?

    ??? success

        **`..`** représente le répertoire parent.

    Que représente **`~`** (tilde) ?

    ??? success

        **`~`** représente le répertoire personnel de l'utilisateur (user) en cours, répertoire inclus dans `home`

??? note "Ouvrir la machine virtuelle dans une session debian"

    * ⚠️ Ne pas utiliser votre login habituel, mais **`debian`** avec le mot de passe **`debian`**
    * Double-cliquer sur l'icône Oracle VirtualBox, puis sur l'icône DEBIAN :  
    ![DEBIAN](images/icone_debian.png){ width=20% }
    * Attendre. Cliquer sur Debian, puis saisir le mot de passe `debian`
    * Fermer la fenêtre d'options :  
    ![options](images/options.png){ width=30% }
    * Cliquer sur afficher les applications :  
    ![applis](images/applis.png){ width=30% }
    * Cliquer sur Terminal :  
    ![terminal](images/terminal.png){ width=50% }

## II. Quelques bases

!!! info "Retrouver des commandes déjà saisies"

    Il suffit d'utiliser la flèche vers le haut du clavier <kbd>↑</kbd>

!!! warning "Couper le son"

    Couper le son de votre poste (en bas à droite dans la barre de tâches)

??? note "Quelques commandes"

    Tester successivement les commandes : 

    * `~$ pwd`
    * `~$ ls`
    * `~$ ls -l`

???+ question "Créer un fichier python"

    **1.** Créer un fichier vide `test.py`

    ??? success "Réponse"

        `touch test.py`   

    **2.** éditer le fichier `test.py` avec la commande : `nano test.py`

    y écrire le code suivant (utiliser quatre espaces pour l'indentation) :

    ```python title=""
    for a in range(100000):
        print(a)
    ```

    Sortir de l'éditeur : <kbd>Ctrl</kbd> + <kbd>X</kbd>, puis <kbd>O</kbd>, puis <kbd>Entrée</kbd> pour confirmer le nom.

    **3.** Lancer le programme avec : `python3 test.py`




## II. La commande top



👉 En console : `$ top`

!!! info "`$ top`"

	La commande top permet d’observer en temps-réel les processus sur votre ordinateur à l’instar du gestionnaire de tâches sur Windows. 

Vous allez obtenir quelque chose qui ressemble à ceci :

![top](images/top.png){ width=100% }

!!! example "la commande `q`"

	👉 Pour commencer : tester `q`, puis lancer à nouveau la commande `top`.

	La commande `q` permet de fermer `top`


## III. D'autres commandes avec `top`

!!! info "`PID`"

	PID est le « processus identifier » : un identifiant sous forme d’entier donné par le système.

👉 Si cela n'a pas été fait, lancer à nouveau la commande `top`.

👉 Notez le `PID` de la commande `top` puis « tuez » le processus `top`.

👉 Pour cela : `k` suivi du `PID` de la commande `top`

L’affichage se « fige » et il s’affiche quelque chose qui ressemble à :

`Send pid 46 signal [15/sigterm]`. 

👉 Appuyer ensuite sur la touche <kbd>Entrée</kbd>

???+ question

    Relancez la commande `top` et observez le `PID`. A-t-il été modifié ?

    ??? success "Solution"

    	Un autre processus s'est ouvert, avec un autre `PID`

???+ question

    Essayer de tuer `systemd`.

    ??? success "Solution"

    	Il est impossible de tuer `systemd`. `systemd` se charge du démarrage du système d'exploitation. On peut trouver à la place `init` dans certains systèmes d'exploitation.

## IV. Commandes à connaître

!!! info "La commande `ps`"

	Dans un terminal, la commande `ps` ("process status") va permettre d'afficher la liste des processus actifs dans le terminal courant.

👉 Quitter `top` puis saisir `ps`. Observez les processus actifs.

`pts/0` représente le terminal. `pts` signifie "pseudoterminal slave" (ou terminal pseudo-esclave)



👉 Ouvrir une deuxième console avec Debian : Cliquer sur Terminal, puis Nouvelle fenêtre

![nouveau](images/nouveau_terminal.png){ width=35% }

👉 La positionner à la droite de la précédente.

???+ question "ps et ps -ef"

    Dans la console (le terminal) de droite, lancer Python3 : `~$ python3`

    **1.** Dans la console de gauche, saisir `ps`. Voit-on le processus lié à la commande `python3` ?
    
    ??? success "Solution"

        On ne voit pas le processus lié à la commande `python3` car `ps` ne permet de voir que les processus actifs dans le terminal courant.

    **2.** Dans la console de gauche, saisir `ps -ef`. Voit-on le processus lié à la commande `python3` ?
    
    ??? success "Solution"

        On voit effectivement le processus lié à la commande `python3`.

        ps -ef fournit une vue plus complète : 

        -e : Affiche tous les processus en cours d'exécution sur le système, indépendamment de l'utilisateur.

        -f : Affiche une sortie "full-format" (format complet), ce qui signifie qu'elle inclut plus de détails sur chaque processus, comme l'UID (identifiant de l'utilisateur), le PID (identifiant du processus), le PPID (identifiant du processus parent), le TTY (terminal associé), le temps d'exécution, et la commande complète utilisée pour lancer le processus.

!!! info "Signification des différents champs"

	La documentation Linux donne la signification des différents champs :

	* UID : identifiant utilisateur effectif ; 
	* PID : identifiant de processus ;
	* PPID : PID du processus parent ;
	* C : partie entière du pourcentage d'utilisation du processeur par rapport au temps de vie des processus ;
	* STIME : l'heure de lancement du processus ;
	* TTY : terminal de contrôle
	* TIME : temps d'exécution
	* CMD :  nom de la commande du processus

???+ question

    Comment sont nommés vos deux terminaux ?

    ??? success "Solution"

        `pts/0` et `pts/1`

!!! info "La commande `kill`"

	👉 La commande `kill` permet de fermer un processus, en donnant son PID en argument.

    Depuis la console de gauche, tuer (on dit aussi « terminer ») le processus de `python3`. Pour cela saisir `kill` suivi du PID de `python3`.

    Vérifier sur la console de droite que le processus de `python3` a bien été terminé : vous devez de nouveau avoir l'invite de commande : `~$`

!!! info "Le PPID"

    PPID (Parent PID) : certains processus vont eux-mêmes lancer plusieurs processus-fils.

    Dans la console de droite saisir `firefox`. 
    
    Pour visualiser les consoles, Cliquer sur Activités :  
    ![activite](images/activite.png){ width=15% }
        
    Dans la console de gauche, grâce à la commande appropriée, observe tous les processus lancés par firefox. Ils ont tous le même PPID. Noter ce PPID, puis tuer firefox en tuant ce processus parent.

???+ question "A vous de jouer"

    Lancer nano.
	
	Noter le PPID de nano. A quoi correspond-il ?

    ??? success "Solution"

        Commande : `ps -ef`

    	Le PPID de nano est le processus parent de nano.

    👉 Tester également `ps -elf`

    ??? success "Solution"

        La sortie de `ps -elf` montre des colonnes supplémentaires comme F (flags), S (state), PRI (priority), NI (nice value), ADDR (memory address), SZ (size), et WCHAN (waiting channel), ce qui rend l'affichage plus détaillé. Nous n'étudierons pas tout cette année.

    👉 Tester également `top`

    ??? success "Solution"

        La commande `top` sans option supplémentaire ne permet pas d'afficher le PPID

    👉 Terminer `top`, lancer `ps -ef`, et remonter les identifiants parents de nano.

    Remarque pour `-ef` :  **e** signifie **e**very, **f** signifie **f**ull

!!! info "utiliser `|` ('pipe')"
 
    👉 Pour obtenir `|` au clavier : <kbd>AltGr</kbd> + <kbd>6</kbd>

    Le symbole | en Linux est appelé "pipe" (ou "tuyau" en français). Il est utilisé pour rediriger la sortie d'une commande vers l'entrée d'une autre commande. Cela permet de combiner plusieurs commandes en une seule ligne, de sorte que la sortie d'une commande devienne l'entrée de la suivante.

    Voici un exemple : `ls -l | grep "txt"`

    Dans cet exemple, la commande `ls -l` liste les fichiers et répertoires avec des détails, puis `grep "txt"` filtre cette liste pour ne montrer que les lignes contenant "txt".

???+ question "A vous de jouer"

    **1.**Quelle commande utiliser pour voir le détail de seulement ce qui concerne `nano`? Le tester.

    ??? success

        `ps -elf | grep nano`   

    **2.** Tuer le processus lié à nano

    ??? success "Solution"

        `kill` suivi du  PID de nano

    **3.** Faire vos propres essais pour réviser les commandes vues.

!!! info "Fin de ce TP"

    Ce TP est terminé : Fermer les terminaux, puis Oracle VirtalBox.
    Se connecter sur votre session habituelle pour passer à la suite.

## V. QCM

???+ question "Quelle commande permet de lister les processus ?"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] ps
        - [ ] sh
        - [ ] proc
        - [ ] dir
        - [ ] man
        - [ ] ls

    === "Solution"
        
        - :white_check_mark: ps
        - :x:
        - :x:
        - :x:
        - :x:
        - :x:

???+ question "Quelle abréviation désigne l'identifiant d'un processus dans un système d'exploitation de type unix ?"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] SIG
        - [ ] PID
        - [ ] SID
        - [ ] PPID

    === "Solution"
        
        - :x:
        - :white_check_mark: PID
        - :x:
        - :x:

???+ question "Quelle commande permet d'interrompre un processus dans un système d'exploitation de type unix ?"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] stop
        - [ ] interrupt
        - [ ] end
        - [ ] kill

    === "Solution"
        
        - :x:
        - :x:
        - :x:
        - :white_check_mark: kill

        
## VI. Réviser Linux (chez vous)

🌐 TD à télécharger : Fichier `Terminus.pdf` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/Terminus.pdf)

😉 Il sera utile de couper le son du site : clic droit sur l'onglet dans le navigateur, puis sélectionner "couper le son du site"

!!! warning "👉 Le symbole **~** au clavier"

    * Il se trouve sur la même touche que  <kbd>2 é ~</kbd>. Il faut appuyer sur la barre espace pour le faire apparaître.
    * En cas de problème : maintenir la touche <kbd>Alt</kbd> enfoncée et saisir `126`


## VII. Un exercice type bac

Avec une ligne de commande dans un terminal sous Linux, on obtient l'affichage suivant :

![capture](images/exo_bac_2021.png){ width=90% }

La documentation Linux donne la signification des différents champs :

* UID : identifiant utilisateur effectif ;
* PID : identifiant de processus ;
* PPID : PID du processus parent ;
* C : partie entière du pourcentage d'utilisation du processeur par rapport au temps de vie des processus ;
* STIME : l'heure de lancement du processus ;
* TTY : terminal de contrôle
* TIME : temps d'exécution
* CMD : nom de la commande du processus

???+ question "Exercice"

    **1.** Question 1.   
    Parmi les quatre commandes suivantes, laquelle a permis cet affichage ? 

    * ls -l
    * ps –ef
    * cd ..
    * chmod 741 processus.txt 

    ??? success "Solution"

        ps -ef

    **2.** Question 2.   
    Quel est l'identifiant du processus parent à l'origine de tous les processus concernant le navigateur Web (chromium-browser) ?

    ??? success "Solution"

        L’identifiant du processus parent à l’origine de tous les processus concernant le navigateur Web est 831

    **3.** Question 3.   
    Quel est l'identifiant du processus dont le temps d'exécution est le plus long ?

    ??? success "Solution"

        L’identifiant du processus dont le temps d’exécution est le plus long est 6211 (00:01:16)



_Avec l'aide du travail d'Antoine Marot, David Sallé, Julien Simonneau_