---
author: Mireille Coilhac, Antoine Marot, David Sallé, Julien Simonneau
title: Processus et Linux old
---

??? note "Debian"

    Nous allons utiliser la distribution Debian de Linux installée sur votre ordinateur Windows.

	Chez vous, si vous avez un ordinateur avec une version de windows assez récente, vous pourrez faire de même en suivant ce lien : 
	[Lien](https://blog.ikoula.com/fr/sous-syst%C3%A8me-linux-pour-windows){ .md-button target="_blank" rel="noopener" }

## I. Se lancer dans Debian

Chercher Debian, puis l’exécuter. Choisir un nom d’utilisateur et un mot de passe (simples) et les noter sur votre cahier.
Compléter 

* Enter new UNIX username :
* New password

!!! danger "Attention"

	⚠ Quand vous écrivez votre mot de passe, il ne se passe rien dans la console (aussi appelée « **terminal** »), c’est normal. Le mot de passe vous sera demandé une deuxième fois.

## II. La commande top

👉 En console : `$ top`

!!! info "`$ top`"

	La commande top permet d’observer en temps-réel les processus sur votre ordinateur à l’instar du gestionnaire de tâches sur Windows. 

Vous allez obtenir quelque chose qui ressemble à ceci :

![top](images/top.png){ width=100% }

!!! example "la commande `q`"

	👉 Pour commencer : tester `q`, puis lancer à nouveau la commande `top`.

	La commande `q` permet de fermer `top`


## III. D'autres commandes avec `top`

!!! info "`PID`"

	PID est le « processus identifier » : un identifiant sous forme d’entier donné par le système.

👉 Si cela n'a pas été fait, lancer à nouveau la commande `top`.

👉 Notez le `PID` de la commande `top` puis « tuez » le processus `top`.

👉 Pour cela : `k` suivi du `PID` de la commande `top`

L’affichage se « fige » et il s’affiche quelque chose qui ressemble à :

`Send pid 46 signal [15/sigterm]`. 

👉 Appuyer ensuite sur la touche « entrée »

???+ question

    Relancez la commande `top` et observez le `PID`. A-t-il été modifié ?

    ??? success "Solution"

    	Un autre processus s'est ouvert, avec un autre `PID`

???+ question

    Essayer de tuer `init`.

    ??? success "Solution"

    	Il est impossible de tuer `init`.

## IV. Commandes à connaître

!!! info "La commande `ps`"

	Dans un terminal, la commande `ps` va permettre d'afficher la liste des processus actifs dans le terminal courant.

👉 Quitter `top` puis saisir `ps`. Observez les processus actifs.

👉 Ouvrir une deuxième console avec Debian. Placer les deux consoles côte à côte.

!!! info "`find`"

	La commande find permet de rechercher des fichiers sur le système d’exploitation.

!!! danger "Attention"

	Attention à bien respecter les espaces donnés.

👉 Se placer sur la console de droite, puis lancer une recherche de tous les scripts Python présents depuis la racine du système de fichiers avec : `find / -name "*.py"`

???+ question

    Dans la console de gauche, saisir `ps`. Voit-on le processus lié à la commande `find` ?
    
    ??? success "Solution"

        On ne voit pas le processus lié à la commande `find` car `ps` ne permet de voir que les processus actifs dans le terminal courant.

???+ question

    La commande `top` permet d'observer les processus en temps réel. Depuis que vous avez lancé 
    `find / -name "*.py"` dans la console de droite, le processus s'est sûrement terminé. (Dans ce cas, vous avez de nouveau l'invite de commande). Recommencez donc l'opération, puis dans la console de gauche, saisir `top`. Voit-on le processus lié à la commande `find` ?
    
    ??? success "Solution"
    
        Dans la console de gauche on voit l’apparition du processus lié à la commande find, son PID, son état, son utilisation du CPU, de la mémoire...

👉 Quitter `top` puis saisir `ps -ef`

!!! info "La commande `ps -ef`"

	La commande `ps -ef` va permettre d'afficher la liste de tous les processus actifs à l'instant où on l'écrit. L'affichage n'est pas actualisé en temps réel comme pour `top`, il n'y a donc pas besoin de "quitter".

👉 Dans la console de gauche, saisir `top`

⌛ Comme la recherche demandée va prendre pas mal de temps et que vous avez changé d’avis, **tuer** le processus « find » en utilisant son PID sur la console de gauche. Si votre processus s'était déjà terminé, vous pouvez en relancer un nouveau, et ensuite le tuer.  
Le PID noté avec `top`et celui noté avec `ps -ef` pour le processus `find` sont-ils identiques ?

👉 quitter top.

👉 Nous allons utiliser l’éditeur de texte nano. 

👉 Dans la console de droite : `nano mon_texte.txt`

Vous pouvez taper un petit texte.

👉 Dans la console de gauche : saisir la commande `ps -ef`

!!! info "Signification des différents champs"

	La documentation Linux donne la signification des différents champs :

	* UID : identifiant utilisateur effectif ; 
	* PID : identifiant de processus ;
	* PPID : PID du processus parent ;
	* C : partie entière du pourcentage d'utilisation du processeur par rapport au temps de vie des processus ;
	* STIME : l'heure de lancement du processus ;
	* TTY : terminal de contrôle
	* TIME : temps d'exécution
	* CMD :  nom de la commande du processus

???+ question

    Comment sont nommés vos deux terminaux de contrôle ?

    ??? success "Solution"

    	TTY1 et TTY2

!!! info "La commande `kill`"

	La commande `kill` permet de fermer un processus, en donnant son PID en argument.

👉 Depuis la console de gauche, tuer (on dit aussi « terminer ») le processus de `nano`. Pour cela taper `kill` suivi du PID de nano.

👉 En procédant de même, en vous plaçant sur la console de gauche, tuer la console de droite.

!!! info "Le `PPID`"

	Le PPID est le « parent processus identifier » qui donne l’identifiant du parent qui a engendré le processus.


???+ question

    Ouvrir à nouveau une console que vous mettez à droite, et y lancer nano.
	
	Noter le PPID de nano. A quoi correspond-il ?

    ??? success "Solution"

    	Le PPID de nano est le processus parent de nano.

👉 Tester également `ps -elf`

👉 Tester également `top` puis `V`

👉 Terminer `top`, lancer `ps -ef`, et remonter les identifiants parents de nano.

Remarque pour `-ef` :  **e** signifie **e**very, **f** signifie **f**ull


## V. QCM

???+ question "Quelle commande permet de lister les processus ?"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] ps
        - [ ] sh
        - [ ] proc
        - [ ] dir
        - [ ] man
        - [ ] ls

    === "Solution"
        
        - :white_check_mark: ps
        - :x:
        - :x:
        - :x:
        - :x:
        - :x:

???+ question "Quelle abréviation désigne l'identifiant d'un processus dans un système d'exploitation de type unix ?"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] SIG
        - [ ] PID
        - [ ] SID
        - [ ] PPID

    === "Solution"
        
        - :x:
        - :white_check_mark: PID
        - :x:
        - :x:

???+ question "Quelle commande permet d'interrompre un processus dans un système d'exploitation de type unix ?"

    === "Cocher la ou les affirmations correctes"
        
        - [ ] stop
        - [ ] interrupt
        - [ ] end
        - [ ] kill

    === "Solution"
        
        - :x:
        - :x:
        - :x:
        - :white_check_mark: kill

        
## VI. Réviser Linux

🌐 TD à télécharger : Fichier `Terminus.pdf` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/Terminus.pdf)

😉 Il sera utile de couper le son du site : clic droit sur l'onglet dans le navigateur, puis sélectionner "couper le son du site"

!!! warning "👉 Le symbole **~** au clavier"

    * Il se trouve sur la même touche que  <kbd>2 é ~</kbd>. Il faut appuyer sur la barre espace pour le faire apparaître.
    * En cas de problème : maintenir la touche <kbd>Alt</kbd> enfoncée et saisir `126`



## VII. Réviser les processus en ligne de commande

Ouvrir Debian

!!! danger "Attention"

    Ce travail doit être réalisé sans regarder le IV, sauf en dernière extrémité.

???+ question "Question 1"

    Ecrire la commande qui permet de voir les processus en temps réel.

    ??? success "Solution"

        Lire le II, et III.


???+ question "Question 2" 

    Quitter ce processus.

    ??? success "Solution"

        q


???+ question "Question 3"

    Ecrire la commande qui permet d'afficher la liste des processus actifs (différente de celle de la question 1)

    ??? success "Solution"

        ps


???+ question "Question 4"

    Ouvrir un deuxième terminal Debian.

    Dans le 2ème terminal saisir `nano mon_texte.txt`

    Quel est le processus parent de `nano` ? 

    ??? success "Solution"

        Dans le premier terminal saisir `ps -ef`, puis lire son PPID, et regarder le processus parent.


???+ question "Question 5"

    Tuer le processus lié à nano.

    ??? success "Solution"

        `kill` suivi du  PID de nano


???+ question "Question 6"

    Tuer le processus lié au 2ème terminal.

    ??? success "Solution"

        `kill` suivi du  PID de TTY2


## VIII. Un exercice type bac

Avec une ligne de commande dans un terminal sous Linux, on obtient l'affichage suivant :

![capture](images/exo_bac_2021.png){ width=90% }

La documentation Linux donne la signification des différents champs :

* UID : identifiant utilisateur effectif ;
* PID : identifiant de processus ;
* PPID : PID du processus parent ;
* C : partie entière du pourcentage d'utilisation du processeur par rapport au temps de vie des processus ;
* STIME : l'heure de lancement du processus ;
* TTY : terminal de contrôle
* TIME : temps d'exécution
* CMD : nom de la commande du processus

???+ question "Exercice"

    **1.** Question 1.   
    Parmi les quatre commandes suivantes, laquelle a permis cet affichage ? 

    * ls -l
    * ps –ef
    * cd ..
    * chmod 741 processus.txt 

    ??? success "Solution"

        ps -ef

    **2.** Question 2.   
    Quel est l'identifiant du processus parent à l'origine de tous les processus concernant le navigateur Web (chromium-browser) ?

    ??? success "Solution"

        L’identifiant du processus parent à l’origine de tous les processus concernant le navigateur Web est 831

    **3.** Question 3.   
    Quel est l'identifiant du processus dont le temps d'exécution est le plus long ?

    ??? success "Solution"

        L’identifiant du processus dont le temps d’exécution est le plus long est 6211 (00:01:16)


