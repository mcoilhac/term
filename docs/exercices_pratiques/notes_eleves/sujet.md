---
author: Gilles Lassus et Mireille Coilhac
title: Notes d'une classe
tags:
  - 3-dictionnaire
--- 

Un professeur de NSI décide de gérer les résultats de sa classe sous la forme d’un
dictionnaire :

- les clefs sont les noms des élèves ;
- les valeurs sont des dictionnaires dont les clefs sont les types d’épreuves sous
forme de chaîne de caractères et les valeurs sont les notes obtenues associées à
leurs coefficients dans une liste. Une telle liste ne peut pas être vide, et est obligatoirement de taille 2.

On garantit que chaque élève de la classe a au moins une note.

Par exemple, avec :

```python
resultats = {'Dupont': {
                        'DS1': [15.5, 4],
                        'DM1': [14.5, 1],
                        'DS2': [13, 4],
                        'PROJET1': [16, 3],
                        'DS3': [14, 4]
                    },
            'Durand': {
                        'DS1': [6 , 4],
                        'DM1': [14.5, 1],
                        'DS2': [8, 4],
                        'PROJET1': [9, 3],
                        'IE1': [7, 2],
                        'DS3': [9, 4],
                        'DS4':[15, 4]
                    }
            }
```

L’élève dont le nom est Durand a ainsi obtenu au DS2 la note de 8 avec un coefficient 4.

Le professeur crée une fonction `moyenne` qui prend en paramètre le nom d’un de ses élèves et renvoie sa moyenne arrondie au dixième.

???+ question "Compléter le code du professeur ci-dessous"
   
    {{ IDE('exo') }}
