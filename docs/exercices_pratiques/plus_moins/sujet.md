---
author: BNS2022-38.2 puis Sébastien Hoarau
title: Jeu "plus ou moins"
tags:
  - 0-simple
  - Difficulté *
---

Le jeu du « plus ou moins » consiste à deviner un nombre entier choisi entre 1 et 99.
Un élève de NSI décide de le coder en langage Python de la manière suivante :

- le programme génère un nombre entier aléatoire compris entre 1 et 99 ;
- si la proposition de l'utilisateur est plus petite que le nombre cherché, l'utilisateur en est averti. Il peut alors en tester un autre ;
- si la proposition de l'utilisateur est plus grande que le nombre cherché, l'utilisateur en est averti. Il peut alors en tester un autre ;
- si l'utilisateur trouve le bon nombre en 10 essais ou moins, il gagne ;
- si l'utilisateur a fait plus de 10 essais sans trouver le bon nombre, il perd.

!!! note "Note"

    La fonction `randint` est utilisée. Si `a` et `b` sont des entiers, `randint(a, b)` renvoie un nombre entier compris entre `a` et `b`, incluant les **deux bornes**.


???+ question

    Recopier le code suivant dans la partie gauche [basthon](https://console.basthon.fr/){ .md-button target="_blank" rel="noopener" }

    Compléter ce code et le tester

    ```python
    from random import randint

    def plus_ou_moins():
        nb_mystere = randint(1,...)
        nb_test = int(input("Proposez un nombre entre 1 et 99 : "))
        compteur = ...

        while nb_mystere != ... and compteur < ... :
            compteur = compteur + ...
            if nb_mystere ... nb_test:
                nb_test = int(input("Trop petit ! Testez encore : "))
            else:
                nb_test = int(input("Trop grand ! Testez encore : "))

        if nb_mystere == nb_test:
            print ("Bravo ! Le nombre était ",...)
            print("Nombre d'essais: ",...)
        else:
            print ("Perdu ! Le nombre était ",...)

    plus_ou_moins()
    ```

    ??? success "Solution"

        ```python
        def plus_ou_moins():
            nb_mystere = randint(1, 99)
            nb_test = int(input("Proposez un nombre entre 1 et 99 : "))
            compteur = 0

            while nb_mystere != nb_test and compteur < 10:
                compteur = compteur + 1
                if nb_mystere > nb_test:
                    nb_test = int(input("Trop petit ! Testez encore : "))
                else:
                    nb_test = int(input("Trop grand ! Testez encore : "))

            if nb_mystere == nb_test:
                print("Bravo ! Le nombre était ", nb_mystere)
                print("Nombre d'essais : ", compteur)
            else:
                print("Perdu ! Le nombre était ", nb_mystere)
        ```
