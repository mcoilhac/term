---
author: équipe e-nsi
title: Cycles
tags:
  - 3-dictionnaire
  - Difficulté **
---

# Recherche d'un cycle 

On considère des personnes, identifiées par un pseudo unique, qui s'envoient des messages en respectant les deux règles suivantes :

- chaque personne envoie des messages à 1 seule et même personne (éventuellement elle-même),
- chaque personne ne peut recevoir des messages qu'en provenance d'une seule
personne (éventuellement elle-même).

### Exemple pour `plan_a`

Voici un exemple, avec 6 personnes, de « plan d'envoi des messages » qui respecte les
règles ci-dessus, puisque chaque personne est présente une seule fois dans en tant qu'expéditeur et récepteur.

- Anne envoie ses messages à Elodie
- Elodie envoie ses messages à Bruno
- Bruno envoie ses messages à Fidel
- Fidel envoie ses messages à Anne
- Claude envoie ses messages à Denis
- Denis envoie ses messages à Claude

Et le dictionnaire correspondant à ce plan d'envoi est le suivant :

```python
plan_a = {'Anne': 'Elodie', 'Bruno': 'Fidel', 'Claude': 'Denis', 
          'Denis': 'Claude', 'Elodie': 'Bruno', 'Fidel': 'Anne'}
```

Sur le plan d'envoi `plan_a` des messages ci-dessus, il y a deux cycles distincts : un premier
cycle avec Anne, Elodie, Bruno, Fidel et un second cycle avec Claude et Denis. 

### Exemple pour `plan_b`

```python
plan_b = {'Anne': 'Claude', 'Bruno': 'Fidel', 'Claude': 'Elodie', 
          'Denis': 'Anne', 'Elodie': 'Bruno', 'Fidel': 'Denis'}
```
En revanche, le plan d'envoi `plan_b` ci-dessus comporte un unique cycle : Anne, Claude, Elodie, Bruno, Fidel, Denis. Dans ce cas, lorsqu'un plan d'envoi comporte un unique cycle, on dit que le plan d'envoi est *cyclique*.


!!! info "Conséquence des règles sur le dictionnaire"
    Les deux règles garantissent que chaque personne apparaît une fois en
    tant que clé du dictionnaire et exactement une fois en tant que valeur
    associée à une clé.

### Principe de l'algorithme

Pour savoir si un plan d'envoi de messages comportant $\text{N}$ personnes est cyclique, on peut utiliser l'algorithme ci-dessous :

- on part d’un expéditeur et on inspecte son destinataire dans le plan d'envoi,
- chaque destinataire devient à son tour expéditeur, selon le plan d’envoi, tant
qu’on ne « retombe » pas sur l’expéditeur initial,
- le plan d’envoi est cyclique si on l’a parcouru en entier.

On garantit qu'un plan est non vide.

Compléter la fonction suivante en respectant la spécification.

??? tip "Longueur d'un dictionnaire"

    La fonction python `len` permet d'obtenir la longueur d'un dictionnaire.
    

!!! example "Exemples"

    ```pycon
    >>> plan_a = {'Anne': 'Elodie', 'Bruno': 'Fidel', 'Claude': 'Denis',
              'Denis': 'Claude', 'Elodie': 'Bruno', 'Fidel': 'Anne'}
    >>> est_cyclique(plan_a)
    False
    >>> plan_b = {'Anne': 'Claude', 'Bruno': 'Fidel', 'Claude': 'Elodie',
              'Denis': 'Anne', 'Elodie': 'Bruno', 'Fidel': 'Denis'}
    >>> est_cyclique(plan_b)
    True
    ```

???+ question "Compléter la fonction `est_cyclique`" 

    {{ IDE('exo') }}




