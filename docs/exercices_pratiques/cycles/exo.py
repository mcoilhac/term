def est_cyclique(plan):
    """
    Prend en paramètre un dictionnaire `plan` correspondant à un plan d'envoi valide de messages.
    Renvoie True si le plan d'envoi de messages est cyclique et False sinon.
    """
    personnes = [key for key in plan]
    expediteur = personnes[0]
    destinataire = plan[...]
    nb_destinaires = 1

    while destinataire != ...:
        destinataire = ...
        nb_destinaires = ...

    return nb_destinaires == ...


# Tests
plan_a = {'Anne': 'Elodie', 'Bruno': 'Fidel', 'Claude': 'Denis',
          'Denis': 'Claude', 'Elodie': 'Bruno', 'Fidel': 'Anne'}
plan_b = {'Anne': 'Claude', 'Bruno': 'Fidel', 'Claude': 'Elodie',
          'Denis': 'Anne', 'Elodie': 'Bruno', 'Fidel': 'Denis'}
assert not est_cyclique(plan_a)
assert est_cyclique(plan_b)

