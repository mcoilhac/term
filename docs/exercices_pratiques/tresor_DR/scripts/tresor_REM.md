On peut réaliser une version **itérative** de cette fonction, qui repose sur un algorithme de dichotomie.

```python
def fausse_piece(tresor, debut, fin):
    """
    Renvoie le numéro de la fausse pièce dans le trésor
    Le trésor initial compte 128 pièces
    """

    while debut < fin:
        if debut + 1 == fin:  # une seule pièce dans la zone d'étude
            return debut
        milieu = (debut + fin) // 2
        observation = compare(tresor, debut, milieu, milieu, fin)
        if observation == "gauche":
            fin = milieu
        else:
            debut = milieu
```
