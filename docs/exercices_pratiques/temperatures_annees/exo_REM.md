!!! danger "Attention"

	Il faut absolument éviter de comparer deux flottants entre eux.

	!!! example "Exemple"

		```pycon
		>>> 0.1 + 0.2 > 0.3
		True
		>>> 0.1 + 0.2
		0.30000000000000004
		```

	Dans cet exercice les températures étant données au dixième de degré près, il aurait suffit de constituer un tableau avec les températures multipliées par 10, pour travailler avec des entiers.
