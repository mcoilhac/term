# Tests
assert ajoute_dictionnaires({1: 5, 2: 7}, {2: 9, 3: 11}) == {1: 5, 2: 16, 3: 11}
assert ajoute_dictionnaires({}, {2: 9, 3: 11}) == {2: 9, 3: 11}
assert ajoute_dictionnaires({1: 5, 2: 7}, {}) == {1: 5, 2: 7}

# Autres tests
assert ajoute_dictionnaires({1: 5, 2: 16, 3: 11}, {1: 5, 2: 16, 3: 11}) == {1: 10, 2: 32, 3: 22}
assert ajoute_dictionnaires({}, {}) == {}
dico_1 = {i:2*i for i in range(20)}
dico_2 = {i:3*i for i in range(20)}
dico_3 = {i:5*i for i in range(20)}
assert ajoute_dictionnaires(dico_1, dico_2) == dico_3