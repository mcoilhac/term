---
author: Gilles Lassus puis Mireille Coilhac
title: Conversion en binaire (guidé)
tags:
  - modulo
  - string
  - binaire
  - important
---

# Conversion d'entiers en base 10 vers la base 2

Pour rappel, la conversion d'un nombre entier positif en binaire peut s'effectuer à l'aide des divisions successives comme illustré ici :

![image](img15_2b.png){: .centrer width=40% }

La fonction `binaire` basée sur la méthode des divisions successives permet de
convertir un nombre entier positif en binaire. Elle renvoie une chaine de caractères.

!!! example "Exemple"
    
    ```pycon
    >>> binaire(0)
    '0'
    >>> binaire(1)
    '1'
    >>> binaire(16)
    '10000'
    >>> binaire(77)
    '1001101'
    ```

???+ question "Compléter la fonction `binaire`"

    {{ IDE('exo') }}