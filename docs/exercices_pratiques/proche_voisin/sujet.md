---
author: Gilles Lassus puis Mireille Coilhac
title: Proche voisin
tags:
  - 3-liste/tableau
  - 2-tuple
  - 2-float
---

On souhaite programmer une fonction donnant le point d'une liste de points le plus proche d'un point
de départ. Les points sont tous à coordonnées entières.
Les points sont donnés sous la forme d'un **tuple** de deux entiers qui sont leurs coordonnées (dans un repère orthonormé).
La liste des points à traiter est donc un tableau de tuples.

On rappelle que la distance entre deux points du plan de coordonnées $(x; y)$ et $(x'; y')$
est donnée par la formule :

$d=\sqrt{(x-x')^2+(y-y')^2}$

On importe pour cela la fonction racine carrée (`sqrt`) du module `math` de Python.

!!! example "Exemples"

    ```pycon
    >>> distance((1, 0), (5, 3))
    5.0
    >>> distance((1, -4), (6, 8))
    13.0
    >>> proche_voisin([(7, 9), (2, 5), (5, 2)], (0, 0))
    (2, 5)
    >>> proche_voisin([(7, 9), (2, 5), (5, 2)], (5, 2))
    (5, 2)
    ```

Compléter le code des fonctions `distance` et `proche_voisin` fournies ci-dessous pour qu’elles répondent à leurs spécifications.

!!! danger "Attention"

    Il est interdit d'utiliser `min`


    
???+ question "Compléter le code ci-dessous"

    {{ IDE('exo', SANS = 'min') }}
