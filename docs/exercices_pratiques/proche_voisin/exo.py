from math import sqrt     # import de la fonction racine carrée

def distance(point1, point2):
    """ Calcule et renvoie la distance entre deux points. """
    return sqrt((...)**2 + (...)**2)

def proche_voisin(tab, depart):
    """ Renvoie le point du tableau tab se trouvant à la plus courte distance du point depart."""
    point = tab[0]
    min_dist = ...
    for i in range (1, ...):
        if distance(tab[i], depart)...:
            point = ...
            min_dist = ...
    return point

# Tests
assert distance((1, 0), (5, 3)) == 5
assert distance((1, -4), (6, 8)) == 13
assert proche_voisin([(7, 9), (2, 5), (5, 2)], (0, 0)) == (2, 5)
assert proche_voisin([(7, 9), (2, 5), (5, 2)], (5, 2)) == (5, 2)

