# Liens vers des exercices de type bac

## Récapitulatif

[Tableau récapitulatif](https://romainjanvier.forge.aeif.fr/nsiterminale/5_algorithmique/9_z_epreuve_pratique/sujet/){ .md-button target="_blank" rel="noopener" }

## Liens CodEx

[tri_selection](https://codex.forge.apps.education.fr/exercices/tri_selection/){:target="_blank" }

[correspond_mot](https://codex.forge.apps.education.fr/exercices/correspond_mot/){:target="_blank" }

[permutation_circulaire](https://codex.forge.apps.education.fr/exercices/permutation_circulaire/){:target="_blank" }

[maximum_nombres](https://codex.forge.apps.education.fr/exercices/maximum_nombres/){:target="_blank" }

[parentheses](https://codex.forge.apps.education.fr/en_travaux/parentheses/){:target="_blank" }

[derniere_occurrence](https://codex.forge.apps.education.fr/exercices/derniere_occurrence/){:target="_blank" }

[plus_proche](https://codex.forge.apps.education.fr/exercices/plus_proche/){:target="_blank" }

[val_ind_max](https://codex.forge.apps.education.fr/exercices/val_ind_max/){:target="_blank" }

[est_trie](https://codex.forge.apps.education.fr/exercices/est_trie/){:target="_blank" }

[resultat_vote](https://codex.forge.apps.education.fr/en_travaux/resultat_vote/){:target="_blank" }

[tri_insertion](https://codex.forge.apps.education.fr/exercices/tri_insertion/){:target="_blank" }

[delta_encoding](https://codex.forge.apps.education.fr/exercices/delta_encoding/){:target="_blank" }

[expr_parenthesee](https://codex.forge.apps.education.fr/en_travaux/expr_parenthesee/){:target="_blank" }

[tri_denombrement](https://codex.forge.apps.education.fr/en_travaux/tri_denombrement/){:target="_blank" }

[moy_ponderee_liste_tuples](https://codex.forge.apps.education.fr/exercices/moy_ponderee/){:target="_blank" }

[zoom_ascii_art](https://codex.forge.apps.education.fr/en_travaux/zoom_ascii_art/){:target="_blank" }

[jeu_plus_moins](https://codex.forge.apps.education.fr/exercices/jeu_plus_moins/){:target="_blank" }

[ind_prem_occ](https://codex.forge.apps.education.fr/exercices/ind_prem_occ/){:target="_blank" }

[insertion_liste_triee](https://codex.forge.apps.education.fr/exercices/insertion_liste_triee/){:target="_blank" }

[dict_extremes](https://codex.forge.apps.education.fr/exercices/dict_extremes/){:target="_blank" }

[paquet_de_cartes](https://codex.forge.apps.education.fr/exercices/paquet_de_cartes/){:target="_blank" }

[moyenne](https://codex.forge.apps.education.fr/exercices/moyenne/){:target="_blank" }

[tri_bulles](https://codex.forge.apps.education.fr/exercices/tri_bulles/){:target="_blank" }

[repetitions](https://codex.forge.apps.education.fr/exercices/repetitions/){:target="_blank" }

[multiplication](https://codex.forge.apps.education.fr/exercices/multiplication/){:target="_blank" }

[recherche_tab_trie](https://codex.forge.apps.education.fr/en_travaux/recherche_tab_trie/){:target="_blank" }

[est_parfait](https://codex.forge.apps.education.fr/exercices/est_parfait/){:target="_blank" }

[manipulation_pixels](https://codex.forge.apps.education.fr/en_travaux/manipulation_pixels/){:target="_blank" }

[partition_tableau](https://codex.forge.apps.education.fr/exercices/partition_tableau/){:target="_blank" }

[moyenne_ponderee_dico _ II](https://codex.forge.apps.education.fr/exercices/moyenne_ponderee/){:target="_blank" }

[ind_min](https://codex.forge.apps.education.fr/exercices/ind_min/){:target="_blank" }

[tableau_0_1](https://codex.forge.apps.education.fr/exercices/tableau_0_1/){:target="_blank" }

[fib_1](https://codex.forge.apps.education.fr/exercices/fib_1/){:target="_blank" }

[triangle_pascal](https://codex.forge.apps.education.fr/exercices/triangle_pascal/){:target="_blank" }

[fusion_listes_triees](https://codex.forge.apps.education.fr/exercices/fusion_listes_triees/){:target="_blank" }

[romain_decimal](https://codex.forge.apps.education.fr/exercices/romain_decimal/){:target="_blank" }

[dichotomie](https://codex.forge.apps.education.fr/exercices/dichotomie/){:target="_blank" }

[ou_exclusif_listes](https://codex.forge.apps.education.fr/exercices/ou_exclusif_listes/){:target="_blank" }

[carre_semi_magiques](https://codex.forge.apps.education.fr/exercices/carre_semi_magiques/){:target="_blank" }

[renverse_chaine](https://codex.forge.apps.education.fr/exercices/renverse_chaine/){:target="_blank" }

[dico_occurrences](https://codex.forge.apps.education.fr/exercices/dico_occurrences/){:target="_blank" }

[est_nb_palindrome](https://codex.forge.apps.education.fr/exercices/est_nb_palindrome/){:target="_blank" }

[rendu_monnaie](https://codex.forge.apps.education.fr/en_travaux/rendu_monnaie/){:target="_blank" }

[bon_enclos](https://codex.forge.apps.education.fr/exercices/bon_enclos/){:target="_blank" }

[dichotomie_iteratif](https://codex.forge.apps.education.fr/exercices/dichotomie_iteratif/){:target="_blank" }

[code_cesar](https://codex.forge.apps.education.fr/exercices/code_cesar/){:target="_blank" }

[top_like](https://codex.forge.apps.education.fr/exercices/top_like/){:target="_blank" }

[eval_postfixe](https://codex.forge.apps.education.fr/exercices/eval_postfixe/){:target="_blank" }

[conway](https://codex.forge.apps.education.fr/exercices/conway/){:target="_blank" }

[Ajouter des dictionnaires / Union de dictionnaires](https://codex.forge.apps.education.fr/exercices/union_dictionnaires/){:target="_blank" }


**Cette page est en cours de construction.**




