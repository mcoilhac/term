def parenthesage(chaine):
    """
    Renvoie True si chaine est bien parenthesee et False sinon
    """
    p = Pile()
    for caractere in chaine:
        if caractere == "(":
            p.empiler(caractere)
        elif caractere == ")":
            if p.est_vide():
                return False
            else:
                p.depiler()
    return p.est_vide()


