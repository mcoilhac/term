# Tests
assert parenthesage("((()())(()))")
assert not parenthesage("())(()")
assert not parenthesage("(())(()")

# Autres tests
assert parenthesage(50*"("+50*")")
assert not parenthesage(50*"()"+")")
assert not parenthesage(50*"()"+")"+50*"("+50*")")

