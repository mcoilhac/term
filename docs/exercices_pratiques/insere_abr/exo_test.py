# Tests
a = Arbre(5)
insere(a, 2)
insere(a, 7)
insere(a, 3)
assert parcours(a, []) == [2, 3, 5, 7]
insere(a, 1)
insere(a, 4)
insere(a, 6)
insere(a, 8)
assert parcours(a, []) == [1, 2, 3, 4, 5, 6, 7, 8]

# Autres Tests
b = Arbre(10)
insere(b, 2)
insere(b, 12)
insere(b, 1)
insere(b, 11)
insere(b, 13)
insere(b, 9)
assert parcours(b, []) == [1, 2, 9, 10, 11, 12, 13]
c = Arbre(10)
insere(c, 2)
insere(c, 12)
insere(c, 1)
insere(c, 11)
insere(c, 13)
insere(c, 9)
insere(c, 14)
assert parcours(c, []) == [1, 2, 9, 10, 11, 12, 13, 14]
d = Arbre(10)
insere(d, 2)
insere(d, 12)
insere(d, 1)
insere(d, 11)
insere(d, 13)
insere(d, 9)
insere(d, 14)
insere(d, 0)
assert parcours(d, []) == [0, 1, 2, 9, 10, 11, 12, 13, 14]

