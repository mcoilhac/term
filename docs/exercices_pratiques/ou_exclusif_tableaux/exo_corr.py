def ou_exclusif_bits(a, b):
    if a == b:
        return 0
    return 1


def ou_exclusif_listes(liste_1, liste_2):
    taille = len(liste_1)
    return [ou_exclusif_bits(liste_1[i], liste_2[i]) for i in range(taille)]
