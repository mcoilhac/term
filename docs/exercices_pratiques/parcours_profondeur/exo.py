def parcours(adjacence, x, sommets_accessibles):
    if x ...: 
        sommets_accessibles.append(x)
        for y in ...: 
            parcours(adjacence, ...) 

def accessibles(adjacence, x):
    sommets_accessibles = []
    parcours(adjacence, ...) 
    return sommets_accessibles


# Tests

assert accessibles([[1, 2], [0], [0, 3], [1], [5], [4]], 0) == [0, 1, 2, 3]  # graphe orienté erreur sujet
assert accessibles([[1, 2], [0], [0, 3], [1], [5], [4]], 4) == [4, 5]  # graphe orienté erreur du sujet
assert accessible([[1, 2], [0, 3], [0,], [1], [5], [4]], 0) == [0, 1, 3, 2]  # graphe non orienté
assert accessible([[1, 2], [0, 3], [0,], [1], [5], [4]], 4) == [4, 5]  # graphe non orienté
