def separe(zeros_et_uns):
    """place tous les 0 de zeros_et_uns à gauche et tous les 1 à droite"""
    debut = ...  # indice de début
    fin = ...   # indice de fin
    while debut < fin:
        if zeros_et_uns[debut] == 0:
            debut = ...
        else:
            valeur = zeros_et_uns[fin]
            zeros_et_uns[fin] = ...
            ...
            fin = ...

# Tests

zeros_et_uns_1 = [0, 1, 0, 1, 0, 1, 0]
separe(zeros_et_uns_1)
assert zeros_et_uns_1 == [0, 0, 0, 0, 1, 1, 1]

zeros_et_uns_2 = [1, 1, 1, 0, 0, 0]
separe(zeros_et_uns_2)
assert zeros_et_uns_2 == [0, 0, 0, 1, 1, 1]
