# Tests
assert a_doublon([]) == False
assert a_doublon([1]) == False
assert a_doublon([1, 2, 4, 6, 6]) == True
assert a_doublon([2, 5, 7, 7, 7, 9]) == True
assert a_doublon([0, 2, 3]) == False

# Autres tests
tab_1 = [1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
tab_2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 13]
tab_3 = [1, 2, 3, 4, 5, 6, 6, 7, 8, 9, 10, 11, 12, 13]
tab_4 = [1, 2, 3, 4, 5, 6, 7, 7, 8, 9, 10, 11, 12, 13]
tab_5 = [3*i for i in range(100)]
tableaux = [tab_1, tab_2, tab_3, tab_4]
for tab in tableaux :
    assert a_doublon(tab) == True
assert a_doublon(tab_5) == False
