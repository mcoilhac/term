---
author: Gilles Lassus puis Mireille Coilhac
title: Insertion dans une liste
tags:
  - liste/tableau
  - important
---

On rappelle que les tableaux sont représentés par des listes en Python du type `list`.

Le but de cet exercice est d’écrire une fonction `ajoute` qui prend en paramètres trois
arguments `indice`, `element` et `tab` et renvoie un tableau `tab_ins` dans lequel les
éléments sont ceux du tableau `tab` avec, en plus, l’élément `element` à l’indice `indice`.

On considère que les variables `indice` et `element` sont des entiers positifs et que les
éléments de `tab` sont également des entiers.

En réalisant cette insertion, Les éléments du tableau `tab` dont les indices sont supérieurs
ou égaux à `indice` apparaissent décalés vers la droite dans le tableau `tab_ins`.

Si `indice` est égal au nombre d’éléments du tableau `tab`, l’élément `element` est ajouté
dans `tab_ins` après tous les éléments du tableau `tab`.

!!! example "Exemples"

    ```pycon
    >>> ajoute(1, 4, [7, 8, 9])
    [7, 4, 8, 9]
    >>> ajoute(3, 4, [7, 8, 9])
    [7, 8, 9, 4]
    >>> ajoute(0, 4, [7, 8, 9])
    [4, 7, 8, 9]
    ```

???+ question "Compléter le script ci-dessous"

    {{IDE('exo')}}

