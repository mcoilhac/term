def partition(pivot, tableau):
    ...




# Tests
assert partition(3, [1, 3, 4, 2, 4, 6, 3, 0]) == ([0, 3, 7], [1, 6], [2, 4, 5])
assert partition(3, [1, 4, 2, 4, 6, 0]) == ([0, 2, 5], [], [1, 3, 4])
assert partition(3, [1, 1, 1, 1]) == ([0, 1, 2, 3], [], [])
assert partition(3, []) == ([], [], [])
