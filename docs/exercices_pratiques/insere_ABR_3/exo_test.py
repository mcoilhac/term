# Tests

n0 = (None, 0, None)
n3 = (None, 3, None)
n2 = (None, 2, n3)
abr1 = (n0, 1, n2)

assert insertion_abr(abr1, 4) == ((None,0,None),1,(None,2,(None,3,(None,4,None))))
assert insertion_abr(abr1, -5) == (((None,-5,None),0,None),1,(None,2,(None,3,None)))
assert insertion_abr(abr1, 2) == ((None,0,None),1,(None,2,(None,3,None)))

# Autres Tests

abr2 = insertion_abr(abr1, -5)
assert insertion_abr(abr2, -2) == (((None, -5, (None, -2, None)), 0, None), 1, (None, 2, (None, 3, None)))

