# Tests
assert positif([-1, 0, 5, -3, 4, -6, 10, 9, -8]) == [0, 5, 4, 10, 9]
assert positif([-2]) == []

# Autres_tests
pile_3 = [i for i in range(100,-100, -1)]
assert positif(pile_3) == [i for i in range(100, -1, -1)]
pile_4 = [i*(-1)**i for i in range(100)]
assert positif(pile_4) == [2*i for i in range(50)]
