???+ note dépliée "La démarche"

	On suit pas à pas la démarche présentée dans l'énoncé.

	Un élément du tableau est un nombre si ce n'est ni l'opérateur $+$ ni $\times$. On teste donc `if element != '+' and element != '*':`.

	Dans le cas où élément est un opérateur, on traite séparément chaque cas (`+` et `*`). 
	À chaque fois on dépile les deux premiers nombres de la pile et on effectue l'opération. On empile le résultat.

	À la fin du parcours, l'expression étant bien formée, il ne reste qu'une valeur dans la pile : le résultat de l'évaluation que l'on dépile et renvoie immédiatement.

