# Tests

assert voisins_entrants([[1, 2], [2], [0], [0]], 0) == [2, 3]
assert voisins_entrants([[1, 2], [2], [0], [0]], 1) == [0]

# Autres tests

adj = [
    [1, 2, 3],
    [2],
    [3],
    [2]
]

assert voisins_entrants(adj, 0) == []
assert sorted(voisins_entrants(adj, 2))== [0, 1, 3]
