from random import randint

# Tests
lancer1 = [5, 6, 6, 2, 2]
assert paire_6(lancer1)
lancer2 = [6, 5, 1, 6, 6]
assert paire_6(lancer2)
lancer3 = [2, 2, 6]
assert not paire_6(lancer3)
lancer4 = lancer(0)
assert lancer4 == []
assert not paire_6(lancer4)

# Autres tests
lancer5 = [2, 6, 1, 2, 3, 5]
assert not paire_6(lancer5)
lancer6 = [6, 6, 1, 2, 3, 5]
assert paire_6(lancer6)
lancer7 = [6, 2, 1, 2, 3, 5, 1, 3, 4, 6]
assert paire_6(lancer7)

