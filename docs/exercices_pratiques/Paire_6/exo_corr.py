from random import randint

def lancer(n):
    return [randint(1, 6) for i in range(n)]

def paire_6(tab):
    cpt = 0
    for elt in tab:
        if elt == 6:
            cpt = cpt + 1
    if cpt >= 2 :
        return True
    else:
        return False
        
