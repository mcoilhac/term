---
author: Mireille Coilhac
title: Nombre d'occurences
tags:
  - 3-liste/tableau
--- 

Écrire une fonction python appelée `nbre_occurrences` qui prend en paramètres un élément `cible` et un tableau de `valeurs` et renvoie le nombre d'occurrences de `cible` dans le tableau `valeurs`.

> Il est **interdit** d'utiliser la méthode `count`

!!! example "Exemples"

    ```python
    >>> nbre_occurrences(5, [2, 5, 3, 5, 6, 9, 5])
    3
    >>> nbre_occurrences('A', ['B', 'A', 'B', 'A', 'R'])
    2
    >>> nbre_occurrences(12, [1, 7, 21, 36, 44])
    0
    ```

{{ IDE('exo', SANS='count') }}
