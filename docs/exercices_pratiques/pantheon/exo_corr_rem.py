def pantheon(eleves, notes):
    note_maxi = 0
    meilleurs_eleves = []

    for i in range(len(eleves)) :
        if notes[i] == note_maxi :
            meilleurs_eleves.append(eleves[i])
        elif notes[i] > note_maxi:
            note_maxi = notes[i]
            meilleurs_eleves = [eleves[i]]
        print(note_maxi, meilleurs_eleves)  # Pour comprendre

    return (note_maxi, meilleurs_eleves)

print(pantheon(['a', 'b', 'c', 'd'], [15, 18, 12, 18]))  # Pour tester
