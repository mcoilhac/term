On peut réaliser une version **itérative** de cette fonction, qui repose sur un algorithme de dichotomie.

```python
def gros_lot(lots, debut, fin):
    """
    Renvoie le numéro du gros lot dans lots
    """

    while debut < fin:
        if debut + 1  == fin:  # une seule pièce dans la zone d'étude
            return debut
        milieu = (debut + fin) // 2
        observation = indication(lots, debut, milieu, milieu, fin)
        if observation == "groupe_1":
            fin = milieu
        else:
            debut = milieu
```
