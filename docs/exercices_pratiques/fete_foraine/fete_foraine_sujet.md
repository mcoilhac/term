---
author: Mireille Coilhac
title: La fête foraine
---

Vous vous amusez à une fête foraine. Vous décidez de jouer au stand "Gagnez le gros lot !".
Des lots visuellement identiques, numérotés de 1 à 32, sont exposés. Ils ont tous la même valeur de 1€, sauf un qui a la valeur de 100 €. Vous devez trouver une stratégie pour déterminer à coup sûr le numéro du lot de valeur 100 €.

La règle du jeu est la suivante : vous pouvez sélectionner deux groupes de lots, nommés `groupe_1` et `groupe_2`. Le forain (qui connaît bien-sûr le numéro du gros lot) vous donnera une indication sur les valeurs  globales de `groupe_1` et `groupe_2`.
Si les deux groupes ont la même valeur globale, il dira `"identique"`,  si le `groupe_1` a plus de valeur, il dira `"groupe_1"`, sinon il dira `"groupe_2"`. 

😥 Vous ne pouvez pas lui demander plus de six indications.

La classe `Lots_en_jeu` permet de créer une sélection de lots numérotés qui seront mis en jeu par le forain.

Vous devez rédiger la fonction `gros_lot` qui prend en paramètres :

* `lots` de la classe `Lots_en_jeu` ;
* le numéro `debut `de début de la zone de recherche (inclus) ;
* le numéro `fin` de fin de la zone de recherche (exclu).

Cette fonction renvoie le numéro du lot de valeur 100 € dans `lots`.

Les indications données par le forain sont mises en oeuvre par `indication(lots, debut_1, fin_1, debut_2, fin_2)`.

Le `groupe_1` contient les lots dont les numéros de début et de fin sont `debut_1` (inclus) et `fin_1` (exclu).
Le `groupe_2` contient les lots dont les numéros de début et de fin sont `debut_2` (inclus) et `fin_2` (exclu).

Le résultat renvoyé sera : 

* `"groupe_1"` si le `groupe_1` a la plus grande valeur ;
* `"identique"` si les deux groupes ont la même valeur ;
* `"groupe_2"` si le `groupe_2` a la plus grande valeur.

Ainsi `indication(lots, 1, 15, 15, 30)` compare les valeurs totales des lots de numéros allant de `1` (inclus) à `15` (exclu) pour le `groupe_1` et de `15` (inclus) à `30` (exclu) pour le `groupe_2`.

La fonction `indication` est déjà écrite, vous ne devez pas l’écrire.

On fournit ci-dessous quelques exemples d’utilisation des différentes fonctions :

```python
>>> lots = Lots_en_jeu()  # une sélection de 32 lots
>>> lots
'Une sélection de 32 lots'
>>> # comparaison des valeurs totales des groupes 
>>> # de lots dont les numéros sont dans [1, 20[ et [28, 32[
>>> indication(lots, 1, 20, 28, 32)
'groupe_1'
>>> indication(lots, 5, 15, 17, 27)
'identique'
>>> gros_lot(lots, 1, 33)
2
```

Votre fonction, pour 32 lots, ne doit pas appeler plus de 6 fois la fonction `indication`.

Toute tentative juste de résolution sera valorisée.

???+ question "Exercice"

    Compléter ci-dessous :

    {{IDE('scripts/fete')}}

