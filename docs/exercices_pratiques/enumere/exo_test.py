# Tests
assert enumere([1, 1, 2, 3, 2, 1]) == {1: [0, 1, 5], 2: [2, 4], 3: [3]}

# Autres tests
assert enumere([]) == {}
assert enumere(50*[10]) == {10:[i for i in range(50)]}
assert enumere([i for i in range(50)]) == {i:[i] for i in range(50)}







