---
author: Gilles Lassus puis l'équipe e-nsi
title: Carrés semi-magiques
tags:
  - 4-grille
  - 7-POO
---

Nous travaillons dans cet exercice sur des tableaux carrés d'entiers positifs.

Nous appelons un carré d'ordre $n$ un tableau de $n$ lignes et $n$ colonnes dont chaque case contient un entier positif.  

!!! info "Carré semi-magique"

    Un carré est dit semi-magique lorsque les sommes des éléments situés sur chaque ligne, chaque
    colonne sont égales.


!!! example "Exemples avec trois carrés"

    === "Carré d'ordre 2 : `carre_2`"

        <table>
        <tr>
            <td style="border:1px solid;">1</td>
            <td style="border:1px solid;">7</td>
        </tr>
        <tr>
            <td style="border:1px solid;">7</td>
            <td style="border:1px solid;">1</td>
        </tr>
        </table>

        `carre_2` est semi-magique car la somme de chaque ligne et de chaque colonne est égale à 8.

    === "Carré d'ordre 3 : `carre_3`"

        <table>
        <tr>
            <td style="border:1px solid;">3</td>
            <td style="border:1px solid;">4</td>
            <td style="border:1px solid;">5</td>
        </tr>
        <tr>
            <td style="border:1px solid;">4</td>
            <td style="border:1px solid;">4</td>
            <td style="border:1px solid;">4</td>
        </tr>
        <tr>
            <td style="border:1px solid;">5</td>
            <td style="border:1px solid;">4</td>
            <td style="border:1px solid;">3</td>
        </tr>
        </table>  

        `carre_3` est semi-magique car la somme de chaque ligne et de chaque colonne est égale à 12.

    === "Carré d'ordre 3 : `carre_3_bis`"

        <table>
        <tr>
            <td style="border:1px solid;">2</td>
            <td style="border:1px solid;">9</td>
            <td style="border:1px solid;">4</td>
        </tr>
        <tr>
            <td style="border:1px solid;">7</td>
            <td style="border:1px solid;">0</td>
            <td style="border:1px solid;">3</td>
        </tr>
        <tr>
            <td style="border:1px solid;">6</td>
            <td style="border:1px solid;">1</td>
            <td style="border:1px solid;">8</td>
        </tr>
        </table>

        `carre_3_bis` n'est pas semi-magique car la somme de la première ligne est égale à 15 alors que celle de la deuxième ligne est égale à 10.


La classe `Carre` ci-après contient des méthodes qui permettent de manipuler des carrés.

* La méthode `__init__` permet de créer un carré sous forme d'un tableau à deux dimensions
à partir d'un p-uplet d'entiers `nombres`. Nous remarquons que l'ordre du carré créé est la racine carré de la longueur du p-uplet passé en paramètre. Par exemple avec un p-uplet de taille 4, on crée un carré d'ordre 2, avec un de taille 9, un carré d'ordre 3.

* La méthode `affiche` permet d'afficher le carré créé.

!!! example "Exemples"

    ```pycon
    >>> nombres_3 = (3, 4, 5, 4, 4, 4, 5, 4, 3)
    >>> carre_3 = Carre(nombres_3)
    >>> carre_3.affiche()
    [3, 4, 5]
    [4, 4, 4]
    [5, 4, 3]
    ```

* La méthode `somme_ligne` prend en paramètre un entier `i` et renvoie la somme des éléments de la ligne `i` du carré.

!!! example "Exemple"

    ```pycon
    >>> carre_3.somme_ligne(0)
    12
    ```

* La méthode `somme_colonne` prend en paramètre un entier `j` et renvoie la somme des éléments de la colonne `j` du carré.

!!! example "Exemple"

    ```pycon
    >>> carre_3_bis.somme_colonne(1)
    10
    ```

* La méthode `est_semi_magique` renvoie `True` si le carré est semi magique,
`False` sinon. 

!!! example "Exemples"

    ```pycon
    >>> carre_2.est_semi_magique()
    True
    >>> carre_3.est_semi_magique()
    True
    >>> carre_3_bis.est_semi_magique()
    False
    ```
    
???+ question

    Compléter le code ci-dessous, puis le tester sur les carrés `carre_2`, `carre_3` et `carre_3_bis`.

    {{ IDE('exo', MAX_SIZE=55) }}
    
