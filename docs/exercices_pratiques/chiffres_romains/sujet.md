---
author: BNS2023-07.2 puis Gilles Lassus
title: Chiffres romains (1)
tags:
  - 3-dictionnaire
  - 6-récursivité
---

Le but de cet exercice est d’écrire une fonction récursive `traduire_romain` qui prend
en paramètre une chaîne de caractères, non vide, représentant un nombre écrit en
chiffres romains et qui renvoie son écriture décimale.


Les chiffres romains considérés sont : I, V, X, L, C, D et M. Ils représentent
respectivement les nombres 1, 5, 10, 50, 100, 500, et 1000 en base dix.


On dispose d’un dictionnaire `romains` dont les clés sont les caractères apparaissant
dans l’écriture en chiffres romains et les valeurs sont les nombres entiers associés en
écriture décimale :


`romains = {"I":1, "V":5, "X":10, "L":50, "C":100, "D":500, "M":1000}`


Le code de la fonction `traduire_romain` fournie repose sur le
principe suivant :

- la valeur d’un caractère est ajoutée à la valeur du reste de la chaîne si ce
caractère a une valeur supérieure (ou égale) à celle du caractère qui le suit ;

- la valeur d’un caractère est retranchée à la valeur du reste de la chaîne si ce
caractère a une valeur strictement inférieure à celle du caractère qui le suit.

Ainsi, XIV correspond au nombre 10 + 5 - 1 puisque :

- la valeur de X (10) est supérieure à celle de I (1), on ajoute donc 10 à la valeur du
reste de la chaîne, c’est-à-dire IV ;

- la valeur de I (1) est strictement inférieure à celle de V (5), on soustrait donc 1 à
la valeur du reste de la chaîne, c’est-à-dire V.


???+ question "Compléter le code fourni dans l'IDE et le tester"

	{{ IDE('exo') }}


??? tip "Astuce"

	On peut utiliser des "slices" avec les chaînes de caractères

