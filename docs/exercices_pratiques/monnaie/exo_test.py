# Tests
assert rendu_monnaie(452, 500) == [20, 20, 5, 2, 1]

# Autres tests
assert rendu_monnaie(452, 500) == [20, 20, 5, 2, 1]
assert rendu_monnaie(100, 100) == []
assert rendu_monnaie(102, 500) == [200, 100, 50, 20, 20, 5, 2, 1]
assert rendu_monnaie(101, 500) == [200, 100, 50, 20, 20, 5, 2, 2]
