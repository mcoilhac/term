On a ici un algorithme glouton, car on part de la fin de la liste `pieces` qui est triée par ordre croissant.

Dans l'algorithme de rendu de monnaie vu en cours, la liste des pièces était triée par ordre décroissant, et donc l'algorithme la parcourait à partir du début.
