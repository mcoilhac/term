def romain(valeur):
    ...


# Tests
assert romain(4) == "IV"
assert romain(5) == "V"
assert romain(6) == "VI"
assert romain(3042) == "MMMXLII"
