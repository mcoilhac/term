!!! info "Un algorithme glouton"

    On procède de façon analogue au « Rendu de monnaie » : au lieu de rendre en priorité les « pièces » les plus grandes, ici, on écrit en priorité les « valeurs » les plus grandes.

    ```text title="📋 Explications"
    Créer une chaîne de caractère vide et l'affecter à la variable résultat
    Pour chaque nombre utilisable :
        Tant que la valeur à convertir est supérieure ou égale à ce nombre :
            Concaténer l'écriture romaine de ce nombre au résultat
            Soustraire ce nombre à la valeur à convertir
    ```
    !!! warning "Remarque"

        Vous avez peut-être utilisé un dictionnaire `VALEURS= {1000: "M, 900:"CM", 500: "D", 400:"CD" ...}`  
        🤔 Cela a fonctionné ...  
        Attention, depuis plusieurs versions, dans python les dictionnaires sont parcourus dans l'ordre dans lequel ils sont "écrits" ou saisis. Si vous travaillez sur une version plus ancienne de python, ce n'est pas le cas. Un dictionnaire n'est pas **ordonné** : l'ordre dans lequel sont écrits les couples (clé, valeur) n'a aucune importance. Vous ne pourrez pas utiliser cette méthode avec d'autres langages. Il faut utiliser pour un algorithme glouton une structure ordonnée.


!!! info "Méthode arithmétique"

    On peut aussi procéder en utilisant des divisions euclidiennes afin de ne pas écrire de boucle `#!py while`. On rappelle en effet que :

    * `#!py 1748 // 1000` est évalué à `#!py 1`, c'est le quotient de la division euclidienne de $1\,748$ par $1\,000$ ;
    * `#!py 1748 %  1000` est évalué à `#!py 748`, c'est le reste de cette même division.

    ```python
    def romain(valeur):
        resultat = ""
        for entier, symbole in VALEURS:
            quotient = valeur // entier
            valeur = valeur % entier
            resultat += symbole * quotient  # concaténation de caractères
        return resultat
    ```

!!! note "Et après ?"

    Les entiers supérieurs ou égaux à $5\,000$ s'écrivent en utilisant une barre horizontale indiquant qu'il faut multiplier le facteur concerné par $1\,000$.

    Par exemple $\overline{\text{VI}}\text{CCCVIII}$ représente $6\,308$ car le $\overline{\text{VI}}$ est multiplié par $1\,000$.
