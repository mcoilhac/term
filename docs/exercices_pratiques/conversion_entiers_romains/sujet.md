---
author: Nicolas Revéret
title: Chiffres romains (2)
tags:
  - 2-tuple
  - 3-liste/tableau
  - 4-glouton
  - Difficulté **
---

# Écrire un nombre en chiffres romains

On souhaite dans cet exercice écrire des nombres entiers strictement positifs en chiffres romains.

Cette notation a évolué au fil des siècles. On se propose ici d'utiliser la méthode fixée au Moyen-Âge en se limitant aux entiers inférieurs à $5\,000$.

??? abstract "1. Symboles utilisés"
    Les symboles valides sont les suivants :

    |  Valeur  | Symbole  |
    | :------: | :------: |
    |   $1$    | `#!py I` |
    |   $5$    | `#!py V` |
    |   $10$   | `#!py X` |
    |   $50$   | `#!py L` |
    |  $100$   | `#!py C` |
    |  $500$   | `#!py D` |
    | $1\,000$ | `#!py M` |

    Ainsi, `#!py MMXII` ($2012$) est une écriture valide, `#!py AM` non.

    
??? abstract "2. Ordre d'écriture"

    La représentation d'un nombre en chiffres romains se lit de gauche à droite. Dans le cas de base, on y rencontre des symboles de valeurs décroissantes.

    Ainsi, `#!py XVI` est l'écriture valide du nombre $16$, `#!py VIX` non.

    :warning: La règle n°5 autorise toutefois certaines entorses à cette règle.

??? abstract "3. Notation additive"

    Dans le cas de base, les valeurs des symboles sont additionnées.

    Ainsi `#!py LXXIII` représente $50+10+10+1+1+1=73$.

??? abstract "4. Répétitions"

    Le symbole `#!py M` peut être répété autant de fois que nécessaire.

    **Tous** les autres symboles ne peuvent pas être répétés plus de 3 fois (inclus).

    Ainsi, `#!py MMMMVI` ($4\,006$) est une écriture valide, `#!py MMMMIIIIII` non.

    :face_with_monocle: Cette règle a été fixée au Moyen-Âge. L'écriture `#!py MMMMIIIIII` était valide dans l'Antiquité !

??? abstract "5. Notation soustractive"

    Afin d'éviter les répétitions interdites, on s'autorise des entorses à la règle n°2.

    On peut ainsi faire précéder les symboles :

    * `#!py X` et `#!py V` par un unique symbole `#!py I`,
    * `#!py C` et `#!py L` par un unique symbole `#!py X`,
    * `#!py M` et `#!py D` par un unique symbole `#!py C`.

    Dans ces cas, la valeur qui précède est soustraite à la valeur précédée.

    On a donc les correspondances suivantes :

    | Symboles  | Valeurs |
    | :-------: | :-----: |
    | `#!py IV` |   $4$   |
    | `#!py IX` |   $9$   |
    | `#!py XL` |  $40$   |
    |    ...    |   ...   |


    Ainsi, `#!py MCMXLI` représente $1\,000+900+40+1=1\,941$.

???+ question "La fonction pour convertir"

    Écrire la fonction `romain` qui prend en argument un nombre entier strictement positif `valeur` et renvoie son écriture en chiffres romains.

    ???+ example "Exemples"

        ```pycon
        >>> romain(4)
        'IV'
        >>> romain(5)
        'V'
        >>> romain(6)
        'VI'
        >>> romain(5042)
        'MMMMMXLII'
        ```

    {{ IDE('exo') }}
    

??? tip "Astuce (1)"

    Penser à un algorithme glouton.


??? tip "Astuce (2)"

    On pourra utiliser la liste `#!py VALEURS = [..., (4, "IV"), ...]` après l'avoir complétée.


??? tip "Astuce (3)"

    On pourra utiliser la liste `#!py VALEURS = [(1000, "M"), (900, "CM"), (500, "D"), ...]` après l'avoir complétée.
