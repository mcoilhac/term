---
author: Mireille Coilhac
title: Cycles - compléments
---

On peut trouver d'autres algorithmes pour la détection de cycles.

!!! info

    Nous nous plaçons dans le cas de graphes connexes[^1] non orientés.


## I. Utiliser les distances

!!! info "Les distances"

    On parcourt le graphe en largeur, à partir d'un sommet `depart` choisi au hasard. Au fur et à mesure de la progression, 
    on construit le dictionnaire `distances` qui prend comme clés les sommets visités, et comme valeur associée à chaque clé la distance 
    à laquelle il se trouve du sommet de départ (nombre d'arcs), que nous appelons ici **distance**. Le parcours étant un parcours en largeur, 
    les distances des sommets parcourus restent les mêmes, ou augmentent. Si la distance d'un voisin d'un sommet, est supérieure ou égale 
    à la distance du sommet, cela signifie qu'on ne provient pas de ce sommet. On arrête le parcours, et la fonction renvoie `True`.


???+ question "À vous de jouer"

    Compléter le script ci-dessous

    Les graphes utilisés pour les tests sont :

    `graphe_5 :`

    ![graphe 5](images/graphe_5.svg){ width=30% }

    `graphe_6 :`

    ![graphe 6](images/graphe_6.svg){ width=30% }

    {{ IDE('scripts/cycle_dist') }}


## II. Algorithme récursif

???+ question "Recherche de cycle récursive"

    Tester ci-dessous

    Les graphes utilisés pour les tests sont :

    `graphe_5 :`

    ![graphe 5](images/graphe_5.svg){ width=30% }

    `graphe_6 :`

    ![graphe 6](images/graphe_6.svg){ width=30% }

    {{ IDE('scripts/cycle_recursif') }}

    Nous pouvons observer le déroulement dans Python Tutor pour ces deux graphes :

    [Déroulé pour graphe_6](https://pythontutor.com/visualize.html#code=def%20cherche_cycle_rec%28graphe,%20sommet,%20vus%3DNone%29%3A%0A%20%20%20%20if%20vus%20is%20None%3A%0A%20%20%20%20%20%20%20%20vus%20%3D%20%7Bsommet%3A%20None%7D%0A%20%20%20%20precedent%20%3D%20vus%5Bsommet%5D%0A%20%20%20%20for%20voisin%20in%20graphe%5Bsommet%5D%3A%0A%20%20%20%20%20%20%20%20if%20voisin%20!%3D%20precedent%3A%0A%20%20%20%20%20%20%20%20%20%20%20%20if%20voisin%20in%20vus%3A%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20return%20True%0A%20%20%20%20%20%20%20%20%20%20%20%20else%3A%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20vus%5Bvoisin%5D%20%3D%20sommet%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20if%20cherche_cycle_rec%28graphe,%20voisin,%20vus%29%3A%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20return%20True%0A%20%20%20%20return%20False%0A%20%20%20%20%0Agraphe_6%20%3D%20%7B%22A%22%3A%20%5B%22B%22,%20%22C%22%5D,%20%22B%22%3A%20%5B%22A%22%5D,%20%22C%22%3A%20%5B%22A%22,%20%22D%22,%20%22E%22%5D,%20%22D%22%3A%20%5B%22C%22,%20%22E%22%5D,%20%22E%22%3A%20%5B%22C%22,%20%22D%22%5D%7D%0Aprint%28cherche_cycle_rec%28graphe_6,%20%22A%22%29%29&cumulative=false&heapPrimitives=nevernest&mode=edit&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false){ .md-button target="_blank" rel="noopener" }

    [Déroulé pour graphe_5](https://pythontutor.com/render.html#code=def%20cherche_cycle_rec%28graphe,%20sommet,%20vus%3DNone%29%3A%0A%20%20%20%20if%20vus%20is%20None%3A%0A%20%20%20%20%20%20%20%20vus%20%3D%20%7Bsommet%3A%20None%7D%0A%20%20%20%20precedent%20%3D%20vus%5Bsommet%5D%0A%20%20%20%20for%20voisin%20in%20graphe%5Bsommet%5D%3A%0A%20%20%20%20%20%20%20%20if%20voisin%20!%3D%20precedent%3A%0A%20%20%20%20%20%20%20%20%20%20%20%20if%20voisin%20in%20vus%3A%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20return%20True%0A%20%20%20%20%20%20%20%20%20%20%20%20else%3A%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20vus%5Bvoisin%5D%20%3D%20sommet%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20if%20cherche_cycle_rec%28graphe,%20voisin,%20vus%29%3A%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20return%20True%0A%20%20%20%20return%20False%0A%20%20%20%20%0Agraphe_5%20%3D%20%7B%22A%22%3A%5B%22B%22,%20%22C%22%5D,%20%22B%22%3A%5B%22A%22,%20%22E%22%5D,%20%22C%22%3A%5B%22A%22%5D,%20%22E%22%3A%5B%22B%22%5D%7D%0Aprint%28cherche_cycle_rec%28graphe_5,%20%22A%22%29%29&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false){ .md-button target="_blank" rel="noopener" }

## III. Site créé par Frédéric ZINELLI

[AlgoGraphe](http://frederic.zinelli.gitlab.io/graph-application/){ .md-button target="_blank" rel="noopener" }

## Crédits

Serge Bays, Romain Janvier

[^1]: Voir : Les graphes - Structure au paragraphe 1.2.4 : [Structures de graphes](1_graphes_generalites.md){ .md-button target="_blank" rel="noopener" }
