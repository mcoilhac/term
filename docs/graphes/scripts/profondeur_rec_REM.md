Autre solution possible : 

```python
def parcours_profondeur(graphe, depart, vus = None):
    if vus is None:
        vus = []
    if not depart in vus:
        vus.append(depart)
        for voisin in graphe[depart]:
            parcours_profondeur(graphe, voisin, vus)
    return vus
```
