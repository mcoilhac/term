---
author: Gilles Lassus et Mireille Coilhac
title: Les graphes - Structure
---

*Ce cours est très largement inspiré du cours de Gilles Lassus, qui s'est lui même intégralement inspiré du cours de Cédric Gouygou*

## I. Notion de graphe et vocabulaire

Le concept de graphe permet de résoudre de nombreux problèmes en mathématiques comme en informatique. C'est un outil de représentation très courant, et nous l'avons déjà rencontré à plusieurs reprises, en particulier lors de l'étude de réseaux.


### 1.1 Exemples de situations

#### 1.1.1 Réseau informatique

![22J2AS1_ex2.png](images/22J2AS1_ex2.png){ width=30% }


#### 1.1.2 Réseau de transport

![carte-metro-parisien-768x890.jpg](images/carte-metro-parisien-768x890.jpg){ width=40% }

#### 1.1.3 Réseau social

![graphe_RS.png](images/graphe_RS.png){ width=30% }

#### 1.1.4 Poster du courrier

Vous êtes le facteur d'un petit village, vous distribuez le courrier à vélo, à la seule force de vos jambes. Vous devez passer devant toutes les maisons du village, ce qui implique de traverser toutes les rues. Mais soucieux de préserver vos forces et de renouveler continuellement votre découverte des paysages, vous ne voulez pas traverser deux fois la même rue. Ici chaque nœud est un carrefour, et chaque arête une rue. Vous êtes en train de chercher un **circuit Eulérien** !

Il doit son nom à Leonhard Euler, qui chercha à savoir s'il était possible de franchir les 7 ponts de Königsberg sans jamais repasser deux fois sur le même (et en ne traversant le fleuve que grâce aux ponts, bien entendu).

<div class="milieu" style="margin-left:10px;">
<img alt="Les 7 ponts de Königsberg" src="http://zestedesavoir.com/media/galleries/912/f16ed77f-92ef-4a07-bd8f-e41f37ffff48.png.960x960_q85.jpg">  </p>
</div>
*Source de l'image : http://zestedesavoir.com*



#### 1.1.5 Généralisation
Une multitude de problèmes concrets d'origines très diverses peuvent donner lieu à des modélisations par des graphes : c'est donc une structure essentielle en sciences, qui requiert un formalisme mathématique particulier que nous allons découvrir. 

![graph_math.png](images/graph_math.png){ width=50% }

L'étude de la théorie des graphes est un champ très vaste des mathématiques : nous allons surtout nous intéresser à l'implémentation en Python d'un graphe et à différents problèmes algorithmiques qui se posent dans les graphes.




### 1.2 Vocabulaire
En général, un graphe est un ensemble d'objets, appelés *sommets* ou parfois *nœuds* (*vertex* or *nodes* en anglais) reliés par des *arêtes* ou *arcs* ((*edges* en anglais)).
Ce graphe peut être **non-orienté** ou **orienté** .

#### 1.2.1 Graphe non-orienté

![exemple_graphe.png](images/exemple_graphe.png){ width=30% }

Dans un graphe **non-orienté**, les *arêtes* peuvent être empruntées dans les deux sens, et une *chaîne* est une suite de sommets reliés par des arêtes, comme C - B - A - E par exemple. La *longueur* de cette chaîne est alors 3, soit le nombre d'arêtes.

Les sommets B et E sont *adjacents* au sommet A, ce sont les *voisins* de A.


**Exemple de graphe non-orienté** : le graphe des relations d'un individu sur Facebook est non-orienté, car si on est «ami» avec quelqu'un la réciproque est vraie.

#### 1.2.2 Graphe orienté

![exemple_graphe_oriente.png](images/exemple_graphe_oriente.png){ width=30% }

Dans un graphe **orienté**, les *arcs* ne peuvent être empruntés que dans le sens de la flèche, et un *chemin* est une suite de sommets reliés par des arcs, comme B → C → D → E par exemple.

Les sommets C et D sont *adjacents* au sommet B (mais pas A !), ce sont les *voisins* de B.

**Exemple de graphe orienté** : le graphe des relations d'un individu sur Twitter est orienté, car on peut «suivre» quelqu'un sans que cela soit réciproque.

#### 1.2.3 Graphe pondéré

![exemple_graphe_pondere.png](images/exemple_graphe_pondere.png){ width=30% }

Un graphe est **pondéré** (ou valué) si on attribue à chaque arête une valeur numérique (la plupart du temps positive), qu'on appelle *mesure*, *poids*, *coût* ou *valuation*.

Par exemple:

- dans le protocole OSPF, on pondère les liaisons entre routeurs par le coût;
- dans un réseau routier entre plusieurs villes, on pondère par les distances.


#### 1.2.4 Connexité

Un graphe est **connexe** s'il est d'un seul tenant: c'est-à-dire si n'importe quelle paire de sommets peut toujours être reliée par une chaîne. Autrement un graphe est connexe s'il est «en un seul morceau».

Par exemple, le graphe précédent est connexe. Mais le suivant ne l'est pas: il n'existe pas de chaîne entre les sommets A et F par exemple.

![exemple_graphe_non_connexe.png](images/exemple_graphe_non_connexe.png){ width=30% }

Il possède cependant deux **composantes connexes** : le sous-graphe composé des sommets A, B, C, D et E d'une part et le sous-graphe composé des sommets F, G et H.


## II. Modélisations d'un graphe

Pour modéliser un graphe, il faut établir par convention une manière de donner les renseignements suivants :

- qui sont les sommets ?
- pour chaque sommet, quels sont ses voisins ? (et éventuellement quel poids porte l'arête qui les relie)


### 2.1 Représentation par matrice d'adjacence

!!! abstract "Principe"
    - On classe les sommets (en les numérotant, ou par ordre alphabétique).
    - on représente les arêtes (ou les arcs) dans une matrice, c'est-à-dire un tableau à deux dimensions où on inscrit un 1 en ligne `i` et colonne `j` si les sommets de rang `i` et de rang `j` sont **voisins** (dits aussi *adjacents*).

    Ce tableau s'appelle une **matrice d'adjacence** (on aurait très bien pu l'appeler aussi *matrice de voisinage*).


#### 2.1.1 Graphe non orienté


![matgraph_1.png](images/matgraph_1.png){ width=50% }


Dans ce graphe non orienté, comme B est voisin de C, C est aussi voisin de B, ce qui signifie que l'arête qui relie B et C va donner lieu à deux "1" dans la matrice, situé de part et d'autre de la diagonale descendante (un mathématicien parlera de matrice *symétrique*).


#### 2.1.2 Graphe orienté

![matgraph_2.png](images/matgraph_2.png){ width=50% }


#### 2.1.3 Graphe pondéré

![matgraph_3.png](images/matgraph_3.png){ width=55% }


???+ question "Exercice 1"

    Soit un ensemble d'amis connectés sur un réseau social quelconque. Voici les interactions qu'on a recensées:

    - André est ami avec Béa, Charles, Estelle et Fabrice,
    - Béa est amie avec André, Charles, Denise et Héloïse,
    - Charles est ami avec André, Béa, Denise, Estelle, Fabrice et Gilbert,
    - Denise est amie avec Béa, Charles et Estelle,
    - Estelle est amie avec André, Charles et Denise,
    - Fabrice est ami avec André, Charles et Gilbert,
    - Gilbert est ami avec Charles et Fabrice,
    - Héloïse est amie avec Béa.
    
    **Q1.** Représenter le graphe des relations dans ce réseau social (on désignera chaque individu par l'initiale de son prénom). Il est possible de faire en sorte que les arêtes ne se croisent pas !

    ??? success "Solution Q1"

        ![grapheRS](images/grapheRS.png){ width=30%; : .center }

        
    **Q2.** Donner la matrice d'adjacence de ce graphe.

    ??? success "Solution Q2"

        $\pmatrix{
        0 & 1 & 1 & 0 & 1 & 1 & 0 & 0 \\
        1 & 0 & 1 & 1 & 0 & 0 & 0 & 1 \\
        1 & 1 & 0 & 1 & 1 & 1 & 1 & 0 \\
        0 & 1 & 1 & 0 & 1 & 0 & 0 & 0 \\
        1 & 0 & 1 & 1 & 0 & 0 & 0 & 0 \\
        1 & 0 & 1 & 0 & 0 & 0 & 1 & 0 \\
        0 & 0 & 1 & 0 & 0 & 1 & 0 & 0 \\
        0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 \\
        }$
        
        
???+ question "Exercice 2"

    Construire les graphes correspondants aux matrices d'adjacence suivantes:

    **Q1.** $M_1 =\pmatrix{
        0&1&1&1&1\\
        1&0&1&0&0\\
        1&1&0&1&0\\
        1&0&1&0&1\\
        1&0&0&1&0\\
        }$

    ??? success "Solution Q1"

        ![ex2_Q1.png](images/ex2_Q1.png){ width=20%; : .center }
        
    **Q2.** $M_2=\pmatrix{
        0&1&1&0&1\\
        0&0&1&0&0\\
        0&0&0&1&0\\
        1&0&0&0&1\\
        0&0&0&0&0\\
        }$
    
    ??? success "Solution Q2"

        ![ex2_Q2.png](images/ex2_Q2.png){ width=20%; : .center }


    **Q3.** $M_3=\pmatrix{
        0&5&10&50&12\\
        5&0&10&0&0\\
        10&10&0&8&0\\
        50&0&8&0&100\\
        12&0&0&100&0\\
        }$    

    ??? success "Solution Q3"

        ![ex2_Q3.png](images/ex2_Q3.png){ width=22%; : .center }


#### 2.1.5 Implémentation Python des matrices d'adjacence

!!! info "Matrices d'adjacence en Python"
    Une matrice se représente naturellement par une liste de listes.

    **Exemple:**
    La matrice $M_1 =\pmatrix{
        0&1&1&1&1\\
        1&0&1&0&0\\
        1&1&0&1&0\\
        1&0&1&0&1\\
        1&0&0&1&0\\
        }$, associée au graphe suivant
        
    ![ex2_Q1.png](images/ex2_Q1.png){ width=20% }

    sera représentée par la variable ```G``` suivante :

    ```python title=""
    G = [[0, 1, 1, 1, 1],
          [1, 0, 1, 0, 0],
          [1, 1, 0, 1, 0],
          [1, 0, 1, 0, 1],
          [1, 0, 0, 1, 0]]
    ```

**Complexité en mémoire et temps d'accès :**

- Pour un graphe à $n$ sommets, la complexité en mémoire (appelée aussi *complexité spatiale*) de la représentation matricielle est en $O(n^2)$.

- Tester si un sommet est isolé (ou connaître ses voisins) est en $O(n)$ puisqu'il faut parcourir une ligne, mais tester si deux sommets sont adjacents (voisins) est en $O(1)$, c'est un simple accès au tableau.



La modélisation d'un graphe par sa matrice d'adjacence est loin d'être la seule manière de représenter un graphe : nous allons voir une autre modélisation, par **liste d'adjacence**.

### 2.2 Représentation par listes d'adjacence

!!! info "listes d'adjacence ... avec un dictionnaire"

    - On associe à chaque sommet sa liste des voisins (c'est-à-dire les sommets adjacents). On utilise pour cela un dictionnaire dont les clés sont les sommets et les valeurs les listes des voisins.

    - Dans le cas d'un graphe orienté on associe à chaque sommet la liste des *successeurs* (ou bien des *prédécesseurs*, au choix).

    Par exemple, le graphe 
    
    ![ex2_Q1.png.png](images/ex2_Q1.png){ width=20%; : .center } 
    sera représenté par le dictionnaire :


    ```python title=""
    G = {'A': ['B', 'C', 'D', 'E'],
         'B': ['A', 'C'],
         'C': ['A', 'B', 'D'],
         'D': ['A', 'C', 'E'],
         'E': ['A', 'D']
        }
    ```

!!! info "listes d'adjacences ... avec une liste"

    Dans le cas où les sommets sont numérotés de $0$ à $n-1$ pour un graphe de taille $n$, le dictionnaire précédent peut être remplacé 
    par une liste de listes `liste_adjacence`: 

    * `liste_adjacence[0]` contient la liste des voisins du sommet `0`
    * `liste_adjacence[i]` contient la liste des voisins du sommet `i`

    ![exemple_1.svg](images/exemple_1_vert.svg){ width=40%; : .center } 

    Ce graphe a pour liste d'adjacence implémentée avec un dictionnaire :

    ```python title=""
    {
        0: [1, 2, 3],
        1: [2],
        2: [0],
        3: [2]
    }
    ```

    Ce graphe a pour liste d'adjacence implémentée avec une liste :

    ```python title=""
    [
        [1, 2, 3],
        [2],
        [0],
        [2]
    ]
    ```



**Complexité en mémoire et temps d'accès :**

- Pour un graphe à $n$ sommets et $m$ arêtes, la complexité spatiale de la représentation en liste d'adjacence est en $O(n+m)$. C'est beaucoup mieux qu'une matrice d'adjacence lorsque le graphe comporte peu d'arêtes (i.e. beaucoup de 0 dans la matrice, non stockés avec des listes).

- Tester si un sommet est isolé (ou connaître ses voisins) est en $O(1)$ puisqu'on y accède immédiatement, mais tester si deux sommets sont adjacents (voisins) est en $O(n)$ car il faut parcourir la liste.

### 2.2.1 Exercices

???+ question "Exercice 3"

    Construire les graphes correspondants aux listes d'adjacence suivantes.

    **Q1.** 
    ```python title=""
    G1 = {
    'A': ['B', 'C'],
    'B': ['A', 'C', 'E', 'F'],
    'C': ['A', 'B', 'D'],
    'D': ['C', 'E'],
    'E': ['B', 'D', 'F'],
    'F': ['B', 'E']
         }
    ```
    ??? success "Solution Q1"

        ![ex3_Q1.png](images/ex3_Q1.png){ width=20%; : .center }


    **Q2.** 
    ```python title=""
    G2 = {
    'A': ['B'],
    'B': ['C', 'E'],
    'C': ['B', 'D'],
    'D': [],
    'E': ['A']
         }

    ```
    ??? success "Solution Q2"

        ![ex3_Q2.png](images/ex3_Q2.png){ width=20%; : .center }

???+ question "Exercice 4"

    Construire le graphe correspondant à la liste d'adjacences suivante.

    ```python title=""
    [
        [1, 2, 3],
        [0, 4, 5],
        [],
        [2],
        [5],
        [2, 4],
    ]
    ```
    ??? success "Solution"

        ![exemple_2_vert.svg](images/exemple_2_vert.svg){ width=40% .autolight .center }





## III. Visualiser un graphe

### 1. Avec le module networks

???+ question "Exemple de graphe"

    On donne le graphe suivant à l'aide du dictionnaire donnant pour chaque sommet la liste des voisins (ou successeurs)  
    `dico = {0:[1, 2], 1:[0, 2, 3], 2 : [0, 1, 3], 3: [1, 2]}`  

    Ce graphe est-il orienté? Pourquoi?  Représentez ce graphe sur votre cahier.

    ??? success "Solution"

        Ce graphe n'est pas orienté, car les arcs entre deux sommets existent tous dans les deux sens.  
        ⏳ Pour sa représentation, voir ci-dessous.

!!! info "La bibliothèque networks"

    Pour visualiser ce graphe, nous allons utiliser les bibliothèques networks et matplotlib.  
    ⏳ Attention, ce n'est pas instantané.  

???+ question "Utiliser les bibliothèques networks et matplotlib"

    La fonction suivante crée un graphe non orienté utilisable par la bibliothèque networks pour le représenter.
    Cette fonction sera automatiquement appelée pour toutes les représentations suivantes de ce cours, car présente dans du code caché.

    ```python title="En code caché"
    def cree_graphe_non_oriente_nx(dictionnaire: dict) -> nx.Graph:
        """
        Cette fonction premet de transformer une représentation en dictionnaire en
        une représentation «complexe» d'un objet graphe orienté.

        - Précondition : l'entrée est un dictionnaire
        - Postcondition : la sortie est un graphe orienté (Graph) de Networkx
        """
        Gnx = nx.Graph() 
        for sommets in dictionnaire.keys():
            Gnx.add_node(sommets) # Creation des sommets
        for sommet in dictionnaire.keys():
            for sommets_adjacents in dictionnaire[sommet]:
                Gnx.add_edge(sommet, sommets_adjacents) # Creation des arcs
        return Gnx   
    ```


    Tester plusieurs fois de suite ci-dessous : 

    {{ IDE('scripts/essai_1_nx') }}

    {{ figure('cible_1') }}

    Faire vos essais personnels ci-dessous : 

    {{ IDE('scripts/essais_perso_no_nx') }}

    {{ figure('cible_2') }}


<!--
<div class="centre">
<iframe 
src="https://notebook.basthon.fr/?ipynb=eJyNVM1uGjEQfpWRc8iutEGB9LRSesmBW5VD1QuglbEHcPDaW_8EKOKBoj5Cb82Ldbxswl_VFA7MeP6_mY8tE6i1Z-Voy2oMXPLAWbllwUUfULJyxrXHXdG6VWHTICuZsBJZwbyNTiRd1Y11AWoeGm2DVtNes0kScA-NDmPTORgMK-uW6_Ru1mMzNhJnIBxiNXe8WWBlrKmsU2gCyetMKhGUNYYrhyUkLYebzxTbGyb_cmyAPmPWflv5AUNAmFnTBkLjkKYCiRAcN35mXY0OokFw2LjXF0-VeOuJBo6rkb5P-Dff3z-FrRuNa_z9C-R1NGCnT1RmPwTsB3h96Y27HDfwSPHCGqna-BL0NXnQG9XxgWqc1H4Psj6cBHHwBKN6DzqrB1mLSp7G_dJBfYnQ0Kzh_h3CLIf9M0FD2WtCy4M67ae3xI3P8g7tLkePS0nrkph1UTlcwYPDPUIS_Vu28_QfZz9qpeLyiQua7qKp0d5jchR33BnK-VtnxWWyi165E12jDkN0JuWhC6cNi5icKmGjCaw0UeuC2RiaGBJpJrvihDdnRKm5W0q7Msdk-WYjscLGZ_wBuKaziIFOstHRK4zO0_TKpw36qOiSaecpYdTtwzMnYvTYWdEDWUn4iKvEx57QPMuv6L6iA5zNCBMHmlCYqXl09JuuXby-yERDgoVgt3Qy29ty1C9gMCmgX45uSSrgjpQBnWZS-3v1jrR-MZjsxmZIUf9mt83H5iodo3R8VQnlRNTcZcMCViosKs2nqP39VxqLHDs3sh4ZqURrhm6eN77iga40rvfqeyRipen9wq6y_P-2OylOgF6iM1SzQZE0w-sW0U1YWHNHCaXyjeabqjM8tgZIFs3NPPL5wZ3taE9mmv6QOFX-dFCqWhnrWDnY_QGKK_nA"
width="900" height="1000" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>

**A vous de jouer** : testez vos propres graphes ci-dessous

<div class="centre">
<iframe 
src="https://notebook.basthon.fr/?ipynb=eJyNVM1u2zAMfhVCO9QGXB-2nQxslx5yG3oYdqkLQ7HoRI1MGfppkhV5oGKPsFv7YqMct83PsC45hJTIj-QnfnkQLRrjRXXzIHoMUskgRfUggos-oBJVJ43HXTGGNWE7oKhEaxWKQngbXZt83Q_WBehlGIwNRs_LYZsskB4GE2qaAgjD2rrVJp3TpqaaFHbQOsRm4eSwxIYsNdZppMD2JlO6DdoSSe2wguTlcPmVc8tZiq9qAv7UYvyO9hWGgNBZGhNhcMhTgUIITpLvrOvRQSQEh4N7fvRcSY6RSHBYjf094N9in361th8MbvDpN6iLSGDnd1xmPwTsB3h-LOsJ4xKuOb-1pPSYX4G54Ag-4zo-cI2j2q9J1oejJAmeadSvSSf1IBtZydO43yaqzxma0Qa-vFKY5bA_ZmoYvWe2POjjfsoVbn2WT2xPGKVUip9LYTZl5fABrhzuGVLoX9BO4d9HP2ilkepOtjzdWVM3-4jbg7zDzlAtXjorzsHOepWunRp1GKKjhMMbzi_cxhTUtDZSEBVFYwphYxhiSKK53RVHujkRSi_dStk1HYrlh42sChvv8SfghtciBl7JwUSvMTrP02ufXtBHzZvMb54AoxkP7iULoxQnRd_EysZ7WmU9lq2RWf6B9ys6wK5jThwYZqHTi-j4N217-_yokgyZFqbd8sqUJS_0jI1_C9bmNfEjKCfX2axY67BsjJyj8Zz5nftLC5ea8Eu7zvL_I_m2OJp3hY4YcMA2eST7cbBtWFr6xIBK-8HIbTNdXI8XkG6MpEWUi7dwsWO6aJ7-FyRX_vzmNL0m60T1cfcHNcjTyQ"
width="900" height="1000" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>

-->

### 2. Avec le module graphviz

!!! info "Astuce pour exécuter une cellule dans un notebook"

    Raccourci clavier : Nous allons utiliser la touche <kbd>Maj</kbd> + <kbd>Entrée</kbd>




### Graphe orienté

[Graphe orienté avec le module graphviz sur basthon](https://notebook.basthon.fr/?ipynb=eJytV-ly2zYQfhWUnqT2VALvS7nGlpNMZ9LW0zjpdMqMDZOQhJgEOARo2fH4gfIcebEueOiK4qaO9IMEdxff7n78AEK3RkrzXBqjf26NgiqSEUWM0e3doLGfqZuSGiOjINVlJubcGBhS1FWqbXvodUXKGUWiYpSrL58RuaIpyikqRFbDbardV-wTknWFLohUM8GNu8FaHkNVtVQ0M0YwoBtZU5HR9YysKEWlJBIXOZsSJVhFE94aF-kWhj7lwKDXNK0VE_wsFTVXxsgZGKJWZa104x82avqe3vf20Lj68ploUJT9XPM2_QoZVwyK_5FuW8CzFpCiZ4sG8TFrhvsHydbm3B00d_gRIFBGJeKC1pns3i1BxZfPQGpG0TmH63nC3yxjpOB6DtLY6Fyq6nyHBGCdbz8xDhMD-t7uO7rHNwbfNra8nbJFqnQbVzSb0p2SoQFbMgboG32vh3yrfX8H7f9JS1gLErJ266HuVsOPd6zl_rax7K-3t7WZYLWZ227c47fR9Kyiss7VN6YvCqXXyixzwjjMfLooZcJyKruCYBP6SFOFiELWtR25aTCJngMsK8iUmvJq-st1kevZL-COrmglIdWzxLCxlRiIcuiX8SkY3p2-GkZgkorwjOSCUzBykRgvnif86U_Hf4xP_z55iQARnbw7evPrGF7n0DT_csemeXx6jN6-f41sbJvmy98TI-HgnSlVjkxzPp_juYtFNTWbrZql0oRgUwfDRF2jbeNMZYnRZBoO0WvKaUXgFaGLm-UG3hWPHOxZ2Eb7jmUHtuP42HItD5SHhsMe4JSpnI7QIwudAA1yhOzOqcufs0zNNAWuVyroeEbZdKa0wQ60AZCuGJ0fiWuwWdiyUHOB8OZmB3CDacAnl8-2telYlqXb6oNG1znjl1tD7TiOzcbddj9FLIPApmedJM2JlL0BnlVFuJyIqgCbTElO921kH6BKgOjpvnXQBuT6wYNSnYMWVmk-nj-ynprtCEylyG-mwCaIKQewvUnzawRQiUv98huoklQgdDCXgnGlSxl6Aw_BZQjwQIq1HDRmLzHM_jUcdqx3Xek90F5pSj-v1ne4Uh4sSVZK2pfHQY-rte1ZzU-D6bcUuHp0o6uLtbHSRifUI220o64ovaCQvgwJT2eiAl_BsizX2AucBibysV4NE_iYDCekYLm2nrKCyoGkFZv0Psk-6XpsrxVFz2ZfXtMT5NPJzWnPy9HXvDj38HL0YF4aClpeNAcP5qWZ0sK4u-HlaBsvh4_3PP_J46l6ssGQ_oqsKkc_rylnOXFV4kTN_psojf-b72En8kHUoYP90IrGPqx5z3IHwwA6iKwQeR6OvAAifBcHsRMjvQm5bjgYgscJwl73m-tqkWdL6sWa8lzsNFAODrwwRK6PA9eD7DBwgggMAY7DAAJ8HIaRgzYmdLmXTI6_Vph7j8LGD1ZYHO9GYQ3OThU2vl9h468V5nyXwsYPU1ho49D2gqXCQh_HfhwuFRbBF9F3g6XC9B4UBO5OFBbF2HK8pYBiC7uBHy4VFjk4bHJ1EtyYsKaw7gqfOLjD8WrttHa3eX77n_-u3guJqJSEyW2nI17n-fph8cNaeuOSVpzmsqSpfuKk0Kjljf4P5gJgxiQcqG7OOsdJ40DakxM-reG0sAg37qBWfqG_tqQ9ovcPZwXjooI_cHf_AoGKrTg){ .md-button target="_blank" rel="noopener" }


### Graphe non orienté

[Graphe non orienté avec le module graphviz sur basthon](https://notebook.basthon.fr/?ipynb=eJytV-tu2zYUfhWOQbcEs3W_2F6SInEvGNBtwZpuGKbCYSTaZiORAknZSYM8UJ-jL7ZDSb4lbtOl9g-KPDz8zvm-Q0r0LU5pnis8-PcWF1STjGiCB7d3ndo-0jclxQNcEHmViTnHHaxEJVNj20OvJSmnFHHBkZCMcv35EyIzmqKcokJkFTwmxmXGPiJVSXRJlJ4Kju86G7GwlpXSNMMD6NB7kVOR0c2orCiF1AqJy5xNiBZM0oQ3xmW4pWERsoPpNU0rzQQfpaLiGg_iDhaVLittyL-_l9O38N_bQ0P5-RMxoCj7qeJN-HuCzBgQ-B7GDegIQEcNKEVHS6JWXYP9g2QrRdfdAceTDwCBMqqAF60y1ZaYoOLzJ9A2o-iCQ3uR8DcrHyW4WYMMNrpQWl7sWAPLxNxP8EmCgfyX508fmR_C_FbpvJ1KR2S6TTiaTejOlTGgjTId9BUBNt2-qIO_Ax3-pCUcFAVh28NStUfl-6mbM_C2tuw_5LidUbDO6LbtL4I07nQkqapy_aX1y3TptbbLnDAOSw-XCY1ZTlWbFrynPtBUI6KRc-1G4zS8dI4BlxVkQm01m_x8XeRm9XN4ohmVCmIdJdi1nAQjyoE14xMwvDt_1e2BSWnCM5ILTsHIRYKfHyf88IcXfwzP_zl7iQARnb07ffPrEKrate2__aFtvzh_gd7-9Rq5lmvbL39PcMJhdqp1ObDt-XxuzX1LyIldv01Yqmxwto0zLDQ5uq6V6SzBdaRuF72mnEoChUKXN6t3fJs88qzAsVy07zlu5HpeaDm-E8AmRN3uAuCc6ZwO0DMHnYEMaoDcdtKkP2eZnhoJ_KDUwHhK2WSqjcGNjAGQZozOT8U12BzLcVDdgHv9cCN4wDLQk6ujbTQ9x3EMrYXT4Dpn_Gqrq9vv9-16umE_QSwDx5qzCZLmRKmFAcZaEq7GQhZgUynJ6b6L3AMkBWx9uu8cNA65GQSQqnfQwGqjx_Ez59BuemAqRX4zATVhM-UAtjeuf_UGkOLKFL-GKomErQ7mUjCuTSrdoBMgaLoAD6I4q05tDhJsL8pw0qresjLvRHeNlBmv53eylh4cTFYqukgPTh1dz23PqX8GzFQp8k3vxmTXN0ZpjF5sesbo9tqkzIFCpukSnk6FhLmCZVlusJc4NUwvtMxpGMOHpjsmBcuN9ZwVVHUUlWy8mFPso8nHDZpNsVBzkV7NCeKZ4PZkocvpQ128r-hy-mRdagkaXYwGT9alXtLA-LvR5XSbLic_7gXhL3VzTyLzNVnfOma8sXXWVq5vcqKnj0tlAvwWBpbXC2Fbx54Vxk5vGPSsyAv9TjdyLS90IhTAM-6BRxBZYRj1kB9akR-AB3S8qBe3Yq74DB_W2f9KnYdPrnO_v5s61zg7rfPwkToPH9bZ-7Y6D59W59i1YjeIVnWOY8uPg3hV515gxZ4brercdyw_CuPtdW5beN3DEy4cG_eXu_s3mv_5Z-QEzUSltl4TnM2L0_uNwPiKSk5zVdLUjDgpDF55Y_6s-ACXMQXXiptRO3FWTyAzkxM-qeCbuXTHd5AlvzTfHAJxg9VgVDAuJB54d_8BqLV65A){ .md-button target="_blank" rel="noopener" }



## IV. Création d'une classe `Graphe`

Dans cette partie, nous ne traiterons que des graphes **non-orientés**.

### Interface souhaitée

Nous voulons que le graphe ![image](images/ex2_Q1.png){: .center} puisse être créé grâce aux instructions suivantes :

```python title=""
g = Graphe(['A', 'B', 'C', 'D', 'E'])
g.ajouter_arete('A', 'B')
g.ajouter_arete('A', 'C')
g.ajouter_arete('A', 'D')
g.ajouter_arete('A', 'E')
g.ajouter_arete('B', 'C')
g.ajouter_arete('C', 'D')
g.ajouter_arete('D', 'E')
```

???+ question "Une classe `Graphe`"

    **1.** On donne : 

    ```python title=""
    class Graphe:

        def __init__ (self, sommets):
            self.sommets = sommets
            self.dic = {sommet: [] for sommet in self.sommets} # création par compréhension
        
        def ajouter_arete(self, x, y):
            if y not in self.dic[x]:
                self.dic[x].append(y)
            if x not in self.dic[y]:
                self.dic[y].append(x)
        
        def get_sommets(self):
            return self.sommets
        
        def get_voisins(self, x):
            return self.dic[x]
            
        def get_dictionnaire(self):
            return self.dic
    ```

    A quoi voit-on que le graphe créé est non orienté ?

    ??? success "Solution"

        La fonction `ajouter_arete(self, x, y)`  ajoute, s'il n'y est pas déjà, `y ` dans la liste d'adjacence de `x`, et `x ` dans la liste d'adjacence de `y`.

    **2.** On donne : 

    ```python title=""
    g = Graphe(['A', 'B', 'C', 'D', 'E'])
    g.ajouter_arete('A', 'B')
    g.ajouter_arete('A', 'C')
    g.ajouter_arete('A', 'D')
    g.ajouter_arete('A', 'E')
    g.ajouter_arete('B', 'C')
    g.ajouter_arete('C', 'D')
    g.ajouter_arete('D', 'E')
    ```

    Déterminer sur papier le dictionnaire représentant le graphe créé. 

    ??? success "Solution"

        ```python title=""
        {'A': ['B', 'C', 'D', 'E'], 'B': ['A', 'C'], 'C': ['A', 'B', 'D'], 'D': ['A', 'C', 'E'], 'E': ['A', 'D']}
        ```

    **3.** Compléter le code suivant pour faire afficher ce dictionnaire

    {{ IDE('scripts/get_dico', MAX_SIZE=45) }}

    ??? success "Solution"

        ```python title=""
        dictionnaire_graphe = g.get_dictionnaire()
        print(dictionnaire_graphe) 
        ```
    **4.** Visualisation du graphe donné par la liste d'adjacence créée ci-dessus

    Exécuter ci-dessous (la fonction `cree_graphe_non_oriente_nx` est déjà présente dans du code caché): 

    {{ IDE('scripts/voir_classe_graphe') }}

    {{ figure('cible_3') }}

    **5.** Compléter ci-dessous pour créer votre propre graphe non orienté et le représenter.

    
    {{ IDE('scripts/voir_classe_graphe_perso') }}

    {{ figure('cible_4') }}


<!--

<div class="centre">
<iframe 
src="https://notebook.basthon.fr/?ipynb=eJy1Vs1u2zgQfhVCOVgCFBfd7klwA2ydwLdFsS32YhsKTY1tJtRQJanaQuAHCvYR9ta82A4l-Ud2nLZZ1AYMifPND2e-mfFDIEApGyTjhyAHxzPueJA8BM6U1kEWJPQAm7hGpa4qIEgCoTMI4sDq0oj6XXFr2cjwYgnJBCfI6JPBnKWpROnSlIUW1DxmVufkw0ZJA_EfL-i35-z9FnEk59kdF4A14qGBJGw8ZXNtWg0msWNqwy6YME-P3EmNrOCGCZ0XdLAEtHTUONhHyu906cCk3ICDNtp1zKrDUOWcVQz13tcurPF6eoA7DZvkfV4UgFlYRR2D6zMGq-8YrHYG19HxXRbg0jYN9U0O70DXK003Vc-pf9XSSrTbRJyzcHjBPaJrKpPC1wC5NPBiODtjRC1Ygyi9Wip0iS5IsFQqDqhGRek8Waeb-PV8XRCNGraG494fvZj1Pvifof-59j83vSllddHvsmILPSsanhddnxfdPCv6cN7g8LzBNvroNSk8SlrOzX2mV3iYuOunR3KWSwTqu9JQXxWSHhWwwypTVX2nWaolR-elizrZdUM-PfYneCOMx1ErltKBR1g6KRz7UkpWkAdq6KzH53MplmRfdO33g5-PfJARXCp7NbBlToDq6pNWdXpYKJT8UoKJ2ODNVuiH2K33mb4lriz6J0SObgczQ7Dbwkh0YQONbj35Jzh4s_U2wVfEenHB_tpn0Ps8NvJTbJd5oY1jOXeF0k7JWb-o_BPjlhXKTbAFILiVNvdrf45rfw_fwsIApE39UiQyaSMpKnpeh4cJSeoKRezyinT7dXO1bT4J6m_9PARH5Z5rFM1YNlBXGpgzHC1N85yqXWKHQTUSsMswaAf4c9hv__hRr4j_3_4lFpXI9OyO3LQkbC5Q07Cxcck-kr7QmMlaP2GqRwg6Iz_WkY-O752Stq6jxGkVGSd3Skf-WFhnJfLX_bNN9WmGRrgmwm1TGEbtMN0vOuuXRacb7qGy4eFQJRs0TDMqV0YTt125tA-HBpoMZWC7i7a7R1-2fhBKut_LR2rjBnG8xLaRQbbYRhafGjuJlRvRBtrui5Hn5y9fE21AgiJ4_9wImGDT_HucP1KuT_-HvHTkZ8cL_dPRo8xkhq9SIY0oFTfh6G3MVtItU8VnoPw_n88UcuvBLvUq_FVTnubP39oSjS2Xlgl5STUgof1fU-jHYp3GHQf3YJDuXoDwb8hzb6mo3FLjOzKYSVsoXqWt4GMtYF6iOC5KvtjDgw3FhzM_Yjh5_n3_ktI60yZIftv8B8qr0Lw"
width="1000" height="1200" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>

-->


???+ question "Exercice : utiliser la classe `Graphe`"

    Utiliser la classe `Graphe` ci-dessous pour créer le graphe dont on donne le dictionnaire des listes d'adjacences:  
    `dico = {0:[1, 2], 1:[0, 2, 3], 2: [0, 1, 3], 3: [1, 2]}`  

    Pour vérifier, vous ferez afficher le dictionnaire obtenu, puis le graphe. Vous pourrez
    pour cela procéder de façon analogue à ce que l'on a déjà vu.

    {{ IDE('scripts/exo_classe_graphe', MAX_SIZE=70) }}

    {{ figure('cible_5') }}


    ??? success "Solution"

        ```python title=""
        g2 = Graphe([0, 1, 2, 3])
        g2.ajouter_arete(0, 1)
        g2.ajouter_arete(0, 2)
        g2.ajouter_arete(1, 2)
        g2.ajouter_arete(1, 3)
        g2.ajouter_arete(2, 3)
        ```

        Puis pour la représentation : 
        

        ```python title=""
        dict_2 = g2.get_dictionnaire()
        print(dict_2)
        plt.cla()
        graphe_2 = cree_graphe_non_oriente_nx(dict_2)
        nx.draw_circular(graphe_2, with_labels = True)
        plt.show()
        ```




