---
author: Mireille Coilhac
title: Parcours de graphes
---

???+ "Listes d'adjacence - liste d'adjacence"

    Ce nom est trompeur, mais très souvent utilisé.  
	On appelle souvent "listes d'adjacence", ou même "liste d'adjacence", le **dictionnaire** dont les clés 
	sont les sommets du graphes, et les valeurs les listes (parfois tuples) des voisins associés à chaque sommet.


## I. Introduction : modéliser un labyrinthe

**Considérons les problèmes suivants :**

- Comment rechercher le chemin le plus court entre deux stations dans le métro? Indépendamment de l'aspect ludique, c'est en fait un problème difficile qu'on aurait bien du mal à résoudre de façon raisonnable sur un gros graphe comme celui du métro.   
- Comment trouver la sortie d'un labyrinthe? 


### Les labyrinthes

Voici l'image d'un labyrinthe :

<img src="https://www.di.ens.fr/~mauborgn/td-99/programmation/petitlab.gif">

Ce labyrinthe peut être représenté par le graphe suivant :

<img src="https://www.di.ens.fr/~mauborgn/td-99/programmation/petit.gif">

- **Chaque sommet du graphe correspond à une case du labyrinthe**

- **une arête relie 2 cases voisines quand le passage est possible.**

- **Si une paroie empêche de passer on ne met pas l'arête.**

Par exemple il n'y a pas d'arête entre les sommets (1,1) et (1,2) mais il y en a une entre (1,1) et (2,1).  

???+ question "Les noeuds"

    A quoi correspondent les deux chiffres des noeuds de cette représentation ?

    ??? success "Solution"

        Le premier chiffre est un numéro de ligne, le deuxième un numéro de colonne.


Nous allons représenter ce graphe par le dictionnaire suivant : 

```python title=""
graphe = {11: [21], 12: [], 13: [23, 14], 14: [13, 15, 24], 15: [14, 25], 16: [26, 17], 17: [16, 18], 18: [17, 28],
		 21: [11, 22], 22: [21, 23, 32], 23: [22, 13], 24: [14, 34], 25: [15, 26], 26: [25, 16, 36, 27], 
		 27: [37, 26], 28: [18, 38], 31: [], 32: [22, 42], 33: [], 34: [24, 35], 35: [34, 36], 
		 36: [26, 35, 46, 37], 37: [27, 36, 47], 38: [28, 48], 41: [], 42: [32, 43], 43: [42, 44], 44: [43], 
		 45: [46], 46: [36, 45], 47: [37, 48], 48: [47, 38]}
```
Pour simplifier la figure, nous avons adopté les conventions suivantes : le nœud 11 représente en fait 1,1, le nœud 12 représente 1,2 etc ...

[Dessiner ce graphe avec netwoks](https://notebook.basthon.fr/?ipynb=eJylVcFu4zYQ_RVCOVheqGosKXHgIj10C-RW7KG3yBBocWwroUiVpNZ2DX9QsJ_Q2-bHOkNJtbIOtkBrwxDfcDjvDTUzPgYlSGmDxeMxqMFxwR0PFsfAmdY6EMECF3CKvFfhDg0Ei6DUAoIosLo1JeGqbrRxrOaukdrJahU3B1oxblkjXa56BwVup83znuxqn6tcCViz0gAUG8ObLRRKq0KbCpTD9T4UVekqrRSvDCwYoSn74Wc8Gz-Q_yJXDD954L9-_RGcA7bWyh9kjQFMiglgznBl19rUYFirgBlozOuLRSbuPUGxMRviLuB7vl-_lLpuJOzh619MTFrF9OoJabokWJfA60vcRaCwDGynAi_wDc9lBg9qz-7_STGcss6M0pnVNWZjWfVWa_wMBxtO-9voY8RcCLxOAWF_asqu2EcDXQYC7BDt2_D_Hn0kpeDiiZeY7YWox85jOTo3VgZiMyiLLoNdaOWm7IUacK1RFCdXWIP4DsqW3IpSt8oFiyQKdOua1lFJL0_Rf69qrNy4lDycXrFPaGOwXqM6wyTqWVeb1uCT6qJ8fRFUsCiwK4D742y2YI_JbBmxWYIreqZkSXGREcoQzQjdRCzxlhuyZIhuCN2S9y0u5oTmtEfojtAdoTl6EkqIaTbDRbKM8vx82UniJeAG0qQJuXoJCWkhlPWEKdEnnp7E3BLy9DekA_fxl5CMhGSk88HHy7jDfZKRznyaYwFp0tNlRJ6m3T2kRJsQLeWZEm1KiEKmQ9YpUmf0JNqUaJN5JyTzFqJOkDoj6uySOiPqlKgp04yoM0KUaUYCOjuRZ0ScEbEPT6KyIc8uPJFlc5_nKVcP998ZV511mivdUEla7ONjp2pCnVjY6k-YsAWbXV9HaLxijldSwmU3du6lltqQ_2S3rRxMIjyBVS4Bi1H4ISfeOYpmNzq6krx8nkTjzbOMwUzdODpjQHxDNu7Aya5y20LyFUjr3X_HXkJ_PMA_Q4n9wZSux8r6EXZi3__kCoeDMHwXPkTsw4f-EmkW-P4bJjGcBzG2p7XVHy2OUepWu9W7cPreVEjHU-HYr4fGF5VtJD8UfkhEwTAreNPIqvREPz7xz9yWpmowVgAS_1SUi9EBlAh3lRJ6Fxcrbt1Wq191_Ru-vV9aGze6Ca-n058CnDSjKXQ6_a-59F5-qpXy7eBbvqEMnsEofF8NlIQUr_2EO5DglHLur6Df-OQ3GO1IrjYt35zdgxPqUyv6K-XInJ1BUVdKGxzBp78B82Wc5w){ .md-button target="_blank" rel="noopener" }

[Dessiner ce graphe avec graphviz](https://notebook.basthon.fr/?ipynb=eJytXAtv5LYR_iuqDmls1NaKD4nSNg80ThoU6OOAXFoUt8ZB3pXX6mmlrSTfnXPwD0r_xv2xzpDSitrlMokkB7E0FJ_zccj5hvR9dNdpntfu8vVHd5c2ySZpEnf50W2qx7pJN-4SXtLnK5nrTfO0T92luy43qXvl1uVjtUb5hZPt9mXV1E55l2fbpCmzKl0VKtHZVsn-4V320yHhLqmbh7JYwX95cvdUZUXzkDpfHjJ63-PLxeWqkCn46SMhS-c1JbdXDqHwhk-GKQxeOEocJIJScOVQmRJgCgcpQCnE3CG8CJQEfkMpQilCSUBOlFaF0_5QbJMQSKe3-Ev2AF6gFSZTZA8odgUl3rbHsHUqW8e-hCjJ1gPsBnyH_yn2gmIvmGjzaO3K_kSQE_vDiBovo21rHNtmrE3FVim2iqNk2CpDCWtk3ZgZtMzxia0ybJUK1Q8uBi0zbJlCyxxb5m3LHFtm2DKOk2PLHCUcJ8f2VTq2zbFdju3K2rFPvBulqhNb4EKO7RlnwAvnpvr0c9JkZeFs0topyvRxU6-K-7Jy1nnqZIWaF-lSdbOfMV4B0_CibqoLyHd5qT4fVwj5nTyDmSwrT6p1jTn-BE9oqXDKKkuL5tPPtbN0LlZusnJBHe7dyr100gZT7lRKgikFVFJm76CAs09qp_78yWmq8vFdWjlp4ew-_W-XOk2620MT2BBM29e3jnO2P58nw17Us_YA9Sd1eaJB_JLm6a7_8FpmvF32EyG7d6Rm5YfLKwffscwl9KFssKTsfFJsVD75TWVTRQb5lqiFl9Dhdsz10-7Tz02V_fcxrfs28Qc_e8l-nxabizMduHTU4CDrof6-Em16pJttegGfX_sw7_BJbi_7jDgN5DqDvWhXGlx7fpCL2kVfzyWsdOmHdP2ICL5Zl49F4y7plVs-NvvHRi2c6r1bHlXu9E2V1o95c6b4YZ1NPzSLfZ5kBZT84tCN-yxP67YzsKr-J103TtI4_gfGRRLeRV9Btdku2aaL-t32Dx92OZb-Gp4OTIYamvpy5RLPX7kwMWC5zootJPz46s_XESTVDQCX5GWRQmJRrtyvv1oVX_zu23_cvPr3y-8cqNF5-eM3f_3LDcy768XiX-xmsfj21bfOD__83iEeWSy--_vKBVWu3Iem2S8Xi_fv33vvmVdW24XS6bpeQOYFZoaC2EdCvE2zWbmypetr5_u0SKsEdhjn7umw8nedd6jHfY84F9QnIaE08Hzmc0Tv-rqr4FXW5OnS-cyHmbVNYY6R9iN2_322aR5gcEHE9w2M-CHNtg8NJIhQJkBN77L0_TflB0jzPd935C_Ijg_IBA8oBvos6i9Nw6S-7-OwukzLD3lWvDVmJXEcL-RnNfqtk20goxwzNrLOk7ruEkBuqqSoYYbvIK1eJ3l6QRxy6VRlA-q68C9VhhwFDl31L1W1Derjq8_8LxbqDZL2Zf60BW3CZMqhshf38kdOgKp8i-DLqvZJBSsKJO9LmPHYlWt-xR34dQ3Vg1L8_kUm85W76GAgndrbYeGiTLRRoax3kBCtg-BTZPs67ToIa2Gq9-6FL3-wNsSJhrLiJ-ygYDiRK5ks8A1TSdR2DI3KwV_XSbF-KCv4tss2mxxr72tqK2IeVnVfFs31fbLLckx-le3S-qpOq-y--1ZnP2GfSDs1Op12XVQDgyax_cW2Uw81qCe2qIfOo54wDOdRTxiSedRDjeoh5PcvePBH-etYVbh-6zMJ5eFM0svq0z5pHn5ZZ9jE32CkMLdJ7EWM8Bsl-bEjX8I48GgkwlaCpYETxloVakOgpwhTmwHQ0QgzbH4WA1A1zWwA1IgwO1UPs6mHjVYPEb0BBDGfoB5Vk6ooimdSDzOqh5-qh9vUw8erhwW9eiidoh5Zk6qIiJnUw82zp7fxY1XheqDPpJP1YVB23PpAQgprQAA7YSBCLxB-dEOC0AtpwCAphAUi8EMAMfaoiDBX4HtBEEaQxLyQcczF4GMYiZOFgxosg9i2TjreNKgvZsJe1TQv9tRsGhp-x7pCrOmvxH6gtt-CvYg9QYA3a9hHgccE0FcN-5h6gpJQxz6OPBYGwoI9CU6xD2xmH4yGPo4PyPPAn4C8rEjVw4OZjD4wr4ma4QanwHMb8IOyI4GnYWf0Pm-Bp35r9DxmLfCEtUbPRdQC74vW6Hkozhi9Yb0nNneBTljwtf1wGvTafjgb9tS84Gv4HesKsQ5-JfZ07IIP63Zr9AfsYXVXRt9jD3uAMvoee9gplNGfx95g9MTmC9FZrJ6JKZ5ib_VMzOQoUrPVBxp6BqsPbcgPyo5DPo4BOkYUE5ACEBL5BKuWPAAFFodnaEB4Cm5owZaE4zdzMRO2VMyOrRzWKbbiVDnCphwxi3L8SSS4V44_EweWwzpVTqhtWeJ04ke2iT8oO5IDw5rFgo4Co8CBAeOTdQQYBXqG_1LDxCc2CkPHz3x9VZsErraqzQUuNc98DaBjTSGY4leCO1DabwCXA38JY_BIGOxPgvniJgAcowghB3eVUEcQD9xUTGDM46FPnQi2wEBhTjzKhWGxi04xj2z2HM1hz5RN4fS9PVM6F6WPzIudZpPRKeSxDfJB2fH2DGbb2zMVQpowDVlvzzSgZ-zZgC2x7WR0FnDBf5oFXBLMRUrN4EaaTRrAJb4N3UHhCeiSQEPXDySgJCY9uiTyz6BriFYO-nyC7vhwpR6QnhaP0wLSs8XjqDFcSfWgMjXga4tIDwtPiEiHXGgR6ZCJNgZNmR6RJucsmNoDJ8QWORkWHjkGHnmChzJyAow4pOSGstiL_ZioyEkkogi6GXk-I1RFTnwOHjQlsUd9HmPkJAJ2bRgcM01g297DJkxg0fsbE6Nm4uBwzBY1Y-YJrOF3rCsJvi1kOiw8dgIHHudxGzKlPqUwiwFMErVhMwJbDyxSxBNhpLCPYSWlgqMfwlXQLOI0OIXeEDuhtrWLzRM7mUaytNjJbCyLGWMnVIt_HOtKQm8LnA0Lj42YEo1DKwlItHrpWLSUztJoGtj9ZWILAA0Ljw8DHNiQFIANyWfHhqRwlg0ZmC6xRXbpeKqrsaFprnHPhmZzjamR6lKd0RioLrHRoWHh8egefGMpgG8sn51vLIVzvjEzcF1qi96y8VxXX4GmwautQLPhy4xsV4foWFcSX1sQb1h45AoEHVZ8l8a-4rsYzZeEl2KoFggvhngV4wU3SjFeEgQt5aV4lGeivMxg2NQWvGWzGPY0WtQb9my0iJkNW-Otx5qSwNuCWMPCEwy7o0VSAFqEzwMtksI5WsQMpJfaglhsFtIbTzmU6TlvNNORDDNSXqqx1mM9SWxtAY1h4fGUF432QHkBQcVykS4cKK8fncHWdEPNFn1m4-9gcb-_YzHtho6qad4bOsx4B4ubLjDZAj58HkY17UhSY1SzHUlyI6NiGik61pW8h2AL-QwLj53_SIj9gwHE8iTSUS_iYAIohedswHTXxHYPkY2_a8KFmMsGhJjfBox3TZjhaJba7uKw8UezA2o56RRDp5ZzHWMw4-Es09mh4XCW2sJiw8ITqOWBlykJiJl66ZiZks5RMxbYnVNqC4sNC08Yw4F9KAnoh3rp-IeSzhMQu59FbRR_WHjkGNBTBrcava2odbDRn8YwHvVD5WCjxw1uNfpbcetgo18ObjUkCWF2sLmBWzFbdIdPOEH3Zzt48Oc_eeBGbqWjd6wrCb0tsDcsPPU-HCUn9-HA3T6-D0fik_twWIXxagw3sCtmW3_5LOxqkv_dk6u5_G9u5FZMo0fHepLA20Jiw8LjudXB_5YCuBPy2fnfUjjnf3MDt2K2oAkfz63C_oI8meJ5hIfr8WQmv4MbuRXT6NGxniS2toDJsPA4bGHrVDYtqDJpTlqLDokyaDzLUVfdQmXOAW-tGbddozEbCBe1xUD5eMIV8N6apzmbqqZ5nU1uJlwGZ5zaCCmfcPFbzHURUCNcs51jcKMzznXOZLr4bYsSDwtPIFyHcwwlUdJSrO4cQ0pnzzG46azKdkzJx59VDTCeeOdt_ntR3HhWxbUL-se6khjbAobDwhMw1q69xd29t3hw8c121sNNpNFGqvl40kh5v7NNvAfFD3vbbKcB3EgaefALHqstcDgsPBJjxtuL_AePFS8dqIv8B4-V4i03eZH_4LFiJFld5Ld4rPade0BdTgYnpu_c0Fc1tm7njng7sm7nRqal_kCh3bkFaUdl2rnb3_U7fLrPV_q_BvL8fPt8Nf7fBzH9CXzxmOf6H9HfPt8OmnTfplWR5vU-XaNUJDusaf-E_3IIgwo3Wb3Pk6c37YeX8oODX_Kk2D4m2z67-wz9K-7wT6oTaJn3wptdVpSVu6TP_wdUUOtH){ .md-button target="_blank" rel="noopener" }


[Créer un fichier png de ce graphe avec graphviz](https://console.basthon.fr/?script=eJytlM-O2jAQxu-R8g4jeiCREGpis1khcah66LWnXhCqTDBgiTipHZBo1Qeir8GLdT47u0BXeysS2PPH8_vGHmGarnU97Zzq9ifzM01MdKyV7_etTZM0Oaj12Rnb7zUtXhOnX7DJ8jQJHoR-FcWclmWxmlBR8g6rgEfwRsKSbBWwZhMqg2cGj2RrBusJ2U-8qWBViMF6hvUMq-JMWGlCw6cEsyjYX67wExTwhikieIKCElJgyYEnQC8DHVqeYAX6DDI4zt8SKkqoENWQc8cNep45E3pEEfsV5UCTYAsxeEEtQUWXAlQBCxXFS8-CyRIrqALUsoo6ZPVAFiCXTJYgy4EsQRYgo08JsoSFPiX40Q-2BFeCG6pDk3zpMtYEQVaht9-YgA_02V0vqjetpY32ZFt93Pg02baO6oMmY-Nc6HmUeZuYqW03OvO9yzgvz2P434KcTwfjex2KK1d7ZHzilUmWWme07a8XT3PKRmo0odF6lJPu2VrDUmxZPtyaEydSpzz58Zl61x5P2pG21Fz_NJp63XRcGgAe1-WK6F0dY_VI9_-FjPsKd_fmxhDRB93cAsuQuJrfHt5sKdxkCOQTwh5nctbQ9jgZRCu7iXkhFtPikYe8Obr_yoKHXv25uV56Z34ctb8x8UF4qrpO2032joCcYnOc-lr_VuRuHPRmpzMOLz_ynGEtVnmcsG_XizNbU8fHyHD19nqptffKOJ3fzR3uP0iu-e14LNKkQ_XsDuPbo6v5EL2Z3iMxY2_4cUyjdvr-323quEPtsq05aKsavRg1rf1-i_ODc4-N6hfjzu7G3H-tDnpR5H8B_eh-BA){ .md-button target="_blank" rel="noopener" }


???+ question "Résoudre ce labyrinthe"

	Ce labyrinthe est assez simple, et une fois sa représentation en graphe tracée comme ci-dessus, il semble assez facile de résoudre ce labyrinthe, c'est à dire de trouver un chemin qui permette d'aller d'une case "entrée" à une case "sortie".

	Supposons que la case "entrée" soit la case (1,1) et la case sortie la case (4,8). Cela correspond respectivement aux noeuds 11 et 48.
	Observons le graphe précédent. On voit assez facilement plusieurs chemins possibles. En donner trois possibles.

	??? success "Solution"

		* 11-21-22-23-13-14-15-25-26-16-17-18-28-38-48
		* 11-21-22-23-13-14-24-34-35-36-37-47-48
		* 11-21-22-23-13-14-24-34-35-36-26-16-17-18-28-38-48


!!! example "Quels parcours ?"

	Nous voulons écrire un script Python qui permette de résoudre automatiquement, dans le cas où c'est possible, n'importe quel labyrinthe, même très compliqué.   

	Pour cela on peut penser parcourir le graphe en partant de l'entrée, en suivant différents chemins, jusqu'à atteindre le sommet de sortie.  
	Plusieurs algorithmes "classiques" permettent de parcourir les graphes. Nous allons commencer par les étudier, puis nous essaierons de résoudre ce problème en choisissant l'algorithme le plus adapté à cette situation.



## II. Les parcours de graphes

Contrairement aux arbres, les graphes n’ont pas de racine, de « début ». On peut donc choisir n’importe
quel sommet pour commencer le parcours. Ceci dit, le parcours de certains graphes orientés demande un
choix de sommet de début réfléchi.  

😅 La vidéo suivante explique le principe des parcours. Pour bien justifier l'utilisation de la pile, la première étape du premier
parcours présenté n'est pas tout à fait celle du parcours en profondeur que nous verrons au IV. 


<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/uTwgr2Gbovk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>

!!! abstract "A retenir"

    * Parcours en largeur : **File**
    * Pacours en profondeur : **Pile**

## III. Le parcours en largeur

!!! info "Parcours en largeur"

	Parcours en largeur ou **BFS** pour Breadth-First Search en anglais.

    <pre>
	<b>parcours</b> est la liste vide qui contiendra les sommets visités par parcours en largeur
	<b>F</b> est une file vide
	On enfile un sommet dans <b>F</b>
	Tant que <b>F</b> n'est pas vide
		<b>S</b> = Tête de <b>F</b>
		On défile <b>F</b> et on ajoute le sommet dans <b>parcours</b>
		On enfile les voisins de <b>S</b> qui ne sont ni dans la file ni dans <b>parcours</b>
	Fin Tant Que
    </pre>
	
<!---
![animation largeur](images/animation_largeur.gif){ width=30% }
-->

👉 9 étapes :

=== "1"
	![](images/largeur1.png)
=== "2"
	![](images/largeur2.png)
=== "3"
	![](images/largeur3.png)
=== "4"
	![](images/largeur4.png)
=== "5"
	![](images/largeur5.png)
=== "6"
	![](images/largeur6.png)
=== "7"
	![](images/largeur7.png)
=== "8"
	![](images/largeur8.png)
=== "9"
	![](images/largeur9.png)


???+ question

    Ecrire le code d'une fonction `parcours_en_largeur` qui parcourt en largeur un graphe à partir du sommet `depart` et dont 
	sa liste d'adjacence est representée par un dictionnaire nommé `graphe`.

	Cette fonction renverra la liste des sommets parcourus en partant du sommet `depart`.

    Compléter le script ci-dessous :

	Graphe du test : 

	![graphe largeur](images/gr_vert.png){ width=50% }

    {{ IDE('scripts/largeur') }}


???+ question "Optimisation"

    Le coût d'une recherche dans une liste (pour tester si un sommet a déjà été visité) est 
	beaucoup plus important que le coût de la recherche dans un ensemble (hors programme NSI), ou dans un dictionnaire. 
	( [Les tables de hash](https://youtu.be/IhJo8sXLfVw){:target="_blank" })


	Nous allons donc proposer une autre version pour améliorer l'efficacité de l'algorithme.

	* Un sommet est "vu" lorsqu'il a été mis dans la liste `parcours`
	* Un sommet est "en attente" lorqu'il est dans la file. 
	* Tous les autres sommets sont "pas vu".

	Compléter le script suivant : 

	{{ IDE('scripts/largeur_opt') }}

	
!!! info "Distances croissantes"

    On peut remarquer que l'algorithme de parcours en largeur permet de dresser la liste des sommets d'un graphe par distance croissante au sommet d'origine

    ![largeur](images/largeur.png){ width=50% }

    A partir du sommet A représenté en bleu,on a par distance croissante :

	* Les sommets à une distance de 1 du sommet A représentés en vert : B, D, E
	* Les sommets à une distance de 2 du sommet A représentés en rouge : C, F, G
	* Le sommet à une distance de 3 du sommet A représenté en jaune : H

	On retrouve bien dans la liste renvoyée par la fonction de parcours en largeur à partir du sommet A les sommets classés par ordre de distance croissante au sommet `A : ['A', 'B', 'D', 'E', 'C', 'F', 'G', 'H']`.

	???+ note dépliée

    	💡 On peut se servir du parcours en largeur pour trouver un chemin de distance minimale entre deux sommets.



## IV. Le parcours en profondeur

### 1. Le parcours en profondeur récursif

#### Une première approche

!!! info "Parcours en profondeur"

	Parcours en profondeur ou **DFS** en anglais pour **D**epth **F**irst **S**earch

	Le parcours en profondeur est un parcours où on va aller «le plus loin possible» sans se préoccuper dans un premier temps 
	des autres voisins non visités : 

	On va visiter le premier des voisins du sommet non traités, puis faire de même avec lui (visiter le premier de ses voisins non traités), etc. 
	Lorsqu'il n'y a plus de voisin, on revient en arrière pour aller voir le dernier voisin non visité.

	Dans un labyrinthe, ce parcours s'explique très bien : on prend par exemple tous les chemins sur la droite jusqu'à rencontrer un mur, 
	auquel cas on revient au dernier embranchement et on prend un autre chemin, puis on repart à droite, etc. 
	Attention, pour ne pas tourner en rond autour d'un bloc, il faut marquer les endroits par lesquels on est déjà passés.

	C'est un parcours qui s'écrit **naturellement de manière récursive**.

!!! info "Méthode récursive"

	L’ensemble des sommets déjà visités est stocké dans un objet Python (liste, ou dictionnaire, ou ensemble).

    La visite est relancée récursivement pour chaque voisin du sommet visité qui n'a pas déjà été visité.

???+ question "A vous"

    Compléter le code de la fonction `parcours_profondeur` qui parcourt en profondeur un graphe à partir du sommet `sommet` 
	et dont sa liste d'adjacence est representée par un dictionnaire nommé `graphe`.

	Cette fonction renverra la liste des sommets parcourus en partant du sommet `depart`.

	🌵 Il peut falloir plusieurs lignes de code pour remplacer les `...`

	Graphe du test : 

	![graphe largeur](images/gr_vert.png){ width=50% }

    {{ IDE('scripts/profondeur_rec') }}


???+ note "return ou pas return ?"

	Testons sur le graphe suivant : 

	![return](images/return.svg){ width=30% }


	Dans les deux cas suivants la différence se situe ligne 7.

	[Sans return pour l'appel récursif de la fonction](https://pythontutor.com/python-compiler.html#code=def%20parcours_profondeur%28graphe,%20depart,%20vus%20%3D%20None%29%3A%0A%20%20%20%20if%20vus%20%20is%20None%3A%0A%20%20%20%20%20%20%20%20vus%20%3D%20%5B%5D%0A%20%20%20%20vus.append%28depart%29%0A%20%20%20%20for%20voisin%20in%20graphe%5Bdepart%5D%3A%0A%20%20%20%20%20%20%20%20if%20voisin%20not%20in%20vus%3A%0A%20%20%20%20%20%20%20%20%20%20%20%20parcours_profondeur%28graphe,%20voisin,%20vus%29%0A%20%20%20%20return%20vus%0A%20%20%20%20%0Agraphe%20%3D%20%7B%22A%22%3A%5B%22B%22,%20%22C%22%5D,%20%22B%22%3A%5B%22A%22,%20%22E%22%5D,%20%22C%22%3A%5B%22A%22%5D,%20%22E%22%3A%5B%22B%22%5D%7D%0Aprint%28parcours_profondeur%28graphe,%20%22A%22%29%29&cumulative=false&heapPrimitives=nevernest&mode=edit&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false){ .md-button target="_blank" rel="noopener" }


	[Avec return pour l'appel récursif de la fonction](https://pythontutor.com/python-compiler.html#code=def%20parcours_profondeur%28graphe,%20depart,%20vus%20%3D%20None%29%3A%0A%20%20%20%20if%20vus%20%20is%20None%3A%0A%20%20%20%20%20%20%20%20vus%20%3D%20%5B%5D%0A%20%20%20%20vus.append%28depart%29%0A%20%20%20%20for%20voisin%20in%20graphe%5Bdepart%5D%3A%0A%20%20%20%20%20%20%20%20if%20voisin%20not%20in%20vus%3A%0A%20%20%20%20%20%20%20%20%20%20%20%20return%20parcours_profondeur%28graphe,%20voisin,%20vus%29%0A%20%20%20%20return%20vus%0A%20%20%20%20%0Agraphe%20%3D%20%7B%22A%22%3A%5B%22B%22,%20%22C%22%5D,%20%22B%22%3A%5B%22A%22,%20%22E%22%5D,%20%22C%22%3A%5B%22A%22%5D,%20%22E%22%3A%5B%22B%22%5D%7D%0Aprint%28parcours_profondeur%28graphe,%20%22A%22%29%29&cumulative=false&heapPrimitives=nevernest&mode=edit&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false){ .md-button target="_blank" rel="noopener" }

	Que s'est-il passé ?

	??? success "Solution"

		Le return devant l'appel récursif empêche de remonter au sommet `A`, pour explorer la branche suivante.  
		On est coincé dans un cul de sac.

#### Optimisation

???+ note dépliée "Optimisation"

    Le coût d'une recherche dans une liste (pour tester si un sommet a déjà été visité) est 
	beaucoup plus important que le coût de la recherche dans un ensemble (hors programme NSI), ou dans un dictionnaire.

	Nous allons donc proposer une autre version pour améliorer l'efficacité de l'algorithme.


???+ question "A vous"

	Compléter le script.

	Graphe du test : 

	![graphe largeur](images/gr_vert.png){ width=50% }

	{{ IDE('scripts/prof_rec_opt') }}


??? note "Autre possibilité"

    On peut aussi construire le parcours par concaténation de listes.

	Tester ci-dessous :

	{{ IDE('scripts/prof_recur_Nico') }}


??? note pliée "Visualiser l'exécution"

	Vous pouvez voir l'exécution avec le graphe suivant ici : 

	![visualiser](images/visualiser.svg){ width=35% }


	[Avec Python Tutor](https://pythontutor.com/visualize.html#code=def%20parcours_profondeur_rec_N%28graphe,%20sommet,%20vus%20%3D%20None%29%3A%0A%20%20%20%20if%20vus%20is%20None%3A%0A%20%20%20%20%20%20%20%20vus%20%3D%20%7Bs%3A%20False%20for%20s%20in%20graphe%7D%0A%20%20%20%20parcours%20%3D%20%5Bsommet%5D%0A%20%20%20%20vus%5Bsommet%5D%20%3D%20True%0A%20%20%20%20for%20voisin%20in%20graphe%5Bsommet%5D%3A%0A%20%20%20%20%20%20%20%20if%20not%20vus%5Bvoisin%5D%3A%0A%20%20%20%20%20%20%20%20%20%20%20%20parcours%20%2B%3D%20parcours_profondeur_rec_N%28graphe,%20voisin,%20vus%29%0A%20%20%20%20return%20parcours%0A%0A%23%20Tests%0Amon_graphe%20%3D%20%7B%22A%22%3A%5B%22B%22,%20%22C%22%5D,%22B%22%3A%5B%22A%22%5D,%22C%22%3A%5B%22A%22,%20%22D%22,%20%22F%22%5D,%0A%22D%22%3A%5B%22C%22,%22E%22%5D,%22E%22%3A%5B%22D%22,%20%22F%22%5D,%22F%22%3A%5B%22C%22,%20%22E%22%5D%7D%0Aprint%28parcours_profondeur_rec_N%28mon_graphe,%20%22A%22%29%29&cumulative=false&heapPrimitives=nevernest&mode=edit&origin=opt-frontend.js&py=311&rawInputLstJSON=%5B%5D&textReferences=false){ .md-button target="_blank" rel="noopener" }



### 2. Le parcours en profondeur itératif

#### Une première approche

???+ note "Algorithme itératif avec une pile"

    Comme nous l'avons vu dans la vidéo, on peut aussi utiliser une pile.


!!! abstract "✍ A noter ... et à mémoriser ... 🐘"

    <pre>
	<b>parcours</b> est la liste vide qui contiendra les sommets visités par parcours en profondeur
	<b>P</b> est une pile vide
	On empile un sommet dans <b>P</b>
	Tant que <b>P</b> n'est pas vide
		<b>S</b> = dépile(<b>P</b>)
		On ajoute <b>S</b> à <b>parcours</b>
		On empile les voisins de <b>S</b> qui ne sont ni dans la pile ni dans <b>parcours</b>
	Fin Tant Que
    </pre>

<!--
![animation profondeur](images/animation_profondeur_2.gif){ width=30% }
-->

👉 9 étapes :

=== "1"
	![](images/profondeur1.png)
=== "2"
	![](images/profondeur2.png)
=== "3"
	![](images/profondeur3_rectif.png)
=== "4"
	![](images/profondeur4.png)
=== "5"
	![](images/profondeur5.png)
=== "6"
	![](images/profondeur6.png)
=== "7"
	![](images/profondeur7.png)
=== "8"
	![](images/profondeur8.png)
=== "9"
	![](images/profondeur9.png)

???+ question "A vous"

    Ecrire le code d'une fonction `parcours_en_profondeur` qui parcourt en profondeur un graphe à partir du sommet `depart` 
	et dont sa liste d'adjacence est representée par un dictionnaire nommé `graphe`

	Cette fonction renverra la liste des sommets parcourus en partant du sommet `depart`.

    Compléter le script ci-dessous de façon itérative :

	Graphe du test : 

	![graphe largeur](images/gr_vert.png){ width=50% }

    {{ IDE('scripts/profondeur') }}


#### Différents parcours en profondeur ?

???+ note dépliée "Il y a souvent plusieurs possibilités"

    Observez les résultats obtenus pour le parcours en profondeur par algorithme récursif, ou itératif.  

	🌵 Les parcours obtenus sont différents. De plus, suivant l'ordre des listes d'adjacences, on obtiendra aussi des résultats de 
	parcours différents. 

	🎲 Dans la vidéo d'introduction, on était amené à faire des choix arbitraires.


#### Optimisation

???+ note dépliée "Optimisation"

    Le coût d'une recherche dans une liste (pour tester si un sommet a déjà été visité) est 
	beaucoup plus important que le coût de la recherche dans un ensemble (hors programme NSI), ou dans un dictionnaire.

	Nous allons donc proposer une autre version pour améliorer l'efficacité de l'algorithme.


???+ question "A vous"

	* Un sommet est "parcouru" lorsqu'il a été mis dans la liste `parcours`
	* Un sommet est "en attente" lorqu'il est dans la pile. 
	* Tous les autres sommets sont "pas vu".

	Graphe du test : 

	![graphe largeur](images/gr_vert.png){ width=50% }

	{{ IDE('scripts/prof_iter_opt') }}


## V. Bilan

!!! info "A retenir"

    * Dans le **parcours en largeur**, on visite tous les sommets en «cercle concentriques» autour du sommet de départ : d’abord 
	les voisins directs, puis les voisins des voisins directs etc, et on continue jusqu’à ce qu’il n’y ait plus de sommets à visiter.  
	👉 On utilise une **file**.

	* Le parcours en profondeur d’un graphe à partir d’un sommet consiste à suivre les arêtes arbitrairement, en marquant les 
	sommets déjà visités pour ne pas les visiter à nouveau. On avance le plus possible et on recule quand on est bloqué.  
	👉 On utilise un algorithme **récursif**, ou une **pile**.



## VI. Le labyrinthe

???+ question "TP : résolution d'un labyrinthe"

    [TP : resolution_labyrinthe](https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/TP_resolution_labyrinthe_sujet.ipynb){ .md-button target="_blank" rel="noopener" }


	😊 La correction est arrivée (ne pas la regarder avant d'avoir bien travaillé 😅)

	[Correction du TP : resolution_labyrinthe](https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/TP_resolution_labyrinthe_corr.ipynb){ .md-button target="_blank" rel="noopener" }

<!--- 
   ⏳ La correction viendra bientôt ... 
[Correction du TP : resolution_labyrinthe](https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/TP_resolution_labyrinthe_corr.ipynb){ .md-button target="_blank" rel="noopener" }
-->

## VII. Remarque

!!! warning "Retour sur l'efficacité"

	🌵 **`ma_liste.pop(0)`**

	Pour une question de simplification des codes, Nous avons souvent utilisé `ma_liste.pop(0)`

	Ce n'est pas une façon efficace de procéder, car le temps d'exécution est proportionnel à la taille de la liste `ma_liste`

	Recopier dans votre éditeur Python (pas sur Basthon) le script suivant, puis l'exécuter :

	```python
	import timeit
	import matplotlib.pyplot as plt

	def suppression_debut(lst):
		lst.pop(0)

	# différentes tailles de listes notées n dans la suite
	abscisse =  [(10**7)*i for i in range(1, 11)]
	print(abscisse)
	ordonnee = []

	for n in abscisse:
		temps = 0
		for i in range(2):
			ma_liste = [0]*n # Création de liste de tailles n
			# Le chronométrage étant fait 2 fois, Il faut donc pour chaque mesure reprendre la liste initiale
			temps += timeit.timeit("suppression_debut(ma_liste)", number=1, globals=globals())
		ordonnee.append(temps)

	# Graphique
	plt.plot(abscisse, ordonnee, "ro") # en rouge
	plt.show()
	plt.close()
	```
	!!! abstract "Résumé"

		👉 Pour pallier à ce problème, nous pouvons utiliser le `deque` du module `collections`
		
		[documentation deque](https://docs.python.org/fr/3/library/collections.html#collections.deque){ .md-button target="_blank" rel="noopener" }  

	🌴 **`ma_liste.pop()`** 

	Recopier dans votre éditeur Python (pas sur Basthon) le script suivant, puis l'exécuter :

	```python title=""
	import timeit
	import matplotlib.pyplot as plt

	def suppression_fin(lst):
		lst.pop()

	# différentes tailles de listes notées n dans la suite
	abscisse =  [(10**7)*i for i in range(1, 11)]
	print(abscisse)
	ordonnee = []

	for n in abscisse:
		temps = 0
		for i in range(2):
			ma_liste = [0]*n # Création de liste de tailles n ne contenant que des 0
			# Le chronométrage étant fait 2 fois, Il faut donc pour chaque mesure reprendre la liste initiale
			temps += timeit.timeit("suppression_fin(ma_liste)", number=1, globals=globals())
		ordonnee.append(temps)

	# Graphique
	plt.plot(abscisse, ordonnee, "go") # en vert
	plt.show()
	plt.close()
	```

	!!! abstract "Résumé"

		Nous observons que `ma_liste.pop()` est beaucoup plus rapide que `ma_list.pop(0)`. En effet, pour supprimer le premier élément d'une liste, 
		il faut tous les décaler d'un rang vers la gauche, alors que pour supprimer le dernier élément d'une liste, il suffit  ... de supprimer 
		le dernier élément de la liste 😅.

		Voici les résultats obtenus par les deux méthodes pour 100 mesures (il faut plus de 10 minutes pour créer cette image)
		
		* Les listes ont des tailles allant de $1 \times 10^7$ à $10 \times 10^7 = 10^8$
		* Les résultats sont exprimés en secondes

		![pop() et pop(0)](images/Figure_pop0_pop_100.png){ width=40% }


## VIII. Crédits

Romain Janvier, Gilles Lassus, Hugues Malherbe, Nicolas Revéret, Jean-Louis Thirot. 
