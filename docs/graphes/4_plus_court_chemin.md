---
author: Mireille Coilhac
title: Le plus court chemin
---

## I. Le plus court chemin

???+ note dépliée

    🤔 Comment est-on sûr qu'un chemin va être trouvé entre deux sommets A et B ?

    Si le graphe est connexe, tout parcours en largeur au départ de A va parcourir l'intégralité du graphe, et donc passera par B à un moment.
     Un chemin sera donc forcément trouvé entre A et B.

    🤔 Comment est-on sûr que ce chemin trouvé est le plus court ?

    Dans un parcours en largeur, on visite d’abord le sommet origine, puis la génération des sommets qui sont à distance 1 (en nombre
    d’arêtes), puis la génération des sommets qui sont à distance 2, etc. Quant on visite le point d’arrivée, le numéro de sa génération
    représente le nombre d’arêtes qui le séparent du point de départ.
    Autrement dit, un parcours en largeur à partir du sommet d’origine permet de trouver un plus court chemin vers le sommet d’arrivée
    en nombre d’étapes (c’est-à-dire d’arêtes traversées).
    Si on prend la précaution de noter (dans un dictionnaire `parents`) le père de chaque génération de sommets, il suffit de remonter de 
    père en père pour reconstituer le chemin d’accès du point d’arrivée.

???+ question "À vous"

    Compléter le script ci-dessous :

	Graphe du test : 

    ![parcours de graphe](images/exo_parcours_vert.png){ width=25% }

    {{ IDE('scripts/chemin_court', MAX_SIZE = 45 ) }}

## II. Remarques

!!! warning "Retour sur l'efficacité"

    * Pour une question de simplification des codes, Nous avons  utilisé `ma_liste.pop(0)`.  
    👉 Pour pallier à ce problème, nous pouvons utiliser le `deque` du module `collections`
    [documentation deque](https://docs.python.org/fr/3/library/collections.html#collections.deque){ .md-button target="_blank" rel="noopener" }  

    * Nous avons aussi utilisé une concaténation de liste à gauche. C'est également très coûteux, à cause de tous les décalages 
    qu'il faut effectuer dans la liste à chaque fois qu'on ajoute un élément à gauche.  
    👉 il vaut mieux créer la TAD file avec une liste chaînée  [compléments TAD](../listes_piles_files/complements_TAD.md){ .md-button target="_blank" rel="noopener" } 



## III. Crédits

Romain Janvier, Gilles Lassus, Eduscol, Nicolas Revéret