---
author: Mireille Coilhac
title: Exercices - suite
---

## Exercice 1 : matrice d'adjacence et liste d'adjacence

???+ question "Question 1. Matrice d'adjacence"

    Donner la liste de listes représentant la matrice d'adjacence du graphe suivant :

    ![exemple_2_vert.svg](images/exemple_2_vert.svg){ width=40% .autolight .center }

    ??? success "Solution"

        ```python title=""
        [
        [0, 1, 1, 1, 0, 0],
        [1, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 0],
        [0, 0, 0, 0, 0, 1],
        [0, 0, 1, 0, 1, 0],
        ]
        ```

???+ question "Question 2. liste d'adjacence avec dictionnaire"

    Compléter la fonction `conversion_dico` qui prend en paramètres une liste de listes `m` qui représente la matrice d'adjacence d'un graphe,
     et qui renvoie la liste d'adjacence correspondante, implémentée à l'aide d'un dictionnaire.

    Par exemple, pour le graphe suivant :

    ![exo_1_vert.svg](images/exo_1_vert.svg){ width=50% .autolight .center }

    ```pycon title=""
    >>> m = [[0, 1, 0, 1], [1, 0, 1, 1], [0, 1, 0, 0], [0, 0, 0, 0]]
    >>> conversion_dico(m)
    {0: [1, 3], 1: [0, 2, 3], 2: [1], 3: []}
    >>> 
    ```

    {{ IDE('scripts/adj_dico') }}




???+ question "Question 3. liste d'adjacence avec une liste"

    Compléter la fonction `conversion_liste` qui prend en paramètres une liste de listes `m` qui représente la matrice d'adjacence d'un graphe,
     et qui renvoie la liste d'adjacence correspondante, implémentée à l'aide d'une liste.

    Par exemple, pour le graphe suivant :

    ![exo_1_vert.svg](images/exo_1_vert.svg){ width=50% .autolight .center }

    ```pycon title=""
    >>> m = [[0, 1, 0, 1], [1, 0, 1, 1], [0, 1, 0, 0], [0, 0, 0, 0]]
    >>> conversion_liste(m)
    [[1, 3], [0, 2, 3], [1], []]
    >>> 
    ```

    {{ IDE('scripts/adj_liste') }}



## Exercice 2 : parcours en largeur

![parcours de graphe](images/exo_parcours_vert.png){ width=25% }

???+ question

    Donner la liste des sommets par parcours en largeur du graphe ci-dessus si le sommet de départ est B en conservant l'ordre alphabétique.

    ??? success "Solution"

        `['B', 'A', 'D', 'E', 'C', 'F', 'G', 'H']`


???+ question

    Ecrire le code d'une fonction `parcours_BFS` qui parcourt en largeur un graphe à partir du sommet `depart` et dont sa liste 
    d'adjacence graphe est representée par un dictionnaire nommé `graphe`

	Cette fonction renverra la liste des sommets parcourus à partir du sommet `depart`.

    Compléter le script ci-dessous, en ajoutant le test correspondant au graphe ci-dessus :

    {{ IDE('scripts/bfs') }}

??? success "Solution"

    Voir la leçon sur les parcours de graphes ... 😂

    ```python
    # Test
    mon_graphe = {"A":["B", "C"], "B":["A", "D", "E"], "C":["A", "D"],
    "D":["B", "C", "E"], "E":["B", "D", "F", "G"], "F":["E", "G"], "G":["E", "F", "H"],
    "H":["G"]}
    assert parcours_BFS(mon_graphe, "B") == ['B', 'A', 'D', 'E', 'C', 'F', 'G', 'H']
    ```

## Exercice 3 : parcours en profondeur

![parcours de graphe](images/exo_parcours_vert.png){ width=25% }

???+ question

    Donner une liste des sommets par parcours en profondeur du graphe ci-dessus si le sommet de départ est A.

    ??? success "Solution"

        * `['A', 'B', 'D', 'C', 'E', 'F', 'G', 'H']`
        * `['A', 'C', 'D', 'E', 'G', 'H', 'F', 'B']`
        * et beaucoup d'autres possibilités ...


???+ question

    Ecrire le code d'une fonction `parcours_DFS` qui parcourt en profondeur un graphe à partir du sommet `depart` et dont sa liste d'adjacence graphe est representée par un dictionnaire nommé `graphe`

	Cette fonction renverra la liste des sommets parcourus à partir du sommet `depart`.

    Compléter le script ci-dessous, en ajoutant le test correspondant au graphe ci-dessus :

    {{ IDE('scripts/dfs') }}

??? success "Solution"

    Voir la leçon sur les parcours de graphes ... 😂

