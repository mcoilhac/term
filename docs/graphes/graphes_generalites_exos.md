---
author: Gilles Lassus et Mireille Coilhac
title: Les graphes généralités - Exercices
---

## Exercice 1 :

[Cadeaux circulaires](https://codex.forge.apps.education.fr/exercices/permutation_circulaire/){ .md-button target="_blank" rel="noopener" }

## Exercice 2 :

[Aimer et être aimé](https://codex.forge.apps.education.fr/exercices/aimer_etre_aime/){ .md-button target="_blank" rel="noopener" }

## Exercice 3 :

[Les amis de mes amis](https://codex.forge.apps.education.fr/exercices/dico_amis/){ .md-button target="_blank" rel="noopener" }

## Exercice 4 :

[Liste d'adjacence et matrice d'adjacence](https://codex.forge.apps.education.fr/exercices/liste_adjacence/){ .md-button target="_blank" rel="noopener" }



