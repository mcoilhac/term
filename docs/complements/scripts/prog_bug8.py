class Balle:
    '''Classe pour définir une balle graphique'''
    couleur = {"rouge":(255,0,0), "vert":(0,255,0), "bleu":(0,0,255)}
    def __init__(self, couleur):
        self.couleur = couleur
        self.x = 100
        self.y = 100
        
    def deplacer(self):
        '''Mise à jour de la position de la balle par incrémentation des coordonnées'''
        x += 1
        y += 1
    
    def __str__(self):
        return "Position de la balle :"+self.x+", "+self.y
    
#Création de trois balles de couleurs différentes
balles = [Ballon("jaune"), Ballon("rouge"), Ballon("vert")]

for i in range(4):
    balles[i].deplace()
print(balles[0])         