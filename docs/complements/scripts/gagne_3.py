# Code d'un jeu

score = 0

def affiche():
    print(score) # score est une variable globale
    
def gagne():
    global score # score est la variable globale
    score = score + 10 
    
def perd():
    global score
    score = score - 10
       
affiche()
gagne()
affiche()
gagne()
affiche()
perd()
affiche()
