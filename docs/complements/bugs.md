---
authors: Pierre Marquestaut
title: "Debbugage"
---

## Découverte des exceptions

!!! note "Travail à faire"
    Chaque programme ci-dessous présente une erreur engendrant une exception.

    Pour chaque programme :

    - **Identifier** l'erreur générée en exécutant le code ;
    - **Décrire** succintement la nature de l'erreur ;
    - **Proposer** une correction pour le programme puisse s'exécuter


??? question "Programme 1"

    {{IDE('scripts/prog_bug1')}}

??? question "Programme 2"


    {{IDE('scripts/prog_bug2')}}

??? question "Programme 3"


    {{IDE('scripts/prog_bug3')}}

??? question "Programme 4"

    {{IDE('scripts/prog_bug4')}}

??? question "Programme 5"

    {{IDE('scripts/prog_bug5')}}

??? question "Programme 6"


    {{IDE('scripts/prog_bug6')}}

??? question "Programme 7"

    {{IDE('scripts/prog_bug7')}}

## Mise en pratique

Le programme suivant présente de nombreux bugs.

```python title=""
class Balle:
    '''Classe pour définir une balle graphique'''
    couleur = {"rouge":(255,0,0), "vert":(0,255,0), "bleu":(0,0,255)}
    def __init__(self, couleur):
        self.couleur = couleur
        self.x = 100
        self.y = 100
        
    def deplacer(self):
        '''Mise à jour de la position de la balle par incrémentation des coordonnées'''
        x += 1
        y += 1
    
    def __str__(self):
        return "Position de la balle : "+self.x+", "+self.y
    
#Création de trois balles de couleurs différentes
balles = [Ballon("jaune"), Ballon("rouge"), Ballon("vert")]

for i in range(4):
    balles[i].deplace()
print(balles[0])   
```

Corriger le programme pour qu'il puisse s'exécuter sans erreur.

{{IDE('scripts/prog_bug8')}}

## A vous de jouer...

<div class="centre" markdown="span">
    <iframe 
        src="{{ page.canonical_url }}../activites/Debuggage.html"
        width="100%" height="600"
        id="classe"
        title="Classe" 
        frameborder="0" 
        allow="autoplay; fullscreen; picture-in-picture">
    </iframe>
</div>


## Crédits

Page écrite par Pierre Marquestaut