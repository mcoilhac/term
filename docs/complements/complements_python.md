---
author: Mireille Coilhac
title: Compléments Python
---

## I. Variables locales et globales

### Les variables globales

!!! info "Définition"

    * Une variable globale est une variable déclarée en dehors de toutes les fonctions.
    * Elle est accessible depuis n'importe quelle partie du code après sa déclaration.

!!! info "Portée"

    La portée d'une variable globale est l'ensemble du module (fichier) dans lequel elle est définie. Elle peut être lue par n'importe quelle fonction dans ce module. Elle peut être modifiée par n'importe quelle fonction dans ce module si elle est mutable ou si elle est explicitement déclarée dans la fonction avec le mot-clé `global`.

!!! info "Utilisation"

    Les variables globales sont souvent utilisées pour stocker des informations qui doivent être partagées entre plusieurs fonctions ou parties du programme.

### Les variables Locales

!!! info "Définition"

    Une variable locale est une variable déclarée à l'intérieur d'une fonction. Elle n'est accessible qu'à l'intérieur de cette fonction.  
    
    Si on assigne une variable dans une fonction (`ma_variable = `) celle-ci est locale.  

    Attention, si on assigne une variable à l'intérieur d'une fonction, qui porte le même nom qu'une variable globale, cela peut rendre difficile la lecture et la compréhension du code.

!!! info "Portée"

    La portée d'une variable locale est limitée à la fonction dans laquelle elle est définie. Une fois l'exécution de la fonction terminée, la variable locale est détruite.

!!! info "Utilisation"

    Les variables locales sont utilisées pour stocker des informations temporaires qui ne sont nécessaires que dans le contexte d'une fonction particulière.

### Inconvénients à l'utilisation d'une variable globale

!!! info "À savoir"

    Les variables globales peuvent être modifiées n’importe où dans le programme, ce qui rend le suivi des changements difficile. Cela peut compliquer la compréhension et la maintenance du code.  

    Une modification d’une variable globale dans une partie du code peut avoir des effets inattendus ailleurs.  

    Si on veut vraiment utiliser une variable globale, il faut lui donner le nom le plus explicite possible pour minimiser la possibilité que ce nom soit utilisé ailleurs dans le fichier et ajouter des commentaires expliquant son rôle.

### Exemple 1

???+ question "Exercice 1"

    **1.** Exécuter le script ci-dessous. Que se passe-t-il ?

    {{ IDE('scripts/gagne_1') }}

    ??? success "Solution"

        Dans la fonction `gagne`, `score` est une variable locale qui n'a pas été assignée : 

        À la lecture du code de la fonction, l’instruction `score = ...` entraine la création d’une variable locale score sans valeur précisée. Ensuite à l’exécution de la fonction, pour procéder à l’affectation `score = score + 10`, il faut évaluer `score + 10` et pour cela évaluer `score` qui est le nom d’une variable locale. Mais cette variable locale n’a pas de valeur.

    **2.** Exécuter le script ci-dessous. Que se passe-t-il ?

    {{ IDE('scripts/gagne_2') }}

    ??? success "Solution"

        `score` n'a pas été assignée dans la fonction `affiche`. Python recherche d'abord la variable dans l'espace de noms local. Si elle n'est pas trouvée localement, il cherche ensuite dans l'espace de noms global, et la trouve bien ici.

    **3.** Exécuter le script ci-dessous. Que se passe-t-il ?

    {{ IDE('scripts/gagne_3') }}

    ??? success "Solution"

        Ici la variable globale `score` a bien été modifiée car dans la fonction l’instruction `global score` signifie pour la fonction 
        que la variable `score` utilisée n’est pas une variable locale mais est la variable globale.

    **4.** Exécuter le script ci-dessous. Que se passe-t-il ?

    {{ IDE('scripts/gagne_4') }}

    ??? success "Solution"

        La variable `nbre` est locale dans la fonction `augmente`. La variable globale `score` n'est pas modifiée.

    **5.** Exécuter le script ci-dessous. Que se passe-t-il ?

    {{ IDE('scripts/gagne_5') }}

    ??? success "Solution"

        Ici le paramètre de la fonction `augmente` est nommé `score`. 

        La variable `score` est locale dans la fonction `augmente`. La variable globale `score` n'est pas modifiée. Ces deux variables portent le même nom, mais ne sont pas du tout les mêmes. **⚠️ 🌵 À éviter absolument.** Cela rend le code difficilement compréhensible alors que l’exécution de cette fonction produit exactement les mêmes résultats que celle vu un peu plus haut. 

        ```python
        score = 0

        def augmente(nbre):
            nbre = nbre + 10
            
        augmente(score)
        print(score)
        ``` 
        
        

    **6.** Nous avons vu qu'il vallait mieux éviter l'utilisation des variables globales. Modifier ci-dessous le code pour ne plus en avoir. Exactement la même situation de jeu doit être reproduite.

    {{ IDE('scripts/gagne_6') }}

    ??? success "Solution"

        ```python title="Proposition de solution"
        def affiche(note):
            print(note) # score est une variable globale
            
        def gagne(note):
            return note + 10 
            
        def perd(note):
            return note - 10
            
        score = 0
        affiche(score)
        score = gagne(score)
        affiche(score)
        score = gagne(score)
        affiche(score)
        score = perd(score)
        affiche(score)
        ```

### Exemple 2
    
???+ question "Exercice 2"

    **1.** Exécuter le script ci-dessous. Que se passe-t-il ?

    {{ IDE('scripts/listes_1') }}

    ??? success "Solution"

        `resultats` est une variable globale qui a été modifiée en place.

    **2.** Exécuter le script ci-dessous. Que se passe-t-il ?

    {{ IDE('scripts/listes_2') }}

    ??? success "Solution"

        Dans la fonction `ajoute_2` , `resultats` est une variable locale qui n'a pas été assignée.

    **3.** Exécuter le script ci-dessous. Que se passe-t-il ?

    {{ IDE('scripts/listes_3_1') }}

    ??? success "Solution"

        La variable `resultats` dans la fonction `ajoute_3` est une variable locale. Une nouvelle liste a été affectée à `resultats`. La variable globale `resultats`n'a pas été modifiée.

    **4.** Ici le paramètre de la fonction `ajoute_3` est nommé `resultats`.  
    La variable `resultats` est locale dans la fonction `ajoute_3`, or il existe une variable globale `resultats`. Ces deux variables portent le même nom, mais ne sont pas du tout les mêmes. Cela rend le code difficilement compréhensible.  
    Modifier le code ci-dessous pour éviter ce problème : 

    {{ IDE('scripts/listes_3_2') }}

    ??? success "Solution"

        Exécuter le script ci-dessous. Répond-il à la question ?

        {{ IDE('scripts/listes_3_3') }}

        ??? success "Solution"

            Ce script ne répond pas à la question. `resultats` n'a pas été modifié.

        Exécuter le script ci-dessous, et vérifier qu'il répond bien à la question. 

        {{ IDE('scripts/listes_4') }}

    **5.** Pourquoi la fonction `ajoute_4` se comporte-t-elle différemment de la fonction `ajoute_3` ?  

    ??? success "Solution"

        * Dans la fonction `ajoute_3` on a `ma_liste = resultats` donc les deux noms désignent la même liste. Mais avec `ma_liste = ma_liste + [1]` une nouvelle liste `ma_liste` a été créée.
        * Dans la fonction `ajoute_4` on a `ma_liste = resultats` donc les deux noms désignent la même liste. L'instruction `ma_liste.append(1)` modifie donc en place `ma_liste` et `resultats` qui représentent la même liste. La variable globale `resultats` a donc été modifiée.

    **6.** Nous avons vu qu'il vallait mieux éviter l'utilisation des variables globales. Modifier ci-dessous le code pour ne plus en avoir. Exactement la même situation doit être reproduite (ajout de `1` dans la liste à chaque fois que la fonction `ajoute_5` est appelée)

    {{ IDE('scripts/listes_5') }}

    ??? success "Solution"

        ```python title="Proposition de solution"
        def ajoute_5(ma_liste):
            ma_liste.append(1)
            return ma_liste

        resultats = []
        resultats = ajoute_5(resultats)
        print(resultats)
        ```


## II. La méthode `append`

???+ question "Exercice 3"

    **1.** Exécuter le script ci-dessous. Que se passe-t-il ?

    {{ IDE('scripts/listes_6_1') }}

    ??? success "Solution"

        `append`est une méthode qui modifie la liste mais ne renvoie rien (soit `None`) donc `ma_liste.append(1)` est une expression qui vaut `None`. La fonction `ajoute_6` renvoit donc `None`, qui est affecté à `resultats`.

    **2.** Exécuter le script ci-dessous. Que se passe-t-il ?

    {{ IDE('scripts/listes_6_2') }}

    ??? success "Solution"

        La fonction `ajoute_6` renvoie donc `None`, mais la liste `resultats` a été modifiée en place.

        **⚠️ 🌵 Ce n'est pas du tout une bonne pratique de procéder ainsi**, et il ne faut pas le faire. 
        
        👉 Il faut préférer : 

        ```python title=""
        def ajoute_5(ma_liste):
            ma_liste.append(1)
            return ma_liste
        ```

## III. L'opérateur `+=`

!!! info "Cas des nombres ou des chaînes de caractères"

    Pour les nombres, les chaînes de caractères ou les tuples : `s = s + v` est équivalent à `s += v`

???+ question "Code 1"

    Exécuter le script ci-dessous.

    {{ IDE('scripts/code1') }}

???+ question "Code 2"

    Exécuter le script ci-dessous.

    {{ IDE('scripts/code2') }}

!!! info "Cas des listes"

    * `ma_liste += [4]` se comporte comme `ma_liste.append(4)`. L'opérateur `+=` modifie la liste **en place**
    * `ma_liste = ma_liste + [4]`  crée une nouvelle liste qui est le résultat de la concaténation de `ma_liste` et `[4]`. La liste originale n'est pas modifiée en place.
    * Remarquons que `ma_liste += [4, 5, 6]` est l'équivalent de `ma_liste.extend([4, 5, 6])`. La liste `ma_liste` est aussi modifiée en place.  
    Donc `ma_liste += [4]` est équivalent à `ma_liste.extend([4])`

!!! info "Que se passe-t-il dans le Code 1 et dans le Code 2 ?"

    **👉 Dans le Code 1**

    *  `ma_liste = [1, 2, 3]` : Une liste est créée avec les éléments `[1, 2, 3]`.
    * Appel de la fonction `ajouter_element`. La fonction est appelée avec `ma_liste` comme argument : `ajouter_element(ma_liste)` :.
    * Modification de la liste dans la fonction : `ma_liste = ma_liste + [4]` : Cette ligne crée une nouvelle liste qui est le résultat de la concaténation de `ma_liste` et `[4]`. La nouvelle liste `[1, 2, 3, 4]` est assignée à la variable locale `ma_liste` à l'intérieur de la fonction. Cette opération ne modifie pas la liste originale passée en argument, mais crée une nouvelle liste locale à la fonction.
    * `print(ma_liste)` : La liste originale `ma_liste` n'a pas été modifiée par la fonction `ajouter_element`, donc il s'affiche `[1, 2, 3]`.


    **👉 Dans le Code 2**

    *  `ma_liste = [1, 2, 3]` : Une liste est créée avec les éléments `[1, 2, 3]`.
    *  Appel de la fonction `ajouter_element`. La fonction est appelée avec `ma_liste` comme argument : `ajouter_element(ma_liste)` :  
    * Modification de la liste dans la fonction : `ma_liste += [4]` : Cette ligne utilise l'opérateur `+=` pour ajouter `[4]` à `ma_liste` ce qui modifie la liste en place.
    * Les listes sont des objets mutables et la méthode `append` opère une mutation de la liste sans en créer une nouvelle. Or la variable locale et la variable globale désignent la même liste. La différence dans le code 1 est que la variable locale et la globale désignent aussi la même liste au début, mais après l’affection `liste = liste + [4]`, la variable locale désigne une autre liste.
    * `print(ma_liste)` : Après l'appel de la fonction `ajouter_element`, `ma_liste` a été modifiée pour inclure l'élément `4`, donc il s'affiche `[1, 2, 3, 4]`.

!!! info "Bonne pratique"

    * Il vaut mieux utiliser les méthodes `append` ou `extend` pour modifier une liste en place, l'opérateur `+` pour créer une nouvelle liste.
    * Il vaut mieux éviter d'utiliser l'opérateur `+=` avec des listes.

## IV. QCM récapitulatif

{{ multi_qcm('qcms/complements.json') }}

## V. Crédits

Avec l'aide judicieuse de Serge Bays et de Frédéric Zinelli.







     

    




