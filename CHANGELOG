# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added 

### Fixed

## [0.9.0] - 2022-13-21

### User side modifications

- Major | Updated Pyodide version to 21.3
- Major | Improved console outputs with Rich Text format"
- Major | Restructured pyodide-mkdocs file architecture
- Major | Improved documentation readability

- Minor | Updated ACE version to 12.5
- Minor | Updated Terminal version to 2.23"

- Minor | IDE : Bug quote in vertical ide
- Minor | HDR : A regex is now in place and allows for mistakes in HDR tag
- Minor | HDR : When reloading a page, HDR is now hidden
- Minor | ERRORLOGGER : when making a mistake, the error was not always correctly displayed

### Code quality

- Major | Refactored `main.py` to improve robustness and readability
- Major | CSS : CSS classes are now specialized to avoid scripts-mangling
- Minor | Simplify remark handling for empty IDE

